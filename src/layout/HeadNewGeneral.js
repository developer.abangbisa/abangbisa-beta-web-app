import React, {useEffect, useState} from 'react';
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import clsx from 'clsx';

import { 

    Grid,
    Chip,
    Drawer,
    Divider,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    AppBar,
    Toolbar,
    IconButton,
    TextField,
    Typography,
    Badge,
    Menu,
    MenuItem,
    Collapse,
    Button,
    InputAdornment,
    OutlinedInput,
    InputBase,
    Avatar

  } from '@material-ui/core';

  import axios from 'axios';
  import { URL_API } from '../constants/config-api';

  import InboxIcon from '@material-ui/icons/MoveToInbox';
  import SearchIcon from '@material-ui/icons/Search';
  import MenuIcon from '@material-ui/icons/Menu';
  import AccountCircle from '@material-ui/icons/AccountCircle';
  import NotificationsIcon from '@material-ui/icons/Notifications';
  import MailIcon from '@material-ui/icons/Mail';
  import ExpandLess from '@material-ui/icons/ExpandLess';
  import ExpandMore from '@material-ui/icons/ExpandMore';
  import StarBorder from '@material-ui/icons/StarBorder';
  import SvgIcon from '@material-ui/core/SvgIcon';
  import SaveIcon from '@material-ui/icons/Save';
  
  import ImageDashboardHome from '../assets/images/Subtraction_3.png';
  import Image7WD from '../assets/images/Group_110.png';
  import ImageHumanCapital from '../assets/images/Group_663.png';
  import MahkotaLogoCompany from '../assets/images/Group-862.png';

  import IconMenuOffCanvass from '../assets/images/Group_1923.png';
  import IconMenuOffCanvassGrey from '../assets/images/SVG/Group_709.svg';
  
  import IconArrowLeft from '../assets/images/SVG/Group_1186.svg'
  import { cyan, lightBlue, lightGreen, grey, red, green } from "@material-ui/core/colors";
  
  import Redirect from '../utilities/Redirect';
  import { 
    
      ToMembershipStatus, 
      ToDashboard, 
      ToCompanyProfile, 
      ToRole, 
      ToUserManagement, 
      ToLogin, 
      ToEmptyStateGeneral, 
      ToOrganigram, 
      ToSOTable,
      To7wdPeriode,
      ToGoal

    } from '../constants/config-redirect-url';
  

  import ArrayBufferToBase64 from '../utilities/ArrayBufferToBase64';
  import { styles } from './Style/StyleHeadNewGeneral';

  const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    },
    overrides: {

        MuiAppBar:{

          root:{
            backgroundColor: 'cyan'
          }
        },
        
        MuiToolbar:{

          root:{

            backgroundColor: 'white'
          }
        },
        MuiButton: {
      
          text: {
    
            color: 'white'

          },
          textSecondary: {
    
            color: 'white',
            fontFamily: 'Nunito'
          }
        },
        MuiDrawer: {
    
          root: {

            backgroundColor: lightBlue

          },
          paper: {

            backgroundColor: green
          },

          paperAnchorLeft: {
    
          }
        },
        MuiListItemIcon: {

          root: {

            color: 'white'
          }
        },
        MuiBadge:{

          root: {

            color: '#d1354a',
          }
        }
      }       
});

const HeadNewGeneral = props => {

    const { classes } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */

    const userToken = localStorage.getItem('userToken');
    const [ userTokenState, setUserTokenState ] = useState('');

    const [ fotoQuery, setFotoQuery ] = useState('');
    const [ namaGroupPerusahaan, setNamaGroupPerusahaan ] = useState('');

    useEffect(() => {

      setUserTokenState(userToken);

      if(userToken !== undefined){
  
          const header =  {   

              'Accept': "application/json",
              'Content-Type' : "application/json",
              'Authorization' : "bearer " + userToken

          };
      
          axios.defaults.headers.common = header;    

          axios
            .get(URL_API + `/group/profile/patch`)
            .then(function(response){

              console.log('Response Original from HeadNewGeneral : ', response);

              if(response.status == 200){

                if(response.data.data.fields !== undefined){
                  if(response.data.data.fields.photo_logo_icon !== ''){

                    setFotoQuery(response.data.data.fields.photo_logo_icon.value);
                    setNamaGroupPerusahaan(response.data.data.fields.name.value);

                  }
                }
              };
            })
            .catch(function(error){
                  
                console.log("Error : ", error.response)
            })

      };

    },[])

    /* ````````````START DRAWER`````````````````````*/

      //   const classes = useStyles();
      const [state, setState] = useState({
        top: false,
        // left: true,
        left: false,        
        bottom: false,
        right: false
      });
  
      const handleCloseOffCanvass = () => {
  
        setState(state.left = false);

      };
  
      const toggleDrawer = (side, open) => () => {
  
          setState({ ...state, [side]: open });
      };

      /* ````````````END DRAWER ``````````````````````*/
  
      function handleLogOut (){
          
          localStorage.removeItem('userToken');
          localStorage.removeItem('intro_enabled');
          // localStorage.clear();
          Redirect(ToLogin);

      };
  
      /* 
          ````````````````````````````````
            START APP DRAWER

          ````````````````````````````````
      */
  
     const [anchorEl, setAnchorEl] = useState(null);
     const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
   
     const isMenuOpen = Boolean(anchorEl);
     const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
   
     function handleProfileMenuOpen(event) {
       setAnchorEl(event.currentTarget);
     }
   
     function handleMobileMenuClose() {
       
       setMobileMoreAnchorEl(null);
     }
   
     function handleMenuClose() {
       setAnchorEl(null);
       handleMobileMenuClose();
     }
   
     function handleMobileMenuOpen(event) {
       setMobileMoreAnchorEl(event.currentTarget);
     };
  
    /*
        `````````````````````
          LIST NESTED PROFILE
  
        `````````````````````
    */
  
    const [openNestedList, setOpenNestedList] = useState(false);
  
    function handleOpenNestedList() {
  
      setOpenNestedList(!openNestedList);

    };

    /*
        ```````````````
        LIST NESTED 7WD

        ```````````````
    */

    const [ openNestedSevenWD, setOpenNestedSevenWD ] = useState(false);

    function handleOpenSevenWD() {

        setOpenNestedSevenWD(!openNestedSevenWD);
    };
    
    /*
        ````````````````````````````````
          LIST NESTED HUMAN CAPITAL

        ````````````````````````````````
    */
    const [openNestedHumanCapital, setOpenNestedHumanCapital ] = useState(false);

    function handleOpenHumanCapital() {

      setOpenNestedHumanCapital(!openNestedHumanCapital);
    };
    
    /*
        ````````````````````````````````
          ICON POJOK KANAN ATAS - MENU
  
        ````````````````````````````````
    */
  
    const ITEM_HEIGHT = 24;
  
    const options = [
      
      'Pengaturan',
      'Log-out'

    ];
  
    const [ anchorElMenuPojokKananAtas, setAnchorElPoKaAtas ] = useState(null);
    const isOpenMenuPojokKananAtas = Boolean(anchorElMenuPojokKananAtas);
  
    function handleMenuPojokKananAtas(event) {
      setAnchorElPoKaAtas(event.currentTarget);
    
    };
  
    function handleCloseMenuPojokKananAtas(e, item) {

      if(item == "Log-out"){

        handleLogOut();
      };

      setAnchorElPoKaAtas(null);

    };

    return (

        <MuiThemeProvider theme={theme}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton 
                        onClick={toggleDrawer('left', true)}
                        className={classes.menuButton} 
                        color="inherit" 
                        aria-label="Menu"
                    >
                        <img src={IconMenuOffCanvassGrey} alt='Icon Menu Grey' style={{width: 28, height: 28}} />
                    </IconButton>

                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            placeholder="Ketik untuk pencarian..."
                            classes={{

                                root: classes.inputRoot,
                                input: classes.inputInput
                              }}

                            inputProps={{ 'aria-label': 'Search'}}
                        />
                    </div>

                    {/* 
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            Abang Bisa
                        </Typography> 
                    */}

                    <Grid container spacing={8} alignItems="flex-end">
                        {/* 
                          <Grid item>
                              <SearchIcon className={classes.searchIcon } style={{marginBottom: 20}} />
                          </Grid>
                          <Grid item>
                              <TextField id="input-with-icon-grid" label="Ketik untuk pencarian..." style={{marginBottom: 20}} />
                          </Grid> 
                        */}
                    </Grid>

                    <IconButton color="inherit">
                        <Badge badgeContent={17} color="primary">
                            <NotificationsIcon className={classes.notifIcon}/>
                        </Badge>
                    </IconButton>

                    <IconButton color="inherit">
                        <MailIcon className={classes.mail}/>
                    </IconButton>

                    <IconButton
                        aria-haspopup="true"
                          
                        color="inherit"
                        
                    >                        
                        {
                          fotoQuery !== '' ? (
                                                        
                            <img src={URL_API + '/' +fotoQuery+ "&token=" + userToken}  className={classes.bigRealFoto} />

                          ) : (

                            <AccountCircle className={classes.accountCircle} />

                          )
                        }

                    </IconButton>

                    <IconButton
                        aria-label="More"
                        aria-owns={isOpenMenuPojokKananAtas ? 'long-menu' : undefined}
                        aria-haspopup="true"
                        onClick={handleMenuPojokKananAtas}
                    >
                        <i className="material-icons">
                            expand_more
                        </i>
                    </IconButton>

                    <Menu
                        id="long-menu"
                        anchorEl={anchorElMenuPojokKananAtas}
                        open={isOpenMenuPojokKananAtas}
                        onClose={handleCloseMenuPojokKananAtas}
                        PaperProps={{
                            style: {
                            maxHeight: ITEM_HEIGHT * 4.5,
                            width: 200,
                            },
                        }}
                    >
                        {
                            options.map(option => (
                                <MenuItem key={option} selected={option === 'Pengaturan'} onClick={(e) => handleCloseMenuPojokKananAtas(e, option)}>
                                    {option}
                                </MenuItem>
                            ))
                        }
                    </Menu>
                </Toolbar>
            </AppBar>

            {/* 
                ``````
                DRAWER

                ``````
            */}

          <Drawer  
            open={state.left} onClose={toggleDrawer('left', false)}
          >
              <div style={{background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)', height: 1000, width: 303}}>
                <span
                  style={{padding: 14, marginLeft: 244}}
                >
                  <img 
                    src={IconMenuOffCanvass} 
                    className={classes.iconMenuOffCanvass} 
                    onClick={handleCloseOffCanvass}  
                  />
                </span>

                <List
                  component="nav"
                  className={classes.drawerRoot}
                >
                  <ListItem button onClick={handleOpenNestedList} style={{paddingLeft: 0}}>
                    <ListItemIcon>

                      {/* <img src={MahkotaLogoCompany} style={{marginRight: 0, paddingRight:0}}/> */}
                      <IconButton
                          aria-haspopup="true"
                          // onClick={() => handleLogOut()}   
                          color="inherit"
                      >  

                        {
                          fotoQuery !== '' ? (
                                                        
                            <img src={URL_API + '/' +fotoQuery+ "&token=" + userToken}  className={classes.bigRealFotoDrawer} />

                          ) : (

                            <AccountCircle className={classes.accountCircle} />

                          )
                        }
                      </IconButton>


                    </ListItemIcon>

                    <ListItemText 
                      
                      secondary = {

                        <Typography variant='subtitle1' style={{ color: 'white', fontFamily: 'Nunito' }}>

                        </Typography>
                      } 

                      primary = {

                        <Typography variant='subtitle2' className={classes.namaPerusahaan}>
                          { 
                            namaGroupPerusahaan !== '' ? namaGroupPerusahaan : '-'
                          }
                        </Typography>
                      } 
                    />
                      {openNestedList ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse in={openNestedList} timeout="auto" unmountOnExit>
                      <List component="div" disablePadding>
                        <ListItem 
                          onClick={() => Redirect(ToCompanyProfile)}
                          button 
                          className={classes.nested}
                        >
                          <ListItemText 
                            inset 
                            primary={
                              <Typography 
                                type="ProfilPerusahaan" 
                                style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                              >
                                Profil Perusahaan
                              </Typography>
                            }  
                          />
                        </ListItem>
                      </List>
                      <List component="div" disablePadding>
                        <ListItem 
                            onClick={() => Redirect(ToMembershipStatus)}
                            button className={classes.nested}
                        >                   
                          <ListItemText 
                            inset 
                            primary={<Typography type="Keanggotaan" style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}>Keanggotaan</Typography>}  
                          />
                        </ListItem>
                      </List>

                      <List component="div" disablePadding>
                        <ListItem 
                          button 
                          className={classes.nested}
                          onClick={() => Redirect(ToUserManagement)}
                        >
                
                          <ListItemText 
                            inset 
                            primary={<Typography type="PengaturanUser" style={{ color: 'white', fontFamily: 'Nunito',marginLeft: 10  }}>Pengaturan User</Typography>}  
                            
                          />
                        </ListItem>
                      </List>

                      <List component="div" disablePadding>
                        <ListItem button className={classes.nested}>
                          <ListItemText 
                            inset 
                            onClick={() => Redirect(ToRole)}
                            primary={
                                <Typography type="PengaturanRole" 
                                    style={{ color: 'white', fontFamily: 'Nunito',marginLeft: 10  }}
                                >
                                    Pengaturan Role
                                </Typography>
                            }  
                          />

                          
                        </ListItem>
                      </List>
                    </Collapse>

                    <List component="nav">
                      <ListItem 
                        button
                        onClick={() => Redirect(ToDashboard)}  
                      >
                        <ListItemIcon>
                          <img src={ImageDashboardHome} />
                        </ListItemIcon>
                        <ListItemText 
                          primary={
                            <Typography 
                              type="PengaturanRole" 
                              style={{ color: 'white', fontFamily: 'Nunito' }}
                            >
                              Dashboard
                            </Typography>
                          }  
                        />
                      </ListItem>

                      <ListItem 
                        button
                        onClick={handleOpenSevenWD}
                      >
                        <ListItemIcon>
                          <img src={Image7WD} />  
                        </ListItemIcon>
                        <ListItemText 
                            primary={
                              <Typography type="PengaturanRole" style={{ color: 'white', fontFamily: 'Nunito' }}>
                                7WD
                              </Typography>
                            }  
                        />

                          {openNestedSevenWD ? <ExpandLess /> : <ExpandMore />}

                      </ListItem>
                      <Collapse in={openNestedSevenWD} timeout="auto" unmountOnExit>

                        <List component="div" disablePadding>
                          <ListItem 
                            // onClick={() => Redirect(ToCompanyProfile)}
                            onClick={() => Redirect(ToGoal)}
                            button 
                            className={classes.nested}
                          >
                            <ListItemText 
                              inset 
                              primary={
                                <Typography 
                                  type="so" 
                                  style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                >
                                  Goal
                                </Typography>
                              }  
                            />
                          </ListItem>
                          <ListItem 
                            // onClick={() => Redirect(ToCompanyProfile)}
                            onClick={() => Redirect(To7wdPeriode)}
                            button 
                            className={classes.nested}
                          >
                            <ListItemText 
                              inset 
                              primary={
                                <Typography 
                                  type="karyawan" 
                                  style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                >
                                  Periode
                                </Typography>
                              }  
                            />
                          </ListItem>
                        </List>
                        </Collapse>

                        {/* 
                            `````````````````````
                            HUMAN RESOURCE NESTED

                            `````````````````````
                          
                        */}

                      <ListItem 
                        onClick={handleOpenHumanCapital}
                        button
                      >
                        <ListItemIcon>
                          <img src={ImageHumanCapital} />
                        </ListItemIcon>

                        <ListItemText 
                            primary={
                                <Typography type="PengaturanRole" 
                                  style={{ color: 'white', fontFamily: 'Nunito' }}
                                >
                                  Human Capital

                                </Typography>
                            }  
                            />

                            {openNestedHumanCapital ? <ExpandLess /> : <ExpandMore />}

                      </ListItem>

                      <Collapse in={openNestedHumanCapital} timeout="auto" unmountOnExit>

                        <List component="div" disablePadding>
                          <ListItem 
                            // onClick={() => Redirect(ToCompanyProfile)}
                            onClick={() => Redirect(ToSOTable)}
                            button 
                            className={classes.nested}
                          >
                            <ListItemText 
                              inset 
                              primary={
                                <Typography 
                                  type="so" 
                                  style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                >
                                  SO
                                </Typography>
                              }  
                            />
                          </ListItem>
                          <ListItem 
                            // onClick={() => Redirect(ToCompanyProfile)}
                            onClick={() => Redirect(ToEmptyStateGeneral)}
                            button 
                            className={classes.nested}
                          >
                            <ListItemText 
                              inset 
                              primary={
                                <Typography 
                                  type="karyawan" 
                                  style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                >
                                  Karyawan
                                </Typography>
                              }  
                            />
                          </ListItem>
                        </List>
                    </Collapse>
                    </List>
                  </List>
                </div>
             </Drawer>
        </MuiThemeProvider>
    )
};

export default withStyles(styles)(HeadNewGeneral);



  /*
      ``````````````
      CALL REAL FOTO

      ``````````````
  */

  // const [fotoContent, setFotoContent ] = useState('');

  // useEffect(() => {


  //     if(userToken !== undefined){
  
  //       const header =  {   

  //           'Accept': "application/json",
  //           'Content-Type' : "application/json",
  //           'Authorization' : "bearer " + userToken,
  //           'responseType': 'arraybuffer'

  //       };
    
  //       axios.defaults.headers.common = header;    

  //       axios
  //         .get(URL_API + '/' +fotoQuery)
  //         .then(function(response){

  //           console.log('Response Original from IMAGE : ', response);

  //           if(response.status == 200){

  //             // let datas = JSON.parse(response.data);
  //             // let myData = atob(response.data)
  //             // let btoaData = btoa(myData);

  //             // var elem = document.createElement("img");
  //             // elem.src = "data:image/png;base64,"+btoaData;
  //             // document.getElementById("Image").append(elem);
              
        
  //             // let imgFile = new Blob([response.data]);
  //             // let imgUrl = URL.createObjectURL(imgFile);
  //             // console.log(imgUrl);
  //             // setFotoContent(imgUrl);

  //             setFotoContent(response.data);

  //             /*
  //               ```````````````````````````````````````````
  //               https://stackoverflow.com/questions/29452031/how-to-handle-file-downloads-with-jwt-based-authentication

  //               ```````````````````````````````````````````
  //             */

  //             /*
  //                 ```````````````````````````````````````````````````````````````````````````````````
  //                 https://medium.com/front-end-weekly/fetching-images-with-the-fetch-api-fb8761ed27b2
                  
  //                 ````````````````````````````````````````````````````````````````````````````````````

  //             */

  //             // response.data.arrayBuffer().then((buffer) => {

  //             //   console.log('buffer', buffer);
  //             //   var base64Flag = 'data:image/png;base64,';
  //             //   var imageStr = ArrayBufferToBase64(buffer);
                
  //             //   console.log('imageStr', base64Flag + imageStr);
  //             //   // document.querySelector('img').src = base64Flag + imageStr;
  //             // });
              

  //             //
  //           };

  //         })
  //         .catch(function(error){
                
  //             console.log("Error : ", error.response)
  //         })
  //   };
  // },[fotoQuery])