import React, {useEffect, useState} from 'react';
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider, fade } from '@material-ui/core/styles';
import clsx from 'clsx';

import { 

    Grid, Chip, Drawer, Divider, List, ListItem, ListItemIcon, ListItemText, AppBar, Toolbar, InputBase,
    IconButton, TextField, Typography, Badge, Menu, MenuItem, Collapse, Button, InputAdornment,OutlinedInput,
    Avatar, CssBaseline, Hidden

} from '@material-ui/core';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import axios from 'axios';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import SearchIcon from '@material-ui/icons/Search';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MailIcon from '@material-ui/icons/Mail';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import SaveIcon from '@material-ui/icons/Save';

import ImageDashboardHome from '../assets/images/Subtraction_3.png';
import Image7WD from '../assets/images/Group_110.png';
import ImageHumanCapital from '../assets/images/Group_663.png';

import IconMenuOffCanvass from '../assets/images/Group_1923.png';
import IconMenuOffCanvassGrey from '../assets/images/SVG/Group_709.svg';
import { cyan, lightBlue, lightGreen, grey, red, green } from "@material-ui/core/colors";

import Redirect from '../utilities/Redirect';
import { URL_API } from '../constants/config-api';

import { styles }  from './Style/StyleHeaderGoal';

import { 
  
    ToMembershipStatus, 
    ToDashboard, 
    ToCompanyProfile, 
    ToRole, 
    ToUserManagement, 
    ToLogin, 
    ToEmptyStateGeneral, 
    ToOrganigram, 
    ToSOTable,
    To7wdPeriode,
    ToGoal,
    ToGoalDetail,
    ToGoalDetailTabComplete

  } from '../constants/config-redirect-url';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    },
    overrides: {
        MuiAppBar:{
          root:{
            backgroundColor: 'cyan'
          }
        },
        MuiToolbar:{
          root:{
            backgroundColor: 'white'
            // backgroundColor: '#d6d6d6'

          }
        },
        MuiButton: {
      
          text: {
    
            color: 'white'

          },
          textSecondary: {
    
            color: 'white',
            fontFamily: 'Nunito'
          }
        },
        MuiDrawer: {
    
          root: {

            backgroundColor: lightBlue

          },
          paper: {

            backgroundColor: green
          },

          paperAnchorLeft: {
    
          }
        },
        MuiListItemIcon: {

          root: {

            color: 'white'
          }
        },
        MuiBadge:{

          root: {

            color: '#d1354a',
          }
        }
      }       
});



const HeaderGOAL = props => {

    const { classes } = props;


    /* 
        ````````````````````
        TO KNOW CURRENT PAGE
        
        ````````````````````
    */
    

    const [ inisateLabel, setInisisateLabel ] = useState({

        label: ''
    });

    const currentLocation = window.location.pathname;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    const userToken = localStorage.getItem('userToken');
    const [ userTokenState, setUserTokenState ] = useState('');

    const [ fotoQuery, setFotoQuery ] = useState('');
    const [ fotoQueryUser, setFotoQueryUser ] = useState('');

   const [ namaGroupPerusahaan, setNamaGroupPerusahaan ] = useState('');
   const [ roleName, setRoleName ] = useState([]);

   const statusUserLogin = localStorage.getItem('status_user_login');
   const statusUserLoginAfterParse = JSON.parse(statusUserLogin);   

   useEffect(() => {

     setUserTokenState(userToken);
    
     if(statusUserLoginAfterParse !== null){

         setFotoQueryUser(statusUserLoginAfterParse.member_photo_url);
         setRoleName(statusUserLoginAfterParse.userRole[0].name);
     };
     
     if(userToken !== undefined){
 
         const header =  {   
             
             'Accept': "application/json",
             'Content-Type' : "application/json",
             'Authorization' : "bearer " + userToken
         };
     
         axios.defaults.headers.common = header;    

         axios
           .get(URL_API + `/group/profile/patch`)
           .then(function(response){

             console.log('Response Original from HeadNewGeneral : ', response);

             if(response.status == 200){

               if(response.data.data.fields !== undefined){
                 if(response.data.data.fields.photo_logo_icon !== ''){

                   setFotoQuery(response.data.data.fields.photo_logo_icon.value);
                   setNamaGroupPerusahaan(response.data.data.fields.name.value);

                 }
               }
             };
           })
           .catch(function(error){
                 
               console.log("Error : ", error.response)
           })

     };


    //* TO KNOW CURRENT LOCATION PAGE
    if(currentLocation === ToGoal){

        setInisisateLabel({label: 'Goal'})
    };

    if(currentLocation === ToGoalDetail){

        setInisisateLabel({label: 'Goal'})
    };

    if(currentLocation === ToGoalDetailTabComplete){

        setInisisateLabel({label: 'Goal'})
    };


    if(currentLocation === ToDashboard){

        setInisisateLabel({label: 'Home'})        
    };


   },[])


    /*
        ```````````````````````
        OPEN HANDLE DRAWER OPEN

        ```````````````````````
    */

    const [open, setOpen] = useState(false);

    function handleDrawerOpen() {

        setOpen(true);

    };
  
    function handleDrawerClose() {
      
        setOpen(false);
    };

    /*
        `````````````````````
          LIST NESTED PROFILE
  
        `````````````````````
    */
  
    const [openNestedList, setOpenNestedList] = useState(false);
    
    function handleOpenNestedList() {
    
        if(roleName == 'superadmin'){

            setOpenNestedList(!openNestedList);
        };
    };

    /*
        ```````````````
        LIST NESTED 7WD

        ```````````````
    */
    const [ openNestedSevenWD, setOpenNestedSevenWD ] = useState(false);

    function handleOpenSevenWD() {

        setOpenNestedSevenWD(!openNestedSevenWD);
    };
   
    /*
        ````````````````````````````````
            LIST NESTED HUMAN CAPITAL

        ````````````````````````````````
    */
    const [openNestedHumanCapital, setOpenNestedHumanCapital ] = useState(false);

    function handleOpenHumanCapital() {

        setOpenNestedHumanCapital(!openNestedHumanCapital);
    };
    /*
        ``````````````
        HANDLE LOG OUT
  
        ``````````````
    */
    function handleLogOut (){
          
        localStorage.removeItem('userToken');
        localStorage.removeItem('intro_enabled');
        
        localStorage.removeItem('verifyToken');
        localStorage.removeItem('status_user_login');
        localStorage.removeItem('goal_detail');
        localStorage.removeItem('employee_id');
        localStorage.removeItem('response_employee_detail');


        // localStorage.clear();
        Redirect(ToLogin);

    };

    /*
        ````````````````````````````````
          ICON POJOK KANAN ATAS - MENU
  
        ````````````````````````````````
    */
  
    const ITEM_HEIGHT = 24;
  
    const options = [
        
        'Pengaturan',
        'Log-out'

    ];
 
    const [anchorElMenuPojokKananAtas, setAnchorElPoKaAtas] = useState(null);
    const isOpenMenuPojokKananAtas = Boolean(anchorElMenuPojokKananAtas);
 
    function handleMenuPojokKananAtas(event) {

        // console.log("Event : ", event.currentTarget);
        setAnchorElPoKaAtas(event.currentTarget);

    };
 
    function handleCloseMenuPojokKananAtas(e, item) {
        
        // console.log("Item : ", item);

        if(item == "Log-out"){

            handleLogOut();
        };

        setAnchorElPoKaAtas(null); 
    };

    /* ````````````START DRAWER`````````````````````*/

    const [state, setState] = useState({
        top: false,
        // left: true,
        left: false,        
        bottom: false,
        right: false
    });

  
    const toggleDrawer = (side, open) => () => {

        setState({ ...state, [side]: open });
    };

    return (

        <MuiThemeProvider theme={theme}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        // onMouseLeave = {() => console.log('Mouse leave...')}                        
                        // onMouseOver = {() => console.log('Mouse over...')}  
                        // onMouseLeave = {() => handleDrawerClose()}                        
                        // onMouseOver = {() => handleDrawerOpen()}  
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}
                        style={{color: 'grey'}}    
                                            

                    >
                        <img src={IconMenuOffCanvassGrey} alt='Icon Menu Grey' style={{width: 28, height: 28}} />
                    </IconButton>

                    <Typography variant="h6" className={classes.textHeaderGoal}>
                        <b>{inisateLabel.label}</b>
                    </Typography>

                    <Grid container spacing={8} alignItems="flex-end"></Grid>

                    <Hidden only='xs'>          
                        <IconButton color="inherit">
                            <Badge badgeContent={17} color="primary">
                                <NotificationsIcon className={classes.notifIcon}/>
                            </Badge>
                        </IconButton>
                  
                        <IconButton color="inherit">
                            <MailIcon className={classes.mail}/>
                        </IconButton>
                    </Hidden>

                    <IconButton
                        aria-haspopup="true"
                        // onClick={() => handleLogOut()}   
                        color="inherit"
                        
                    >                        
                        {
                            fotoQueryUser !== '' ? (
                                                            
                                <img src={URL_API + '/' +fotoQueryUser+ "&token=" + userToken}  className={classes.bigRealFoto} />

                            ) : (

                                <AccountCircle className={classes.accountCircle} />

                            )
                        }

                    </IconButton>

                    <IconButton
                        aria-label="More"
                        aria-owns={isOpenMenuPojokKananAtas ? 'long-menu' : undefined}
                        aria-haspopup="true"
                        onClick={handleMenuPojokKananAtas}
                    >
                        <i className="material-icons">
                            expand_more
                        </i>
                    </IconButton>

                    <Menu
                        id="long-menu"
                        anchorEl={anchorElMenuPojokKananAtas}
                        open={isOpenMenuPojokKananAtas}
                        onClose={handleCloseMenuPojokKananAtas}
                        PaperProps={{
                            style: {
                            maxHeight: ITEM_HEIGHT * 4.5,
                            width: 200,
                            },
                        }}
                    >
                        {
                            options.map(option => (

                                <MenuItem key={option} selected={option === 'Pengaturan'} onClick={(e) => handleCloseMenuPojokKananAtas(e, option)}>
                                    {option}
                                </MenuItem>
                            ))
                        }
                    </Menu>
                </Toolbar>
            </AppBar>

            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                    }),
                }}
                onMouseLeave = {() => setOpen(false)}                        
                onMouseOver = {() => setOpen(true)}  
                open={open}
            >
                <div style={{background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)', height: 1000, width: 303}}>

                    <IconButton onClick={handleDrawerClose} style={{padding: 14, marginLeft: 245}}>
                        { 
                            theme.direction === 'rtl' ? 

                                <ChevronRightIcon /> : 

                                    <img src={IconMenuOffCanvass} className={classes.iconMenuOffCanvass} />
                        }
                    </IconButton>
                    
                    <List
                        component="nav"
                        className={classes.drawerRoot}
                       
                    >
                        <ListItem 
                            button 
                            onClick={handleOpenNestedList} 
                            style={{paddingLeft: 0}}
                            // disabled={ roleName == 'superadmin' ? false : true }
                        >
                            <ListItemIcon>
                                <IconButton
                                    aria-haspopup="true"
                                    color="inherit"
                                    

                                    
                                >  
                                    {
                                        fotoQuery !== '' ? (
                                                                        
                                            <img src={URL_API + '/' +fotoQuery+ "&token=" + userToken}  className={classes.bigRealFotoDrawer} />

                                        ) : (

                                            <AccountCircle className={classes.accountCircle} />

                                        )
                                    }
                                </IconButton>
                            </ListItemIcon>

                            <ListItemText 
                                        
                                primary = {

                                    <Typography variant='subtitle2' className={classes.namaPerusahaan} style={{marginLeft: 8}}>
                                        { 
                                            namaGroupPerusahaan !== '' ? namaGroupPerusahaan : '-'
                                        }
                                    </Typography>
                                } 
                            />

                            { 
                                openNestedList  ? 

                                    <ExpandLess /> : 
                                        
                                        <ExpandMore style=
                                            {
                                                roleName == 'superadmin' ?
                                                    {
                                                        color: 'white'
                                                    } : {
                                                        color: 'transparent'
                                                    }
                                        } 
                                        /> 
                            }

                        </ListItem>

                        <Collapse in={openNestedList} timeout="auto" unmountOnExit>
                            <List 
                                component="div" 
                                disablePadding
                            >
                                <ListItem 
                                    onClick={() => Redirect(ToCompanyProfile)}
                                    button 
                                    className={classes.nested}
                                >
                                <ListItemText 
                                    inset 
                                    primary={
                                        <Typography 
                                            type="ProfilPerusahaan" 
                                            style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                        >
                                            Profil Perusahaan
                                        </Typography>
                                    }  
                                />
                                </ListItem>
                            </List>

                            <List component="div" disablePadding>
                                <ListItem 
                                    onClick={() => Redirect(ToMembershipStatus)}
                                    button className={classes.nested}
                                >                   
                                <ListItemText 
                                    inset 
                                    primary={<Typography type="Keanggotaan" style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}>Keanggotaan</Typography>}  
                                />
                                </ListItem>
                            </List>

                            <List component="div" disablePadding>
                                <ListItem 
                                button 
                                className={classes.nested}
                                onClick={() => Redirect(ToUserManagement)}
                                >
                        
                                <ListItemText 
                                    inset 
                                    primary={<Typography type="PengaturanUser" style={{ color: 'white', fontFamily: 'Nunito',marginLeft: 10  }}>Pengaturan User</Typography>}  
                                    
                                />
                                </ListItem>
                            </List>

                            <List component="div" disablePadding>
                                <ListItem button className={classes.nested}>
                                    <ListItemText 
                                        inset 
                                        onClick={() => Redirect(ToRole)}
                                        primary={
                                            <Typography type="PengaturanRole" 
                                                style={{ color: 'white', fontFamily: 'Nunito',marginLeft: 10  }}
                                            >
                                                Pengaturan Role
                                            </Typography>
                                        }  
                                    />
                                </ListItem>
                            </List>
                        </Collapse>

                        <List component="nav">
                            <ListItem 
                                button
                                onClick={() => Redirect(ToDashboard)}  
                            >
                                <ListItemIcon>
                                    <img src={ImageDashboardHome} style={{marginLeft: 4}} />
                                </ListItemIcon>
                                <ListItemText 
                                    primary={
                                        <Typography 
                                        type="PengaturanRole" 
                                        style={{ color: 'white', fontFamily: 'Nunito' }}
                                        >
                                        Dashboard
                                        </Typography>
                                    }  
                                />
                            </ListItem>

                            <ListItem 
                                button
                                onClick={handleOpenSevenWD}
                            >
                                <ListItemIcon>
                                    <img src={Image7WD} />  
                                </ListItemIcon>
                                <ListItemText 
                                    primary={
                                        <Typography type="PengaturanRole" style={{ color: 'white', fontFamily: 'Nunito' }}>
                                            7WD
                                        </Typography>
                                    }  
                                />

                                {openNestedSevenWD ? <ExpandLess /> : <ExpandMore />}

                            </ListItem>

                            <Collapse in={openNestedSevenWD} timeout="auto" unmountOnExit>

                                <List component="div" disablePadding>
                                    <ListItem 
                                        onClick={() => Redirect(ToGoal)}
                                        button 
                                        className={classes.nested}
                                    >
                                        <ListItemText 
                                            inset 
                                            primary = {

                                                <Typography 
                                                    type="so" 
                                                    style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                                >
                                                    Goal
                                                </Typography>
                                            }  
                                        />
                                    </ListItem>

                                    <ListItem 
                                        onClick={() => Redirect(To7wdPeriode)}
                                        button 
                                        className={classes.nested}
                                    >
                                        <ListItemText 
                                        inset 
                                        primary = {
                                                <Typography 
                                                    type="karyawan" 
                                                    style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                                >
                                                    Periode
                                                </Typography>
                                            }  
                                        />
                                    </ListItem>
                                </List>
                                </Collapse>

                                {/* 
                                    `````````````````````
                                    HUMAN RESOURCE NESTED

                                    `````````````````````
                                
                                */}

                                {
                                    roleName == 'superadmin' && (

                                        <ListItem 
                                            onClick={handleOpenHumanCapital}
                                            button
                                        >
                                            <ListItemIcon>
                                                <img src={ImageHumanCapital} />
                                            </ListItemIcon>

                                            <ListItemText 
                                                primary={
                                                    <Typography type="PengaturanRole" 
                                                    style={{ color: 'white', fontFamily: 'Nunito' }}
                                                    >
                                                    Human Capital

                                                    </Typography>
                                                }  
                                                />

                                                {openNestedHumanCapital ? <ExpandLess /> : <ExpandMore />}

                                        </ListItem>
                                    )
                                }


                            <Collapse in={openNestedHumanCapital} timeout="auto" unmountOnExit>

                                <List component="div" disablePadding>
                                    <ListItem 
                                        onClick={() => Redirect(ToSOTable)}
                                        button 
                                        className={classes.nested}
                                    >
                                        <ListItemText 
                                        inset 
                                        primary={
                                            <Typography 
                                                type="so" 
                                                style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                            >
                                            SO
                                            </Typography>
                                        }  
                                        />
                                    </ListItem>

                                    <ListItem 
                                        onClick={() => Redirect(ToEmptyStateGeneral)}
                                        button 
                                        className={classes.nested}
                                    >
                                        <ListItemText 
                                            inset 
                                            primary={
                                                <Typography 
                                                    type="karyawan" 
                                                    style={{ color: 'white', fontFamily: 'Nunito', marginLeft: 10 }}
                                                >
                                                    Karyawan
                                                </Typography>
                                            }  
                                        />
                                    </ListItem>
                                </List>
                            </Collapse>
                        </List>
                    </List>
                </div>
            </Drawer>
        </MuiThemeProvider>
    )
};

export default withStyles(styles)(HeaderGOAL);
