/*

    ``````````````````````````````````````
    TAB OVERVUEW GOAL & MEASURE ACTIVITIES

    ``````````````````````````````````````
*/

export const RouteToOverviewGoal = '/overview-goal';
export const RouteToMeasureActivities = '/measure-activities';
