import React, { Component, useEffect, useState, useCallback, Fragment } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Snackbar, Fab
} from '@material-ui/core';

import {useDropzone} from 'react-dropzone'
import StickyFooter from 'react-sticky-footer';
import { ThemeProvider } from "@material-ui/styles";
import { DatePicker, MuiPickersUtilsProvider, } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import moment from 'moment';
import AddIcon from '@material-ui/icons/Add';


import DoneIcon from '@material-ui/icons/Done';  
// import ButtonSimpanNOW from '../KontakDarurat/Components/ButtonSimpanNOW';

import Snackber from '../Components/Snackber';
import Snackbeer from '../Components/Snackbeer';


import Redirect from '../../../utilities/Redirect';
import { ToHrEmployeeIdentitasAlamat } from '../../../constants/config-redirect-url';
import { extractImageFileExtensionFromBase64 } from '../../../utilities/ReusableUtils';
import AvatarDummy from '../../../assets/images/Avatar_dummy.png';
import PictInfoErrur from '../../../assets/images/Group_2237.png';
import { styles } from './Style/StyleViewIdentitas';

import 'moment/locale/id';

class LocalizedUtils extends MomentUtils {

    getDatePickerHeaderText(date) {
        return this.format(date, "DD MMMM YYYY", { locale: this.locale });
    }
};

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }       
});

const ViewIdentitas = props => {

    const { classes } = props;

    /* 
        ````````````````````
        COMPONENT DID MOUNT
        
        ````````````````````
    */

    const dataIdentitasLocalStorage = localStorage.getItem('data_identitas');
    const dataIdentitasLocalStorageAfterParse = JSON.parse(dataIdentitasLocalStorage);

    const foto = dataIdentitasLocalStorageAfterParse !== null ? dataIdentitasLocalStorageAfterParse.foto : '';
    const nipInLocal = dataIdentitasLocalStorageAfterParse !== null ? dataIdentitasLocalStorageAfterParse.nip : '';
    const namaDepanInLocal = dataIdentitasLocalStorageAfterParse !== null ? dataIdentitasLocalStorageAfterParse.nama_depan : '';
    const namaBelakangInLocal = dataIdentitasLocalStorageAfterParse !== null ? dataIdentitasLocalStorageAfterParse.nama_belakang : '-';
    const namaPanggilanInLocal = dataIdentitasLocalStorageAfterParse !== null ? dataIdentitasLocalStorageAfterParse.nama_panggilan : '';
    const noHpInLocal = dataIdentitasLocalStorageAfterParse !== null ? dataIdentitasLocalStorageAfterParse.no_hp : '';
    const emailLocal = dataIdentitasLocalStorageAfterParse !== null ? dataIdentitasLocalStorageAfterParse.email : '';
    const tempatLahirInLocal = dataIdentitasLocalStorageAfterParse !== null ? dataIdentitasLocalStorageAfterParse.tempat_lahir : '';


    // const agama = dataIdentitasLocalStorageAfterParse !== '' ? dataIdentitasLocalStorageAfterParse.agama : '';
    /* 
        ````````````````````
        Feature Upload Foto 
        
        ````````````````````
    */
    const [imageBinaryPreviewUrl, setImageBinaryPreviewUrl] = useState(foto);
    const [imgSrcExt, setImgSrcExt] = useState();
    const [ contentFile, setContentFile ] = useState(undefined);
    const[ nameFile, setNameFile ] = useState('');
   
    // const [imgSrc, setImgSrc ] = useState();
    const onDrop = useCallback(acceptedFiles => {
       
        // Do something with the files
        //    setContentFile(acceptedFiles); // *Belum di pake
        setNameFile(acceptedFiles[0].name);


       //*
       const reader = new FileReader()
   
       reader.onabort = () => console.log('file reading was aborted')
       reader.onerror = () => console.log('file reading has failed')
       reader.onload = () => {
           // Do whatever you want with the file contents
           const binaryStr = reader.result
           console.log("Binary String : ",binaryStr);

           setImageBinaryPreviewUrl(binaryStr);
           setImgSrcExt(extractImageFileExtensionFromBase64(binaryStr))

       };
   
       // acceptedFiles.forEach(file => reader.readAsBinaryString(file))
       acceptedFiles.forEach(file => reader.readAsDataURL(file))

   }, []);

   const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});

    
    /*
        ``````````````````````
        Dropdown

        ``````````````````````

    */

    const [religions, setReligion] = useState({

        religion: ''

    });   


    const [ dataForSimpanNow, setDataForSimpanNow ] = useState(null);

    const handleChangeDropdown = name => event => {

        // console.log("Name drop down : ", event.target.value);
        setReligion({ ...religions, [name]: event.target.value });

        //*FIRE LENGKAPI DATA 
        setOpenDialogSimpanSekarang(true);

        let paramsData = {

            foto: imageBinaryPreviewUrl,
            foto_name: nameFile,
            // foto: contentFile,
            nip : nip,
            nama_depan: namaDepan,
            nama_belakang: namaBelakang !== '' ? namaBelakang : '-' ,
            nama_panggilan: namaPanggilan,
            no_hp: noHp,
            email: email,
            tempat_lahir: tempatLahir,
            tanggal_lahir: tanggalLahir,
            agama: event.target.value.id,//religions.religion,
            jenis_kelamin: selectedValue
    
        };

        // console.log('paramsData simpan NOW :  ', paramsData);
        setDataForSimpanNow(paramsData);

    };

    /*
        ```````````````````````````````````````````````````
        HANDLE DIALOG SIMPAN SEKARANG || FIRE LENGKAPI DATA 

        ```````````````````````````````````````````````````

    */
    const [ isOpenDialogSimpanSekarang, setOpenDialogSimpanSekarang ] = useState(false);

    /*
        `````````````
        RADIO BUTTON

        `````````````

    */

    const [selectedValue, setSelectedValue] = useState('lakiLaki');

    function handleChangeRadioButton(event) {

        setSelectedValue(event.target.value);

        if(event.target.value == 'lakiLaki'){

            console.log(event.target.value);
            
            // callListMasterRole(event.target.value)

        } else {

            console.log(event.target.value);
        };



    };

    /*
        `````````
        FORM DATA

        `````````
    */
    const [nip, setNip] = useState(nipInLocal);
    const [namaDepan, setNamaDepan] = useState(namaDepanInLocal);
    const [namaBelakang, setNamaBelakang] = useState(namaBelakangInLocal);
    const [namaPanggilan, setNamaPanggilan] = useState(namaPanggilanInLocal);
    const [noHp, setNoHp] = useState(noHpInLocal);
    const [email, setEmail] = useState(emailLocal);
    const [tempatLahir, setTempatLahir] = useState(tempatLahirInLocal);
    const [tanggalLahir, setTanggalLahir ] = useState('');

    let paramsData = {

        foto: imageBinaryPreviewUrl,
        foto_name: nameFile,
        // foto: contentFile,
        nip : nip,
        nama_depan: namaDepan,
        nama_belakang: namaBelakang !== '' ? namaBelakang : '-' ,
        nama_panggilan: namaPanggilan,
        no_hp: noHp,
        email: email,
        tempat_lahir: tempatLahir,
        tanggal_lahir: tanggalLahir,
        agama: religions.religion,
        jenis_kelamin: selectedValue

    };

    // const [data, setData] = useState(paramsData);
    const handleChangeNIP = (e) => setNip(e.target.value);
    const handleChangeNamaDepan = (e) => setNamaDepan(e.target.value);
    const handleChangeNamaBelakang = (e) => setNamaBelakang(e.target.value);
    const handleChangeNamaPanggilan = (e) => setNamaPanggilan(e.target.value);
    const handleChangeNoHp = (e) => setNoHp(e.target.value);
    const handleChangeEmail = (e) => setEmail(e.target.value);
    const handleChangeTempatLahir = (e) => setTempatLahir(e.target.value);

    // const handleChangeTanggalLahir = (e) => {

    //     setTanggalLahir(e.target.value);
    //     setOpenDatePickerComponent(true);
    // }

    /*
        `````````````````````````````````````````

        HANDLE POST LANJUT & VALIDATION SNACKBAR

        `````````````````````````````````````````
    */
    //  const { register, handleSubmit, errors } = useForm(); // initialise the hook 

    const [isFormFilledAll, setFormFilledAll] = useState(false);

    const [ infoError, setInfoError ] = useState(false);

    const handlePOSTLanjut = () => {

        // console.log(data);
        console.log(paramsData);
        localStorage.setItem('data_identitas', JSON.stringify(paramsData));

        //*FIRST CONDITIONS
        if(paramsData.foto == ''){ setInfoError(true) }
        if(paramsData.nip == ''){ setInfoError(true) }
        if(paramsData.nama_depan == ''){ setInfoError(true) }
        if(paramsData.nama_belakang == ''){ setInfoError(true) }
        if(paramsData.nama_panggilan == ''){ setInfoError(true) }
        if(paramsData.no_hp == ''){ setInfoError(true) }
        if(paramsData.email == ''){ setInfoError(true) }
        if(paramsData.tempat_lahir == ''){ setInfoError(true) }
        if(paramsData.tanggal_lahir == ''){ setInfoError(true) }
        if(paramsData.agama == ''){ setInfoError(true) }
        // if(paramsData.jenis_kelamin == ''){ setInfoError(true) }

        //*SECOND CONDITIONS
        if( paramsData.foto !== '', paramsData.nip !== '' && paramsData.nama_depan !== '' && paramsData.nama_belakang !== '' 
            && paramsData.nama_panggilan !== '' && paramsData.no_hp !== '' && paramsData.email !== '' 
            && paramsData.tempat_lahir !== '' && paramsData.tanggal_lahir !== '' && paramsData.agama !== '' && paramsData.jenis_kelamin !== ''
            // selectedValue !== ''&& religions.religion !== ''

        ){
            
            console.log("Oke tersimpan semua di Local storage.. PART-1 [Informasi Dasar]");
            Redirect(ToHrEmployeeIdentitasAlamat);

        } else {

            // alert('Field harus terisi semua !');
            setFormFilledAll(true);

        };

    };


    //********************************** */
    const [locale, setLocale] = useState("id");

    const [isOpenDatePicker, setOpenDatePicker ] = useState(false);
    const [ isOpenDatePickerComponent, setOpenDatePickerComponent] = useState(false);

    const handleOpenComponentDatePicker = () => {

        setOpenDatePickerComponent(true);
        setOpenDatePicker(true);
    };

    return (
        <MuiThemeProvider theme={theme}>
            <br />
            <br />
            <Grid container>
                <Grid item xs={4} style={{textAlign : 'left'}}>

                    <Paper elevation={1} className={classes.paperInfoIdentitas} square={true}> 
                        <br />
                        <Typography variant='subtitle2' className={classes.titleHeader}>
                            <b>Informasi Identitas</b> 
                        </Typography>
                        <ul>
                            <li className={classes.titleActive}  style={{margin: 8}}>
                                <Typography variant='subtitle2' className={classes.titleActive}>
                                    <b>Informasi Dasar</b> 
                                    {/* <DoneIcon style={{color: 'green', position: 'absolute', marginTop: 1, marginLeft: 8, fontSize: 17}} /> */}
                                </Typography>
                            </li>

                            <li className={classes.titleNotActive} style={{margin: 8}}>
                                <Typography variant='subtitle2' className={classes.titleNotActive}>
                                    <b>Alamat </b>
                                </Typography>
                            </li>

                            <li className={classes.titleNotActive} style={{margin: 8}}>
                                <Typography variant='subtitle2' className={classes.titleNotActive}>
                                    <b>Informasi Tambahan</b>
                                </Typography>
                            </li>

                        </ul>                    
                        <br />

                    </Paper>

                </Grid>

                <Grid item xs={8} style={{textAlign : 'left '}}>
                    <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}}> 
                            <br />
                            <Grid container>
                                <Grid item sm={10}>
                                    <Typography variant='subtitle1' className={classes.titleHeader}>
                                        <b>Informasi Dasar</b> 
                                    </Typography>
                                </Grid>

                                <Grid item sm={2}>
                                    {/* 
                                        <IconButton>
                                            <i className='material-icons'>
                                                edit
                                            </i>
                                        </IconButton> 
                                    */}
                                </Grid>
                            </Grid>

                            <br />
                            <List className={classes.list}>

                                <ListItem style={{textAlign: 'left'}}>  
                                    <ListItemText primary={
                                            <Typography variant='subtitle1' className={classes.titleForm}>
                                                <b>Foto<span style={{color: '#cc0707'}}>*</span></b>
                                            </Typography>
                                        } 
                                    />

                                   <div {...getRootProps()}>
                                        <input {...getInputProps()} />
                                        {
                                            imageBinaryPreviewUrl ? (
                                                <Avatar alt="You" src={ imageBinaryPreviewUrl} className={classes.realAvatar}  />
                                                
                                            ) : (
                                                    <Avatar alt="You" 
                                                        src={AvatarDummy} className={classes.bigAvatar} />
                                            )
                                        }
                                    </div>

                                </ListItem>
                                <br />

                                <ListItem >  
                                    <ListItemText 
                                        primary={
                                            <Typography variant='subtitle1' className={classes.titleForm}>
                                                <b>NIP<span style={{color: '#cc0707'}}>*</span></b>
                                            </Typography>
                                        } 
                                    />
                                    <TextField
                                        id="outlined-bare"
                                        onChange= {handleChangeNIP}
                                        value={nip}
                                        className={classes.textField}
                                        placeholder={'NIP'}
                                        variant="outlined"
                                        // error={data.nip === "" ? true : false}
                                        // helperText={data.nip === "" ? 'Empty NIP field!' : data.nip}
                                        // required={true} 
                                        // name='nip'
                                        // color='primary'
                                        // onKeyDown={handleEnterPress}
                                        // disabled= {isLockedStatusState == 1 ? true : false}
                                        // fullWidth
                                        error={infoError == true && nip == '' ? true : false}
                                        helperText={infoError == true && nip == '' ? "Wajib di isi" : ' '}
                                    />
                                    
                                </ListItem>

                                <ListItem >  
                                    <ListItemText 
                                        primary={
                                            <Typography variant='subtitle1' className={classes.titleForm}>
                                                <b>Nama Depan<span style={{color: '#cc0707'}}>*</span> </b>
                                            </Typography>
                                        } 
                                    />
                                    <TextField
                                        id="outlined-bare"
                                        className={classes.textField}
                                        onChange= {handleChangeNamaDepan}
                                        value={namaDepan}
                                        placeholder={'Nama Depan'}
                                        variant="outlined"
                                        // inputProps= {{
                                        //     style: { textAlign: "right" }
                                        // }}
                                        error={infoError == true && namaDepan == '' ? true : false}
                                        helperText={infoError == true && namaDepan == '' ? "Wajib di isi" : ' '}
                                    />
                                </ListItem>

                                <ListItem >  
                                    <ListItemText 
                                        primary={
                                            <Typography variant='subtitle1' className={classes.titleForm}>
                                                <b>Nama Belakang<span style={{color: '#cc0707'}}>*</span></b>
                                            </Typography>
                                        } 
                                    />
                                    <TextField
                                        id="outlined-bare"
                                        className={classes.textField}
                                        onChange= {handleChangeNamaBelakang}
                                        value={namaBelakang}
                                        placeholder={'Nama Belakang'}
                                        variant="outlined"
                                        error={infoError == true && namaBelakang == '' ? true : false}
                                        helperText={infoError == true && namaBelakang == '' ? "Wajib di isi" : ' '}
                                    />
                                </ListItem>

                                <ListItem >  
                                    <ListItemText primary = {
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Nama Panggilan<span style={{color: '#cc0707'}}>*</span></b>
                                        </Typography>
                                    } 
                                    />
                                    <TextField
                                        id="outlined-bare"
                                        className={classes.textField}
                                        onChange= {handleChangeNamaPanggilan}
                                        value={namaPanggilan}
                                        placeholder={'Nama Panggilan'}
                                        variant="outlined"
                                        error={infoError == true && namaPanggilan == '' ? true : false}
                                        helperText={infoError == true && namaPanggilan == '' ? "Wajib di isi" : ' '}
                                    />
                                </ListItem>

                                <ListItem >  
                                    <ListItemText primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>No <i>Handphone</i><span style={{color: '#cc0707'}}>*</span></b>
                                        </Typography>
                                    } 
                                    />
                                    <TextField
                                        id="outlined-bare"
                                        className={classes.textField}
                                        onChange= {handleChangeNoHp}
                                        value={noHp}
                                        placeholder={'No Handphone'}
                                        variant="outlined"
                                        error={infoError == true && noHp == '' ? true : false}
                                        helperText={infoError == true && noHp == '' ? "Wajib di isi" : ' '}
                                    />
                                </ListItem>

                                <ListItem>  
                                    <ListItemText primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Alamat <i>Email</i>
                                                <span style={{color: '#cc0707'}}>*</span>
                                            </b>
                                        </Typography>
                                    } 
                                    />
                                    <TextField
                                        id="outlined-bare"
                                        className={classes.textField}
                                        onChange= {handleChangeEmail}
                                        value={email}
                                        placeholder={'Alamat Email'}                                      
                                        variant="outlined"
                                        error={infoError == true && email == '' ? true : false}
                                        helperText={infoError == true && email == '' ? "Wajib di isi" : ' '}
                                    />
                                </ListItem>

                                <ListItem>  
                                    <ListItemText primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tempat Lahir
                                                <span style={{color: '#cc0707'}}>*</span>
                                            </b>
                                        </Typography>
                                    } 
                                    />
                                    <TextField
                                        id="outlined-bare"
                                        className={classes.textField}
                                        onChange= {handleChangeTempatLahir}
                                        value={tempatLahir}
                                        placeholder={'Tempat Lahir'}
                                        variant="outlined"
                                        error={infoError == true && tempatLahir == '' ? true : false}
                                        helperText={infoError == true && tempatLahir == '' ? "Wajib di isi" : ' '}
                                    />
                                </ListItem>

                                
                                <ListItem>  
                                    <ListItemText 
                                        primary={
                                            <Typography variant='subtitle1' className={classes.titleForm}>
                                                <b>Tanggal Lahir
                                                    <span style={{color: '#cc0707'}}>*</span>
                                                </b>
                                            </Typography>
                                        } 
                                    />

                                    
                                    
                                    <MuiPickersUtilsProvider utils={LocalizedUtils} locale={locale}>
                                        <ThemeProvider theme={theme}>
                                            <Fragment>
                                            {
                                                isOpenDatePickerComponent == true && (
                                                    
                                                    <DatePicker
                                                        value={tanggalLahir}
                                                        format="DD MMMM YYYY"
                                                        onChange={setTanggalLahir}
                                                        animateYearScrolling
                                                        open={isOpenDatePicker}
                                                        onOpen={() => setOpenDatePicker(true)}
                                                        onClose={() => setOpenDatePicker(false)}
                                                        variant="dialog"// dialog, static, inline
                                                        disableToolbar={false}
                                                        label=""
                                                        className={classes.textField}
                                                        // onChange={handleChangeTanggalLahir}
                                                        // orientation="landscape"
                                                        // format
                                                        // TextFieldComponent =
                                                        // ToolbarComponent 
                                                        // style={{marginLeft: 16}}
                                                    />
                                                )
                                            }
                                            </Fragment>
                                        </ThemeProvider>
                                    </MuiPickersUtilsProvider>

                                    {
                                        isOpenDatePickerComponent == false && (

                                            <form className={classes.container} noValidate>
                                                <TextField
                                                    id="date"
                                                    onClick={handleOpenComponentDatePicker}
                                                    placeholder="17 Agustus 1945"
                                                    variant="outlined"
                                                    className={classes.textField}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    error={infoError == true && tanggalLahir == '' ? true : false}
                                                    helperText={infoError == true && tanggalLahir == '' ? "Wajib di isi" : ' '}
                                                    // onChange={handleChangeTanggalLahir}
                                                    // value={tanggalLahir}
                                                    // onClick={() => setOpenDatePickerComponent(true)}
                                                    // defaultValue="2019-05-24"
                                                    // type="date"
                                                    // label="Birthday"
                                                />
                                            </form>
                                        )
                                    }

                                </ListItem>
                                
                                <ListItem>  
                                    <ListItemText primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Agama
                                                <span style={{color: '#cc0707'}}>*</span>
                                            </b>
                                        </Typography>
                                    } 
                                    />
                                <TextField
                                        id="outlined-select-currency"
                                        select
                                        label="Pilih Agama : "
                                        className={classes.textField}
                                        value={religions.religion}
                                        onChange={handleChangeDropdown('religion')}
                                        SelectProps={{
                                            MenuProps: {
                                                className: classes.menu,
                                            },
                                        }}
                                        margin="normal"
                                        variant="outlined"
                                        error={infoError == true && religions.religion == '' ? true : false}
                                        helperText={infoError == true && religions.religion == '' ? "Wajib di isi" : ' '}
                                    >
                                        {
                                            religionsData.map (

                                                option => (
                                                    // <MenuItem key={option.value} value={option.value}>
                                                    <MenuItem key={option.value} value={option}>

                                                        {option.label}
                                                    </MenuItem>
                                                )
                                            )
                                        }

                                    </TextField>
                                </ListItem>

                                <ListItem>  
                                    <ListItemText primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Jenis Kelamin
                                                <span style={{color: '#cc0707'}}>*</span>
                                            </b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <Radio 
                                                checked={selectedValue === 'lakiLaki'}
                                                onChange={handleChangeRadioButton}
                                                value="lakiLaki"
                                                name='Laki-laki'
                                                // disabled= {isLockedStatusState == 1 ? true : false}
                                            />
                                            <span style={{fontFamily: 'Nunito'}}>Laki- laki</span>

                                            <Radio 
                                                checked={selectedValue === 'perempuan'}
                                                onChange={handleChangeRadioButton}
                                                value="perempuan"
                                                name='Perempuan'
                                                // disabled= {isLockedStatusState == 1 ? true : false}
                                            />
                                            <span style={{fontFamily: 'Nunito'}}>Perempuan</span>
                                        </Typography>
                                    } 
                                    />
                                </ListItem>
                            </List>
                    </Paper>
                    <br />
                    <br />
                    <br />
                </Grid>
            </Grid>

            {/* 

                ````````````````````
                STICKY FOOTER

                ````````````````````
            */}
            <Grid  
                container
                spacing={8}
                direction="row"
                justify="center"
                // alignItems="center"
            >  
                <Grid item sm={9}></Grid>
                
                <Grid item sm={1} style={{textAlign: 'right'}}>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <StickyFooter bottomThreshold={10}>
                        <Button 
                            variant="outlined" 
                            color="secondary" 
                            className={classes.buttonOutlined}
                            style={{margin : 14}}
                            // onClick={() => Redirect(ToMembershipStatusScenarioPENDING_SEMENTARA)}
                        >
                            Keluar  
                        </Button>

                    </StickyFooter>
                </Grid>
                <Grid item sm={2} style={{textAlign: 'left'}}>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <StickyFooter bottomThreshold={10}>
                        <Button 
                            variant="contained" 
                            color="secondary" 
                            className={classes.button}
                            style={{margin : 14}}
                            disabled={false}
                            // onClick={handleSubmit(handlePOSTLanjut)}
                            onClick={handlePOSTLanjut}
                        >
                            Lanjut  
                        </Button>
                    </StickyFooter>
                </Grid>
            </Grid>


             {/*  

                `````````````````````
                VALIDATION SNACK BAR

                `````````````````````

            */}
            <Snackber 
                isFormFilledAll={isFormFilledAll}
                setFormFilledAll={setFormFilledAll}
                
            />

            {/*  
                ````````````````````````````````````
                INFORMATION TO SIMPAN NOW SEKARANG !

                ````````````````````````````````````
            */}
            <Snackbeer 
                classes = { classes }
                isOpenDialogSimpanSekarang = { isOpenDialogSimpanSekarang }
                setOpenDialogSimpanSekarang = { setOpenDialogSimpanSekarang }
                dataForSimpanNow = { dataForSimpanNow }

                setReligion = { setReligion }

                

            />

            {/* 
                `````````````````````````
                TRYING

                `````````````````````````
            */}


             
        </MuiThemeProvider>
    )
};

export default withStyles(styles)(ViewIdentitas) ;

const religionsData = [
    {
        id: 'de5e0c55-9a31-4d3d-929d-e53421c62477',
        value: 'Buddha',
        label: 'Buddha',
    },
    {
        id: 'ace04e30-8065-408a-9c31-4b04685549b0',
        value: 'Katolik',
        label: 'Katolik',
    },
    {
        id: '93c97c1b-dc96-4397-b0cf-7b339a82c0d4',
        value: 'Kristen protestan',
        label: 'Kristen protestan',
    },
    {
        id: '41b5d847-a6e9-4b0e-a1d0-12b40c51239a',
        value: 'Hindu',
        label: 'Hindu',
    },
    {
        id: '007fba3e-35ce-485f-bc22-feda9125c813',
        value: 'Islam',
        label: 'Islam',
    },

  ];