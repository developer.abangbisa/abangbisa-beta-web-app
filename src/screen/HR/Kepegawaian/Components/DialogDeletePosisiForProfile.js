
import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

import DialogError from '../../../../components/DialogError';


const DialogDeletePosisiForProfile = (props) => {
    
    const { 

            classes, 
            modalDelete, 
            setModalDelete,
            idPosition,
            positionTitleName,
            listPositionNow,
            setlistPositionNow

    } = props;

    /*
        ```````````````````````````
        GET ALL COLLECTION POSITION

        ```````````````````````````
    */
    const [ listPosition, setListPosition ] = useState([]);
    const [ userTokenState, setUserTokenState ] = useState('');
    const [ employeeIdState, setEmployeeIdState ] = useState('');

    const [ updatedAt, setUpdatedAt ] = useState('');
    // isOpenDialogError, setOpenDialogError, textErrorInformation

    useEffect(() => {

        if(modalDelete == true ){

            const userToken = localStorage.getItem('userToken');
            const employeeId = localStorage.getItem('employee_id');

            setUserTokenState(userToken);
            setEmployeeIdState(employeeId);

            if(userToken !== undefined ){
            
                const header =  {    
    
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
                };
    
                axios.defaults.headers.common = header;    
                
                /*
                    ``````````````
                    GET UPDATED AT

                    ``````````````
                */
                axios
                    .get(URL_API + `/human-resource/employee/${employeeId}/patch`)
                    .then(function(response){
                        
                        console.log("Response Original - Update At: ", response)
                        
                        if(response.status == 200){

                            setUpdatedAt(response.data.data.updated_at);

                        };
                    })
                    .catch(function(error){
                        
                        if(error.response !== undefined){

                            if(error.response.status == 422){
                                alert("Error : Unprocesity entity !");
                            };

                        } else {
                            alert('Periksa koneksi internet Anda !')
                        };

                        console.log("Error : ", error.response)
                    })
    
            } else { console.log("No Access Token available!")};
        };

    }, [modalDelete])
            

    const handleDelete = () => {

        let listAfterFilter = [];

        listPositionNow.length > 1 && listPositionNow.map((item) => {

            if(item.id !== idPosition){

                listAfterFilter.push(item.id);
            };

        });

        console.log('listAfterFilter :', listAfterFilter);

        let data = {

            Employee : {

                updated_at: updatedAt,
                position_id: listAfterFilter

            },
            _method: 'patch'
        };

        console.log("Data : ", data);

        if(userTokenState !== undefined && userTokenState !== null ){

            const header =  {       

                'Accept': "application/json",
                'Content-Type' : "multipart/form-data",
                'Authorization' : "bearer " + userTokenState,
            };
          
            axios.defaults.headers.common = header;    
      
            axios
                .post(URL_API + `/human-resource/employee/${employeeIdState}`, data)
                .then(function(response){
      
                    console.log("Response Original : ", response);
      
                    if(response.status == 200 ){

                        setModalDelete(false);
                        setlistPositionNow(listPositionNow.filter(item => item.id !== idPosition));

                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    if(error.response == 500){
      
                      alert(error.response.data.message !== undefined ? error.response.data.message : "500");
      
                    } else {
      
                      alert("Ooops, something went wrong !")
                    }
                    
                })
      
          } else { console.log("No Access Token available!")};
        
    };
    
    return (

        <Dialog
            open={modalDelete}
            onClose={() => setModalDelete(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='h6' className={classes.title}>
                    {/* Tambah Anggota */}
                </Typography>
                
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.title} style={{color: 'black'}}>
                        <b>Apakah Anda yakin ingin menghapus <i>{ positionTitleName }</i> ?</b>
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    onClick={handleDelete}
                    variant='contained' 
                    className={classes.buttonModalDelete}
                    // fullWidth
                >  
                    Yakin
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};


export default DialogDeletePosisiForProfile;
