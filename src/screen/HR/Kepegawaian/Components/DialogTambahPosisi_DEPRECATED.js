


/*
    ````````````````````````````````````
        Component ini sudah deprecated !

    ````````````````````````````````````
*/






import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';


const DialogTambahPosisi = props => {

    const { 
        
            classes,
            modalTambahPosisi,
            setmodalTambahPosisi,
            // handleChangeNamaAnggotaKeluarga,
            // nameAnggotaKeluarga,
            // hubunganKeluarga,
            // handleChangeHubunganKeluarga,
            // // hubunganKeluargas,
            // handleChangeTanggalLahir,
            // tanggalLahir,
            // handleSimpanDataAnggotaKeluarga
        
        } = props;

    
    /*
        ``````````````
        HANDLE CHEKBOX

        ``````````````
    */

    // const [ isChecked, setChecked ] = useState({

    //     name: false
    // });

    // const handleChangeCheckbox = name => event => {

    //     setChecked({ ...isChecked, [name]: event.target.checked });
    // };

    const [ isChecked, setChecked ] = useState (
        {
            id: '',
            value: '',
            label: ''
        }
    );
    const handleChangeCheckbox = (e, data) => {

        // e.preventDefault();

        console.log("Data checkbox : ", data);

        // if(data !== undefined){

        //     setChecked(data.id);

        // };
    };

    return (

        <Dialog
            open={modalTambahPosisi}
            onClose={() => setmodalTambahPosisi(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Tambah Posisi</b>
            </Typography>
        </DialogTitle>
        <DialogContent>

            <List style={{width: 245}}>

                {
                    positions.length > 0 ? positions.map((item, i) => (

                        <ListItem key={i}>  

                            <FormControlLabel
                                className={classes.checkboxStyle}
                                control={
                                    <Checkbox 
                                        // className={classes.checkboxStyle}
                                        checked={isChecked.id == item.id ? true : false} 
                                        // onChange={handleChangeCheckbox('name')} 
                                        onChange={(e) => handleChangeCheckbox(e, item)}
                                        value={item} 
                                    />
                                }
                                label={item.label}
                            />
                        </ListItem>

                    )) : null
                }

                {/* <ListItem >  
                    <FormControlLabel
                        className={classes.checkboxStyle}
                        control={
                            <Checkbox 
                                // className={classes.checkboxStyle}
                                checked={isChecked.name} 
                                onChange={handleChangeCheckbox('name')} 
                                value="CEO" 
                            />
                        }
                        label="CEO"
                    />
                </ListItem> */}

               
            </List>

            <DialogContentText id="alert-dialog-description">
                <Typography variant='h6'>
                    {/* {infoResponse500 != '' ? infoResponse500 : 'Oops, something went wrong !'} */}
                </Typography>
            </DialogContentText>
        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            {/* <Button 
                onClick={() => setmodalTambahPosisi(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button> */}
            
            <Button 
                // onClick={() => Redirect(ToHrEmployeeKepegawaianInfoPegawai)}
                // onClick= {handleSimpanDataAnggotaKeluarga}
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Tambah
            </Button>
        </DialogActions>
        <br />
        <br />
        </Dialog>
    )   
};

export default DialogTambahPosisi;


const positions = [
    {
        id: 1,
        value: "CEO",
        label: "CEO",
    },
    {
        id: 2,
        value: "Manajer HRD",
        label: "Manajer HRD"
    },
    {
        id: 3,
        value: "Staff HRD",
        label: "Staff HRD"
    }
];


