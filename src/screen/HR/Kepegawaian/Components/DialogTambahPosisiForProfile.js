import React, { Component, useEffect, useState, Fragment } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Badge, Switch

} from '@material-ui/core';

import axios from 'axios';

import DialogModalDetail from '../../../HR-SO/Table/Components/DialogModalDetail';

import { URL_API } from '../../../../constants/config-api';

const DialogTambahPosisiForProfile = props => {

    const { 
            classes,
            modalTambahPosition,
            setModalTambahPosition,
            isChecked,
            handleChangeCheckbox,
            
            employeeIdState,
            setlistPositionNow

    } = props;


    /*
        ``````````````````````````
        GET LIST AVAILABLE FROM DB
        
        ``````````````````````````
    */
    const userToken = localStorage.getItem('userToken');
    
    const [ listPosition, setListPosition ] = useState([]);
    const [ listPositionNowOfEmployee, setListPositionNowOfEmployee ] = useState([]);
    const [ updatedAt, setUpdatedAt ] = useState('');

    const [ userTokenState, setUserTokenState ] = useState('');
    
    useEffect(() => {
           
        setUserTokenState(userToken)

        if(userToken !== undefined ){
        
            const header =  {    

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };

            axios.defaults.headers.common = header;    

            if(modalTambahPosition == true){

                /*
                    ``````````````````````````````````````````````````````````
                    GET DETAIL EMPLOYEE, UPDATED AT & POSITION NOW OF EMPLOYEE

                    ``````````````````````````````````````````````````````````
                */
                axios
                    .get(URL_API + `/human-resource/employee/${employeeIdState}/patch`)
                    .then(function(response){
                        
                        console.log("Response Original - DETAIL EMPLOYEE : ", response)
                        
                        if(response.status == 200){

                            setUpdatedAt(response.data.data.updated_at);

                            if(response.data.data !== null){

                                setListPosition(response.data.data.positionCollections);

                                if(response.data.data.fields.position_id !== null){
                                    if(response.data.data.fields.position_id.value !== null){
                                        if(response.data.data.fields.position_id.value.length > 0){
                                            
                                            setListPositionNowOfEmployee(response.data.data.fields.position_id.value);

                                        }
                                    }
                                }
                            };
                        };
                    })
                    .catch(function(error){
                        
                        if(error.response !== undefined){

                            if(error.response.status == 422){
                                alert("Error : Unprocesity entity !");
                            };

                        } else {
                            alert('Periksa koneksi internet Anda !')
                        };

                        console.log("Error : ", error.response)
                    })
            };

        } else { console.log("No Access Token available!")};
       

   }, [modalTambahPosition])

   /*
        ``````````````````
        HANDLE SIMPAN DATA

        ``````````````````
   */

    const handleSimpanData = () => {

        let templateListPositionId = [];
        
        if(listPositionNowOfEmployee.length > 0){

            listPositionNowOfEmployee.map((item) => {

                templateListPositionId.push(item);
            })
        };

        templateListPositionId.push(isChecked.id);


        let data = {

            Employee : {

                updated_at: updatedAt,
                position_id : templateListPositionId

            },
            _method: 'patch'
        };

        // console.log("Data : ", data);

        if(userTokenState !== undefined && userTokenState !== null ){

            const header =  {       

                'Accept': "application/json",
                'Content-Type' : "multipart/form-data",
                'Authorization' : "bearer " + userTokenState,
            };
          
            axios.defaults.headers.common = header;    
      
            axios
                .post(URL_API + `/human-resource/employee/${employeeIdState}`, data)
                .then(function(response){
      
                    console.log("Response Original : ", response)
      
                    if(response.status == 200 ){
      
                        setlistPositionNow(response.data.data.position)
                        setModalTambahPosition(false);
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    if(error.response == 500){
      
                      alert(error.response.data.message !== undefined ? error.response.data.message : "500");
      
                    } else {
      
                      alert("Ooops, something went wrong !")
                    }
                    
                })
      
          } else { console.log("No Access Token available!")};

    };


    /*
        ``````````````
        HANDLE SWITCH

        ``````````````
    */
    const [switchState, setSwitchState] = useState({

        checked: true
    });

    const handleChangeSwitch = name => event => {

        setSwitchState({ ...switchState, [name]: event.target.checked });

    };

    /*
        ````````````````````
        HANDLE DIALOG DETAIL 

        ````````````````````
    */

    const [ isModalDetail, setModalDetail ] = useState(false);
    const [ dataDetail, setDataDetail ] = useState(null);
    const handleDataDetail = (e, data) => {

        setModalDetail(true);
        setDataDetail(data);

        console.log("Data : ", data);

    };

    return (

        <Fragment>

            <Dialog
                open={modalTambahPosition}
                onClose={() => setModalTambahPosition(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>

                    <Grid container>
                        <Grid item sm={6}>
                            <Typography variant='subtitle1' className={classes.title}>
                                <b>Tambah Posisi</b>
                            </Typography>
                        </Grid>

                        <Grid item sm={6} style={{textAlign: 'right'}}>
                            <Tooltip title={ switchState.checked !== true ? "Lihat Posisi yang masih kosong" : '' } placement="bottom-left">

                                <Switch 
                                    // checked={isLockedStatusState == 0 ? true : false} 
                                    checked={switchState.checked}
                                    className={classes.switch}
                                    onChange={handleChangeSwitch('checked')}

                                />
                            </Tooltip>
                        </Grid>
                    </Grid>
                </DialogTitle>
                <DialogContent>

                    <List >
                        {
                            listPosition.length > 0 ? listPosition.map((item, i) => {

                                if(switchState.checked == true){

                                    if(item.member_id == null || item.member_id == '0'){

                                        return (
            
                                            <ListItem key={i}>  
            
                                                <Grid container>
                                                    <Grid item sm={10} style={{textAlign: 'left'}}>
                                                        <FormControlLabel
                                                            className={classes.checkboxStyle}
                                                            control={
                                                                <Checkbox 
                                                                    checked={isChecked.id == item.id ? true : false} 
                                                                    onChange={(e) => handleChangeCheckbox(e, item)}
                                                                    value={item} 
                                                                />
                                                            }
                                                            label = {
            
                                                                <Tooltip title={item.member_id == null ? "Available Position" : 'Posisi sudah di gunakan' } placement="right-start">
                                                                    <Badge
            
                                                                        color="primary"  
                                                                        variant='dot'  
                                                                        
                                                                    >
                                                                        <Typography variant='subtitle1' className={classes.title}>
                                                                            { item.structure_position_title_name + " "} 
                                                                            ({ item.code })
            
                                                                        </Typography>
                                                                    </Badge>
                                                                </Tooltip>
                                                            }
                                                        />
                                                    </Grid>
            
                                                    <Grid item sm={2} >
                                                        <Typography 
                                                            onClick={ (e) => handleDataDetail(e, item)}
                                                            variant='subtitle2' 
                                                            className={classes.title} 
                                                            style={{color: '#2194e1', marginTop: 8, cursor: 'pointer'}}    
                                                        > 
                                                            Detail
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            </ListItem>
            
                                        )
                                    }

                                } else {

                                    return (
            
                                        <ListItem key={i}>  
        
                                            <Grid container>
                                                <Grid item sm={10} style={{textAlign: 'left'}}>

                                                    <Typography variant='subtitle1' className={classes.title}>
                                                        { item.structure_position_title_name }

                                                    </Typography>
                                            
                                                </Grid>
        
                                                <Grid item sm={2} >
                                                    <Typography variant='subtitle2' className={classes.title} style={{color: '#2194e1', marginTop: 8, cursor: 'pointer'}}> 
                                                        Detail
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </ListItem>
        
                                    )
                                }

                            }
                            ) : (

                                <Grid container>
                                    <Grid item sm={12} style={{textAlign: 'center'}}> 
                                        <CircularProgress size={32} style={{color: '#cc0707'}} />
                                    </Grid>
                                </Grid>
                            )
                        }

                    </List>

                    <DialogContentText id="alert-dialog-description">
                    
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                    <Button 
                        onClick={() => setModalTambahPosition(false)} 
                        variant='outlined' 
                        size='small'
                        className={classes.buttonModalCancel}    
                    >
                        Batal
                    </Button>
                    
                    <Button 
                        // onClick={() => Redirect(ToCompletionProfile)}
                        onClick= {handleSimpanData}
                        variant='contained' 
                        color="primary" 
                        size='small'
                        className={classes.buttonModal}
                    >  
                        Simpan
                    </Button>
                </DialogActions>
                <br />
                <br />
            </Dialog>

            <DialogModalDetail 
                classes = { classes }
                isModalDetail = { isModalDetail }
                setModalDetail = { setModalDetail }
                dataDetail = { dataDetail }
            />
        </Fragment>

    )
};

export default DialogTambahPosisiForProfile;