import React, { Component, useEffect, useState, useCallback } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Snackbar, Fab
} from '@material-ui/core';

const Keahlian = props => {

    const { classes } = props;

    /*
        ````````````````````````````````````
        COMPONENT DID MOUNT

        ````````````````````````````````````
    */
    const [ listDataKeahlian, setListDataKeahlian ] = useState('');
        
    let dataList = [];
    
    const employeeDataKeahlian = localStorage.getItem('employee_data_keahlian');
    let employeeDataKeahlianAfterParse = employeeDataKeahlian !== null ? JSON.parse(employeeDataKeahlian) : [];

    useEffect(() => {

        console.log("employeeDataKeahlianAfterParse : ", employeeDataKeahlianAfterParse)

        setListDataKeahlian(employeeDataKeahlianAfterParse);

    },[])

    return (

        <Grid container>
            <Grid item xs={4} style={{textAlign : 'left'}}>

                <Paper elevation={1} className={classes.paperInfoIdentitas} square={true}> 
                    <br />
                    <Typography variant='subtitle2' className={classes.titleHeader}>
                        <b>Eksport ke PDF</b> 
                    </Typography>

                    <Button variant='outlined' size='large' className={classes.buttonEksportPDF}>
                        <b>Eksport</b>
                    </Button>
                </Paper>
            </Grid>



            <Grid item xs={8} style={{textAlign : 'left '}}>
                    {
                        listDataKeahlian.length > 0 && listDataKeahlian.map((item, i) => (
                            <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}}  key={i}> 
                                <br />
                                <Typography variant='subtitle1' className={classes.titleHeader}>
                                    <b>Keahlian - {i + 1}: </b> 
                                </Typography>

                                <br />

                                        <List className={classes.list}>
                                            <ListItem alignItems='flex-start'>  
                                                <ListItemText 
                                                    primary={
                                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                                            <b>Nama Keahlian</b>
                                                        </Typography>
                                                    } 
                                                />
                                
                                                <TextField  
                                                    id="outlined-bare"
                                                    value={item.name_keahlian}
                                                    className={classes.textField}
                                                    inputProps={{className: classes.title}} 
                                                    disabled
                                                    // onChange= {handleChangeNIP}
                                                    // placeholder={'NIP'}
                                                    // variant="outlined"
                                                />
                                            </ListItem>

                                            <ListItem alignItems='flex-start'>  
                                                <ListItemText 
                                                    primary={
                                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                                            <b>Level</b>
                                                        </Typography>
                                                    } 
                                                />
                                
                                                <TextField  
                                                    id="outlined-bare"
                                                    value={item.level !== '' ? item.level : '-'}
                                                    className={classes.textField}
                                                    inputProps={{className: classes.title}} 
                                                    disabled
                                                />
                                            </ListItem>

                                            <ListItem alignItems='flex-start'>  
                                                <ListItemText 
                                                    primary={
                                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                                            <b>Sertifikat</b>
                                                        </Typography>
                                                    } 
                                                />
                                
                                                <TextField  
                                                    id="outlined-bare"
                                                    value={item.file_name}
                                                    className={classes.textField}
                                                    inputProps={{className: classes.title}} 
                                                    disabled
                                                />
                                            </ListItem>
                                            
                                            {/* <hr style={{borderColor: '#c4c1c1',  }}/> */}

                                        </List>
                            </Paper>
                        ))
                    }

                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
            </Grid>
        </Grid>
    )
};

export default Keahlian;
