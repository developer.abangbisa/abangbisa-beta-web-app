import React, { Component, useEffect, useState, useCallback } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Snackbar, Fab
    
} from '@material-ui/core';

import RiwayatKesehatan from './RiwayatKesehatan';
import RiwayatPendidikanFormal from './RiwayatPendidikanFormal';
import RiwayatPendidikanINFORMAL from './RiwayatPendidikanINFORMAL';
import RiwayatOrganisasi from './RiwayatOrganisasi';
import RiwayatExperience from './RiwayatExperience';




const Riwayat = props => {

    const { classes } = props;

    /*
        ````````````````````````
        HANDLE TAB INFO PROFILE

        ````````````````````````
    */

    let informationRiwayatLet = {

        value: 'kesehatan',
        label: 'Kesehatan'
    };

    const [ isChoosed, setChoosed ] = useState(informationRiwayatLet);

    const [ kesehatan, setKesehatanDasar ] = useState(true);
    const [ pendidikanFormal, setPendidikanFormal ] = useState(false);
    const [ pendidikanInformal, setPendidikanInformal ] = useState(false);
    const [ organisasi, setOrganisasi] = useState(false);
    const [ pengalamanKerja, setPengalamanKerja ] = useState(false);

    const handleChooseTab = (item) => {

        console.log("Item : ", item);
        setChoosed(item);

        if(item.value == 'kesehatan'){

            setKesehatanDasar(true);
            setPendidikanFormal(false);
            setPendidikanInformal(false);
            setOrganisasi(false);
            setPengalamanKerja(false);
           
        };

        if(item.value == 'pendidikan-formal'){

            setKesehatanDasar(false);
            setPendidikanFormal(true);
            setPendidikanInformal(false);
            setOrganisasi(false);
            setPengalamanKerja(false);
           
        };

        if(item.value == 'pendidikan-informal'){

            setKesehatanDasar(false);
            setPendidikanFormal(false);
            setPendidikanInformal(true);
            setOrganisasi(false);
            setPengalamanKerja(false);
           
        };

        if(item.value == 'organisasi'){

            setKesehatanDasar(false);
            setPendidikanFormal(false);
            setPendidikanInformal(false);
            setOrganisasi(true);
            setPengalamanKerja(false);
           
        };

        if(item.value == 'pengalaman-kerja'){

            setKesehatanDasar(false);
            setPendidikanFormal(false);
            setPendidikanInformal(false);
            setOrganisasi(false);
            setPengalamanKerja(true);
           
        };
        
        
    
    };

    return (

        <Grid container>
            <Grid item xs={4} style={{textAlign : 'left'}}>

                <Paper elevation={1} className={classes.paperInfoIdentitas} square={true}> 
                    <br />
                    <Typography variant='subtitle2' className={classes.titleHeader}>
                        <b>Informasi Riwayat</b> 
                    </Typography>
                    <ul>
                        {
                            informationRiwayat.length > 0 && informationRiwayat.map((item, i) => (

                                <li 
                                    key={i}
                                    className={isChoosed.value == item.value ? classes.titleActive : classes.titleNotActive }  
                                    style={{margin: 8, cursor: 'pointer'}} 
                                    onClick={() => handleChooseTab(item)}
                                >

                                    <Typography 
                                        variant='subtitle2' 
                                        className={isChoosed.value == item.value ? classes.titleActive : classes.titleNotActive }
                                    >
                                        <b>{item.label}</b> 
                                    </Typography>
                                </li>
                            ))
                        }
                    </ul>   
                    <br />
                    <Typography variant='subtitle2' className={classes.titleHeader}>
                        <b>Eksport ke PDF</b> 
                    </Typography>
                    <Button variant='outlined' size='large' className={classes.buttonEksportPDF}>
                        <b>Eksport</b>
                    </Button>                 
                    <br />

                </Paper>
            </Grid>

            <Grid item xs={8} style={{textAlign : 'left '}}>

                {/* 
                    ``````````````````````````
                    CONTENT RIWAYAT KESEHATAN

                    ``````````````````````````

                */}
                {
                    kesehatan == true && (

                        <RiwayatKesehatan
                            classes={classes}

                        />
                    )
                }

                {
                    pendidikanFormal == true && (

                        <RiwayatPendidikanFormal 
                            classes={classes}

                        />
                    )
                }

                {
                    pendidikanInformal == true && (

                        <RiwayatPendidikanINFORMAL classes ={ classes } />
                    )
                }

                {
                    organisasi == true && (

                        <RiwayatOrganisasi classes={classes} />
                    )
                }

                {
                    pengalamanKerja == true && (

                        <RiwayatExperience classes={classes} />
                    )
                }

   


                <br />
                <br />
                <br />
            </Grid>
        </Grid>

    )

};

export default Riwayat;

const informationRiwayat = [

    {
        value: 'kesehatan',
        label: 'Kesehatan'
    },
    {
        value: 'pendidikan-formal',
        label: 'Pendidikan Formal'
    },
    {
        value: 'pendidikan-informal',
        label: 'Pendidikan Informal'
    },
    {
        value: 'organisasi',
        label: 'Organisasi'
    },
    {
        value: 'pengalaman-kerja',
        label: 'Pengalaman Kerja'
    }
    
];