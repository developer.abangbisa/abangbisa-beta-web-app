import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

const RiwayatOrganisasi = props => {

    const { classes } = props;

        /*
        ````````````````````````````````````
        COMPONENT DID MOUNT

        ````````````````````````````````````
    */
   const [ listDataRiwayatOrganisasi, setListDataRiwayatOrganisasi ] = useState('');
        
   const employeeDataRiwayatORG = localStorage.getItem('employee_data_riwayat_org');
   let employeeDataRiwayatORGAfterParse = employeeDataRiwayatORG !== null ? JSON.parse(employeeDataRiwayatORG) : [];

   useEffect(() => {

       console.log("employeeDataRiwayatORGAfterParse : ", employeeDataRiwayatORGAfterParse)

       setListDataRiwayatOrganisasi(employeeDataRiwayatORGAfterParse);

   },[])

    return (
        <div>
            {
                listDataRiwayatOrganisasi.length > 0 && listDataRiwayatOrganisasi.map((item, i) => (
                    <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}} key={i}> 
                        <br />
                        <Typography variant='subtitle1' className={classes.titleHeader}>
                            <b>Organisasi - {i + 1}</b> 
                        </Typography> 
                        <br />

                        <List className={classes.list} >
                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Nama Organisasi</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.organisasi}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                    // onChange= {handleChangeNIP}
                                    // placeholder={'NIP'}
                                    // variant="outlined"
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Nama Jabatan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.name_jabatan}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Kota</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.kota}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tahun Masuk</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.tahun_masuk}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tahun Selesai</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.tahun_selesai}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>


                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Keterangan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.keterangan}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>


                        </List>
                    </Paper>
                ))
            }

            <br />
            <br />
            <br />
            <br />
        </div>
    )
};

export default RiwayatOrganisasi;