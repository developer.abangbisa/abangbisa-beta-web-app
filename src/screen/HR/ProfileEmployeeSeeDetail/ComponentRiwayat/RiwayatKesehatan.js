import React, { Component, useEffect, useState, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

const RiwayatKesehatan = props => {

    const { classes } = props;

    const [listRiwayatKesehatan, setListRiwayatKesehatan ] = useState([]);

    return (

        <Fragment>
            <br />
            
                {
                    listRiwayatKesehatan.length == 0 && (
        
                        <Typography 
                            variant='subtitle2' 
                            className={classes.titleTambahAnggotaKeluarga}
                            // onClick={handleTambahAnggotaKeluarga}
                        >
                            <b>+ Tambah Riwayat Kesehatan</b> 
                        </Typography>
                    )
                }

                {
                    listRiwayatKesehatan.length > 0 && (

                        <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16, marginBottom: 16, color: 'grey'}}> 
                            <br />
                            
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                        </Paper>
                    )
                }

        </Fragment>

    )
};

export default RiwayatKesehatan;
