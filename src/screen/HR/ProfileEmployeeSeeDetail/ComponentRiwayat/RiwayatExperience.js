import React, { Component, useEffect, useState, Fragment } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

const RiwayatExperience = props => {

    const { classes } = props;

        /*
        ````````````````````````````````````
        COMPONENT DID MOUNT

        ````````````````````````````````````
    */
    const [ listDataRiwayatPengalaman, setListDataRiwayatPengalaman ] = useState('');
            
    const employeeDataRiwayatExperience = localStorage.getItem('employee_data_riwayat_experience');
    let employeeDataRiwayatExperienceAfterParse = employeeDataRiwayatExperience !== null ? JSON.parse(employeeDataRiwayatExperience) : [];

    useEffect(() => {

        console.log("employeeDataRiwayatExperienceAfterParse : ", employeeDataRiwayatExperienceAfterParse)

        setListDataRiwayatPengalaman(employeeDataRiwayatExperienceAfterParse);

    },[])

    return(
        
        <Fragment>
            <br />
            {
                listDataRiwayatPengalaman.length == 0 && (

                    <Typography 
                        variant='subtitle2' 
                        className={classes.titleTambahAnggotaKeluarga}
                        // onClick={handleTambahAnggotaKeluarga}
                    >
                        <b>+ Tambah Riwayat Pengalaman Kerja</b> 
                    </Typography>
                )
            }
            {
                listDataRiwayatPengalaman.length > 0 && listDataRiwayatPengalaman.map((item, i) => (
                    <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}} key={i}> 
                        <br />
                        <Typography variant='subtitle1' className={classes.titleHeader}>
                            <b>Pengalaman kerja - {i + 1}</b> 
                        </Typography> 
                        <br />

                        <List className={classes.list} >
                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Nama Jabatan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.nama_jabatan}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                    // onChange= {handleChangeNIP}
                                    // placeholder={'NIP'}
                                    // variant="outlined"
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Nama Perusahaan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.nama_perusahaan}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Bulan Masuk</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.bulan_masuk}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tahun Masuk</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.tahun_masuk}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Bulan Keluar</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.bulan_keluar}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Tahun Keluar</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.tahun_keluar}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Alamat Perusahaan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.alamat_perusahaan}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Penghargaan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.penghargaan}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>No. Telp Perusahaan</b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.no_telp_perusahaan}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Alasan Berhenti  </b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.alasan_berhenti}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>

                            <ListItem alignItems='flex-start'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                            <b>Surat Rekomendasi  </b>
                                        </Typography>
                                    } 
                                />
                
                                <TextField  
                                    id="outlined-bare"
                                    value={item.name_file}
                                    className={classes.textField}
                                    inputProps={{className: classes.title}} 
                                    disabled
                                />
                            </ListItem>
                        </List>
                    </Paper>
                ))
            }
            <br />
            <br />
            <br />
            <br />
        </Fragment>
    )
};

export default RiwayatExperience;