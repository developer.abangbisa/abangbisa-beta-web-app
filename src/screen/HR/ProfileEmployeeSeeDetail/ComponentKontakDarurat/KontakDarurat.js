import React, { Component, useEffect, useState, useCallback } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Snackbar, Fab
} from '@material-ui/core';

import DialogEditUrgentContacts from '../../KontakDarurat/Components/DialogEditUrgentContacts';

const RiwayatKontakDarurat = props => {

    const { classes } = props;

    /*
        
        ````````````````````````````````````
        COMPONENT DID MOUNT

        ````````````````````````````````````
    */
    const [ listDataKontakDarurat, setListDataKontakDarurat ] = useState('');
    
    const employeeDataKontakDarurat = localStorage.getItem('employee_data_kontak_darurat');
    let employeeDataKontakDaruratAfterParse = employeeDataKontakDarurat !== null ? JSON.parse(employeeDataKontakDarurat) : [];

    useEffect(() => {

        console.log("employeeDataKontakDaruratAfterParse : ", employeeDataKontakDaruratAfterParse)

        setListDataKontakDarurat(employeeDataKontakDaruratAfterParse);

    },[]);


    /*

        ```````````````````
        HANDLE DIALOG EDIT 

        ```````````````````
    */
//    handleChangeHubunganKeluarga

    const [ nameAnggotaKeluarga, setNameAnggotaKeluarga ] = useState('');  
            
    const handleChangeNamaAnggotaKeluarga = (e) => setNameAnggotaKeluarga(e.target.value);

    // const [ hubunganKeluarga, setHubunganKeluarga ] = useState('');
    // const handleChangeHubunganKeluarga = (e) => setHubunganKeluarga(e.target.value);

    const [ hubunganKeluarga, setHubunganKeluarga ] = useState([]);
   
    const handleChangeHubunganKeluarga = name => e => {
 
        setHubunganKeluarga({ ...hubunganKeluarga, [name]: e.target.value });
 
    };
 



    const [nomorHandphone, setNomorHandphone ] = useState('');  
    const handleChangeNoHandphone = (e) => setNomorHandphone(e.target.value);

    const [ modalEditAnggotaKeluarga, setModalEditAnggotaKeluarga ] = useState(false);
    const [ dataDetailEdit, setDataDetailEdit ] = useState('');

    const handleEditDataDetailAnggotaKeluarga = (e, data) => {

        e.preventDefault();
        setModalEditAnggotaKeluarga(true);
        
        console.log("Data edit detail IN PROFILE : ", data);

        setDataDetailEdit(data);

    };

    return (
        <Grid container>
            <Grid item xs={4} style={{textAlign : 'left'}}>
                <Paper elevation={1} className={classes.paperInfoIdentitas} square={true}> 
                    <br />
                    <Typography variant='subtitle2' className={classes.titleHeader}>
                        <b>Eksport ke PDF</b> 
                    </Typography>

                    <Button variant='outlined' size='large' className={classes.buttonEksportPDF}>
                        <b>Eksport</b>
                    </Button>
                </Paper>
            </Grid>

            <Grid item xs={8} style={{textAlign : 'left '}}>

                <br />
                {
                    listDataKontakDarurat.length == 0 && (

                        <Typography 
                            variant='subtitle2' 
                            className={classes.titleTambahAnggotaKeluarga}
                            // onClick={handleTambahAnggotaKeluarga}
                        >
                            <b>+ Tambah Kontak Darurat</b> 
                        </Typography>
                    )
                }

                {
                    listDataKontakDarurat.length > 0 && listDataKontakDarurat.map((item, i) => (
                        <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}} key={i}> 
                            <br />

                            {/* 
                                <Typography variant='subtitle1' className={classes.titleHeader}>
                                    <b>Keluarga : </b> 
                                </Typography> 
                                <br />
                            */}

                            <Grid container>
                                <Grid item sm={12} style={{textAlign : 'right'}}>
                                    <IconButton 
                                        onClick={(e) => handleEditDataDetailAnggotaKeluarga(e, item)}
                                        className={classes.iconEdit}
                                    >
                                        <i className='material-icons'>edit</i>
                                    </IconButton>
                                </Grid>
                            </Grid>

                            <List className={classes.list} >
                                <ListItem alignItems='flex-start'>  
                                    <ListItemText 
                                        primary={
                                            <Typography variant='subtitle1' className={classes.titleForm}>
                                                <b>Nama</b>
                                            </Typography>
                                        } 
                                    />
                    
                                    <TextField  
                                        id="outlined-bare"
                                        value={item.name_anggota_keluarga}
                                        className={classes.textField}
                                        inputProps={{className: classes.title}} 
                                        disabled
                                        // onChange= {handleChangeNIP}
                                        // placeholder={'NIP'}
                                        // variant="outlined"
                                    />
                                </ListItem>

                                <ListItem alignItems='flex-start'>  
                                    <ListItemText 
                                        primary={
                                            <Typography variant='subtitle1' className={classes.titleForm}>
                                                <b>Hubungan</b>
                                            </Typography>
                                        } 
                                    />
                    
                                    <TextField  
                                        id="outlined-bare"
                                        value={item !== undefined ? item.hubungan_keluarga.label : ''}
                                        className={classes.textField}
                                        inputProps={{className: classes.title}} 
                                        disabled
                                    />
                                </ListItem>
                                
                                <ListItem alignItems='flex-start'>  
                                    <ListItemText 
                                        primary={
                                            <Typography variant='subtitle1' className={classes.titleForm}>
                                                <b>No Telepon</b>
                                            </Typography>
                                        } 
                                    />
                    
                                    <TextField  
                                        id="outlined-bare"
                                        value={item.no_hp}
                                        className={classes.textField}
                                        inputProps={{className: classes.title}} 
                                        disabled
                                    />
                                </ListItem>
                                

                            </List>
                        </Paper>
                    ))
                }
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </Grid>

            <DialogEditUrgentContacts 
                classes={classes}
                setModalEditAnggotaKeluarga = {setModalEditAnggotaKeluarga}
                modalEditAnggotaKeluarga = {modalEditAnggotaKeluarga}
                dataDetailEdit = { dataDetailEdit}

                handleChangeHubunganKeluarga = {handleChangeHubunganKeluarga}
                handleChangeNoHandphone = {handleChangeNoHandphone}
                handleChangeNamaAnggotaKeluarga = {handleChangeNamaAnggotaKeluarga}

                nameAnggotaKeluarga={nameAnggotaKeluarga}
                hubunganKeluarga = {hubunganKeluarga}
                nomorHandphone = {nomorHandphone}   
                
                // handleSimpanDataAnggotaKeluarga = {handleSimpanDataAnggotaKeluarga}
            />
        </Grid>
    )

};

export default RiwayatKontakDarurat;

