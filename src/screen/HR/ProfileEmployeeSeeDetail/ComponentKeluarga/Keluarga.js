import React, { Component, useEffect, useState, useCallback, Fragment } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Snackbar, Fab
} from '@material-ui/core';

import moment from 'moment';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

import DialogTambahAnggotaKeluargaForProfile from '../../Keluarga/Components/DialogTambahAnggotaKeluargaForProfile';
import DialogEditAnggotaKeluargaForProfile from '../../Keluarga/Components/DialogEditAnggotaKeluargaForProfile';


const Keluarga = props => {

    const { classes } = props;

    /* 
        ````````````````````
        COMPONENT DID MOUNT
        
        ````````````````````
    */

    let dataList = [];

    const [ listDataKeluarga, setListDataKeluarga ] = useState('');
    const dataKeluarga = localStorage.getItem('employee_data_keluarga');
    const dataKeluargaAfterParse = JSON.parse(dataKeluarga);

    const userToken = localStorage.getItem('userToken');
    const employeeId = localStorage.getItem('employee_id');

    const [ userTokenState, setUserTokenState ] = useState('');
    const [ employeeIdState, setEmployeeIdState ] = useState('');

    // const [ listAnggotaKeluarga, setListAnggotaKeluarga ] = useState([])
    const [ subIdFamily, setSubIdFamily ] = useState();

    useEffect(() => {

        setUserTokenState(userToken);
        setEmployeeIdState(employeeId);

        const header =  {     
            'Accept': "application/json",
            'Content-Type' : "application/json",
            'Authorization' : "bearer " + userToken,
        };

        axios.defaults.headers.common = header;    
            
        axios
            .get(URL_API + `/human-resource/employee/${employeeId}/family`)
            .then(function(response){
                
                console.log("Response Original tp get sub-id: ", response);

                if(response.status == 200){

                    setListDataKeluarga(response.data.data);
                 
                };
                
            })
            .catch(function(error){
                
                console.log("Error : ", error.response)
                
            });
    }, []);


    /*
        ````````````````````````````````````
        HANDLE MODAL UPDATE
        
        ````````````````````````````````````
    */

    const [ modalEditAnggotaKeluarga, setModalEditAnggotaKeluarga ] = useState(false);
    const [ dataDetailEdit, setDataDetailEdit ] = useState('');

    const handleEditDataDetail = (e, data) => {

        e.preventDefault();
        setModalEditAnggotaKeluarga(true);
        
            //    console.log("Data edit detail  : ", data);

        setDataDetailEdit(data);

    };

    /*
        ```````````
        HANDLE FORM 

        ```````````
    */
    const [ nameAnggotaKeluarga, setNameAnggotaKeluarga ] = useState('');  

    const handleChangeNamaAnggotaKeluarga = (e) => setNameAnggotaKeluarga(e.target.value);
    
    const [ hubunganKeluarga, setHubunganKeluarga ] = useState(
        {
            
            name : ''
        }
    );
    
    const handleChangeHubunganKeluarga = name => e => {

        setHubunganKeluarga({ ...hubunganKeluarga, [name]: e.target.value });

    };

    const [tanggalLahir, setTanggalLahir ] = useState('');  
    const handleChangeTanggalLahir = (e) => setTanggalLahir(e.target.value);

    const handleSimpanDataAnggotaKeluarga = () => {

        setModalTambahAnggotaKeluarga(false);

        let data = {
            
            EmployeeFamily : {

                name: nameAnggotaKeluarga,
                family_relationship_type_id : hubunganKeluarga.name.id,
                date_of_birth: tanggalLahir
                
            }
        };

        console.log("Data : ", data);


        if(userTokenState !== undefined  ){

            const header =  {  

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState

            };

            axios.defaults.headers.common = header;    
            
            axios
                .post(URL_API + `/human-resource/employee/${employeeIdState}/family`, data)
                .then(function(response){

                    console.log("Response Original : ", response);

                    if(response.status == 200 ){    

                        setListDataKeluarga([...listDataKeluarga, response.data.data]);
                    };
                })
                .catch(function(error){

                    alert('Whoops, something went wrong !')
                    console.log("Error : ",  error.response);

                })
        
        } else { alert("No Access Token available!")};

    };

    /*
        ````````````````````````````````````
        HANDLE MODAL TAMBAH, DELETE, 
        
        ````````````````````````````````````
    */
    const [ modalTambahAnggotaKeluarga, setModalTambahAnggotaKeluarga ] = useState(false);

    const [ modalDeleteAnggotaKeluarga, setModalDeleteAnggotaKeluarga ] = useState(false); 

    const handleTambahAnggotaKeluarga = () => {


        setNameAnggotaKeluarga('');
        setHubunganKeluarga({
            name: ''
        });

        setTanggalLahir('');

        setModalTambahAnggotaKeluarga(true);

    };

    return (

        <Grid container>
            <Grid item xs={4} style={{textAlign : 'left'}}>

                <Paper elevation={1} className={classes.paperInfoIdentitas} square={true}> 
                    <br />
                    <Typography variant='subtitle2' className={classes.titleHeader}>
                        <b>Eksport ke PDF</b> 
                    </Typography>

                    <Button variant='outlined' size='large' className={classes.buttonEksportPDF}>
                        <b>Eksport</b>
                    </Button>
                </Paper>
            </Grid>

            <Grid item xs={8} style={{textAlign : 'left '}}>

                <br />

                {/* {
                    listDataKeluarga.length == 0 && (

                        <Typography 
                            variant='subtitle2' 
                            className={classes.titleTambahAnggotaKeluarga}
                            onClick={handleTambahAnggotaKeluarga}
                        >
                            <b>+ Tambah Anggota Keluarga</b> 
                        </Typography>
                    )
                } */}

                    <Typography 
                        variant='subtitle2' 
                        className={classes.titleTambahAnggotaKeluarga}
                        onClick={handleTambahAnggotaKeluarga}
                    >
                        <b>+ Tambah Anggota Keluarga</b> 
                    </Typography>

                    {
                        listDataKeluarga.length > 0 ? listDataKeluarga.map((item, i) => (

                            <Fragment>
                                 
                                <Paper elevation={1} className={classes.paperInfoDasar} square={true} style={{marginRight: 16}} key={i}> 
                                    <br />

                                    {console.log('item : ', item)}
                                
                                        <Grid container>
                                            <Grid item sm={12} style={{textAlign : 'right'}}>
                                                <IconButton 
                                                    onClick={(e) => handleEditDataDetail(e, item)}
                                                    className={classes.iconEdit}
                                                >
                                                    <i className='material-icons'>edit</i>
                                                </IconButton>
                                            </Grid>
                                        </Grid>

                                        <List className={classes.list} >
                                            <ListItem alignItems='flex-start'>  
                                                <ListItemText 
                                                    primary={
                                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                                            <b>Nama</b>
                                                        </Typography>
                                                    } 
                                                />
                                
                                                <TextField  
                                                    id="outlined-bare"
                                                    value={item.name}
                                                    className={classes.textField}
                                                    inputProps={{className: classes.title}} 
                                                    disabled
                                                    // onChange= {handleChangeNIP}
                                                    // placeholder={'NIP'}
                                                    // variant="outlined"
                                                />
                                            </ListItem>

                                            <ListItem alignItems='flex-start'>  
                                                <ListItemText 
                                                    primary={
                                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                                            <b>Hubungan</b>
                                                        </Typography>
                                                    } 
                                                />
                                
                                                <TextField  
                                                    id="outlined-bare"
                                                    // value={item !== null ? item.hubungan_keluarga.value : ''}
                                                    value={item.familyRelationshipType !== null ? item.familyRelationshipType.name : '-'}
                                                    className={classes.textField}
                                                    inputProps={{className: classes.title}} 
                                                    disabled
                                                />
                                            </ListItem>

                                            <ListItem alignItems='flex-start'>  
                                                <ListItemText 
                                                    primary={
                                                        <Typography variant='subtitle1' className={classes.titleForm}>
                                                            <b>Tanggal Lahir</b>
                                                        </Typography>
                                                    } 
                                                />
                                
                                                <TextField  
                                                    id="outlined-bare"
                                                    value={moment(item.tanggal_lahir).format('DD MMMM YYYY')}
                                                    className={classes.textField}
                                                    inputProps={{className: classes.title}} 
                                                    disabled
                                                />
                                            </ListItem>

                                        </List>
                                </Paper>
                            </Fragment>

                        )) : (

                            <Grid container>
                                <Grid item sm={12} style={{textAlign: 'center'}}> 
                                    {/* <CircularProgress size={32} style={{color: '#cc0707'}} /> */}
                                    <Typography variant='subtitle2' className={classes.titleHeader} style={{color: 'grey'}}>
                                        Anda belum menambahkan Anggota keluarga !
                                    </Typography>
                                </Grid>
                            </Grid>
                        )
                        
                    }

                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />

                    
            </Grid>

            <DialogEditAnggotaKeluargaForProfile 
                classes = {classes}
                modalEditAnggotaKeluarga = {modalEditAnggotaKeluarga}
                setModalEditAnggotaKeluarga = { setModalEditAnggotaKeluarga} 
                dataDetailEdit = {dataDetailEdit}
                
                handleChangeNamaAnggotaKeluarga = {handleChangeNamaAnggotaKeluarga}
                handleChangeHubunganKeluarga = {handleChangeHubunganKeluarga}
                handleChangeTanggalLahir = { handleChangeTanggalLahir }

                hubunganKeluarga = {hubunganKeluarga}
                nameAnggotaKeluarga = { nameAnggotaKeluarga}
                tanggalLahir = {tanggalLahir}
            />

            <DialogTambahAnggotaKeluargaForProfile 
                classes = {classes}
                modalTambahAnggotaKeluarga = {modalTambahAnggotaKeluarga}
                setModalTambahAnggotaKeluarga = {setModalTambahAnggotaKeluarga}
                handleChangeNamaAnggotaKeluarga = {handleChangeNamaAnggotaKeluarga}
                nameAnggotaKeluarga = {nameAnggotaKeluarga}
                hubunganKeluarga = {hubunganKeluarga}
                handleChangeHubunganKeluarga = {handleChangeHubunganKeluarga}
                
                handleChangeTanggalLahir = { handleChangeTanggalLahir }
                tanggalLahir = { tanggalLahir }
                handleSimpanDataAnggotaKeluarga = {handleSimpanDataAnggotaKeluarga}
            />

        </Grid>
    )

};

export default Keluarga;



