

/*

        Theme CSS IN JS for Dashboard 

*/

import { createMuiTheme } from "@material-ui/core/styles";
import { cyan, lightBlue, lightGreen, grey, red, green} from "@material-ui/core/colors";

export default createMuiTheme({

  
  palette: {
    primary: red, //linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)
    secondary: grey,
    background : {
      paper:  grey[500]

    }
  },

  spacing : {
    unit: 7
  },
  overrides: {
    MuiAppBar:{
      root:{
        backgroundColor: 'cyan'
      }
    },
    MuiToolbar:{
      root:{
        backgroundColor: 'white'
      }
    },
    MuiButton: {
      
      root: {  

        marginTop: 30,
        marginRight: 40,
        width: '122.2px',
        height: '40px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
      },
      text: {

        color: 'white'
      },
      textSecondary: {

        color: 'white',
        fontFamily: 'Nunito'
      },
      // textOutlined: {
      //   color: 'white',
      //   fontFamily: 'Nunito'
      // },
    },
    MuiDrawer: {

      root: {
        backgroundColor: lightBlue
      },
      paper: {
        backgroundColor: green
      },
      paperAnchorLeft: {

      }
    },
    MuiListItemIcon: {
      root: {
        color: 'white'
      }
    },

    MuiListItemText: {
      root: {
        color: 'cyan', // Not work 
        
      },
   
    },

    MuiBadge:{
      root: {
        color: '#d1354a',
        // backgroundColor: 'red'  
        // background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
      }
    }



  
  }
});