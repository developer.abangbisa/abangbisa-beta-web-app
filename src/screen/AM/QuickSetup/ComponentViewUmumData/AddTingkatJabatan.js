import React,{useState, useEffect, useContext} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
// import { Button} from 'react-bootstrap'; 

import { URL_API } from '../../../../constants/config-api';
import { useGetHttp_v2 } from '../Hook/useGetHttp_v2'
import ModalInputTingkatJabatan from '../ComponentViewDefineStructureDefault/ModalInputTingkatJabatan'

const AddTingkatJabatan = props => {

    const { 
        classes,
        listTingkatJabatan,
        setListTingkatJabatan
    } = props;

    const [isChooseButtonPlus, setChooseButtonPlus ] = useState(false)
    const handleButtonPlusTingkatJabatan = () => setChooseButtonPlus(true);

    /*

        ``````````````````````````
        GET LABEL TINGKAT JABATAN

        ``````````````````````````

    */

    const [isLoading_v2, dataLabelTingkatJabatan] = useGetHttp_v2(URL_API + '/human-resource/master-structure-position-level/create', [isChooseButtonPlus]);
  
    const insiateLabelTingkatJabatan = {
        prev_id: '',
        next_id: ''
    };

    const [label, setLabel] = useState(insiateLabelTingkatJabatan);

    useEffect(() => {

        if(isChooseButtonPlus == true){
            // console.log(" dataLabelTingkatJabatan", dataLabelTingkatJabatan);

            if(dataLabelTingkatJabatan !== null){
                if(dataLabelTingkatJabatan.fields !== null){
                    if(dataLabelTingkatJabatan.fields.prev_id !== null){
    
                        setLabel({  
                            prev_id: dataLabelTingkatJabatan.fields.prev_id.value,
                            next_id:  dataLabelTingkatJabatan.fields.next_id.value,
                        })
                    }
                }
            };
        }
    }, [isChooseButtonPlus])

    return(
        <div>
            <Button     
                variant='contained' 
                onClick={handleButtonPlusTingkatJabatan}
                style={{position:'absolute'}}
                className={classes.buttonPlusAddTingkatJabatan}
                size='small'
            >
                <span style={{padding: 3, fontSize:'17px'}}> + </span>
            </Button>    
    
            <ModalInputTingkatJabatan 
                setChooseButtonPlus={setChooseButtonPlus}
                isChooseButtonPlus={isChooseButtonPlus}
                setListTingkatJabatan={setListTingkatJabatan}
                listTingkatJabatan={listTingkatJabatan}   
                label={label}
                classes={classes}
            />
        </div>
    )
};

export default AddTingkatJabatan;