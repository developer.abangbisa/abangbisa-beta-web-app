import React,{Component, useState, useEffect} from 'react';
import { 
    Grid, CardHeader, withStyles, CardContent, Card, CircularProgress, Typography, Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, 
    List, ListItem, ListItemIcon, ListItemText, Radio, ListItemAvatar, Avatar
} from '@material-ui/core';
import axios from 'axios';

// import ImageUmum from '../ComponentViewUnitStructure/ImageUmum';
// import ImageMenyesuaikan from '../ComponentViewUnitStructure/ImageMenyesuaikan';
import { Container, Row, Col,  Modal, ModalFooter} from 'react-bootstrap';
// import Button from '../../../../components/Button';
import GearPicture from '../../../../assets/images/Group_1214.png';
import ObengPicture from '../../../../assets/images/Group_1215.png';
import PictInfo from '../../../../assets/images/icon-info-24px.svg';
import ModalInputTingkatJabatanMenyesuaikan from '../ComponentViewDefineStructureDefault/ModalInputTingkatJabatan';
import { URL_API } from '../../../../constants/config-api';
import { useGetHttp_v2 } from '../Hook/useGetHttp_v2'

const ModalComponentTingkatJabatan = props => {

    const { classes } = props;

    const [isModalRadioButton, setIsModalRadioButton ] = useState(false);
    const [ isChooseUnitUmum, setIsChooseUnitUmum ] = useState(false);
    const [ isChooseUnitMenyesuaikan, setIsChooseUnitMenyesuaikan] = useState(false);
    const [selectedOption, setSelectedOption ] = useState("");

    const [isResponse422, setIsResponse422 ] = useState(false);

    const [ isChooseButtonPlus, setChooseButtonPlus ] = useState(false);
     
    function openModalRadioButton (){

        setIsModalRadioButton(true)
    };

    function closeModalRadioButton (){
        
        setIsModalRadioButton(false)
    };

    function closeModalResponse422(){

        setIsResponse422(false);
    };

    function handleRadioChange(e){
        // console.log("Data : ", e.target.value);
        setSelectedOption(e.target.value);
        
    };

    function chooseOption (e) {
        e.preventDefault();
        const userToken = localStorage.getItem('userToken');

        console.log("userToken : ", userToken);

        console.log("selectedOption : ", selectedOption)

        if(selectedOption == "umum"){

            const header =  {
    
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : 'bearer ' + userToken
             
            };
    
            axios.defaults.headers.common = header;
            axios
                // .post(URL_API + `/human-resource/structure-wizard/quick-fill/typical`)
                .post(URL_API + `/human-resource/structure-wizard/quick-fill/master-structure-position-level/typical`)
                .then((response) => {
                    
                    console.log("Response Original : ", response)
                    // setIsChooseUnitUmum(true);
                    window.location.reload();
                })
                .catch((error) => {
                    console.log("Error : ", error.response)
                    if(error.response.status == 422){
                        setIsModalRadioButton(false);
                        setIsResponse422(true);

                    };
                })
        };

        if(selectedOption == "menyesuaikan"){

            console.log("Menyesuaikan !!!")
            setIsChooseUnitMenyesuaikan(true);
            setChooseButtonPlus(true);
            setIsModalRadioButton(false);
        };
    };


     /*

        ``````````````````````````
        GET LABEL TINGKAT JABATAN

        ``````````````````````````

    */

   const [isLoading_v2, dataLabelTingkatJabatan] = useGetHttp_v2(URL_API + '/human-resource/master-structure-position-level/create', [isChooseButtonPlus]);
    console.log("dataLabelTingkatJabatan : ", dataLabelTingkatJabatan);

   const insiateLabelTingkatJabatan = {
       prev_id: '',
       next_id: ''
   };

   const [label, setLabel] = useState(insiateLabelTingkatJabatan);

   useEffect(() => {

       if(isChooseButtonPlus == true){
           // console.log(" dataLabelTingkatJabatan", dataLabelTingkatJabatan);

           if(dataLabelTingkatJabatan !== null){
               if(dataLabelTingkatJabatan.fields !== null){
                   if(dataLabelTingkatJabatan.fields.prev_id !== null){
   
                       setLabel({  
                           prev_id: dataLabelTingkatJabatan.fields.prev_id.value,
                           next_id:  dataLabelTingkatJabatan.fields.next_id.value,
                       })
                   }
               }
           };
       }
   }, [isChooseButtonPlus])
        
    if(isChooseUnitMenyesuaikan == true){

        console.log("User hendak sesuaikan nama Tingkat Jabatan!");
        
        return (
            // <ModalChooseMenyesuaikan />
            <ModalInputTingkatJabatanMenyesuaikan 
                // isChooseButtonPlus ={isChooseUnitMenyesuaikan} // true
                // setChooseButtonPlus= { setChooseButtonPlus }

                setChooseButtonPlus={setChooseButtonPlus}
                isChooseButtonPlus={isChooseButtonPlus}
                setListTingkatJabatan={props.setListTingkatJabatan}
                listTingkatJabatan={props.listTingkatJabatan}   
                label={label}
                classes={classes}
            />
        );
    };

    return (

        <div>
             <Row className="justify-content-md-center marginTop">
                <Button
                    variant='contained' 
                    className={classes.button}
                    onClick={() => openModalRadioButton()}
                    // fullWidth
                    style={{width: 122}}
                >
                    Mulai
                </Button>
            </Row>

            <Dialog
                open={isModalRadioButton}
                onClose={closeModalRadioButton}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"            
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                    <Typography variant='h6' className={classes.title}>
                    
                    </Typography>
                    {/* <hr/> */}
                </DialogTitle>
                <DialogContent style={{textAlign: "center"}}>

                    <List>
                        <ListItem 
                            role={undefined} 
                            dense 
                            button 
                            // onClick={handleToggle(value)}
                        >
                            <ListItemIcon>
                                <Radio
                                    checked={selectedOption === 'umum'}
                                    value="umum"
                                    onChange={handleRadioChange}
                                    name="radio-button-demo"
                                    inputProps={{ 'aria-label': 'Umum' }}
                                />    
                            </ListItemIcon>

                            <ListItemAvatar>
                                <Avatar alt="Remy Sharp" src={GearPicture} />
                            </ListItemAvatar>
                                {/* <img src={GearPicture} alt="Gear Picture" /> */}
                            <ListItemText 
                                // inset 
                                className={classes.listItemText}
                                primary={
                                    <Typography variant='h6' className={classes.title}>
                                        <b>Umum</b>
                                    </Typography>
                                } 
                                secondary={
                                    <Typography variant='subtitle1' className={classes.title}>
                                        Atur dengan pengaturan standar kami !
                                    </Typography>
                                } 
                            />
                        </ListItem>
                        
                        <br />
                        <ListItem 
                            role={undefined} 
                            dense 
                            button 
                            // onClick={handleToggle(value)}
                        >
                            <ListItemIcon>
                                <Radio
                                    checked={selectedOption === 'menyesuaikan'}
                                    value="menyesuaikan"
                                    onChange={handleRadioChange}
                                    name="radio-button-demo"
                                    inputProps={{ 'aria-label': 'Menyesuaikan' }}
                                />    
                            </ListItemIcon>
                            <img src={ObengPicture} alt="Obeng Picture" />
                            <ListItemText 
                                // inset 
                                className={classes.listItemText}
                                primary={
                                    <Typography variant='h6' className={classes.title}>
                                        <b>Menyesuaikan</b>
                                    </Typography>
                                } 
                                secondary={
                                    <Typography variant='subtitle1' className={classes.title}>
                                        Susun pengaturan struktur Anda sendiri  !
                                    </Typography>
                                } 
                            />
                        </ListItem>
                    </List>

                
                    <DialogContentText id="alert-dialog-description">
                        <Typography variant='subtitle1' className={classes.titleModal}>
                            
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                    <Button 
                        onClick={(e) => chooseOption(e)}
                        variant='contained' 
                        className={classes.buttonModalPictOptions}
                        // fullWidth
                    >  
                        Lanjut
                    </Button>
                </DialogActions>
                <br />
            </Dialog>

            <Modal show={isResponse422} onHide={closeModalResponse422} centered >
                <div className='text-center' style={{padding:'30px'}}>
                    <img src={PictInfo} className="App-icon-80" alt="info-icon" />                
                    <br/>
                    <br />
                    <h6>Anda sudah pernah melakukan pengaturan Standard !</h6>
                    <br />
                    <Button title='Ok, mengerti!' handleClick={(e) => closeModalResponse422(e)} />  
                </div>
            </Modal>
        </div>
    )
};

export default ModalComponentTingkatJabatan;
// export default withStyles(styles)(ModalComponentTingkatJabatan);
