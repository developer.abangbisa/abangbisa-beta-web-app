import React,{useState, useEffect, useContext} from 'react';
import {useGetHttp} from '../Hook/useGetHttp';
import {URL_API} from '../../../../constants/config-api';


import { Typography } from '@material-ui/core';
import { Button} from 'react-bootstrap'; 
import TingkatJabatanItem from './TingkatJabatanItem';

import {    
            Grid, 
            CardHeader, 
            withStyles, 
            CardContent, 
            CircularProgress, 
            Chip, 
            Paper, 
            Card, 
            CardActions,
            IconButton,
            Menu, 
            MenuItem,
            List,
            ListItem,
            ListItemText,
            ListItemAvatar,
            Avatar
        
        } from '@material-ui/core';

const styles = {

    card: {
        // minWidth: 100,
        width: 240,
        marginTop: 3,
        paddingTop: 0,
        paddingBottom: 0,
        marginLeft:7

    },

    cardContent: {
        textAlign: 'center',
        marginTop: 0,
        paddingTop:0, 
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0 
    },

    root: {
       
    },

    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

    },
    actions: {
        display: 'flex',
        direction:'row-reverse'
    },

    fontListItemText : {
        fontFamily: 'Nunito',
        fontSize:'1rem',
        fontWeight: 'bold',
        lineHeight: '1.76',
        letterSpacing: 'normal'
    },

    
};

// const TingkatJabatanList = props => {
const TingkatJabatanList = React.memo(function TingkatJabatanList(props) {

    // console.log("Props Jabatan List : ", props);

    const { 
            classes, 
            // listTingkatJabatan,
            // setListTingkatJabatan
        } = props;

    const [isLoading_v3, dataRealTingkatJabatan] = useGetHttp(URL_API + '/human-resource/master-structure-position-level', []);

    useEffect(() => {
        
        if(dataRealTingkatJabatan !== null){
            const newData = [...dataRealTingkatJabatan.data.data]
            // console.log("newData : ", newData)
            props.setListTingkatJabatan(newData)
        }
    },[dataRealTingkatJabatan])
 
    return props.listTingkatJabatan.map((item, index) => {

        // console.log("Props from listTingkatJabatan : ", props);

        // console.log("Item : ", item);

        return (

            <div  key={index}>
            <br />
            
                {/* <TingkatJabatanItem item={item} index={index} key={index}  /> */}
                <TingkatJabatanItem 
                    item={item} 
                    index={index} 
                    props={props} 
                    classes={classes}
                />             
            </div>
        )
    })

});

export default withStyles(styles)(TingkatJabatanList);


