import React,{Component, useState, useEffect, useContext, useRef } from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
// import { Button} from 'react-bootstrap'; 
import ModalInputNamaJabatan from '../ComponentViewDefineStructureDefault/ModalInputNamaJabatan';


const AddNamaJabatan = (props) => {

    const { classes } = props;

    const [isChooseButtonPlus, setChooseButtonPlus ] = useState(false);
    const handleButtonPlusNamaJabatan = () => setChooseButtonPlus(true);

    return (
        <div>
            {
                props.listNamaJabatan.length > 0 ? (
                    
                    <Button 
                        variant='contained'
                        onClick={handleButtonPlusNamaJabatan}
                        style={{marginTop: 0, marginBottom: 27, position:'absolute'}}
                        className={classes.buttonPlusAddAnggota}
                        size='small'
                    >
                        <span style={{padding: 3, fontSize:'17px'}}> +</span>
                    </Button>   
                ) : null
            }

            <ModalInputNamaJabatan 
                setChooseButtonPlus={setChooseButtonPlus}
                isChooseButtonPlus={isChooseButtonPlus}
                listNamaJabatan={props.listNamaJabatan}
                setListNamaJabatan= {props.setListNamaJabatan}
                classes={classes}
            />

        </div>
    )
};

export default AddNamaJabatan