import React,{useState, useEffect, useContext} from 'react';
// import { Button} from 'react-bootstrap'; 
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';

import ModalInputAnggota from '../ComponentViewDefineStructureDefault/ModalInputAnggota';

const AddAnggota = props => {

    const { classes } = props;

    const [isChooseButtonPlus, setChooseButtonPlus ] = useState(false);
    const [imagePreviewUrl, setImagePreviewUrl] = useState('');

    const handleButtonPlusAnggota = () => {

        setChooseButtonPlus(true);
        setImagePreviewUrl('');
    };   

    return (
        <div>
             {
                props.listAnggota.length > 0 ? (
                    
                    <Button 
                        variant='contained' 
                        onClick={handleButtonPlusAnggota}
                        style={{marginTop: 0, marginBottom: 27, position:'absolute'}}
                        className={classes.buttonPlusAddAnggota}
                        size='small'
                    >
                        <span style={{padding: 3, fontSize:'17px'}}> +</span>
                    </Button>   
                ) : null
            }

            <ModalInputAnggota 
                classes ={classes}
                setChooseButtonPlus={setChooseButtonPlus}
                isChooseButtonPlus={isChooseButtonPlus}
                listAnggota={ props.listAnggota}
                setListAnggota = {props.setListAnggota}
                imagePreviewUrl={imagePreviewUrl}
                setImagePreviewUrl={setImagePreviewUrl}


                
            />
        </div>
    )
};

export default AddAnggota;