import { useState, useEffect } from 'react';
import axios from 'axios';

export const useGetHttp_UmumData = (url, dependencies) => {

  const [isLoading, setIsLoading] = useState(false);
  const [fetchedData, setFetchedData] = useState(null);

  const userToken = localStorage.getItem('userToken');

  // if(userToken !== undefined){



  // };

  //*fetch('https://swapi.co/api/people')
  useEffect(() => {

    // const abortController = new AbortController();
    // const signal = abortController.signal;
    let mounted = true;
    setIsLoading(true);

    const header =  {       
        'Accept': "application/json",
        'Content-Type' : "application/json",
        'Authorization' : "bearer " + userToken,
    };

    axios.defaults.headers.common = header;    
        
    axios
        .get(url)
        .then(function(response){
            // console.log("Response Original : ", response)

            if(mounted == true){

                setFetchedData(response.data.data);
            }
            
        })
        .catch(function(error){
            
            console.log("Error : ", error.response)

            if(mounted){

                setFetchedData(error.response);
                setIsLoading(false);
            }
        })

        //CLEAN UP
        return  () => {
            mounted = false;
        }




  }, dependencies);

  return [isLoading, fetchedData];
};