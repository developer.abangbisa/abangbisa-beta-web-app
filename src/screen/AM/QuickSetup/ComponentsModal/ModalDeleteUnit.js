
import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import axios from 'axios';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
// import Redirect from 'react-router/Redirect';


import { Row,  Modal} from 'react-bootstrap';
import { URL_API } from '../../../../constants/config-api';

// import ContextDefineStrucutureDefault from '../Context/ContextDefineStructureDefault';
// import Button from '../../../components/Button';
// import Button from '../../../../components/Button';
// import TextField from '@material-ui/core/TextField';
// import themeModalDeleteUnit from './themeModalDeleteUnit';
// import { fromJS } from 'immutable';

const styles = {

    card: {

      minWidth: 425,
    },

    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   
    }
};

const ModalDeleteUnit = (props) => {
    
    const { 
    
        classes, 
        isOpenModalDelete,
        setOpenModalDelete,
        idUnit,
        nameUnit
    } = props;

    const closeModalDeleteTingkatJabatan = () => setOpenModalDelete(false);
    
    const handleDelete = () => {

        // console.log("Data Modal Deleet: " , idUnit );

        const userToken = localStorage.getItem('userToken');
        
        if(userToken !== undefined && idUnit !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .delete(URL_API + `/human-resource/master-structure-unit-type/${idUnit}`)
                .then(function(response){

                    console.log("Response Original : ", response)
                    window.location.reload();
                    // setOpenModalDelete(false)
                    // setTimeout(() => {

                    //     window.location.reload();
                    // },2000)
                    // context.handleTabAnggota();
                    
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
               
                })

        } else { console.log("No Access Token available!")};
    };
    
    return (

        // <Modal show={isOpenModalDelete} onHide={closeModalDeleteTingkatJabatan}  centered>
        //     <Modal.Header closeButton>
        //         <Modal.Title id="contained-modal-title-vcenter">
        //         </Modal.Title>
        //     </Modal.Header> 
        //     <Modal.Body>
        //         <MuiThemeProvider theme={themeModalDeleteUnit}>
        //             <h6><b>Apakah Anda yakin ingin menghapus unit <i>{nameUnit} </i>  ? </b></h6>
        //         </MuiThemeProvider>
        //     </Modal.Body>
        //     <Modal.Footer>
        //         <Row style={{marginRight: 3}}>
        //             <Button 
        //                 title='Yakin' 
        //                 handleClick={handleDelete}  
        //             />  
        //         </Row>
        //     </Modal.Footer>
        // </Modal>
        <Dialog
            open={isOpenModalDelete}
            onClose={closeModalDeleteTingkatJabatan}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='h6' className={classes.title}>
                </Typography>
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
            
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.titleModal}>
                        <b>Apakah Anda yakin ingin menghapus <i>{nameUnit}</i> ?</b>
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    onClick={handleDelete}
                    variant='contained' 
                    className={classes.buttonModal}
                    // fullWidth
                >  
                    Yakin
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

// export default withStyles(styles)(ModalDeleteUnit);
export default ModalDeleteUnit;
