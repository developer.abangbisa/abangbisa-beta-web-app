
import React,{Component, useState, useEffect, useContext, useRef, useCallback} from 'react';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
// import Redirect from 'react-router/Redirect';

import { Row,  Modal} from 'react-bootstrap';
import { URL_API } from '../../../../constants/config-api';

import Button from '../../../../components/Button';
// import ButtonDisabled from '../../../components/ButtonDisabled';
import TextField from '@material-ui/core/TextField';
import theme from '../ComponentViewUmumData/theme';

import { fromJS } from 'immutable';
import { useGetHttp_formUnit } from '../Hook/useGetHttp_formUnit';

import ContextDefineStrucutureDefault from '../Context/ContextDefineStructureDefault';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import {red, grey, green, amber} from '@material-ui/core/colors';

import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';

import { useDebounce, useDebouncedCallback } from "use-debounce";

const styles = {

    card: {

      minWidth: 425,
    },

    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

    },
    icon : {
        top: 0  
    },
      root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
      },
      input: {
        marginLeft: 8,
        flex: 1,
      },
      iconButton: {
        padding: 10,
      },
      divider: {
        width: 1,
        height: 28,
        margin: 4,
      },
      icon: {
        margin: 2 * 2,
      },
      iconHover: {
        margin: 2 * 2,
        color: red[800],
        '&:hover': {
          color: grey[500],
          
        },
      },
      iconHoverAdd: {
        margin: 2 * 2,
        color: green[800],
        '&:hover': {
          color: grey[800],
        },
      },

      warning: {
        backgroundColor: amber[700],
      },

      success: {
          backgroundColor: green[500],
      }
};

const ModalUpdateNamaJabatan = (props) => {
    
    const { 
    
        classes, 
        isOpenModalUpdate,
        setOpenModalUpdate,
        nameUnit,
        listChildUnit,
        setListChildUnit,
        idUnit,

        prevId,
        nextId,
        updatedAt

    } = props;

    // console.log(props)
    // const context = useContext(ContextDefineStrucutureDefault);

    const [valueUpdateNameUnit, setUpdateNameUnit] = useState('')
    const closeModalUpdate = () => setOpenModalUpdate(false);

 

    // const [loading, fetchedData] = useGetHttp_formUnit(URL_API + `/account-management/master-structure-position-level/${idNameJabatan}/update`, [isOpenModalUpdate])

    /*

        ````` 
        GET 

        `````

    */

    const [fetchedData, setFetchedData] = useState(undefined);
    const userToken = localStorage.getItem('userToken');

    useEffect(() => {

        if(userToken !== undefined && idUnit !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    
                
            axios
                // .get(URL_API + `/human-resource/master-structure-unit-type/${idUnit}/update/batch `)
                // .get(URL_API + `/human-resource/master-structure-unit-type/${idUnit}?options[embedded][]=masterStructureUnit`)
                .get(URL_API + `/human-resource/master-structure-unit-type/batch/create`)
                // .get(URL_API + `/human-resource/master-structure-unit-type?options[embedded][]=masterStructureUnit`)
                .then(function(response){
                    console.log("Response Original Modal Update Unit   : ", response)
                    setFetchedData(response.data.data);
                    
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    setFetchedData(error.response);
                })
    
        };

    },[isOpenModalUpdate])

    // console.log("fetchedData Update Modal : ", fetchedData)

    const [isDisableButtonSimpan, setDisabledButtonSimpan ] = useState(true)
    const handleChangeNameUnit = (e) => {

      setDisabledButtonSimpan(false);
      setUpdateNameUnit(e.target.value);
    }
    

    const handleUpdate = () => {

        // console.log("Data Update : " , valueUpdateNameUnit );
        let newArray = [...arrayTest];
        newArray = arrayTest

        let data = {

          MasterStructureUnitType: {
	
            name: valueUpdateNameUnit,
            prev_id: prevId !== null ? prevId : "null",
            next_id: nextId !== null ? nextId : 'null',
            icon_id: idSelectedIcon !== undefined || idSelectedIcon !== null ? idSelectedIcon : 'e95cf810-7d05-4e24-8578-1d74b9acefeb',
            updated_at: updatedAt
            
         },
    
          MasterStructureUnit : newArray
          // [
            
            /*

              `````````````````````````````````````````
              Kalau id KOSONG maka create baru !

              ````````````````````````````````````````````
              {
                  id : "",
                  name : "Designer Oke"
            
              },

              `````````````````````````````````````````
                Kalau id & name KOSONG maka delete semua !

              ````````````````````````````````````````````
            */
            
            // {
            //     id : "b7717aff-c37a-40ab-981d-541f0c1f1584 TEST",
            //     name: "Designer Oke TEST"
            // }            
          // ]
        };

        console.log("Data : ", data);

        const userToken = localStorage.getItem('userToken');

        if(userToken !== undefined ){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                // .put(URL_API + `/account-management/master-structure-unit/${idUnit}/update/batch?`, data)
                .put(URL_API + `/human-resource/master-structure-unit-type/batch/${idUnit}`, data)
                .then(function(response){

                    console.log("Response Original After Update: ", response);

                    // closeModalUpdate();
                    
                    window.location.reload();

                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    setOpenModalUpdate(false);
               
                })

        } else { console.log("No Access Token available!")};
    };

    //*
    const [todoValue, setTodoValue] = useState();
    const [todos, setTodo] = useState([]);

    const handleChange = (e) => setTodoValue(e.target.value);

    const handleSubmit = e => {

      e.preventDefault();    

      const todo = {
  
        value: todoValue,
        done: false     

      };

      if(!todoValue) return;

        setTodo([...todos, todo]);
        document.getElementById('todoValue').value = '';

    };

    // const handleDelete = e => {

    //     const { id } = e.target.parentElement;
    
    //     todos.splice(id, 1)
    
    //     setTodo([...todos]);
    // }

    const handleDelete = (e, item) => {

      console.log("Item : ", item.bind.uuid);
      
      //*
      const userToken = localStorage.getItem('userToken');
        
      if(userToken !== undefined ){
      
          const header =  {       
              'Accept': "application/json",
              'Content-Type' : "application/json",
              'Authorization' : "bearer " + userToken,
  
          };

          axios.defaults.headers.common = header;    

          axios
              .delete(URL_API + `/account-management/master-structure-component/${item.bind.uuid}`)
              .then(function(response){

                  console.log("Response Original : ", response)
                  // window.location.reload();
                  listChildUnit.splice(item.bind.uuid, 1)
                  setListChildUnit([...listChildUnit])
                  
              })
              .catch(function(error){
                  
                  console.log("Error : ", error.response)
                  setOpenModalUpdate(false);
             
              })

      } else { console.log("No Access Token available!")};
    };
    

  const [dataComponentUpdate, setDataComponentUpdate ] = useState()
  const handleDone = (e, item) => {
    
      // const { id } = e.target.parentElement;

      // console.log("item.bind.uuid : ", item.bind.uuid);
      // console.log(" item.bind.name : ", item.bind.name);
      // setDataComponentUpdate(componentRef.current.value);

      // listChildUnit[id].done = !listChildUnit[id].done
      setListChildUnit([...listChildUnit])

  };

  const [arrayTest, setArrayTest] = useState([]); 

  const handleComponent =  (e, item, index) => {

    let data = {        

      name: e.target.value,
      id: item.detail.id

    };

    let newArr = [...arrayTest]; // copying the old datas array
      
    newArr[index] = data; //e.target.value ==>  Replace e.target.value with whatever you want to change it to
      
    setArrayTest(newArr);

  };


  /*

      ``````````````````
      Choose Image Icon

      ``````````````````

  */
  
  const [idSelectedIcon, setIdSelectedIcon] = useState();
  const handleChooseImage = (item) => {

    // console.log("Item : ", item );
    setIdSelectedIcon(item.bind.id);

  };

  /*

    `````````````````````````
    Handle Adding Component

    `````````````````````````

  */

 const [componentValue, setComponentValue] = useState('');
 const [components, setComponent] = useState([]);

  useEffect(() => {

    setComponent([]);

  }, [isOpenModalUpdate])

 const handleChangeAddComponent = (e) => setComponentValue(e.target.value);

 const handleSubmitAddComponent = e => {

   e.preventDefault();    

   const component = {

     value: componentValue,
     done: false      
   };
   
   if(!componentValue) return;
   
      setComponent([...components, component]);
      document.getElementById('componentValue').value = '';


      
      
    if(isDisableButtonSimpan !== true ){

        /* Talk with arrayTest, next, refactor with some scenario !!!! */
        setArrayTest([...arrayTest, {name: componentValue, id: ''}]);

    } else {

      /* Talk with Back-End fire if only klik button Plus Component without edit text field */
      const data = {
        
        MasterStructureUnit: {
  
          name: componentValue,
          structure_unit_type_id: idUnit
        
        }
      };
  
       console.log("Handle Add Component : ", data);
  
       const userToken = localStorage.getItem('userToken');
           
       if(userToken !== undefined && data !== null && data !== undefined && idUnit !== undefined){
       
           const header =  {       
               'Accept': "application/json",
               'Content-Type' : "application/json",
               'Authorization' : "bearer " + userToken,
   
           };
  
           axios.defaults.headers.common = header;    
  
           axios
               .post(URL_API + '/human-resource/master-structure-unit', data)
               .then(function(response){
  
                   // closeModalAnggota();
                   console.log("Response Original : ", response)
  
                   if(response.status == 200 ){
                      
                      console.log("Nama unit berhasil di tambahkan !")
                      setOpenResponse200(true); 
                      setContentResponse200("Nama unit berhasil di tambahkan :)")
                   };
               })
               .catch(function(error){
  
                console.log("Nama unit gagal di tambahkan karena : ",  error.response);
  
                if(error.response.data !== undefined){
                  if(error.response.data.info !== undefined){
                      if(error.response.data.info.status == 400){
                          if(error.response.data.info.errors !== undefined){
                            
                            setOpenResponse400(true)
                            setContentErrorResponse400(error.response.data.info.errors.name);
  
                          }
                      }
                  }
                }
                   
               })
  
       } else { console.log("No Access Token available!")};
    }
    

 };

  /*

    `````````````````````````
    Handle DELETE Component

    `````````````````````````

  */

  const handleDeleteComponent = e => {

    const { id } = e.target.parentElement;
    components.splice(id, 1)

    setComponent([...components]);
  };

  const handleDoneDelete = e => {
  
    const { id } = e.target.parentElement;
    components[id].done = !components[id].done
    setComponent([...components]);

  };

  /*

      ````````````````````````````
      Error Response 400 Snackbar

      ````````````````````````````

  */

   const [ isOpenErrorResponse400, setOpenResponse400] = useState(false);
   const [ contentErrorResponse400, setContentErrorResponse400] = useState()
   
   function handleSnackbar() {
       setOpenResponse400(true);
     }
   
     function handleCloseSnackbar(event, reason) {
       if (reason === 'clickaway') {
         return;
       }

       setOpenResponse400(false);
     };


  /*

        ````````````````````````````
        Response 200 OKE Snackbar

        ````````````````````````````

  */

  const [ isOpenResponse200, setOpenResponse200] = useState(false);
  const [ contentResponse200, setContentResponse200] = useState()
   
  function handleSnackbar200() {
    setOpenResponse200(true);
  };

  function handleCloseSnackbar200(event, reason) {
    
    if (reason === 'clickaway') {
      return;
    };

    setOpenResponse200(false);
    
  };

    return (

        <div>
          <Modal show={isOpenModalUpdate} onHide={closeModalUpdate}  centered>
              <Modal.Header closeButton>
                  <Modal.Title id="contained-modal-title-vcenter">
                      Ubah 
                  </Modal.Title>
              </Modal.Header> 
              <Modal.Body>
                  <MuiThemeProvider theme={theme}>
                      <TextField
                          id="outlined-bare"
                          className={classes.textField}
                          defaultValue={nameUnit}
                          onChange= { handleChangeNameUnit }
                          variant="outlined"
                          fullWidth
                      />
                      <br />
                      <br />
                          {                                
                              listChildUnit && listChildUnit.map((item, i) => (
      
                                <div key={item.id} id={i} style={{marginLeft:30}}>
                                  <Paper className={classes.root} elevation={1}  >
                                  
                                      <InputBase 
                                          // disabled
                                          className={classes.input} 
                                          // placeholder={todo.value}
                                          defaultValue={item.name}
                                          // onChange={handleDone}
                                          onChange={(e) => handleComponent(e, {detail: item}, i)}
                                          // onChange={e => debouncedFunction(e.target.value, {detail: item}, i)}
                                          // ref={componentRef}
                                      />
                                      <Divider className={classes.divider} />

                                      <IconButton 
                                          // onClick={handleDelete}
                                          // onClick={(e) => handleDelete(e, {bind: item})}
                                          color="secondary" 
                                          className={classes.iconButton} 
                                          aria-label="Remove"
                                          // disabled
                                      >
                                          {/* <Icon className={classes.iconHover} color="secondary" style={{ fontSize: 27 }}>
                                            remove_circle
                                          </Icon> */}
                                          <i className="material-icons">
                                            edit
                                          </i>
                                      </IconButton>
                                  </Paper>
                                  <br/>
                                </div>
                              ))
                          }

                          {/* 
                          
                              `````````````````````````````````````````````````
                              IN HERE ONLY TERKAIT DENGAN PENAMBAHAN COMPONENT 
                              
                              `````````````````````````````````````````````````

                          */}

                          {
                              components && components.map((component, i) => (

                                <div key={component.value} id={i}>
                                  <Paper className={classes.root} elevation={1} style={{marginLeft: 30}}>
                                      <InputBase 
                                        disabled
                                        className={classes.input} 
                                        // placeholder={todo.value}
                                        defaultValue={component.value}
                                        onChange={handleDoneDelete}
                                      />
                                      <Divider className={classes.divider} />

                                      <IconButton 
                                        onClick={handleDeleteComponent}
                                        color="secondary" 
                                        className={classes.iconButton} 
                                        aria-label="Remove"
                                      >
                                        <Icon className={classes.iconHover} color="secondary" style={{ fontSize: 27 }}>
                                          remove_circle
                                        </Icon>
                                      </IconButton>

                                      {/* <button className={todo.done ? 'done' : 'not-done'} onClick={handleDone}>{todo.value}</button> */}
                                      {/* <button className="delete-todo" onClick={handleDelete} >x</button>               */}
                                  </Paper>
                                  <br/>
                                </div>
                              ))
                          }

                          <Paper className={classes.root} elevation={1} style={{marginLeft: 30}}>
                              <InputBase 
                                className={classes.input} 
                                placeholder="" 
                                id="componentValue" 
                                onChange={handleChangeAddComponent}
                              />
                              <Divider className={classes.divider} />
                              <IconButton 
                                onClick={handleSubmitAddComponent}
                                color="primary" 
                                className={classes.iconButton} 
                                aria-label="Adding"
                                
                              >
                                  <Icon className={classes.iconHoverAdd} color="secondary" style={{ fontSize: 27 }}>
                                      add_circle
                                  </Icon>
                              </IconButton>
                          </Paper>

                          <br />
                          <h6>Pilih Icon baru : </h6>

                          {/* {
                            fetchedData !== undefined && fetchedData !== null ? fetchedData.iconCollections.map((item, i) => {
                              
                              return (
                                  <IconButton
                                      key={i}
                                      onClick ={() => handleChooseImage({bind: item})} 
                                      color="secondary" 
                                      className={classes.iconButton} 
                                      aria-label="Icon" 
                                      datakey={item}
                                  >
                                    <img 
                                        src={URL_API + "/" +item.url} 
                                        style={{width:54}} 
                                        alt={item.name}
                                    />
                                  </IconButton>
                              )
                          }) : null
                        } */}
                  </MuiThemeProvider>
              </Modal.Body>
              <Modal.Footer>
                  <Row style={{marginRight: 3}}>
                        
                      {
                        isDisableButtonSimpan == true ? (
                          <Button 
                            title='Simpan !' 
                            // disabled
                            handleClick={() => window.location.reload()}  
                          />
                        ) : (

                          <Button 
                              title='Simpan' 
                              handleClick={handleUpdate}  
                          />  
                        )
                      }
                  </Row>
              </Modal.Footer>
          </Modal>


          {/* RESPONSE 400 */}
          <Snackbar
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            open={isOpenErrorResponse400}
            autoHideDuration={6000}
            onClose={handleCloseSnackbar}

            ContentProps={{
                'aria-describedby': 'message-id',
                classes: {
                    root: classes.warning
                }

            }}

            message={<span id="message-id">{contentErrorResponse400}</span>}
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={handleCloseSnackbar}
                >
                    <CloseIcon />
                </IconButton>,
            ]}
          />

            {/*  RESPONSE 200 */}
            <Snackbar

                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={isOpenResponse200}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar200}
                ContentProps={{
                    'aria-describedby': 'message-id',
                    classes: {
                        root: classes.success
                    }

                }}
                message={<span id="message-id">{contentResponse200}</span>}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleCloseSnackbar200}
                    >
                        <i className="material-icons">
                                done
                        </i>
                    </IconButton>,
                ]}
            />
        </div>
    )
};

export default withStyles(styles)(ModalUpdateNamaJabatan);
