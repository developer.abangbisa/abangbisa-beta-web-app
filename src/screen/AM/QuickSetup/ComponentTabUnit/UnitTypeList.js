import React,{Component, useState, useEffect, useContext, useRef } from 'react';
import axios from 'axios';
import { Grid, CardHeader, withStyles, CardContent, Card, CircularProgress, CardActions, Chip } from '@material-ui/core';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import {useGetHttp_UnitType}  from '../Hook/useGetHttp_UnitType';
import { URL_API } from '../../../../constants/config-api';
import UnitTypeItem from './UnitTypeItem';

const styles = {
    
    card: {
        width: 742,
        marginTop: 3,
        marginLeft:7
    },
    root: {

        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        padding: 1 / 2,
    },

    chip: {
        margin: 3 / 2,

    }
};

// const UnitTypeList = props => {
const UnitTypeList = React.memo(function UnitTypeList(props) {

    const [loading, fetchedData, setFetchedData ] = useGetHttp_UnitType(URL_API + `/human-resource/master-structure-unit-type?options[embedded][]=masterStructureUnit`, []);

    useEffect(() => {

        if(fetchedData !== null && fetchedData !== undefined){

            const newData = [...fetchedData]
            props.setListUnitType(newData);
        };

    },[fetchedData])
  
    return props.listUnit.map((item, index) => {

        // console.log("Item : ", item);

        return (
            <div key={index}>
                    <br />
                    <UnitTypeItem item={item} index={index} props={props} /> 
            </div>
            
        )
    })
// };
});

export default withStyles(styles)(UnitTypeList);