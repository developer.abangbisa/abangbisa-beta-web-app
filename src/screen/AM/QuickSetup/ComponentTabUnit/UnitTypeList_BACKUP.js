import React,{Component, useState, useEffect, useContext, useRef } from 'react';
import axios from 'axios';
import { Grid, CardHeader, withStyles, CardContent, Card, CircularProgress, CardActions, Chip } from '@material-ui/core';
import {useGetHttp_UnitType}  from '../../AccountManagement/Hook/useGetHttp_UnitType';
import { URL_API } from '../../../constants/config-api';
import Number from '../../AccountManagement/ComponentViewUnitStructure/Number';
import Title from '../../AccountManagement/ComponentViewUnitStructure/Title';
// import ContextDefineStructureDefault from '../../AccountManagement/Context/ContextDefineStructureDefault';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';

import ModalDeleteUnit from '../../AccountManagement/ComponentViewUmumData/ModalDeleteUnit';
import ModalUpdateUnit from '../../AccountManagement/ComponentViewUmumData/ModalUpdateUnit';

import { useGetHttp_formUnit } from '../../AccountManagement/Hook/useGetHttp_formUnit';

const styles = {
    
    card: {
        width: 742,
        marginTop: 3,
        marginLeft:7
    },
    root: {

        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        padding: 1 / 2,
    },

    chip: {
        margin: 3 / 2,

    }
};

const UnitTypeList = props => {

    const { classes } = props;
    // const context = useContext(ContextDefineStructureDefault);

    const [loading, fetchedData, setFetchedData ] = useGetHttp_UnitType(URL_API + `/human-resource/master-structure-unit-type?options[embedded][]=masterStructureUnit`, []);

    // console.log("fetced Data : ", fetchedData);
    // const [listUnitTypeList, setListTypeList] = useState([]);
    const [ anchorEl, setAnchorEl] = useState(null);
    const [ idUnit, setIdUnit ] = useState();
    const [ nameUnit, setNameUnit ] = useState();

    const [prevId, setPrevId ] = useState();
    const [nextId, setNextId ] = useState();
    const [updatedAt, setUpdatedAt ] = useState();   

    const [ listChildUnit, setListChildUnit ] = useState();
    const [ isOpenModalUpdate, setOpenModalUpdate ] = useState(false);
    const [ isOpenModalDelete, setOpenModalDelete ] = useState(false);

    /*
        `````````````````````
        Menu Update

        ``````````````````````

    */

    const open = Boolean(anchorEl);

    function handleMenu(event, params) {

        // console.log("Params Umum Data : ", params);
 
        setIdUnit(params.bind.id);
        setNameUnit(params.bind.name); 
        setListChildUnit(params.bind.embedded.masterStructureUnit);
        setAnchorEl(event.currentTarget);
 
        setPrevId(params.bind.prev_id);
        setNextId(params.bind.next_id)
        setUpdatedAt(params.bind.updated_at);
        
    };
 
    function handleMenuClose(params) {
  
        if(params.bind == "Ubah"){
            
            setOpenModalUpdate(true);
            setAnchorEl(null);
 
        } else if(params.bind == "Hapus"){
 
          
             setOpenModalDelete(true)
             setAnchorEl(null);
 
        } else if(params.bind == "component"){
 
 
        } else {
            
            setAnchorEl(null);
        };
    };

    /*
        `````````````````````
        Delete

        ``````````````````````

    */
    const [isDeleteComponent, setDeleteComponent] = useState('');

    function filteredArray(arr, elem) {

        console.log("Elem : ", elem)
        // array.filter(fn) should remove all elements
        // where fn returns false 

        // console.log("Array : ", arr)
        return arr.filter(function(item){
              // keep items that does not include elem

              console.log("Item from filter : ", item.embedded.masterStructureUnit)
              return !item.embedded.masterStructureUnit.includes(elem.id);
          });
    }

    function handleDeleteComponent(e, item, index) {

        // alert('Apakah kamu yakin ingin menghapus Unit ini ? '); // eslint-disable-line no-alert
        e.preventDefault();
        console.log("Item deleted : ", item.bind.id);

        /* v1 */
        // let copy = Object.assign({}, state) // assuming you use Object.assign() polyfill!
        // delete copy[item.bind.id] // shallowly mutating a shallow copy is fine
        // return copy

        /* v2 */
        // const newData = {...fetchedData};
        // console.log("newData : ", newData);

        /* v3 */
        // let { [item.bind.id]: deletedItem, ...fetchedData } = state
        // return fetchedData


        // setFetchedData(fetchedData.filter(item => item.id !== item.bind.id));

        /* v4 */
        // const dara = [
            // const datar = [...fetchedData];
            // const datar = [...fetchedData.slice(0, index) ...playlist.slice(indexOfSong + 1))];
            // console.log("Datar :",datar);
            // ...playlist.slice(indexOfSong + 1));
        // ]


        /* v5 */
        //  const copyArray = [...fetchedData];
        // const dataMe = filteredArray(copyArray, item.bind);
        // console.log("Data me : ", dataMe);

        /* v6 
        
            - https://medium.freecodecamp.org/lets-explore-slice-splice-spread-syntax-in-javascript-e242a6f21e60 
            
        */


        /* SEMENTARA */
       const userToken = localStorage.getItem('userToken');
        
       if(userToken !== undefined && item.bind.id !== undefined){
       
           const header =  {       
               'Accept': "application/json",
               'Content-Type' : "application/json",
               'Authorization' : "bearer " + userToken,
   
           };

           axios.defaults.headers.common = header;    

           axios 
               .delete(URL_API + `/human-resource/master-structure-unit/${item.bind.id}`)
               .then(function(response){

                   console.log("Response Original : ", response)
                    window.location.reload();

               })
               .catch(function(error){
                   
                   console.log("Error : ", error.response)
              
               })

       } else { console.log("No Access Token available!")};
        
    };

    const [isRedirectToFormUnitUpdate, setRedirectToFormUnitUpdate] = useState(false);

    function handlItemDetail(e, item){

        e.preventDefault();
        console.log("Item Detail : ", item);
        // context.handleDefaultDataWantToUpdate(item)
        // setRedirectToFormUnitUpdate(true);

    };

    return (

        <Grid  
            container
            spacing={8}
            direction="row"
            justify="center"
            alignItems="center"
        >
            <Grid item sm={2}></Grid>
            <Grid item sm={10}>

                {   
                    fetchedData != null ? fetchedData.map((item, i) => {  
                    // listUnitTypeList.length > 0 ? listUnitTypeList.map((item, i) => {             

                        return (
                                <Card className={classes.card} key={i}>
                                    <CardHeader 
                                        style={{marginTop: 0, paddingTop:0, paddingBottom:0}}
                                        action={
                                            <div>
                                                <IconButton
                                                    style={{marginBottom:0, paddingBottom:0, marginTop:12}}
                                                    aria-label="More"
                                                    aria-owns={open ? 'long-menu' : undefined}
                                                    aria-haspopup="true"
                                                    onClick={(e) => handleMenu(e, {bind: item})}
                                                >   
                                                    <MoreVertIcon />
                                                </IconButton>
                                                <Menu id="simple-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleMenuClose}>
                                                    <MenuItem onClick={() => handleMenuClose({bind: 'Ubah'})}>Ubah Unit</MenuItem>
                                                    <MenuItem onClick={() => handleMenuClose({bind: 'Hapus'})}>Hapus Unit</MenuItem>                                            
                                                </Menu>
                                            </div>
                                        }
                                    />

                                    <CardContent style={{ marginTop: 0, paddingTop:0, paddingBottom:0, marginBottom:0  }}>
                                    <Number handlItemDetail
                                        onClick={(e) => handlItemDetail(e, {bind: item})}
                                        index={i + 1}
                                    />

                                    <img src={URL_API + '/' + item.self.rel.icon} alt={item.name} style={{height: 42}}/>

                                    <Title  
                                        title={item.name}
                                    />                                               
                                    </CardContent>
                            
                                <CardActions style={{marginLeft:100, paddingTop:0, marginTop:0}}>

                                    {

                                        item.embedded.masterStructureUnit.length != [] ? item.embedded.masterStructureUnit.map((wew, index) => {
                                            
                                            let icon = null;

                                            return (
                                            
                                                    <Chip
                                                        key={index}
                                                        icon={icon}
                                                        label={wew.name}
                                                        onDelete={(e) => handleDeleteComponent(e, {bind: wew}, index)}
                                                        // onClick={(e) => handlItemDetail(e, {bind: item})}
                                                        className={classes.chip}

                                                    />
                                            )
                                        }) : null

                                    }

                                </CardActions>
                                
                            </Card>
                        )

                    }) : ( 
                            <Grid  
                                container
                                spacing={0}
                                direction="row"
                                justify="center"
                                alignItems="center"
                            >
                                <CircularProgress size={32} style={{marginTop: 24, color: 'red'}} />

                            </Grid>
                        )
                    }
                </Grid>

                <ModalDeleteUnit 
                    isOpenModalDelete={isOpenModalDelete}
                    setOpenModalDelete={setOpenModalDelete}
                    idUnit={idUnit}
                    nameUnit={nameUnit}
                />

                <ModalUpdateUnit 
                    isOpenModalUpdate = {isOpenModalUpdate}
                    setOpenModalUpdate = {setOpenModalUpdate}
                    nameUnit={nameUnit}
                    listChildUnit={listChildUnit}
                    setListChildUnit={setListChildUnit}
                    idUnit={idUnit}
                    prevId={prevId}
                    nextId={nextId}
                    updatedAt={updatedAt}

                />
        </Grid>
    )
};

export default withStyles(styles)(UnitTypeList);