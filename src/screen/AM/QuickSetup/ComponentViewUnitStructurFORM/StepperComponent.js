import React, {useEffect, useState, useCallback} from 'react';
import { connect } from 'react-redux';
import { withRouter  } from "react-router-dom";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { makeStyles, withStyles } from '@material-ui/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
// import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';

import CardStepperFirst from './CardStepperFirst';
import CardStepperSecond from './CardStepperSecond';
import CardStepperThird from './CardStepperThird';

import theme from './theme';

// import { getDataInPageRegister } from '../../../actions/Signup/get_data_in_page_register';
// import { getLabelUnit } from '../../../actions/DefineStructures/get_label_unit';

import PleaseWait from '../../../../assets/images/Group_1113.png';
// import { Redirect } from 'react-router';
import Redirect from '../../../../utilities/Redirect';
import { URL_API } from '../../../../constants/config-api';
import { ToStructureOrganization } from '../../../../constants/config-redirect-url';

import { useGetHttp} from '../Hook/useGetHttp';
import { Typography } from '@material-ui/core';

// const useStyles = makeStyles(theme => ({
//   root: {
//     width: '90%',
//   },

//   button:{
//     background: '#eaf3f5'
//   },

//   actionsContainer: {
//     marginBottom: 20 * 2,
//   },

//   resetContainer: {
//     padding: 30 * 3,
//     background: '#eaf3f5'
    
//   },

// }));

function getSteps() {

  return ['Jenis Unit', 'Nama ', 'Pilih Icon'];
  
};

function getStepContent(step, props) {

  switch (step) {
   
    case 0:

      return( 
          <CardStepperFirst 

            setAnyJenisUnit={props.setAnyJenisUnit}
            isAnyJenisUnit ={props.isAnyJenisUnit}
            // getStepContent={getStepContent}
          />
      )
    
    case 1:

      return(

        <CardStepperSecond 
          setArrayComponent={props.setArrayComponent}
          isAnyArrayComponent={props.isAnyArrayComponent}

        />
      )

    case 2:

      return (

        <CardStepperThird />
      )

    default:

      return 'Butuh bantuan IT Support ? ';
  }
};


const StepperComponent = (props) => {
  
  // const classes = useStyles();
  const { classes } = props;
  const steps = getSteps();  

  const [activeStep, setActiveStep] = useState(0);
  const [isRedirectToUmumData, setRedirectToUmumData ] = useState(false)

  const handleNext = () => setActiveStep(prevActiveStep => prevActiveStep + 1);

  const handleBack = () =>setActiveStep(prevActiveStep => prevActiveStep - 1);
  // const handleReset = () => setRedirectToUmumData(true);

  // if(isRedirectToUmumData == true){
  //   return (

  //     <Redirect to='/structure-organization' />
  //   )
  // };

  return (
          <div className={classes.root}>
            <MuiThemeProvider theme={theme}>
              <Stepper activeStep={activeStep} orientation="vertical" >

                  { 
                    steps.map((label, index) => (

                      <Step key={label}>
                          <StepLabel>{label}</StepLabel>

                          <StepContent>
                          
                            {getStepContent(index, props)}
                         
                          <div className={classes.actionsContainer}>
                              
                              <Grid  
                                container
                                direction="row"
                                justify="flex-end"
                                alignItems="flex-end"
                              >
                                  <Button
                                      disabled={activeStep === 0}
                                      onClick={handleBack}
                                      className={classes.buttonOutlined}
                                      variant='outlined'
                                  >
                                      Batal Simpan
                                  </Button>
                                  <Button
                                      variant="contained"
                                      color="secondary"
                                      onClick={handleNext}
                                      className={classes.button}
                                  >
                                    {activeStep === steps.length - 1 ? 'Upload' : 'Lanjut'}
                                  </Button> 
                              </Grid>
                          </div>
                          </StepContent>
                      </Step>
                  ))}
              </Stepper>

              { activeStep === steps.length && (

                  <Paper square elevation={0} className={classes.resetContainer}>
                  
                  <img src={PleaseWait}  alt="info-icon" />  
                    <br />
                    <br />
                    <Typography variant='h6' className={classes.title}>
                        <b>Tunggu sebentar...</b>
                        <CircularProgress size={20} style={{marginLeft: 14}} />
                    </Typography>
                  
                    <Typography variant='subtitle2' className={classes.title}>
                      Unit Struktur baru Anda akan segera ditambahkan !
                    </Typography>
                    <Button onClick={() => Redirect(ToStructureOrganization)} className={classes.buttonOutlined} variant='outlined'>
                        ...
                    </Button>

                      {
                        setTimeout(() => {
                          Redirect(ToStructureOrganization)
                        }, 4000)
                      }

                  </Paper>
              )}

          </MuiThemeProvider>
      </div>
  );
}

export default React.memo(StepperComponent);