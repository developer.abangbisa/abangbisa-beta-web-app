
import React, {useState, useEffect, useContext} from 'react';
import { 
  Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
  FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
  DialogContentText, DialogActions, Breadcrumbs, Link,CardHeader, Card, CardContent, Chip, Avatar, IconButton

} from '@material-ui/core';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
// import ShareIcon from '@material-ui/icons/Share';
import {red, grey, green} from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';

import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import ContextFormUnit from '../Context/ContextFormUnit';
import { URL_API} from '../../../../constants/config-api';
import HelpUploadIcon from '../../../../assets/images/Group_1096.png';
import styled from 'styled-components';

const styles = {
    card: {
      minWidth: 425,
      // width: 425
    },
    cardHelp: {
      minWidth: 355,
      backgroundColor: 'transparent',
      // marginTop: 34
      marginLeft: 17
    },
    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

      },
    icon : {
        top: 0  
    },
      root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
        
      },
      input: {
        marginLeft: 8,
        flex: 1,
      },
      iconButton: {
        padding: 10,
      },
      divider: {
        width: 1,
        height: 28,
        margin: 4,
      },
      icon: {
        margin: 2 * 2,
      },
      iconHover: {
        margin: 2 * 2,
        color: red[800],
        '&:hover': {
          color: grey[500],
          
        },
      },
      iconHoverAdd: {
        margin: 2 * 2,
        color: green[800],
        '&:hover': {
          color: grey[800],
        },
      },
      help: {
        color: '#97a2a5',
        fontFamily: 'Nunito',
      },
      chip: {
        margin: 3 / 2,
      },
  };

const CardStepperSecond = (props) => {

    const { classes } = props;
    const context = useContext(ContextFormUnit);
    // console.log("Context : ", context);
    
    const [todoValue, setTodoValue] = useState('');
    const [todos, setTodo] = useState([])

    const handleChange = (e) => setTodoValue(e.target.value);

    const handleEnterPress = (e) => {

        if(e.keyCode == 13){
            setTodoValue(e.target.value);
            handleSubmit();
        };
    };
    

    const [isHideTextHelpComponentUnit, setHideTextHelpComponentUnit ] = useState(false);

    const handleSubmit = () => {

      // e.preventDefault();  
      
      setHideTextHelpComponentUnit(true);

      const todo = {
        value: todoValue,
        done: false    

      };
      
      if(!todoValue) return;
      
        setTodo([...todos, todo]);

        props.setArrayComponent([...props.isAnyArrayComponent, todoValue]); //Add element array inside Component 'ViewUnitStructureFORM'
        document.getElementById('todoValue').value = '';

        //*context
        const data = {
          
          MasterStructureUnit: {
	
            name: todoValue,
            structure_unit_type_id: context.idResponse200JenisUnit
          
          }
        };

        console.log("Data : ", data);

        // console.log("Data from CardStepperSecond : ", data);

        const userToken = localStorage.getItem('userToken');
            
        if(userToken !== undefined && data !== null && data !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + '/human-resource/master-structure-unit', data)
                .then(function(response){

                    // closeModalAnggota();
                    console.log("Response Original : ", response)
                    
                    if(response.status == 200 ){
                        if(response.data.data !== undefined){
                            // context.handleTabAnggota();
                            // context.handleResponse200JenisUnit(response.data.data)
                        }
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    
                })

        } else { console.log("No Access Token available!")};

    };

    const handleDelete = e => {

      const { id } = e.target.parentElement;
  
      todos.splice(id, 1)
  
      setTodo([...todos]);

      props.isAnyArrayComponent.splice(id, 1);
      props.setArrayComponent([...props.isAnyArrayComponent])
    }
  
    const handleDone = e => {
  
      const { id } = e.target.parentElement;
      todos[id].done = !todos[id].done
      setTodo([...todos])
    }
    
  
    return (
              <Grid container wrap="nowrap">
                <Card className={classes.card}>
                    <CardContent>
                         {
                            todos && todos.map((todo, i) => (
                              <div key={todo.value} id={i}>
                              <Paper className={classes.root} elevation={1} >
                              
                                  <InputBase 
                                    disabled
                                    className={classes.input} 
                                    defaultValue={todo.value}
                                    onChange={handleDone}
                                    
                                  />
                                  <Divider className={classes.divider} />

                                  <IconButton 
                                    onClick={handleDelete}
                                    color="secondary" 
                                    className={classes.iconButton} 
                                    aria-label="Remove"
                                  >
                                    <Icon className={classes.iconHover} color="secondary" style={{ fontSize: 27 }}>
                                      remove_circle
                                    </Icon>
                                  </IconButton>

                                  {/* <button className={todo.done ? 'done' : 'not-done'} onClick={handleDone}>{todo.value}</button> */}
                                  {/* <button className="delete-todo" onClick={handleDelete} >x</button>               */}
                              </Paper>
                              <br/>
                              </div>
                            ))
                        }

                        <Paper className={classes.root} elevation={1}>

                            <InputBase 
                              className={classes.input} 
                              placeholder="" 
                              id="todoValue" 
                              onChange={handleChange}
                              onKeyDown={handleEnterPress}
                            />

                            <Divider className={classes.divider} />
                            
                            <IconButton 
                              onClick={handleSubmit}
                              color="primary" 
                              className={classes.iconButton} 
                              aria-label="Adding"
                              
                            >
                                <Icon className={classes.iconHoverAdd} color="secondary" style={{ fontSize: 27 }}>
                                    add_circle
                                </Icon>
                            </IconButton>
                        </Paper>

                    </CardContent>
                </Card> 

                {
                  isHideTextHelpComponentUnit !== true ? (

                    <Card className={classes.cardHelp} nowrap="true">
                      <CardContent style={{ textAlign: 'center' }}>
                          <Typography variant='subtitle1' className={classes.help}>
                            Ketik Nama Unit yang terkait pada Jenis Unit yang ditambahkan sebelumnya, contoh : 
                          </Typography>

                          <br />
                          <Chip
                              // avatar={<Avatar>1</Avatar>}
                              // icon={icon}
                              label="Finance"
                              // onDelete={handleDeleteComponent}
                              className={classes.chip}
                          />
                        
                          <Chip
                              // avatar={<Avatar>2</Avatar>}
                              // icon={icon}
                              label="Sales"
                              // onDelete={handleDeleteComponent}
                              className={classes.chip}
                          />
                    
                          <Chip
                              // avatar={<Avatar>2</Avatar>}
                              // icon={icon}
                              label="IT"
                              // onDelete={handleDeleteComponent}
                              className={classes.chip}
                          />
                          <hr style={{borderStyle:'solid', borderColor:'#e0e0e0', borderWidth: 1, marginTop: 17, marginBottom: 12 }} />
                          <img src={HelpUploadIcon}  alt="info-icon" />   
                          <br />
                          <br />
                          <Typography variant='subtitle1' className={classes.help}>
                              <b>Butuh bantuan mengisi form ?</b>
                          </Typography>
                          <br />
                          <Typography variant='subtitle1' className={classes.help}>
                              Dengan menekan tombol dibawah, saya mengizinkan untuk melengkapi form dengan data yang telah disediakan
                          </Typography>

                          <br />
                          <PilihIcon /> 
                      </CardContent>
                    </Card>
                  ) : null
                }

              </Grid>    
    )
};

export default withStyles(styles)(CardStepperSecond);



const Wrapper = styled.button`

    width: 236.5px;
    height: 49px;
    opacity: 0.17;
    border-radius: 5px;
    background-color: #84b7fc;
    border-color: 1px solid cyan;
    cursor: pointer;
    font-family: Nunito;
    font-size: 18px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 2.06;
    letter-spacing: normal;    
    
`;

const PilihIcon = () => (
    
    <Wrapper>
      Isikan otomatis Nama Unit
    </Wrapper>
);
