import React, {useEffect, useState, useCallback} from 'react';
import { connect } from 'react-redux';
import { withRouter  } from "react-router-dom";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { makeStyles, withStyles } from '@material-ui/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
// import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';

import CardStepperFirstUpdate from './CardStepperFirstUpdate';
import CardStepperSecondUpdate from './CardStepperSecondUpdate';
import CardStepperThirdUpdate from './CardStepperThirdUpdate';

import theme from './theme';

// import { getDataInPageRegister } from '../../../actions/Signup/get_data_in_page_register';
// import { getLabelUnit } from '../../../actions/DefineStructures/get_label_unit';

import PleaseWait from '../../../assets/images/Group_1113.png';
import { Redirect } from 'react-router';
import { URL_API } from '../../../constants/config-api';

import { useGetHttp} from '../Hook/useGetHttp';

const useStyles = makeStyles(theme => ({
  root: {
    width: '90%',
  },

  button:{
    background: '#eaf3f5'
  },

  actionsContainer: {
    marginBottom: 20 * 2,
  },

  resetContainer: {
    padding: 30 * 3,
    background: '#eaf3f5'
    
  },

}));

function getSteps() {

  return ['Jenis Unit', 'Nama ', 'Pilih Icon'];
};

function getStepContent(step) {

  switch (step) {
   
    case 0:

     
      return( 
          <CardStepperFirstUpdate />
      )
    
    case 1:

      return(

        <CardStepperSecondUpdate />
      )

    case 2:

      return (

        <CardStepperThirdUpdate />
      )

    default:

      return 'Butuh bantuan IT Support ? ';
  }
};


const StepperComponentUpdate = (props) => {
  
  const classes = useStyles();
  const steps = getSteps();  

  const [activeStep, setActiveStep] = useState(0);
  const [isRedirectToUmumData, setRedirectToUmumData ] = useState(false)

  const handleNext = () => setActiveStep(prevActiveStep => prevActiveStep + 1);


  
  const handleBack = () =>setActiveStep(prevActiveStep => prevActiveStep - 1);
  const handleReset = () => setRedirectToUmumData(true);


  // useEffect(() => {
  //   if(activeStep == 1){

  //     console.log("activeStep : ", activeStep);
  //   }
  // },[activeStep])

  function redirect(){
    
    setTimeout(() => {

      return(
        <Redirect to='/umum-data' />
      )
    }, 2000)
  }

  if(isRedirectToUmumData == true){
    return (

      <Redirect to='/umum-data' />
    )
  };

  return (
          <div className={classes.root}>
            <MuiThemeProvider theme={theme}>
              <Stepper activeStep={activeStep} orientation="vertical" >
                  {steps.map((label, index) => (
                      <Step key={label}>
                          <StepLabel>{label}</StepLabel>
                          <StepContent>
                          
                            {getStepContent(index)}
                         
                          <div className={classes.actionsContainer}>
                              
                              <Grid  
                                container
                                direction="row"
                                justify="flex-end"
                                alignItems="flex-end"
                              >
                                  <Button
                                      disabled={activeStep === 0}
                                      onClick={handleBack}
                                      className={classes.button}
                                  >
                                      Batal Simpan
                                  </Button>
                                  <Button
                                      variant="text"
                                      color="secondary"
                                      onClick={handleNext}
                                  >
                                    {activeStep === steps.length - 1 ? 'Upload' : 'Lanjut'}
                                  </Button> 
                              </Grid>
                          </div>
                          </StepContent>
                      </Step>
                  ))}
              </Stepper>

              {activeStep === steps.length && (

                  <Paper square elevation={0} className={classes.resetContainer}>
                  
                  <img src={PleaseWait}  alt="info-icon" />  
                    <br />
                    <br />
                    <h6>
                        <b>Tunggu sebentar...</b>
                    <CircularProgress size={20} style={{marginLeft: 14}} />
                    </h6> 
                    <h6>
                      Unit Struktur baru Anda akan segera ditambahkan !
                    </h6>
                    <Button onClick={handleReset} className={classes.button}>
                        ...
                    </Button>
                    {setTimeout(() => {
                      handleReset()
                    }, 4000)}
                    
                  </Paper>
              )}

          </MuiThemeProvider>
      </div>
  );
}

export default React.memo(StepperComponentUpdate);