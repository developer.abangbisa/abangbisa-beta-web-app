
import React, {useState, useEffect, useContext, useCallback} from 'react';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';

import { useDebounce, useDebouncedCallback } from "use-debounce";
// import ContextFormUnit from '../../AccountManagement/Context/ContextFormUnit';
import ContextDefineStructureDefault from '../../AccountManagement/Context/ContextDefineStructureDefault';

import { URL_API} from '../../../constants/config-api';



const styles = {
    card: {
      minWidth: 425,
    },
    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

      }
  };

const CardStepperFirstUpdate = (props) => {
    
    const { classes } = props;
    const context = useContext(ContextDefineStructureDefault);
    console.log("Context Stepper First Update : ", context)
    // console.log("Context Stepper nameJabatanDefault : ", context.listJabatanDefault)


    useEffect(() => {

      console.log("nameJabatan : ", context.nameJabatan);

    }, [context.nameJabatan])

    const [debouncedFunction, cancel] = useDebouncedCallback(
        // to memoize debouncedFunction we use useCallback hook.
        // In this case all linters work correctly
        useCallback(value => {

            // console.log("Updated Value Jenis Unit : ", value)
            context.updateJenisUnit(value)

        // }, [context.labelJenisUnit]), 2000, // DEBOUNCE
        }, []), 2000,

        // The maximum time func is allowed to be delayed before it's invoked:
        { maxWait: 5000 }
      );

    return(
       <Card className={classes.card}>
            <CardContent>
                
                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    // defaultValue={context.nameJabatan}
                    placeholder={context.nameJabatan}
                    // margin="normal"
                    variant="outlined"
                    // fullWidth
                    // onChange={e => {
                    //   setText(e.target.value);
                    // }}

                    onChange={e => debouncedFunction(e.target.value)}


                    
                    />
            </CardContent>
        </Card> 
    )
};

export default withStyles(styles)(CardStepperFirstUpdate);
