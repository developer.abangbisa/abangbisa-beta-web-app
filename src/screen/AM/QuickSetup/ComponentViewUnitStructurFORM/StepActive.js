import React,{ Component } from 'react';
import styled from 'styled-components';
// import BuildingPicture from '../../../assets/images/Group_755.png';

const IconWrapper = styled.div`

    width: 26px;
    height: 26px;
    box-shadow: 0 3px 16px 0 rgba(0, 0, 0, 0.1);
    background-color: #ffffff;
    border-radius: 20px;
    
    
`;

const Icon = styled.div`

    width: 8.7px;
    height: 8.7px;
    background-color: #c50303;
    z-index: 1000;
    border-radius: 7px;
    margin-left: 8px;
    margin-top: 8px;
    position:absolute; 

`;





    // top:0;

const IconStepActive = () => (

    <IconWrapper>
        <Icon></Icon>
    </IconWrapper>
);

export default IconStepActive;