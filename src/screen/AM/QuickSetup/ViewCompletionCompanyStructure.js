import React, { useCallback, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link

} from '@material-ui/core';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import Redirect from '../../../utilities/Redirect';
import { 
        ToStructureOrganization, 
        ToCompletionProfile, 
        ToCompletionCompanyStructureFormulirKosong, 
        ToCompletionCompanyStructureQuestionTHIRD

    } from '../../../constants/config-redirect-url';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }       
});

const styles = theme => ({

    root: {
        
        // padding: theme.spacing(5, 2),
        // marginTop: theme.spacing(4),
        // width: 575,
        borderRadius: 2

    },
    button: {
        // width: '503px',
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'
    },
    breadcrumb: {
        marginLeft: theme.spacing(5)
    },
    title: {
        fontFamily: 'Nunito'
    },
    titleLater: {
        fontFamily: 'Nunito',
        cursor: 'pointer',
        marginLeft: theme.spacing(5),
        color: 'grey'
    }
});

const ViewCompletionCompanyStructure = props => {

    const { classes } = props;
    
    return (

        <MuiThemeProvider theme={theme}>
            <br />
            <br />
            <Paper elevation={0}>
                <Grid container 
                    direction="row"
                    justify="center"
                    alignItems="center"
                >  
                    <Grid item xs={4} style={{textAlign: 'center'}}></Grid>
                    <Grid item xs={4} style={{textAlign: 'center'}}>
                        <Breadcrumbs 
                            className={classes.breadcrumb}
                            separator={<NavigateNextIcon fontSize="small" />} 
                            aria-label="Breadcrumb">
                            
                            <Typography color="inherit" className={classes.title}>Keanggotaan</Typography>
                            <Typography color="inherit" className={classes.title}>Profil</Typography>
                            <Typography color="primary" className={classes.title}><b>Struktur Perusahaan</b></Typography>
                        </Breadcrumbs>
                    </Grid>
                    <Grid item xs={4} style={{textAlign: 'center'}}></Grid>
                </Grid>
            </Paper>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <Paper className={classes.root} elevation={0}>
                <Grid container >  
                    <Grid item xs={12} style={{textAlign: 'center'}}>
                        <Typography variant="h5" className={classes.title}>
                            <b>Apakah anda ingin kami bantu untuk mengisi data struktur organisasi ?</b>
                        </Typography>
                        <br />
                      
                        <Typography variant="subtitle1" className={classes.title} style={{color:'grey'}}>
                            Kami akan memberikan petunjuk pengisian & contohnya
                        </Typography>
                        <br />
                        <br />
                        <Button 
                            onClick={() => Redirect(ToCompletionCompanyStructureQuestionTHIRD)}
                            variant='contained' 
                            size='medium' 
                            className={classes.button}
                        >
                            Ya, bantu saya
                        </Button>

                        <br />  
                        <br />
                        <br />
                        <span 
                            // variant='subtitle1' 
                            className={classes.titleLater}
                            // onClick={handleBackInsideComponentCrop}
                            onClick={() => Redirect(ToCompletionCompanyStructureFormulirKosong)}
                        >
                            <u>Tidak, berikan saya formulir kosong</u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span>
                    </Grid>
                </Grid>
            </Paper>
        </MuiThemeProvider>
    )
};


export default withStyles(styles)(ViewCompletionCompanyStructure);