import React,{ Component } from 'react';
import styled from 'styled-components';
import ImagePilihIcon from '../../../assets/images/Group_1118.png';


const Img = styled.img`

    width: 245px;
    height: 245px;
    
`;

const InfoImagePilihIcon = () => (

    <span> 
        <Img src={ImagePilihIcon} />
    </span>
);


export default InfoImagePilihIcon;