import React,{Component} from 'react';
import styled from 'styled-components';

const H3 = styled.span`

    font-family: Nunito;
    font-size: 17px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.76;
    letter-spacing: normal;
    color: #000000;
    margin-left: 17px;
    
`;
  
//   text-align: center;

const Title = ({title}) => (

    <H3>{title}</H3>
);

export default Title;