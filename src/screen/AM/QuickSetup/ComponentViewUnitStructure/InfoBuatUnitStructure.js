import React,{ Component } from 'react';
import styled from 'styled-components';
import ImageBuatUnitStructure from '../../../assets/images/Group_1121.png';


const Img = styled.img`

    width: 245px;
    height: 245px;
    
`;
    
// background-color: #f2f2f2;

const InfoBuatUnitStructure = () => (

    <span> 
        <Img src={ImageBuatUnitStructure} />
    </span>
);


export default InfoBuatUnitStructure;