import React,{ Component } from 'react';
import { Button } from '@material-ui/core';
// import { Button } from 'react-bootstrap';
import styled from 'styled-components';
// import IconArrowRight from '../assets/images/Group_1222.png';
import IconArrowRight from '../../../../assets/images/Group_1222.png';

const White = styled.span`
    color: white;

`;

const TextNumber = styled.h2`

    // height: 17px;
    font-family: Nunito;
    // font-size: 18px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    // line-height: 1.5;
    letter-spacing: normal;
    color: #b50707;
`;

const Number = ({index, onClick}) => {

    return (
        <Button 
            size='small'
            onClick={onClick}
            variant="outlined" 
            style={{margin: 1, borderColor: '#cc0707', height: 43, minWidth:'42px',fontFamily: 'Nunito', color: '#b50707', fontSize: 17 }}
            
        >
            {/* <TextNumber> */}
                <b>{index}</b>
            {/* </TextNumber> */}
        </Button>
        
    
    )
}

//     };
// };

export default Number;

