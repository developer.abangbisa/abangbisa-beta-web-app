
import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';
import ContextDefineStrucutureDefault from '../Context/ContextDefineStructureDefault';

const ModalDeleteAnggota = (props) => {
    
    const { 
    
        classes, 
        isOpenModalDelete,
        setOpenModalDelete,
        idMember,
        nameMember
        
    } = props;

    const context = useContext(ContextDefineStrucutureDefault);
    // console.log("context : ", context);

    // const [valueUpdateTingkatJabatan, setUpdateTingkatJabatan] = useState('');
    const closeModalDeleteTingkatJabatan = () => setOpenModalDelete(false);
    // const handleChange = (e) => setUpdateTingkatJabatan(e.target.value);

    const handleDelete = () => {

        // console.log("Data Modal Deleet: " , {nameJabatan, idMember} );

        const userToken = localStorage.getItem('userToken');
        
        if(userToken !== undefined && idMember !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .delete(URL_API + `/account-management/member/${idMember}`)
                .then(function(response){

                    console.log("Response Original : ", response)
                    setOpenModalDelete(false)
                    // context.handleTabAnggota();
                    props.setListAnggota(props.listAnggota.filter(item => item.id !== idMember));
                    
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
               
                })

        } else { console.log("No Access Token available!")};
    };
    
    return (
        <Dialog
            open={isOpenModalDelete}
            onClose={closeModalDeleteTingkatJabatan}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='h6' className={classes.title}>
                    {/* Tambah Anggota */}
                </Typography>
                {/* <hr/> */}
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
            
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.titleModal}>
                        <b>Apakah Anda yakin ingin menghapus <i>{nameMember}</i> ?</b>
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    onClick={handleDelete}
                    variant='contained' 
                    className={classes.buttonModal}
                    // fullWidth
                >  
                    Yakin
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

// export default withStyles(styles)(ModalDeleteAnggota);
export default ModalDeleteAnggota;
