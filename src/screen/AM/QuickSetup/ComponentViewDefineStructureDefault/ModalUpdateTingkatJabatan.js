import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
// import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
// import { Row,  Modal} from 'react-bootstrap';
import { URL_API } from '../../../../constants/config-api';

// import Button from '../../../../components/Button';
// import TextField from '@material-ui/core/TextField';
// import theme from './theme';
// import ContextDefineStrucutureDefault from '../Context/ContextDefineStructureDefault';

const styles = {

    card: {

      minWidth: 425,

    },

    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

    }
};

const ModalUpdateTingkatJabatan = (props) => {
    
    const { 
    
        classes, 
        isOpenModalUpdate,
        setOpenModalUpdate,
        idTingkatJabatan,
        nameTingkatJabatan,

    } = props;

    // console.log(props)
    // const context = useContext(ContextDefineStrucutureDefault);
   
    const closeModalUpdateTingkatJabatan = () => setOpenModalUpdate(false);

    /*

        ````` 
        GET 

        `````

    */

    const [fetchedData, setFetchedData] = useState();
    const userToken = localStorage.getItem('userToken');

    useEffect(() => {

    

        if(isOpenModalUpdate == true){

            if(userToken !== undefined){
        
                const header =  {           
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
                };
            
                axios.defaults.headers.common = header;    
                    
                axios
                    .get(URL_API + `/human-resource/master-structure-position-level/${idTingkatJabatan}/update`)
                    .then(function(response){
                        console.log("Response Original : ", response)
                        setFetchedData(response.data.data);
                        
                    })
                    .catch(function(error){
                        
                        console.log("Error : ", error.response)
                        setFetchedData(error.response);
                        // setIsLoading(false);
                    })
        
            };
        }

    },[isOpenModalUpdate])
   
    const [valueUpdateTingkatJabatan, setUpdateTingkatJabatan] = useState('')

    const handleChange = (e) => {

        setUpdateTingkatJabatan(e.target.value);
    };

    const handleEnterPress = (e) => {

        if(e.keyCode == 13){
            setUpdateTingkatJabatan(e.target.value);
            handleUpdate();
        };
    };
    

    const handleUpdate = () => {

        //*console.log("Data Update : " , idTingkatJabatan );

        let data = {

            MasterStructurePositionLevel: {
            
                name: valueUpdateTingkatJabatan,
                prev_id: fetchedData !== undefined ? fetchedData.fields.prev_id.value : '',
                next_id: fetchedData !== undefined ? fetchedData.fields.next_id.value : '',
                updated_at : fetchedData !== undefined ? fetchedData.updated_at : ''
                
            }
        };

        if(userToken !== undefined && fetchedData !== undefined && idTingkatJabatan !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .put(URL_API + `/human-resource/master-structure-position-level/${idTingkatJabatan}`, data)
                .then(function(response){

                    console.log("Response Original : ", response);

                    closeModalUpdateTingkatJabatan();
                    
                    props.setListTingkatJabatan(props.listTingkatJabatan.map(item => (item.id === idTingkatJabatan ? response.data.data : item)));

                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);

                    if(error.response.status == 400){
                        alert(error.response.data.info.message);
                        closeModalUpdateTingkatJabatan();
                    }
               
                })

        } else { console.log("No Access Token available!")};
    };
    
    return (
        <Dialog
            open={isOpenModalUpdate}
            onClose={closeModalUpdateTingkatJabatan}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            // className={classes.dialogInputAnggota}
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='h6' className={classes.title}>
                    Ubah Tingkat Jabatan
                </Typography>
                <hr/>
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>

                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    defaultValue={nameTingkatJabatan}
                    onChange= { handleChange }
                    onKeyDown ={handleEnterPress}
                    variant="outlined"
                    fullWidth
                />
                <DialogContentText id="alert-dialog-description">
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                <Button 
                    onClick={handleUpdate}
                    variant='contained' 
                    className={classes.buttonModalUpdateNamaJabatan}
                    // fullWidth
                >  
                    Simpan
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

export default withStyles(styles)(ModalUpdateTingkatJabatan);
