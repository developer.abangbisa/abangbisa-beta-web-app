import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';
import ContextDefineStrucutureDefault from '../Context/ContextDefineStructureDefault';

const ModalUpdateNamaJabatan = (props) => {
    
    const { 
    
        classes, 
        isOpenModalUpdate,
        setOpenModalUpdate,
        idNameJabatan,
        nameJabatan

    } = props;

    // console.log(props)
    const context = useContext(ContextDefineStrucutureDefault);

    const [valueUpdateTingkatJabatan, setUpdateTingkatJabatan] = useState('')
    const closeModalUpdateNamaJabatan = () => setOpenModalUpdate(false);

    // const [loading, fetchedData] = useGetHttp_formUnit(URL_API + `/account-management/master-structure-position-level/${idNameJabatan}/update`, [isOpenModalUpdate])
    /*

        ````` 
        GET 

        `````

    */

    const [fetchedData, setFetchedData] = useState();
    const userToken = localStorage.getItem('userToken');

    useEffect(() => {

        if(isOpenModalUpdate == true){

            if(userToken !== undefined && idNameJabatan !== undefined){
        
                const header =  {       
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
                };
            
                axios.defaults.headers.common = header;    
                    
                axios
                    .get(URL_API + `/human-resource/master-structure-position-title/${idNameJabatan}/update`)
                    .then(function(response){
                        console.log("Response Original : ", response)
                        setFetchedData(response.data.data);
                        
                    })
                    .catch(function(error){
                        
                        console.log("Error : ", error.response)
                        setFetchedData(error.response);
                        // setIsLoading(false);
                    })
        
            };
        }
    },[isOpenModalUpdate])

    const handleChange = (e) => setUpdateTingkatJabatan(e.target.value);
    const handleEnterPress = (e) => {

        if(e.keyCode == 13){
            setUpdateTingkatJabatan(e.target.value);
            handleUpdate();
        };
    };
    
    const handleUpdate = () => {

        let data = {

            MasterStructurePositionTitle: {
            
                name: valueUpdateTingkatJabatan,
                updated_at : fetchedData !== undefined ? fetchedData.updated_at : ''
                
            }
        };

        // console.log("Data : ", data);

        // console.log("List Nama Jabatan : ", props.listNamaJabatan)
        
        if(userToken !== undefined && fetchedData !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .put(URL_API + `/human-resource/master-structure-position-title/${idNameJabatan}`, data)
                .then(function(response){

                    console.log("Response Original Updated : ", response);

                    closeModalUpdateNamaJabatan();
                    // context.handleTabNamaJabatan();
                    props.setListNamaJabatan(props.listNamaJabatan.map(item => (item.id === idNameJabatan ? response.data.data : item)));

                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);
                    alert("Oops, something went wrong !")
               
                })

        } else { console.log("No Access Token available!")};
    };
    
    return (
        <Dialog
            open={isOpenModalUpdate}
            onClose={closeModalUpdateNamaJabatan}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            // className={classes.dialogInputAnggota}

        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
            <Typography variant='h6' className={classes.title}>
                Ubah
            </Typography>
            <hr/>
        </DialogTitle>
        <DialogContent style={{textAlign: "center"}}>

            <TextField
                id="outlined-bare"
                className={classes.textField}
                defaultValue={nameJabatan}
                onChange= { handleChange }
                onKeyDown={handleEnterPress}
                variant="outlined"
                fullWidth
            />

            <br />
            <br />
            <TextField
                id="outlined-bare"
                label="Kode Jabatan"
                className={classes.textField}
                // onChange= {handleChangeLabelCodeNamaJabatan}
                // onKeyDown={handleEnterPress}
                variant="outlined"
                fullWidth
                // placeholder={listLabelNamaJabatan}
            />

            <DialogContentText id="alert-dialog-description">
            </DialogContentText>
        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            <Button 
                onClick={handleUpdate}
                variant='contained' 
                className={classes.buttonModalUpdateNamaJabatan}
                // fullWidth
            >  
                Simpan
            </Button>
        </DialogActions>
        <br />
    </Dialog>
    )
};

// export default withStyles(styles)(ModalUpdateNamaJabatan);
export default ModalUpdateNamaJabatan;
