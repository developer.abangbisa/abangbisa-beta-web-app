import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import axios from 'axios';
// import Redirect from 'react-router/Redirect';
import { withStyles, createMuiTheme} from '@material-ui/core/styles';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { Container, Row, Col,  Modal, ModalFooter, Form} from 'react-bootstrap';
import { Dialog, DialogTitle, Typography, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';

import { URL_API } from '../../../../constants/config-api';

// import Button from '../../../../components/Button';
import TextField from '@material-ui/core/TextField';
// import theme from '../ComponentViewUmumData/theme';
// import {useGetHttp} from '../Hook/useGetHttp';
// import { fromJS } from 'immutable';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';


import { Grid } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import AvatarDummy from '../../../../assets/images/Avatar_dummy.png';

const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#cc0707', //#cc0707, #c62828
        light: '#ff5f52',
        dark: '#8e0000',
        contrastText: '#ffffff'
      }
    },
    overrides: {
      MuiButton: {
        root: {
          marginTop: 30,
          marginRight: 40,
          width: '122.2px',
          height: '40px',
          borderRadius: 5,
          background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
          border:0,
        },
        text: {
          color: 'white'
        },
        textSecondary: {
          color: 'white',
          fontFamily: 'Nunito'
        },
      },
      MuiStepper:{
        root:{
          backgroundColor: '#eaf3f5'
        }
      },
      MuiStepContent:{
          root:{
            backgroundColor: '#eaf3f5'
          }
      },
      MuiStepIcon: {
        root: {
            color: 'grey', // or 'rgba(0, 0, 0, 1)'
            '&$active': {
                color: '#c2322d',
            },
            '&$completed': {
                color: 'green',
            },
        },
    } 
    }
  });

const styles = theme => ({

    // card: {

    //   minWidth: 425,

    // },

    // close: {
        
    //     padding: 7 / 2,
    // },



    /*
        ```````````````````````````
        DIALOG MODAL INPUT ANGGOTA

        ``````````````````````````
    */

});

const ModalInputAnggota  = (props) => {
    
    const { 
    
        classes, 
        isChooseButtonPlus, 
        setChooseButtonPlus

    } = props;

    // const context = useContext(ContextDefineStrucutureDefault);

    const [valueNamaJabatan, setValueNamaJabatan] = useState('');
    const [namaDepan, setNamaDepan ] = useState('');
    const [namaBelakang, setNamaBelakang ] = useState('');
    const [email, setEmail] = useState('');
    const [sPTitleId, setStructurePositionTitleId] = useState('');

    const [isErrorLength32Character, setErrorLength32Character ] = useState(false)

    const closeModalAnggota = () => setChooseButtonPlus(false);
    const handleChange = (e) => setValueNamaJabatan(e.target.value);
    const handleChangeNamaDepan = (e) => {

        const stringData = e.target.value.toString();
        

        if(stringData.length > 32){

            setErrorLength32Character(true);

        } else if(stringData.length < 32){

            setErrorLength32Character(false)

        } else {
          
            console.log("Something went wrong in 'isErrorLength32Character' ")
        };

        setNamaDepan(e.target.value);
    };

    const handleChangeNamaBelakang = (e) => {

        const stringData = e.target.value.toString();
        
        if(stringData.length > 32){

            setErrorLength32Character(true)

        } else if(stringData.length < 32){

            setErrorLength32Character(false)

        } else {

            console.log("Something went wrong in 'isErrorLength32Character' ")
        } 

        setNamaBelakang(e.target.value);
    };
    
    const [isErrorEmailValidation, setErrorEmailValidation ] = useState(false);
    
    const handleChangeEmail = (e) => {

        const emailValid = e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        // const stringData = e.target.value.toString();

        if(!emailValid){
            console.log('Email is Invalid !');
            setErrorEmailValidation(true);

        } else {

            setErrorEmailValidation(false);
        } 
		// fieldValidationErrors.email = emailValid ? '' : ' Email is invalid';
        setEmail(e.target.value);
    };
    
    const handleDropdown = (e) => setStructurePositionTitleId(e.target.value)

     /*

        ````````````````````````````
        Error Response 400 Snackbar

        ````````````````````````````

    */
    const [ isOpenErrorResponse400, setOpenResponse400] = useState(false);
    const [ contentErrorResponse400, setContentErrorResponse400] = useState()
   
    function handleSnackbar() {
       setOpenResponse400(true);
    }
   
    function handleCloseSnackbar(event, reason) {
       if (reason === 'clickaway') {
         return;
       }

       setOpenResponse400(false);
     }
     /*

        ````````````````````````````
        Response 200 OKE Snackbar

        ````````````````````````````

    */

    const [ isOpenResponse200, setOpenResponse200] = useState(false);
    const [ contentResponse200, setContentResponse200] = useState()
   
    function handleSnackbar200() {

        setOpenResponse200(true);
    }

    function handleCloseSnackbar200(event, reason) {
        if (reason === 'clickaway') {
         return;
        }

        setOpenResponse200(false);
        
    };

    /* Feature Upload Foto */

    // this.state = {file: '',imagePreviewUrl: ''};
    const [file, setFile ] = useState('');
    // const [imagePreviewUrl, setImagePreviewUrl] = useState('');

    // const handleSubmit = (e) => {
    //     e.preventDefault();
    //     // TODO: do something with -> this.state.file
    //     console.log('handle uploading-', file);
    // }
    
    const handleImageChange = (e) => {
        
        e.preventDefault();
    
        let reader = new FileReader();
        let file = e.target.files[0];
    
        reader.onloadend = () => {

            setFile(file);
            props.setImagePreviewUrl(reader.result);
        };
    
        reader.readAsDataURL(file)
      
    };

    const handleAdd = () => {

        // console.log('handle uploading-', file);

        let data = {

            Member: {
            
                first_name: namaDepan,
                last_name: namaBelakang,
                email: email,
                photo_url: ''
                // structure_position_title_id: [sPTitleId]
                
            }
        };

        console.log("Data : ", data);

        const userToken = localStorage.getItem('userToken');
        
        if(userToken !== undefined && data !== null && data !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .post(URL_API + '/account-management/member', data)
                .then(function(response){

                    console.log("Response Original : ", response)
                    setOpenResponse200(true);
                    setContentResponse200("Berhasil menambahkan Anggota :)")
                    closeModalAnggota();

                    if(response.status == 200 ){
                        if(response.data.data !== undefined){
                            props.setListAnggota([...props.listAnggota, response.data.data ])
                        }
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error)

                    if(error.response.data !== undefined){
                        if(error.response.data.info !== undefined){
                            if(error.response.data.info.status == 400){
                                if(error.response.data.info.errors !== undefined){

                                    setOpenResponse400(true)
                                    setContentErrorResponse400(error.response.data.info.errors.email)
                                }
                            }
                        }
                    }
                })

        } else { console.log("No Access Token available!")};
    };


    
    return (

        <div>
            {/* <Modal show={isChooseButtonPlus} onHide={closeModalAnggota}  centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Tambah Anggota
                    </Modal.Title>
                </Modal.Header> 
                <Modal.Body>
                    
                        <Grid container justify="center" alignItems="center">
                            {
                                props.imagePreviewUrl ? (
                                    <Avatar alt="You" src={ props.imagePreviewUrl} className={classes.realAvatar} />
                                  
                                ) : (
                                    <span >

                                        <Avatar alt="You" src={AvatarDummy} className={classes.bigAvatar} />
                                        <br />
                                        <input 
                                            style={{marginLeft: 124}}
                                            type="file" 
                                            onChange={(e)=> handleImageChange(e)} 
                                        />
                                    </span>
                                )
                            }

                          
                        </Grid>

                        
                        <br />
                        {
                            isErrorLength32Character == true ? (

                                <p className={classes.textError}>Jumlah huruf tidak boleh melebihi 32 karakter !</p>
                            ) : null
                        }
                        {
                            isErrorEmailValidation == true ? (

                                <p className={classes.textError}>Error ! Email tidak valid</p>
                            ) : null
                        }
                        
                        <TextField
                            id="outlined-dense"
                            label="Nama Depan"
                            className={classes.textField}
                            onChange= {handleChangeNamaDepan}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />
                      
                        
                        <TextField
                            id="outlined-dense"
                            label="Nama Belakang"
                            className={classes.textField}
                            onChange= {handleChangeNamaBelakang}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />

                        <TextField
                            id="outlined-dense"
                            label="Email"
                            className={classes.textField}
                            onChange= {handleChangeEmail}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />

                        <br />
                        <br />
                    
                </Modal.Body>
                <Modal.Footer>
                    <Row style={{marginRight: 3}}>
                        <Button title='Tambah' handleClick={handleAdd}  />  
                    </Row>
                </Modal.Footer>
            </Modal> */}

            <Dialog
                open={isChooseButtonPlus}
                onClose={closeModalAnggota}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                // className={classes.dialogInputAnggota}
                
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                    <Typography variant='h6' className={classes.title}>
                        Tambah Anggota
                    </Typography>
                    <hr/>
                </DialogTitle>
                <DialogContent style={{textAlign: "center"}}>
                    <Grid container>
                        <Grid item sm={5}></Grid>

                        <Grid item sm={3} style={{textAlign: 'center'}}>

                            {
                                props.imagePreviewUrl ? (
                                    <Avatar alt="You" src={ props.imagePreviewUrl} className={classes.realAvatar}  />
                                    
                                ) : (
                                    <span >
                                        <Avatar alt="You" src={AvatarDummy} className={classes.bigAvatar} />
                                        <br />
                                        <input 
                                            // style={{marginLeft: 124}}
                                            type="file" 
                                            onChange={(e)=> handleImageChange(e)} 
                                        />
                                    </span>
                                )
                            }

                            <br />
                            <br />
                        </Grid>
                        <Grid item sm={4}></Grid>
                    </Grid>

                    {
                        isErrorLength32Character == true ? (

                            <p className={classes.textError}>Jumlah huruf tidak boleh melebihi 32 karakter !</p>
                        ) : null
                    }
                    {
                        isErrorEmailValidation == true ? (
                            <p className={classes.textError}>Error ! Email tidak valid</p>

                        ) : null
                    }

                    <TextField
                        id="outlined-dense"
                        label="Nama Depan"
                        className={classes.textField}
                        onChange= {handleChangeNamaDepan}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                    />
                    
                    
                    <TextField
                        id="outlined-dense"
                        label="Nama Belakang"
                        className={classes.textField}
                        onChange= {handleChangeNamaBelakang}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                    />

                    <TextField
                        id="outlined-dense"
                        label="Email"
                        className={classes.textField}
                        onChange= {handleChangeEmail}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                    />
                    <DialogContentText id="alert-dialog-description">
                        <Typography variant='h6'>
                            
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                     <Button 
                        onClick={handleAdd}
                        variant='contained' 
                        className={classes.buttonModalInputAnggota}
                        // fullWidth
                    >  
                        Tambah
                    </Button>
                </DialogActions>
                <br />
            </Dialog>
            
            {/* RESPONSE 400 */}
            <Snackbar

                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={isOpenErrorResponse400}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
                ContentProps={{
                    'aria-describedby': 'message-id',
                    classes: {
                        root: classes.warning
                    }

                }}
                message={<span id="message-id">{contentErrorResponse400}</span>}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleCloseSnackbar}
                    >
                        <CloseIcon />
                    </IconButton>,
                ]}
            />

            {/*  RESPONSE 200 */}
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={isOpenResponse200}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar200}
                ContentProps={{
                    'aria-describedby': 'message-id',
                    classes: {
                        root: classes.success
                    }
                }}
                message={<span id="message-id">{contentResponse200}</span>}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={handleCloseSnackbar200}
                    >
                        <i className="material-icons">
                            done
                        </i>
                    </IconButton>,
                ]}
            />
        </div>
    )
};

export default withStyles(styles)(ModalInputAnggota );



/*

    ```````````````````````````````
    Feature yang mesti di tambahkan :

        1. Warna Text field di buat Gradient


*/

/* 
    
    ````STYLING CSS`````
    https://www.quirksmode.org/dom/inputfile.html

    ````REFERENSI UPLOAD PREVIEW IMAGE`````
    https://codepen.io/hartzis/pen/VvNGZP?editors=0010

    ````Get base64 from new File`````
    https://stackoverflow.com/questions/36280818/how-to-convert-file-to-base64-in-javascript
    https://jsbin.com/hobiwuyima/edit?js,console,output

    ````````````````````````````````````````````````````

    <form onSubmit={(e)=> handleSubmit(e)}> */
        /* <input 
            type="file" 
            onChange={(e)=> handleImageChange(e)} 
        /> */
        /* <button className="submitButton" 
            type="submit" 
            onClick={(e)=>handleSubmit(e)}>Upload foto</button> */
    /* </form> 
    
*/