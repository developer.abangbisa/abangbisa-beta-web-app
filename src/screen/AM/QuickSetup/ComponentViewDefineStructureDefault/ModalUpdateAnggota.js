import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';

import { Row,  Modal} from 'react-bootstrap';
import { URL_API } from '../../../../constants/config-api';

// import Button from '../../../../components/Button';
import theme from './theme';

import { fromJS } from 'immutable';
import { useGetHttp_formUnit } from '../Hook/useGetHttp_formUnit';

// import ContextDefineStrucutureDefault from '../Context/ContextDefineStructureDefault';

const styles = {

    card: {

      minWidth: 425,
    },

    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

    }
};

const ModalUpdateAnggota = (props) => {
    
    const { 
    
        classes, 
        isOpenModalUpdate,
        setOpenModalUpdate,
        dataUpdated
        // idNameJabatan,
        // nameJabatan

    } = props;

    console.log("dataUpdated : ", dataUpdated)
    // const context = useContext(ContextDefineStrucutureDefault);

    const [valueUpdateAnggota, setUpdateAnggota] = useState('')
    const closeModalUpdateAnggota = () => setOpenModalUpdate(false);

    // const [loading, fetchedData] = useGetHttp_formUnit(URL_API + `/account-management/master-structure-position-level/${idNameJabatan}/update`, [isOpenModalUpdate])
    
    /*

        ````` 
        GET 

        `````

    */

    const [fetchedData, setFetchedData] = useState();
    const userToken = localStorage.getItem('userToken');

    // useEffect(() => {

    //     if(userToken !== undefined ){
    
    //         const header =  {       
    //             'Accept': "application/json",
    //             'Content-Type' : "application/json",
    //             'Authorization' : "bearer " + userToken,
    //         };
        
    //         axios.defaults.headers.common = header;    
                
    //         axios
    //             .get(URL_API + `/account-management/master-structure-position-title/${idNameJabatan}/update`)
    //             .then(function(response){
    //                 console.log("Response Original : ", response)
    //                 setFetchedData(response.data.data);
                    
    //             })
    //             .catch(function(error){
                    
    //                 console.log("Error : ", error.response)
    //                 setFetchedData(error.response);
    //                 // setIsLoading(false);
    //             })
    
    //     };

    // },[isOpenModalUpdate])

    // console.log("fetchedData Update Modal : ", fetchedData)

    const handleChange = (e) => setUpdateAnggota(e.target.value);

    const handleUpdate = () => {

        // console.log("Data Update : " , idNameJabatan );

        let data = {

            MasterStructurePositionTitle: {
            
                name: valueUpdateAnggota,
                updated_at : fetchedData !== undefined ? fetchedData.updated_at : ''
                
            }
        };

        console.log("Data : ", data);

        // if(userToken !== undefined && fetchedData !== undefined){
        
        //     const header =  {       
        //         'Accept': "application/json",
        //         'Content-Type' : "application/json",
        //         'Authorization' : "bearer " + userToken,
    
        //     };

        //     axios.defaults.headers.common = header;    

        //     axios
        //         .put(URL_API + `/account-management/master-structure-position-title/${idNameJabatan}`, data)
        //         .then(function(response){

        //             console.log("Response Original : ", response);

        //             closeModalUpdateAnggota();
        //             // props.setListTingkatJabatan(props.listTingkatJabatan.map(item => (item.id === idTingkatJabatan ? response.data.data : item)));

        //         })
        //         .catch(function(error){
                    
        //             console.log("Error : ", error.response)
               
        //         })

        // } else { console.log("No Access Token available!")};
    };
    
    return (

            <Dialog
                open={isOpenModalUpdate}
                onClose={closeModalUpdateAnggota}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                // className={classes.dialogInputAnggota}

            >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='h6' className={classes.title}>
                    Ubah
                </Typography>
                <hr/>
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    defaultValue={dataUpdated.first_name}
                    variant="outlined"
                    fullWidth
                />
                <br />
                <br />

                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    defaultValue={dataUpdated.last_name}
                
                    variant="outlined"
                    fullWidth
                />
                <br />
                <br />

                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    defaultValue={dataUpdated.email}
                    variant="outlined"
                    fullWidth
                />
                <br />
                
                <DialogContentText id="alert-dialog-description">
                    {/* <Typography variant='h6' className={classes.title}>
                        Apakah Anda yakin ingin menghapus ?
                    </Typography> */}
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                <Button 
                    onClick={handleUpdate}
                    variant='contained' 
                    className={classes.buttonModalUpdateAnggota}
                    // fullWidth
                >  
                    Simpan
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

export default withStyles(styles)(ModalUpdateAnggota);

