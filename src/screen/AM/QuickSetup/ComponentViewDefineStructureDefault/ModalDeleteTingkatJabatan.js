
import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import axios from 'axios';
import { withStyles } from '@material-ui/core/styles';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
// import Redirect from 'react-router/Redirect';


// import { Row,  Modal} from 'react-bootstrap';
import { URL_API } from '../../../../constants/config-api';

import ContextDefineStrucutureDefault from '../Context/ContextDefineStructureDefault';
// import Button from '../../../components/Button';
// import Button from '../../../../components/Button';
// import TextField from '@material-ui/core/TextField';
// import theme from '../ComponentViewUmumData/theme';
// import { fromJS } from 'immutable';

const styles = {

    card: {

      minWidth: 425,
    },

    textField: {
        marginLeft: 7,
        marginRight: 7,
        minWidth: 425   

    },
};

const ModalDeleteTingkatJabatan = (props) => {
    
    const { 
    
        classes, 
        isOpenModalDelete,
        setOpenModalDelete,
        idTingkatJabatan,
        nameTingkatJabatan

    } = props;

      
    const context = useContext(ContextDefineStrucutureDefault);
    // console.log("context : ", context)

    
    
    // const [valueUpdateTingkatJabatan, setUpdateTingkatJabatan] = useState('')
    const closeModalDeleteTingkatJabatan = () => setOpenModalDelete(false);
    
    // const handleChange = (e) => setUpdateTingkatJabatan(e.target.value);

    const handleDelete = () => {

        // console.log("Data Modal Deleet: " , {nameTingkatJabatan, idTingkatJabatan} );

        const userToken = localStorage.getItem('userToken');
        
        if(userToken !== undefined){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .delete(URL_API + `/human-resource/master-structure-position-level/${idTingkatJabatan}`)
                .then(function(response){

                    console.log("Response Original : ", response)
                    setOpenModalDelete(false);
                    props.setListTingkatJabatan(props.listTingkatJabatan.filter(item => item.id !== idTingkatJabatan));
                    
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
               
                })

        } else { console.log("No Access Token available!")};
    };
    
    return (
        <Dialog
            open={isOpenModalDelete}
            onClose={closeModalDeleteTingkatJabatan}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"            
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='h6' className={classes.title}>
                    {/* Tambah Anggota */}
                </Typography>
                {/* <hr/> */}
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
            
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.titleModal}>
                        <b>Apakah Anda yakin ingin menghapus tingkat jabatan <i>{nameTingkatJabatan} </i>  ?</b>
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    onClick={handleDelete}
                    variant='contained' 
                    className={classes.buttonModal}
                    // fullWidth
                >  
                    Yakin
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

export default withStyles(styles)(ModalDeleteTingkatJabatan);
