import React, { useCallback, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link

} from '@material-ui/core';
import axios from 'axios';
import {useDropzone} from 'react-dropzone';
import AvatarEditor from 'react-avatar-editor';  
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import pictJpgPngDummy from '../../../assets/images/Group-814.png';

import uploadIcon from '../../../assets/images/Group-1.png';
import uploadIconColor from '../../../assets/images/Group-813.png';

import { 
        base64StringtoFile, 
        downloadBase64File, 
        extractImageFileExtensionFromBase64, 
        image64toCanvasRef
    } from '../../../utilities/ReusableUtils';

import ConvertDataUrlToFile from '../../../utilities/ConvertDataUrlToFile';

import './styles/styleCompletionProfileGroup.css';
import FormCompletionProfileGroup from './ComponentsProfileCompletion/FormCompletionProfileGroup';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }       
});
    
const styles = theme => ({

    root: {
        
        // padding: theme.spacing(5, 2),
        // marginTop: theme.spacing(4),
        // width: 575,
        borderRadius: 2

    },
    button: {
        width: '503px',
        height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1)
    },

    title: {
        fontFamily: 'Nunito'
    },
    titleDrag: {
        fontFamily: 'Nunito',
        cursor: 'pointer'
    },
    breadcrumb: {
        marginLeft: theme.spacing(5)
    },
    media: {
        width: 200,
        height: 137,
        cursor: 'pointer'
    },
    cardAvatar: {
        marginLeft: theme.spacing(4),
        marginRight: theme.spacing(4)
    },
    titleBack: {
        fontFamily: 'Nunito',
        color: '#333333',
        // marginTop: theme.spacing(16),
        // marginLeft: theme.spacing(4),
        // marginRight: theme.spacing(4),
        cursor: 'pointer',
        fontWeight: 'bold'
    },
    button: {
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        // marginLeft:theme.spacing(1)
        color: 'white',
        textTransform: 'capitalize'
    },
    dropDown: {
        // flexBasis: 200,
        minWidth: 100,
        width: 120,
    
    }
});

const ViewCompletionProfileGroup = props => {

    const { classes } = props;  

    /*
        ```````````````````````
        HANDLE DROPZONE

        ````````````````````````
    */

    const [isPick, setPick ] = useState(true);
    // const [isPick, setPick ] = useState(false);
    const [isCropActive, setCropActive] = useState(false);
    const [isForm, setForm ] = useState(false);
    const [isHover, setHoverPict]= useState(false);

    //*
    const [imgSrc, setImgSrc ] = useState();
    const [imgSrcExt, setImgSrcExt] = useState();
    const [ scale, setScale ] = useState(1);

    //*
    const [ contentFile, setContentFile ] = useState(undefined);
    const[ nameFile, setNameFile ] = useState('');

    const onDrop = useCallback(acceptedFiles => {

        //*
        console.log("acceptedFiles : ", acceptedFiles);
        setNameFile(acceptedFiles[0].name);

        setContentFile(acceptedFiles)

        //*
        setCropActive(true);
        setPick(false);

        //*
        const reader = new FileReader()
        let dataImage = new FormData();
    
        reader.onabort = () => console.log('file reading was aborted')
        reader.onerror = () => console.log('file reading has failed')
        reader.onload = () => {
            // Do whatever you want with the file contents

            //  for (var i = 0; i < acceptedFiles.length; i++) {

            //     let file = acceptedFiles.item(i);
            //     dataImage.append('images[' + i + ']', file, file.name);

            // };



            const binaryStr = reader.result
            console.log("Binary String : ",binaryStr);

            setImgSrc(binaryStr);
            setImgSrcExt(extractImageFileExtensionFromBase64(binaryStr));

            // setContentFile(dataImage)


        };
    
        // acceptedFiles.forEach(file => reader.readAsBinaryString(file))
        acceptedFiles.forEach(file => reader.readAsDataURL(file))

    }, [])

    const { getRootProps, getInputProps, isDragActive, isDragReject, rejectedFiles } = useDropzone({onDrop});

    /*
        `````````````````````
        HANDLE AVATAR EDITOR 

        ``````````````````````
    */

    const[ editorState, setEditorState ] = useState(null);
    const setEditorRef = (editor) => {

        
        if(editor !== null){
            
            // console.log("Editor :", editor);
            setEditorState(editor);
            // const canvas = editor.props.image.getImage().toDataURL();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
            // console.log("Canvass : ", canvas);
        };
        
    } ;

    const onChangeScale = (e) => {

        setScale(e.target.value)
    };

    /*
        ``````````````````````````````
        HANDLE BACK & HANDLE CONTINUE

        ``````````````````````````````
    */

    const handleBackInsideComponentCrop = () => {

        setPick(true);
        setCropActive(false);
    };

    const handleCropAndContinue = () => {

        setPick(false);
        setCropActive(false);
        setForm(true);


        const canvas = editorState.getImage().toDataURL();
        const dataFileReadyCrop = ConvertDataUrlToFile(canvas, contentFile[0].name );

        setImgSrc(canvas);
        setContentFile(dataFileReadyCrop);

        // console.log("Canvass : ", canvas);
    };

    return (
        <MuiThemeProvider theme={theme}>
            <br />
            <br />
            <Paper elevation={0}>
                <Grid container 
                     direction="row"
                     justify="center"
                     alignItems="center"
                >  
                    <Grid item xs={4} style={{textAlign: 'center'}}></Grid>
                    <Grid item xs={4} style={{textAlign: 'center'}}>
                        <Breadcrumbs 
                            className={classes.breadcrumb}
                            separator={<NavigateNextIcon fontSize="small" />} 
                            aria-label="Breadcrumb">
                            <Typography color="inherit" className={classes.title}>Keanggotaan</Typography>
                            <Typography color="primary" className={classes.title}><b>Profil</b></Typography>
                            <Typography color="inherit" className={classes.title}>Struktur Perusahaan</Typography>
                        </Breadcrumbs>
                    </Grid>
                    <Grid item xs={4} style={{textAlign: 'center'}}></Grid>
                </Grid>
            </Paper>

            <br />
            <br />
            {
                isPick !== false && (

                    <Paper className={classes.root} elevation={0}>
                        <Grid container >  
                            <Grid item xs={12} style={{textAlign: 'center'}}>
                                <Typography variant="h4" className={classes.title}>
                                    <b>Upload Logo</b>
                                </Typography>
                                <br />
                                <br />
                                <br />
                                <div 
                                    onMouseOver={() => setHoverPict(true)}
                                    onMouseOut={() => setHoverPict(false)}
                                    
                                    {...getRootProps()}>

                                    <input
                                        {...getInputProps()} 

                                    />

                                    {/* <p>Drag 'n' drop some files here, or click to select files</p> */}

                                    <img src={isHover ? uploadIconColor :uploadIcon} alt="PictUploader" className={classes.media} />
                                    <br/>
                                    <br/>

                                    <Typography variant="h4" className={classes.titleDrag}>

                                        {!isDragActive && (isHover?'Click to Upload':'Drag and drop image to upload')}
                                        {isDragActive && !isDragReject && "Drop it here"}
                                        {isDragReject && "File type not accepted, sorry!"}
                                        {/* {isFileTooLarge && (<div className="text-danger mt-2" >File is too large.</div>)} */}
                                    </Typography>
                                    
                                    {
                                        isHover ? 
                                            <span style={{color:'#a8a8a8', fontFamily: 'Nunito'}}>(up to 1 Mb) </span> :
                                            
                                            <span style={{contentFilecolor:'#a8a8a8', fontFamily: 'Nunito'}}>or <font color='#67aded'><b style={{cursor: 'pointer',  fontFamily: 'Nunito'}}>browse</b></font> to choose a file</span>
                                    }

                                    <br/>
                                    <div style={{color:'#a8a8a8',  fontFamily: 'Nunito'}}>
                                        {
                                            isHover ? 
                                                <div>&nbsp;</div>:
                                                '(up to 1 Mb)'
                                        } 
                                    </div>
                                </div> 
                            </Grid>
                        </Grid>
                    </Paper>
                )
            }

            {
                isCropActive && 
                        <Grid container >  
                            <Grid item xs={12} style={{textAlign: 'center'}}>
                                <Typography variant="h4" className={classes.title}>
                                    Crop your Logo
                                </Typography>
                                <br />
                                <br />
                            </Grid>

                            <Grid item xs={4} style={{textAlign: 'center'}}></Grid>
                            <Grid item xs={4} style={{textAlign: 'center'}}>
                                <Paper className={classes.cardAvatar} elevation={2}>    
                                    <br />
                                    <br />
                                    <Typography variant="h5" className={classes.title}>
                                        Logo Perusahaan
                                    </Typography>
                                    <br />
                                    <br />
                                    <AvatarEditor
                                        ref={setEditorRef}
                                        image={imgSrc}
                                        width={250}
                                        height={250}
                                        border={0}
                                        color={[255, 255, 255, 0.6]}
                                        scale={scale}
                                        rotate={0}
                                        style={{borderRadius:'50%',border:'1px solid #e3e3e3'}}
                                    /> 

                                    <br/>
                                    <span style={{fontSize:'25px',fontWeight:'bold'}}>
                                        -
                                    </span> &nbsp;
                                    <input 
                                        id="typeinp" 
                                        type="range" 
                                        min="1" 
                                        max="5" 
                                        value={scale} 
                                        onChange={onChangeScale}
                                        step="0.1"
                                        className='slider-crop'

                                    />&nbsp;
                                        
                                    <span style={{fontSize:'20px',fontWeight:'bold'}}>+</span>
                                    <Typography
                                        variant='subtitle2' 
                                        className={classes.title}
                                    >
                                        Cropped Image : 100 x 100
                                    </Typography>
                                    <br />
                                    <br />

                                    <span 
                                        // variant='subtitle1' 
                                        className={classes.titleBack}
                                        onClick={handleBackInsideComponentCrop}
                                    >
                                        Kembali &nbsp;&nbsp;&nbsp;
                                    </span>
                                    <Button 
                                        onClick={handleCropAndContinue}
                                        variant='contained' 
                                        size='small' 
                                        className={classes.button}>
                                        Crop & continue
                                    </Button>
                                    <br />
                                    <br />
                                </Paper>
                                <br />
                                <br />
                                <br />
                                <br />
                            </Grid>
                            <Grid item xs={4} style={{textAlign: 'center'}}></Grid>
                        </Grid>
            }

            {
                isForm && 
                    <FormCompletionProfileGroup 
                        classes={classes} 
                        imgSrc={imgSrc} 
                        nameFile = { nameFile }    
                        contentFile = { contentFile }
                    />
            }

        </MuiThemeProvider>
    )

};

export default withStyles(styles)(ViewCompletionProfileGroup);
