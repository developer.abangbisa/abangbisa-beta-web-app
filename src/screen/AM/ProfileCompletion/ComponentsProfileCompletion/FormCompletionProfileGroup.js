import React, { useCallback, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link

} from '@material-ui/core';

import axios from 'axios';

import { useGetHttp } from '../../../../utilities-hook/useGetHttp';
import { URL_API } from '../../../../constants/config-api';
import Redirect from '../../../../utilities/Redirect';
import { ToDashboard, ToCompletionCompanyStructureQuestionONE} from '../../../../constants/config-redirect-url';

import ConvertDataUrlToFile from '../../../../utilities/ConvertDataUrlToFile';

const FormCompletionProfileGroup = props => {

    const { classes, imgSrc, nameFile, contentFile } = props;

    /*
        ```````````````
        GET USER TOKEN

        ```````````````
    */
    const [ userTokenState, setUserTokenState] = useState(undefined);

    useEffect(() => {

        /*
            ````````````````
            GET ACCESS TOKEN

            ````````````````
        */

        const userToken = localStorage.getItem('userToken');

        setUserTokenState(userToken)

    },[])

     /*

        `````````
        GET LABEL 

        `````````

    */
    
    const [ loading, fetchedData, setFetcedData ] = useGetHttp(URL_API + `/group/profile/patch`, []);
    // console.log("Fetched Data : ", fetchedData);
    
    const province = [];
    let updatedAt = undefined;

    if(fetchedData !== null && fetchedData !== undefined){

        //*
        updatedAt = fetchedData.updated_at;

        //**
        Object.getOwnPropertyNames(fetchedData.provinceOptions).forEach((val, idx, array) => {
            const data = {
                key: val,
                value: fetchedData.provinceOptions[val]
            };			
            province.push(data);

        });
    };

    /*

        ```````````````````````
        HANDLE FORM TEXT FIELD :

            - COMPANY NAME,

            - NPWP, 

            - ADDRESS,

            - VISSION & MISSION

        ```````````````````````
    */

    const [companyName, setCompanyName] = useState('');
    const handleChangeCompanyName = (e) => setCompanyName(e.target.value);

    const [npwp, setNpwp ] = useState('');
    const handleChangeNPWP = (e) => setNpwp(e.target.value);

    const [address, setAddress] = useState('');
    const handleAddress = (e) => setAddress(e.target.value);

    const [vissionMission, setVissionMission] = useState('');
    const handleVissionMission = (e) => setVissionMission(e.target.value)
        
   /*

       `````````````````````````
       Dropdown JUMLAH KARYAWAN

       `````````````````````````

   */

    const [employeeTotal, setEmployeeTotal] = useState({
        
            employee: '1 - 10'
    }); 

    const handleChangeDropdownJumlahKaryawan = name => event => {

        setEmployeeTotal({ ...employeeTotal, [name]: event.target.value });
    };  


    /*

       `````````````````````````
       Dropdown COUNTRY

       `````````````````````````

    */
    
    
    const [countryList, setCountryList] = useState({
    
        country: 'Indonesia'
    })  

    const handleChangeDropdownCountry = name => e => {

        setCountryList({ ...countryList, [name]: e.target.value });
      
    };  

    /*

       `````````````````````````
       Dropdown PROVINCE

       `````````````````````````

    */
    const [provinceList, setProvinceList] = useState({
    
        province: ''

    });
    
    const handleChangeDropdownProvince =  name => e => {

        setProvinceList({ ...provinceList, [name]: e.target.value });
        
        if(userTokenState !== undefined){

            console.log(e.target.value);

            const headers =  {
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Access-Control-Allow-Origin': '*',
                'crossorigin':true,
                'crossDomain': true,
                'Authorization': 'bearer ' + userTokenState
            };
        
            axios.defaults.headers.common = headers;
            axios
                .get(URL_API + `/group/profile/patch?options[filter][city_id][0][0]=province_id&options[filter][city_id][0][1]=eq&options[filter][city_id][0][2]=${e.target.value}`)
                .then((response) => {
                    
                    console.log("Original response : ",response);
                    
                    const listCity = [];
            
                    if(response.data.data !== undefined) {
    
                        Object.getOwnPropertyNames(response.data.data.cityOptions).forEach((val, idx, array) => {
                                const data = {
                                    key: val,
                                    value: response.data.data.cityOptions[val]
                                };			
                                listCity.push(data);
                            }
                        )
                    };  

                    setStateListCity(listCity);
                })
                .catch((error) => {
                    console.log("Error response : ",error.response);
                    
                });

        } else { console.log("Ga dapet User Token !")}
    }; 

    /*

       `````````````````````````
       Dropdown CITY

       `````````````````````````

    */
    const [stateListCity, setStateListCity ] = useState({

        city:''
    });
    
    const [ stateListCityV2, setStateListCityV2 ] = useState({ // State ini as TAMBAHAN TO SOLVE BUG !
        
        city: ''

    });

    const handleChangeDropdownCity = name => e => {
        
        setStateListCityV2({...stateListCityV2, [name]:e.target.value})
        console.log("City Selected : ", e.target.value);

        if(userTokenState !== undefined){

            const headers =  {
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Access-Control-Allow-Origin': '*',
                'crossorigin':true,
                'crossDomain': true,
                'Authorization': 'bearer ' + userTokenState
            };
        
            axios.defaults.headers.common = headers;
            axios
                
                .get(URL_API + `/group/profile/patch?options[filter][postal_code_id][0][0]=city_id&options[filter][postal_code_id][0][1]=eq&options[filter][postal_code_id][0][2]=${e.target.value}`)
                .then((response) => {
                    
                    console.log("Original response : ",response);
                    
                    const postalCodeList = [];
            
                    if(response.data.data !== undefined) {
    
                        Object.getOwnPropertyNames(response.data.data.postalCodeOptions).forEach((val, idx, array) => {
                                const data = {
                                    key: val,
                                    value: response.data.data.postalCodeOptions[val]
                                };			
                                postalCodeList.push(data);
                            }
                        );
    
                            
                        setPostalCodeState(postalCodeList);
                        
                    };  
                })
                .catch((error) => {
                    console.log("Error response : ",error.response);
                    
                });
        };
    };
    
    /*

       `````````````````````````
       Dropdown POSTAL CODE

       `````````````````````````

    */
    const [postalCode, setPostalCodeState ] = useState({

        code:''
    });

    const [ postalCodeV2, setPostalCodeStateV2 ] = useState({ // State ini as TAMBAHAN TO SOLVE BUG !
        city: ''
    }); 
    
    
    const handleChangeDropdownPostalCode = name => e => {
        
        setPostalCodeStateV2({...postalCodeV2, [name]:e.target.value})
        console.log("Postal Code Selected : ", e.target.value);

    };

    /*
       ```````````````````````````````
       HANDLE POST COMPLETION PROFILE

       ``````````````````````````````
    */
    const handlePOSTCompletionProfile = () => {
    
        console.log('contentFile', contentFile);

        // const file = new Blob([contentFile[0]], { type: 'image/png' }); //*Work
        // const file = new Blob([contentFile[0]]); //*Work
        const file = new Blob([contentFile]); //*Work

        // const file = new Blob([imgSrc]); //*Work
        let dataImage = new FormData();

        dataImage.append('_method', 'patch');

        dataImage.append('GroupProfile[name]', companyName);
        dataImage.append('GroupProfile[tax_id_number]', npwp);
        dataImage.append('GroupProfile[member_count]', employeeTotal !== undefined ? employeeTotal.employee : '1');
        dataImage.append('GroupProfile[address]', address);
        dataImage.append('GroupProfile[vision_and_mission]', vissionMission);
        dataImage.append('GroupProfile[photo_logo_icon]',file);      
        dataImage.append('GroupProfile[updated_at]', updatedAt);

        // let data = { 
        //     GroupProfile: {
        //         name: companyName,
        //         tax_id_number: npwp,
        //         member_count: employeeTotal !== undefined ? employeeTotal.employee : '1',
        //         address: address,
        //         vision_and_mission: vissionMission,
        //         updated_at: updatedAt,
        //         photo_logo_icon: dataImage
        //         // photo_logo_icon: contentFile
        //     },
        //     _method: 'patch'
        // };

        // dataImage.append(data);
        
        // console.log(`Data Completion Profile : `, data);

        if(userTokenState !== undefined && userTokenState !== null ){

            const header =  {       

                'Accept': "application/json",
                'Content-Type' : "multipart/form-data",
                'Authorization' : "bearer " + userTokenState,
            };
          
            axios.defaults.headers.common = header;    
      
            axios
                .post(URL_API + `/group/profile`, dataImage)
                .then(function(response){
      
                    console.log("Response Original Completion Profile: ", response)
      
                    if(response.status == 200 ){
      
                        Redirect(ToCompletionCompanyStructureQuestionONE);

                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    if(error.response == 500){
      
                      alert(error.response.data.message !== undefined ? error.response.data.message : "500")
      
                    } else {
      
                      alert("Ooops, something went wrong !")
                    }
                    
                })
      
          } else { console.log("No Access Token available!")};
    };      

    return (

        <Grid container > 
            <Grid item xs={12} style={{textAlign: 'center'}}>
                <Typography variant="h4" className={classes.title}>
                    Isi Data Perusahaan
                </Typography>
                <br />
                <br />
            </Grid> 

            <Grid item xs={4} style={{textAlign: 'center'}}>
                
                <img src={imgSrc} key={imgSrc} style={{borderRadius:'50%', width: 150, height: 150}}/> 
                
                <br />
                <br />
            </Grid>
            <Grid item xs={5} style={{textAlign: 'left'}}>
                <Typography variant="subtitle1" className={classes.title}>
                    Nama Perusahaan
                </Typography>
                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    onChange= {handleChangeCompanyName}
                    // onKeyDown={handleEnterPress}
                    variant="outlined"
                    fullWidth
                />
                <br />
                <br />

                <Typography variant="subtitle1" className={classes.title}>
                    NPWP
                </Typography>
                <TextField
                    id="outlined-bare"
                    className={classes.textField}
                    onChange= {handleChangeNPWP}
                    // onKeyDown={handleEnterPress}
                    variant="outlined"
                    fullWidth
                />

                <br />
                <br />
                <Typography variant="subtitle1" className={classes.title}>
                    Jumlah Karyawan
                </Typography>
                <TextField
                    id="outlined-select-currency"
                    select
                    // label="Pilih bahasa : "
                    className={classes.dropDown}
                    value={employeeTotal.employee}
                    onChange={handleChangeDropdownJumlahKaryawan('employee')}
                    SelectProps={{
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    // helperText="Please select your currency"
                    // margin="normal"
                    variant="outlined"
                    fullWidth
                >
                    {
                        employees.map (

                            option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            )
                        )
                    }

                </TextField>

                <br />
                <br />
                <Typography variant="subtitle1" className={classes.title}>
                    Alamat Lengkap
                </Typography>
                <TextField
                    onChange={handleAddress}
                    style={{marginTop: 0}}
                    id="outlined-multiline-static"
                    // label="Multiline"
                    multiline
                    rows="4"
                    // defaultValue="Default Value"
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    fullWidth
                />

                <br />
                <br />
                {/* <Typography variant="subtitle1" className={classes.title}>
                    Negara
                </Typography> */}
                <TextField
                    id="outlined-select-country"
                    select
                    label="Negara"
                    className={classes.dropDown}
                    value={countryList.country}
                    onChange={handleChangeDropdownCountry('country')}
                    SelectProps={{
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    variant="outlined"
                    fullWidth
                >
                    {
                        countries.map (

                            option => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            )
                        )
                    }

                </TextField> &nbsp;
                
                <TextField
                    style={{display: 'inline-block'}}
                    id="outlined-select-province"
                    select
                    label="Provinsi"
                    className={classes.dropDown}
                    value={provinceList.province}
                    onChange={handleChangeDropdownProvince('province')}
                    SelectProps={{
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    variant="outlined"
                    fullWidth
                >
                    {
                        province.length > 0 ? province.map (

                            (option, i) => (
                                <MenuItem key={i} value={option.key}>
                                    {option.value}
                                </MenuItem>
                            )
                        ) : (
                            <MenuItem value="Pilih Provinsi">
                                    Pilih Provinsi : 
                            </MenuItem>
                        )
                    }

                </TextField> &nbsp; 

                <TextField
                    style={{display: 'inline-block'}}
                    id="outlined-select-city"
                    select
                    label="City"
                    className={classes.dropDown}
                    // value={stateListCity.city !== undefined ? stateListCity : 'Kota'}
                    value={stateListCityV2.city}
                    onChange={handleChangeDropdownCity('city')}
                    SelectProps={{
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    variant="outlined"
                    fullWidth
                    
                >
                    {
                        stateListCity.city !== '' ? stateListCity.map (
                        // listCity.city !== '' ? listCity.map (

                            (option, i) => (
                                <MenuItem key={i} value={option.key}>
                                    {option.value}
                                </MenuItem>
                            )
                        ) : (
                            <MenuItem value="Pilih Kota">
                                    Pilih Kota : 
                            </MenuItem>
                        )
                    }

                </TextField>  &nbsp; 

                <TextField
                    style={{display: 'inline-block'}}
                    id="outlined-select-postalcode"
                    select
                    label="Kode Pos"
                    className={classes.dropDown}
                    // value={stateListCity.city !== undefined ? stateListCity : 'Kota'}
                    value={postalCodeV2.code}
                    onChange={handleChangeDropdownPostalCode('code')}
                    SelectProps={{
                        MenuProps: {
                            className: classes.menu,
                        },
                    }}
                    variant="outlined"
                    fullWidth
                    
                >
                    {
                        postalCode.code !== '' ? postalCode.map (
                        // listCity.city !== '' ? listCity.map (

                            (option, i) => (
                                <MenuItem key={i} value={option.key}>
                                    {option.value}
                                </MenuItem>
                            )
                        ) : (
                            <MenuItem value="Pilih Kode Pos">
                                    Pilih Kode Pos : 
                            </MenuItem>
                        )
                    }

                </TextField> 


                <br />
                <br />
                <Typography variant="subtitle1" className={classes.title}>
                    Visi & Misi
                </Typography>
                <TextField
                    onChange={handleVissionMission}
                    style={{marginTop: 0}}
                    id="outlined-multiline-static-visi"
                    // label="Multiline"
                    multiline
                    rows="4"
                    // defaultValue="Default Value"
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    fullWidth
                />

                <br />
                <br />
                {/* <span 
                    // variant='subtitle1' 
                    className={classes.titleBack}
                    // onClick={handleBackInsideComponentCrop}
                    onClick={() => Redirect(ToDashboard)}
                >
                    Lewati &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </span> */}
                <Button 
                    // onClick={() => Redirect(ToCompletionCompanyStructure)}
                    onClick={handlePOSTCompletionProfile}
                    variant='contained' 
                    size='medium' 
                    className={classes.button}>
                    Selanjutnya
                </Button>

                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </Grid>
            <Grid item xs={3} style={{textAlign: 'center'}}>

            </Grid>
        </Grid>
    )
}

export default FormCompletionProfileGroup;



const employees = [
    {
      value: '1',
      label: '1 - 10',
      
    },
    {
      value: '11',
      label: '11 - 50',
      
    },
    {
      value: '51',
      label: '51 - 100',
    },
    {
        value: '101',
        label: '101 - 500',
    },
    {
        value: '500',
        label: '500',
    },
  ];

const countries = [
    {
        value: 'Indonesia',
        label: 'Indonesia'
    }
]