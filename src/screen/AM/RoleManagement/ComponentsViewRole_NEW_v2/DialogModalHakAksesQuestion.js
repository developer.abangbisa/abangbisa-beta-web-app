import React, { Component, useEffect, useState } from "react";
import axios from 'axios'; 
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton, Radio, FormLabel
    
} from '@material-ui/core';

import DialogModalHakAksesQuestionDataSensitif from './DialogModalHakAksesQuestionDataSensitif';

const DialogModalHakAksesQuestion = props => {

    const { 
        
        classes, 
        isModalQuestionHakAkses, 
        setModalQuestionHakAkses, 
        selectedModule, 
        

        userTokenState,
        nameRoleState,
        updatedAt,
        idRoleState,

        // setDisabledTabPengaturan,
        setModalTambahHakAkses

    } = props;

    /*
        ````````````````````
        HANDLE RADIO BUTTON 

        ````````````````````
        
    */
    
    const [ hakAksesViewIDState, setHakViewIDState ] = useState('');
    
    let hakAksesViewID = undefined;
    
    const [selectedValue, setSelectedValue] = useState('');

    const handleChangeRadioButton = (event) => {

        setSelectedValue(event.target.value);


        if(event.target.value == 'view'){
            
            console.log("VIEW - Question_01");

            console.log("selectedModule : ", selectedModule);

            if(selectedModule !== undefined){
                if(selectedModule.embedded.child.length > 0){
    
                    hakAksesViewID = selectedModule.embedded.child.map(item => {
                        
                        return item.name == event.target.value ? item.id : null
                    })
                };
            };

            console.log("hakAkses id view: ", hakAksesViewID[3]);
                        
        }; 

        if(hakAksesViewID !== undefined){

            setHakViewIDState( hakAksesViewID[3])
        };
        
        if(event.target.value == 'Tidak'){
            
            console.log("User choose 'Tidak' ! ");
            setHakViewIDState('')
            // setModalQuestionHakAkses(false);

        };
    };



    


    /*
        ````````````````````````````````````````
        HANDLE DIALOG MODAL UNTUK DATA SENSITIF

        ````````````````````````````````````````

    */

    const [ modalQuestionDataSensitif, setModalQuestionDataSensitif ] = useState(false);


    return (
        <div>
            <Dialog
                open={isModalQuestionHakAkses}
                onClose={() => setModalQuestionHakAkses(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                    <Typography variant='subtitle1' className={classes.title}>
                        <b>Tambah Hak Akses</b>
                    </Typography>
                </DialogTitle>
                <DialogContent>

                    <Typography variant='subtitle2' className={classes.title}>
                        <b>Apakah <i>Role</i> ini memiliki hak akses untuk melihat data ?</b>
                    </Typography>
                    
                    <br />
                    <FormControlLabel 
                        style={{fontFamily: 'Nunito'}}
                        label="Ya" 
                        control={
                            <Radio 
                                checked={selectedValue === 'view'}
                                onChange={handleChangeRadioButton}
                                value="view"
                                name='answer-ya'
                                label='Ya'
                                labelPlacement="end"
                            />
                        } 
                    />
                    
                    <br />
                    <FormControlLabel 
                        style={{fontFamily: 'Nunito'}}
                        label="Tidak" 
                        control={
                            <Radio 
                                checked={selectedValue === 'Tidak'}
                                onChange={handleChangeRadioButton}
                                value="Tidak"
                                name='answer-no'
                                label='Tidak'
                            />
                        } 

                    />
                    <DialogContentText id="alert-dialog-description">
                    
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>

                    <Button 
                        onClick={() => setModalQuestionHakAkses(false)} 
                        variant='outlined' 
                        size='small'
                        className={classes.buttonModalCancel}    
                    >
                        Batal
                    </Button>

                    <Button 
                        variant='contained' 
                        color="primary" 
                        size='small'
                        className={classes.button}
                        style={{marginRight: 24}}
                        onClick={() => setModalQuestionDataSensitif(true)}
                    >  
                        Lanjut 
                    </Button>
                </DialogActions>
                <br />
                <br />
            </Dialog>

            <DialogModalHakAksesQuestionDataSensitif 
                classes={classes} 
                hakAksesViewID={ hakAksesViewIDState}
                setModalQuestionDataSensitif = {setModalQuestionDataSensitif}
                modalQuestionDataSensitif = {modalQuestionDataSensitif}
                selectedModule={selectedModule}


                userTokenState={userTokenState}
                nameRoleState={nameRoleState}
                updatedAt={updatedAt}
                idRoleState={idRoleState}

                // setDisabledTabPengaturan= {setDisabledTabPengaturan}
                setModalQuestionHakAkses={setModalQuestionHakAkses}
                setModalTambahHakAkses={setModalTambahHakAkses}


            />
        </div>
    );
};

export default DialogModalHakAksesQuestion;


/*

    ```````````````````````
    - 7WD BELOM ADA ISI-NYA

    - SELF BELOM ADA ISI-NYA

    - View Downward Sensitive Data
*/