import { green, amber } from '@material-ui/core/colors';

export const styles = theme => ({

    root: {
      
        width: '100%',
        marginTop: 1 * 3
  
    },
   
    button: {
        // width: '503px',
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'
    },
    buttonModal: {
            
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        color: 'white',
        textTransform: 'capitalize',
    },

    table: {

        minWidth: 1020,
        // marginLeft: theme.spacing(1),
        // marginRight: theme.spacing(1),
        // textAlign: 'center'

    },
    tableWrapper: {

        overflowX: 'auto',

    },
    iconPreviewGrid: {

        // marginTop: 14,
        marginTop:theme.spacing(1),
        width: 20,
        height: 20,
        cursor: 'pointer',
        marginRight: theme.spacing(7),
        marginLeft: theme.spacing(1),
        marginBottom: theme.spacing(0.2)

    },
    iconPreviewList: {

        // marginTop: 14,
        marginTop: theme.spacing(1),
        width: 24,
        height: 24,
        
    },
    iconFilter: {

        marginLeft: 24,
        marginTop: 17,
        width: 20,
        height: 20,
        cursor: 'pointer'

    },
    paper : {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
    },
    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
    
        padding: 10,
    },
    divider: {
    
        width: 1,
        height: 28,
        margin: 4,
    },
    avatar: {
        width: 14,
        height: 14,
        
    },
    detailButton: {

        backgroundColor: 'cyan'
    },
    iconDone: {
        margin: 1 * 2,
        color: '#16e616'
    },

    iconNonActive: {
        margin: 1 * 2,
        color: 'grey'
    },
    iconDoneHover: {
        margin: 1 * 2,
        '&:hover': {
        color: green[800],
        },
    },

    title: {
        fontFamily: 'Nunito'
    },
    avatarJumlahRole: {
        backgroundColor: '#d1354a', 
        fontSize: 14, 
        width: 20, 
        height: 20,
        marginLeft: theme.spacing(1)
    },

    /*
        `````````````````````````
        DIALOG MODAL TAMBAH ROLE 

        `````````````````````````
    */
    textField: {
        minWidth: 425   
    },
    titleLabelNamaRole: {
        fontFamily: 'Nunito',
        marginBottom: theme.spacing(1)
    },

    /*
        `````````````````````````
        DIALOG MODAL RESPONSE 400

        `````````````````````````
    */
    media: {
        height: 80,
        width: 80,
        // marginLeft: theme.spacing(7)
    },

    /*
        `````````````````````
        SNACKBAR RESPONSE 200

        `````````````````````
    */
    warning: {

        backgroundColor: amber[700],
    },

    success: {

        backgroundColor: green[500],
    },
    close: {
        // padding: theme.spacing.unit / 2,
        padding: 7 / 2,
    },
    
});
