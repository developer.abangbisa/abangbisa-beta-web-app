import React, {useState} from 'react';
import axios from 'axios';
import classNames from 'classnames';
import { makeStyles, useTheme } from '@material-ui/styles';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Chip, Checkbox, Select, ListItemText, FormControlLabel, Input, Radio, InputLabel, List, ListItem, Typography, Grid} from '@material-ui/core';

import { URL_API } from '../../../../constants/config-api';

const ListStructureOrgRole = props => {

    const { classes, isLockedStatusState } = props;
    
    const [selectedValue, setSelectedValue] = useState('Nanti');

    const [listDataMasterRole, setDataMasterRole ] = useState();    
    
    function callListMasterRole(string){

        console.log(`User choose ${string} !`);

        const userToken = localStorage.getItem('userToken');

        if(userToken !== undefined){

            const abortController = new AbortController();
            const signal = abortController.signal;
        
            const header =  {     
        
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    
                
            axios
                .get(URL_API + `/account-management/master-role/create`, { signal: signal })
                .then(function(response){
                  
                    let copyData = {...response.data.data};
                    // console.log("Copy Data Master Role : ", copyData)

                    /*
                        ``````````````````````````````````````````````````````````
                        Play with 'unitCollection'
                        ```````````````````````````````````````````````````````````
                    */
                       const inisiateUnitCollection = {

                            unitCollection : []
                        };

                        if(copyData.masterStructureUnitCollections.length > 0){

                            copyData.masterStructureUnitCollections.map(unit => {

                                /* work 1 */
                                inisiateUnitCollection.unitCollection.push(unit);

                            })
                        };   

                     /*
                        ````````````````````````````````
                        Play with 'masterStructureUnitTypeCollections'

                        ````````````````````````````````
                    */

                    const inisiateStructureUnitTypeCollection = {

                        unitTypeCollection : [],
                    };

                    if(copyData.masterStructureUnitTypeCollections.length > 0){

                        copyData.masterStructureUnitTypeCollections.map(unitType => {

                            // console.log('inisiateUnitCollection : ', inisiateUnitCollection);
                            
                            /*work2 */
                            let result = inisiateUnitCollection.unitCollection.filter(function(unit) {
                                return unit.structure_unit_type_id === unitType.id;
                            });
                           

                            /* work1*/
                            let dataReal = {
                                ...unitType,
                                unitCollection: result,
                                
                            };
                            
                            inisiateStructureUnitTypeCollection.unitTypeCollection.push(dataReal);
                        })
                    };
                    console.log(inisiateStructureUnitTypeCollection);
                    
                    setDataMasterRole(inisiateStructureUnitTypeCollection)
                 
                    
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                    // setFetchedData(error.response);
                    // setIsLoading(false);
                })
        
                //CLEAN UP
                return function cleanup() {
                    abortController.abort();
                };

        } else { console.log("Ooops, token is something went wrong !")}

    };

    function handleChangeRadioButton(event) {

        setSelectedValue(event.target.value);

        if(event.target.value == 'Ya'){
            
            callListMasterRole(event.target.value)

        } else {
            console.log("User choose 'Nanti' ! ")
        }
    }

    return (
            <div>
                <Grid  
                    container
                    spacing={16}
                    direction="row"
                    justify="center"
                    alignItems="center"
                >   
                 <Grid item sm={12}>
                    <Typography variant='h6' className={classes.titleBerikanKetentuan}>
                        Berikan Ketentuan :
                    </Typography>

                    <Typography variant='subtitle' className={classes.titleLabelNamaRole}>
                        Berikan hak akses ini kepada struktur organisasi tertentu ?
                        <Radio 
                            checked={selectedValue === 'Nanti'}
                            onChange={handleChangeRadioButton}
                            value="Nanti"
                            name='answer-next-time'
                            disabled= {isLockedStatusState == 1 ? true : false}
                        />
                        <Radio 
                            checked={selectedValue === 'Ya'}
                            onChange={handleChangeRadioButton}
                            value="Ya"
                            name='answer-yes'
                            disabled= {isLockedStatusState == 1 ? true : false}
                        />
                    </Typography>
                </Grid>
            </Grid>

            <Grid  
                container
                spacing={16}
                direction="row"
                justify="center"
                alignItems="center"
            >   
                <Grid item sm={12}>
                    
                    {
                        listDataMasterRole !== undefined && listDataMasterRole.unitTypeCollection.length > 0 ? listDataMasterRole.unitTypeCollection.map((collection, index) => {

                            return (
                                
                                <List component="div" disablePadding key={index}>
                                    <ListItem className={classes.nested}>
                                    
                                        <ListItemText 
                                            inset 
                                            primary={
                                                <Typography type="collectionname" style={{ color: '#707070', fontFamily: 'Nunito' }}>
                                                    <b>{collection.name}</b> 
                                                    <span style={{color: '#df383e', marginLeft: 7, fontSize: 11 }}><b>Pilih semua</b></span>
                                                    <br />
                                                    <br />

                                                    {
                                                        collection.unitCollection.length > 0 ? collection.unitCollection.map((unitWew, indexUnit) => {
                                                            return <Chip 
                                                                        key={indexUnit} 
                                                                        label={ unitWew.name} 
                                                                        className={classes.chip} 
                                                                        variant="outlined" 
                                                                        onClick={() => console.log("Chip clicked !")}
                                                                    />
                                                        }) : null
                                                    }
                                                </Typography>
                                            } 
                                        />

                                    </ListItem>
                                </List>

                            )
                        }): null
                    }
                    
                </Grid>
            </Grid>
            </div>

        )
    };
    
    
    // export default withStyles(styles) (ListStructureOrgRole);
    export default ListStructureOrgRole;
    // <Chip 
    //     // key={indexAction} 
    //     // label={ actionComponent.name} 
    //     // className={classes.chip} 
    //     // variant="outlined" 
    //     label='Test'
    //     onClick={() => console.log("Chip clicked !")}
    // />