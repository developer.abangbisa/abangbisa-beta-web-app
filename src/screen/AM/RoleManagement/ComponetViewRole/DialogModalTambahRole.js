import React, { Component, useEffect, useState } from "react";
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton
    
} from '@material-ui/core';
import axios from 'axios';

import PictInfo from '../../../../assets/images/icon-info-24px.svg';
import Redirect from '../../../../utilities/Redirect';
import {ToDashboard, ToOTP, ToRoleDetail} from '../../../../constants/config-redirect-url';

import { useGetHttp } from '../Hook/useGetHttp';
import { URL_API } from '../../../../constants/config-api';

const DialogModalTambahRole = props => {

    const {classes, isModalTambahRole, setModalTambahRole, data, setFetchedData } = props;

    // console.log("Props v1 : ", props);

    /* 
    
      `````````````````````````
      Get List Label FORM Role

      `````````````````````````
      
    */
    const [loading, fetchedLabel, setFetchedDataLabel ] = useGetHttp(URL_API + `/account-management/master-role/create`, []);
    // console.log("List Label Role : ", fetchedLabel);

    let idprivilegeSetCollections = {};

    if(fetchedLabel !== null){
        if(fetchedLabel !== undefined){
            if(fetchedLabel.privilegeSetCollections !== undefined){

                if(fetchedLabel.privilegeSetCollections.length > 0){
                    fetchedLabel.privilegeSetCollections.map((item, i) => {
        
                        // console.log("Item : ", item);
        
                        if(item.embedded !== null){
                            if(item.embedded.descendant.length == 0){
                                // console.log("Id Item : ", item.id)
                                idprivilegeSetCollections = item.id;
                                //const idprivilegeSetCollections = idprivilegeSetCollections;
                                // console.log("idprivilegeSetCollections : ", idprivilegeSetCollections);

                            };
                        };
                    })
                };
            };
        };
    };
    
    /* 
    
      ```````````````````
      HANDLE TAMBAH ROLE 

      ```````````````````
      
    */
    const [nameRole, setNameRole] = useState('');
    const handleChange = (e) => {

        setNameRole(e.target.value)
    };
   
    const handleTambahRole = () => {

        setModalTambahRole(false);

        console.log("idprivilegeSetCollections : ", idprivilegeSetCollections);

        const userToken = localStorage.getItem('userToken');

        let data = {

            MasterRole: {
                name: nameRole !== '' ? nameRole : 'DEFAULT',
                // is_locked: 0,
                
            },
            // PrivilegeSet: [idprivilegeSetCollections]

        };

            
        if(userToken !== undefined && data !== null && data !== undefined){
        
            const header =  {   

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            // console.log("Data POST : ", data);
            // console.log("Props v2: ", props);

            axios
                .post(URL_API + '/account-management/master-role', data)
                .then(function(response){

                    console.log("Response Original : ", response);

                    if(response.status == 200){
                        
                        localStorage.setItem('idRole', response.data.data.id);
                        localStorage.setItem('nameRole', response.data.data.name)
                        Redirect(ToRoleDetail) 
                  
                        // window.location.reload();
                        // setResponse200(true);

                        // if(setFetchedData !== undefined){

                        //     setFetchedData([...data, response.data.data]);
                        // }

                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);

                    if(error.response !== undefined){

                        setModalTambahRole(false);
                        setResponseError400(true);
                    };
                    
                    if(error.response.status == 400){

                        setModalTambahRole(false);
                        setResponseError400(true);
                    };
                })

        } else { console.log("No Access Token available!")};

    };

    /* 
    
      ``````````````````````````
      DIALOG MODAL RESPONSE 400

      ``````````````````````````
      
    */
    const [ isResponseError400, setResponseError400 ] = useState(false);

    /* 
    
      ```````````````````
      DIALOG SNACKBAR 200

      ```````````````````
      
    */
    const [ isResponse200, setResponse200 ] = useState(false);

    return (
        <div>

            <Dialog
                open={isModalTambahRole}
                onClose={() => setModalTambahRole(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                    <Typography variant='h6' className={classes.title}>
                        <b>Tambah <i>Role</i> Baru</b>
                    </Typography>
                </DialogTitle>
                <DialogContent>
                    <Typography variant='subtitle2' className={classes.titleLabelNamaRole}>
                        <b>Nama <i>Role</i></b>
                    </Typography>
                    <TextField
                        id="outlined-bare"
                        className={classes.textField}
                        onChange= {handleChange}
                        // color='primary'
                        // onKeyDown={handleEnterPress}
                        variant="outlined"
                        fullWidth
                    />
                    <DialogContentText id="alert-dialog-description">
                        <Typography variant='h6' className={classes.title}>
                            
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                    <Button 
                        variant='contained' 
                        onClick={handleTambahRole} 
                        // color="primary" 
                        // size='small'
                        className={classes.buttonModal}
                        // inputProp={{color: 'cyan'}}
                    >  
                        Tambah
                    </Button>
                </DialogActions>
                <br />
            </Dialog>


            {/* 
                ``````````````````
                INFO RESPONSE 400

                ``````````````````
            */}


            <Dialog
                open={isResponseError400}
                onClose={() => setResponseError400(false)}
                aria-labelledby="alert-dialog-title-400"
                aria-describedby="alert-dialog-description-400"
            >
                <DialogTitle id="alert-dialog-title-400" style={{textAlign: "center"}}>
                    <img src={PictInfo} className={classes.media} alt="info-icon" />  
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description-400">
                        <Typography variant='h6' className={classes.title}>
                            Oops, something went wrong
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                     <Button 
                        variant='contained' 
                        onClick={() => setResponseError400(false)} 
                        color="primary" 
                        size='small'
                        className={classes.buttonModal}
                    >  
                        Silahkan coba lagi
                    </Button>
                </DialogActions>
                <br />
                <br />
            </Dialog>

            {/*  
                ````````````
                RESPONSE 200 
                
                ````````````
            */}
              <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={isResponse200}
                autoHideDuration={6000}
                onClose={() => setResponse200(false)}
                ContentProps={{
                    'aria-describedby': 'message-id',
                    classes: {
                        root: classes.success
                    }

                }}
                message={
                    <span id="message-id">
                        Berhasil membuat <i>Role</i> baru !
                    </span>
                }
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        className={classes.close}
                        onClick={() => setResponse200(false)}
                    >
                        <i className="material-icons">
                                done
                        </i>
                    </IconButton>,
                ]}
            />
        </div>
    )
};

export default DialogModalTambahRole;

  