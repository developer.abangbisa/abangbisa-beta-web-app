import React, { Component, useEffect, useState } from "react";
import axios from 'axios'; 
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, 
    Button, CircularProgress, Card, CardActionArea, CardMedia, CardContent, CardActions, 
    FormControl, MenuItem,Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
    Snackbar, IconButton, Radio, FormLabel
    
} from '@material-ui/core';

import PictInfo from '../../../../assets/images/icon-info-24px.svg';
import Redirect from '../../../../utilities/Redirect';
import {ToDashboard, ToOTP} from '../../../../constants/config-redirect-url';

import { useGetHttp } from '../Hook/useGetHttp';
import { URL_API } from '../../../../constants/config-api';


const DialogModalTambahHakAkses_v2 = props => {

    const { classes, isModalTambahHakAkses, setModalTambahHakAkses } = props;


    /*
        ``````````````````````
        Dropdown

        ``````````````````````

    */

    //*

    let dataDummy = {

        descendent: []
    };
    
    const [selectedModule, setSelectedModule ] = useState(dataDummy);
    
    //**
    const [moduls, setModulePrivilage] = useState({
        
        privilage: ''
    });   

    //***
    const handleChangeDropdown = name => event => {

        setModulePrivilage({ ...moduls, [name]: event.target.value });

        console.log("Value Dropdown : ", event.target.value);

        setSelectedModule(event.target.value);

        //** clear in DROP DOWN
        setListValuePaper({descendentSingle: []});

        //*** Set enable button
        if(event.target.value !== undefined){

            if(event.target.value.label == 'All'){
    
                setEnabledButton(true);

                localStorage.setItem('modulHakAkses', event.target.value.label )


            } else {
                setEnabledButton(false);
                localStorage.setItem('modulHakAkses', event.target.value.label )
            }
        };

    };

    /*
        ````````````````````
        HANDLE RADIO BUTTON 

        ````````````````````

    */

    const [selectedValue, setSelectedValue] = useState('Ya');

    const handleChangeRadioButton = (event) => {

        setSelectedValue(event.target.value);

        if(event.target.value == 'Ya'){
            
            console.log("Ya");
            
            // callListMasterRole(event.target.value)

        } else {
            console.log("User choose 'Tidak' ! ");
            setModalTambahHakAkses(false);

        }
    };

    return (
    
        <Dialog
            open={isModalTambahHakAkses}
            onClose={() => setModalTambahHakAkses(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Tambah Hak Akses</b>
            </Typography>
        </DialogTitle>
        <DialogContent>

            <Typography variant='subtitle2' className={classes.title}>
                <b>Apakah <i>Role</i> ini memiliki hak akses untuk melihat data ?</b>
            </Typography>
              
            <br />
            <FormControlLabel 
                style={{fontFamily: 'Nunito'}}
                label="Ya" 
                control={
                    <Radio 
                        checked={selectedValue === 'Ya'}
                        onChange={handleChangeRadioButton}
                        value="Ya"
                        name='answer-ya'
                        label='Ya'
                        labelPlacement="end"
                    />
                } 
            />
            
            <br />
            <FormControlLabel 
                style={{fontFamily: 'Nunito'}}
                label="Tidak" 
                control={
                    <Radio 
                        checked={selectedValue === 'Tidak'}
                        onChange={handleChangeRadioButton}
                        value="Tidak"
                        name='answer-no'
                        label='Tidak'
                    />
                } 
            />

            <DialogContentText id="alert-dialog-description">
               
            </DialogContentText>
        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>

            <Button 
                onClick={() => setModalTambahHakAkses(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button>

            <Button 
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.button}
                style={{marginRight: 24}}
            >  
                Lanjut 
            </Button>
        </DialogActions>
        <br />
        <br />
    </Dialog>
    );  
};

export default DialogModalTambahHakAkses_v2;


const modulePrivilage = [

    {
        value: 'a0b8197e-c1e4-479f-93a3-5340f5a1935e',
        label: 'All',
    },
    {
        value: 'a4ee2739-d7d9-486e-bb27-4d3646e9885a',
        label: '7WD',
    },

    {
        value: 'fddcf257-29ba-4950-a549-f74726a73d9e',
        label: 'View Downward Sensitive Data'
    },

    {
        value: 'efef19fb-4e08-46f4-a8f7-a5ed2214d064',
        label: 'Self'
    },
    {
        value: '38660ffc-61b6-4539-a133-5afc348c0b9c',
        label: 'Human Resource ( Employee, Position )',
    },
    {
        value: 'ae9dec73-24fc-4e95-828d-abc869025821',
        label: 'Account Management (Group)',
       
    },
    {
        value: '32c005e7-5387-42b2-a8ff-c53014b0b3d4',
        label: 'Account Management (User)', 
    },
  ];
