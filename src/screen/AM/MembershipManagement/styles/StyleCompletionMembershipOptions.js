
export const styles = theme => ({

    root: {
        
        marginTop: theme.spacing(4),
        marginLeft: theme.spacing(4),
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(3),
        borderRadius: 2,
        

    },

    /*
        ``````````````````````
        STYLING PAKET TRIAL

        ````````````````````````
    */
    subRootTrial: {
        marginLeft: theme.spacing(15),
        marginRight: theme.spacing(12),
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        padding : 12,
        [theme.breakpoints.only('xs')]: {
    
            marginLeft: theme.spacing(2),
            marginRight: theme.spacing(2),
            padding : 8,
        },
    },

    title: {
        fontFamily: 'Nunito'
    },
    titleTrial: {
        fontFamily: 'Nunito',
        color: 'white',
        marginBottom: theme.spacing(8)
    },
    titleGratis: {
        fontFamily: 'Nunito',
        color: 'white',
    },
    titleIconChecklist: {
        
        fontFamily: 'Nunito',
        color: 'white'    
    },
    icon: {

        color: 'white',
        position: 'absolute'
    },
    button: {
        
        // width: '503px',
        height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginBottom: theme.spacing(4),
        color: 'white',
        textTransform: 'capitalize'
    },

    /*
        ``````````````````````
        STYLING PAKET BERBAYAR

        ````````````````````````
    */

    subRootBerbayar: {

        marginLeft: theme.spacing(15),
        marginRight: theme.spacing(12),
        backgroundColor: 'white',
        padding : 12,
        [theme.breakpoints.only('xs')]: {
    
            marginLeft: theme.spacing(2),
            marginRight: theme.spacing(2),
            padding : 8,
        },

    },
    titleBerbayar: {

        fontFamily: 'Nunito',
        color: 'black',
        marginBottom: theme.spacing(2)
    },
    buttonToggle: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        color:'white',
    },
    titlePotonganHarga: {
        
        fontFamily: 'Nunito',
        color: 'black'
    },
    titleStrikePrice: {

        fontFamily: 'Nunito',
        color: 'grey'
    },
    titlePrice: {

        fontFamily: 'Nunito',
        color: 'black',
        fontWeight: 'bold'
    },

     /* 
        ```````````````
        SLIDER MAHKOTA 
        
        ```````````````
    */
     sliderMahkota: {

        marginTop: theme.spacing(4.5),
        width: 300,
        textAlign: 'center',
        display: 'inline-block',
    },
    thumbIconWrapperMahkota: {

        backgroundColor: '#fff',
    },
    trackSliderMahkota: {

        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        padding: '2px',
        borderRadius: '10%',
    },
    thumbIconMahkota: {

        width: '20px',
        height: '20px',
        marginBottom: '5px',
        position: 'absolute'

    },

    /* 
        ````````````````````````
        FIGURE BOX PICT MAHKOTA 
        
        ````````````````````````
    */
    figureBoxPictMahkota: {

        width: 63,
        height: 63,
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 4,
        borderColor:'#f67070',
        display: 'inline-block',
        margin: 5,
        textAlign: 'center',
        cursor: 'pointer'
    },
    figurePictMahkota: {

        display: 'inline-block',
        margin: 5,
        textAlign: 'center'
    },
    iconFullUpdateMahkota: {

        color: 'black',
        position: 'absolute'
    },
    titleIconChecklistMahkota: {

        fontFamily: 'Nunito',
        color: 'black',
    },

    /*
        ```````````````````
        MODAL, BUTTON MODAL

        ```````````````````
    */
   buttonModal: {
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1)
    },
    media: {
        height: 80,
        width: 80,
        // marginLeft: theme.spacing(7)
    },

    breadcrumb: {
        marginLeft: theme.spacing(5)
    },
    buttonModalCancel: {
        fontFamily:'Nunito',
        textTransform: 'capitalize'
    },
    /* 
        ````````````````````````
        DIALOG MODAL UPGRADE
        
        ````````````````````````
    */
   imageBank: {
        width: 60,
        height: 21,
        // marginTop: theme.spacing(1)
        marginTop: 3,
        marginRight: theme.spacing(2)
    },
    listItemSecondaryActionDialogModal: {
        // paddingLeft: theme.spacing(6)
    },
    titleModal: {
        fontFamily: 'Nunito'
    },

    /* 
        ``````````````````````````````````
        DIALOG MODAL UPGRADE SAVE INVOICE
        
        ``````````````````````````````````
    */
     paperTigaDigitTerakhir: {

        // backgroundColor: 'cyan', 
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        padding : 10, 
        color: 'white'
    },
    pictInvoice: {
        width: 60,
        height: 53
    },
    // buttonProgress: {
    //     // color: red[500],
    //     color: 'white',
    //     position: 'absolute',
    //     top: '50%',
    //     left: '50%',
    //     marginTop: -12,
    //     marginLeft: -12,
    //   },


});