import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Icon, Breadcrumbs, Slider, IconButton, Hidden

} from '@material-ui/core'; 

import PictInfo from '../../../../assets/images/icon-info-24px.svg';


const DialogHubungiAdministrator = props => {

    const { 
            classes, 
            isResponseError, 
            setResponseError, 
            infoResponse400, 
            infoResponse422, 
            infoResponse404 

        } = props;

    return (

                
        <Dialog
            open={isResponseError}
            onClose={() => setResponseError(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <img src={PictInfo} className={classes.media} alt="info-icon" />  
        </DialogTitle>
        <DialogContent>
            <DialogContentText id="alert-dialog-description">
                <Typography variant='h6'>
                    {

                        infoResponse400 != '' ? infoResponse400 : 

                            (
                                (infoResponse422 != '') ? (infoResponse422) :  
                                (infoResponse404 != '') ? (infoResponse404) : null 
                            )

                    }
                </Typography>
                
            
            </DialogContentText>
        </DialogContent>
        <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
            <Button 
                variant='contained' 
                onClick={() => setResponseError(false)} 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Hubungi IT Administrator !
            </Button>
        </DialogActions>
        <br />
        <br />
        </Dialog>
    )

};

export default DialogHubungiAdministrator;
