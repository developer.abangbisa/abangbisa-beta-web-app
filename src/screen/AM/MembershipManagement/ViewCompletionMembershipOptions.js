import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Icon, Breadcrumbs, Slider, IconButton, Hidden

} from '@material-ui/core'; 
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import axios from 'axios';
import { fromJS } from 'immutable';

// import Slider from '@material-ui/lab/Slider';
import { PrettoSlider, AirbnbThumbComponent } from '../../../components/Pretto';

import DialogModalTransferOptionBerbayar from './ComponentsMembershipStatus/DialogModalTransferOptionBerbayar';

import DialogHubungiAdministrator from './ComponentsDialogModal/DialogHubungiAdministrator';

import IconSliderPacketMahkota from '../../../assets/images/Group_1745.png';
import PictPaketBasic from '../../../assets/images/SVG/Group_1655.svg';
import PictPaketSilver from '../../../assets/images/SVG/Group_1.svg';
import PictPaketGold from '../../../assets/images/SVG/Group_2.svg';
import PictPaketPlatinum from '../../../assets/images/SVG/Group_3.svg';
import PictInfo from '../../../assets/images/icon-info-24px.svg';
import Group_724 from '../../../assets/images/Group-724.png';

import  Redirect  from '../../../utilities/Redirect';
import { ToCompletionProfile } from '../../../constants/config-redirect-url';
import { styles } from './styles/StyleCompletionMembershipOptions';
import { URL_API } from '../../../constants/config-api';
const numeral = require('numeral');

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }       
});

const ViewCompletionMembershipOptions = props => {

    const { classes } = props;


    /*
        `````````````````````````````````````````````````````````````````````````
        COMPONENT DID MOUNT SPECIAL to delete data local storage "status_payment"

        ``````````````````````````````````````````````````````````````````````````
    */
    localStorage.removeItem('status_payment');
   
    /*
        ````````````````````
        COMPONENT DID MOUNT

        `````````````````````
    */
    const [ isResponseError, setResponseError ] = useState(false);
    const [ infoResponse422, setiInfoResponse422] = useState('');
    const [ infoResponse404, setInfoResponse404] = useState('');
    const [ infoResponse400, setInfoResponse400 ] = useState('');

    const [ userTokenState, setUserTokenState ] = useState(undefined);

    useEffect(() => {

        const urlParams = new URLSearchParams(window.location.search.substring(1));
		const currentUrl = urlParams.get('redirect');
        console.log("currentUrl : ", currentUrl);


        const data = {

			// redirect_url: 'http://115.124.65.74:4586/abangbisa-beta/api/v1/account-management/user/343362d7-cf4c-463e-a883-472cb9c1bbfe/verify-email/JQQKLzxb-277e4f4d9e0c6c51c61cb216235960eb-bkKDeuHQ'
			redirect_url: currentUrl
		};

		const headers =  {
			'Accept': "application/json",
			'Content-Type' : "application/json",
			'Access-Control-Allow-Origin': '*',
			'crossorigin':true,
			'crossDomain': true
		};

		axios.defaults.headers.common = headers;

		axios
			.get(data.redirect_url)
			.then((response) => {
					
				    // const immutableResponseOke = fromJS(response)
					console.log("Original response : ",response);
					
					if(response.status == 200){
                        localStorage.setItem('userToken',response.data.data.access_token )
                        setUserTokenState(response.data.data.access_token);

                        localStorage.setItem('status_user_login', JSON.stringify(response.data.data))
					};
			})
			.catch((error) => {

					console.log("Error response : ",error.response);
					const imutableError = fromJS(error.response);

					if(error.response !== undefined){

						if(imutableError.getIn(['status']) == 422){
							// console.log(error.response.data.info.message);
                            setResponseError(true);
                            setiInfoResponse422(imutableError.getIn(['data','info', 'message']));
						};
						
						if(imutableError.getIn(['status']) == 404){
                            
                            setResponseError(true);
                            setInfoResponse404("Oops, Something went wrong !");
						
						};
	
						if(imutableError.getIn(['status']) == 400){
                            
                            setResponseError(true);
                            setInfoResponse400("Oops, Something went wrong !");
						
						};

					} else {

                        setResponseError(true);
                        setInfoResponse400("Oops, Something went wrong !");
							
					}; 
			});
        
    }, []);

    /*
        ````````````````````````````````
        HANDLE TOGGLE BOX MAHKOTA PACKET, 

        ````````````````````````````````
    */
    let inisiatePacket = {
        id: 'b28f5f75-9836-40be-b762-7d577a115464',
        value: 'Basic',
        choose: 'Basic',
        image: PictPaketBasic,
        user: 1,
        price: 75000
    };

    const [activeBoxPict, setActiveBoxPict] = useState(inisiatePacket)

    const handleActiveBoxPict = (packet) => {

        console.log("Packet : ", packet);
        setActiveBoxPict(packet);

        if(packet.user == 1){

            setValueSliderMahkota(1);
        };  
        
        if(packet.user == 11){

            setValueSliderMahkota(11);
        };
        
        if(packet.user == 51){

            setValueSliderMahkota(51);
        }; 
        
        if(packet.user == 101){

            setValueSliderMahkota(101);
        }; 
 
    };

    /*
        ````````````````````````````````````
        HANDLE TOGGLE BUTTON DURATION MONTH

        ```````````````````````````
    */
    let inisiateDuration = {

        value: '1 Bulan',
        discount: 0,
        id: '59bef0c5-f5dc-423b-8fc6-9d5f11e618fc',
        duration: 1

    };

    const [activeButton, setActiveButton] = useState(inisiateDuration);
    const [nominalAfterDiscount, setNominalAfterDiscount] = useState(undefined);

    const handleActiveButtonDuration = (value) => {

        console.log("Value : ", value);
        setActiveButton(value);

        // setValueSliderMahkota(valueSliderMahkota)
        setValueSliderMahkota(1);

        let dummyBasic = {

            value: 'Basic',
            choose: 'Basic',
            image: PictPaketBasic,
            user: 1,
            price: 75000
        };
        
        setActiveBoxPict(dummyBasic);

    };

    useEffect(() => {

        //* BACKUP
        let nominalDiscount = (activeBoxPict.price * activeBoxPict.user ) * activeButton.discount/100;
        let nominalNormalPrice = activeBoxPict.price * activeBoxPict.user;

        // console.log("nominalDiscount : ", nominalDiscount);
        // console.log("nominalNormalPrice : ", nominalNormalPrice);

        const nominalPriceAfterSubtractionDiscount = nominalNormalPrice - nominalDiscount;
        // console.log("nominal after disocunt : ", nominalPriceAfterSubtractionDiscount);

        setNominalAfterDiscount(nominalPriceAfterSubtractionDiscount);        
    
    }, [activeButton, activeBoxPict])    

    /*
        `````````````````````````````````
        HANDLE SLIDER BOX PACKET MAHKOTA

        ``````````````````````````````````
    */

    const [valueSliderMahkota, setValueSliderMahkota ] = useState(1);

    const handleChangeSliderMahkota = (event, value) => {

        event.preventDefault();
        event.stopPropagation();

        // numeral(value).format('0,0')
        console.log("Value Slider : ",  numeral(value).format('0,0'));
        const rangeValueSlider = numeral(value).format('0,0');

        // let nominalDiscount = (activeBoxPict.price * activeBoxPict.user)*activeButton.discount/100; //*PErhitungan dengan baris kode ini: KURANG TEPAT !
        let nominalDiscount = (activeBoxPict.price * rangeValueSlider)*activeButton.discount/100;

        let nominalNormalPriceInSlider =  activeBoxPict.price * rangeValueSlider;//*BACKUP
        const nominalPriceAfterSubtractionDiscount = nominalNormalPriceInSlider - nominalDiscount;

        /*

            ```````````````````````````````````
            Important Dummy Data inside SLIDER

            ```````````````````````````````````
        */

       let dummyBasic = {

            value: 'Basic',
            choose: 'Basic',
            image: PictPaketBasic,
            user: 1,
            price: 75000
        };
        
        let dummySilver = {

            value: 'Silver',
            choose: 'Silver',
            image: PictPaketSilver,
            user: 11,
            price: 65000
        };

        let dummyGold = {

            value: 'Gold',
            choose: 'Gold',
            image: PictPaketGold,
            user: 51,
            price: 50000
        };

        let dummyPlatinum = {
        
            value: 'Platinum',
            choose: 'Platinum',
            image: PictPaketPlatinum,
            user: 101,
            price: 40000
        }

        if(rangeValueSlider < 11){
            
            // setActiveBoxPict({choose: 'Basic'})
            setActiveBoxPict(dummyBasic)
            setValueSliderMahkota(value);
            setNominalAfterDiscount(nominalPriceAfterSubtractionDiscount);

        } else if(rangeValueSlider < 51){

            setActiveBoxPict(dummySilver);
            setValueSliderMahkota(value);
            setNominalAfterDiscount(nominalPriceAfterSubtractionDiscount);

        } else if(rangeValueSlider < 101){

            setActiveBoxPict(dummyGold);
            setValueSliderMahkota(value);
            setNominalAfterDiscount(nominalPriceAfterSubtractionDiscount);

        } else {

            setActiveBoxPict(dummyPlatinum);
            setValueSliderMahkota(value);
            setNominalAfterDiscount(nominalPriceAfterSubtractionDiscount);
        };
    };



    /*     
        `````````````````````````
        MODAL TRIAL CONFIRMATION

        `````````````````````````
    */

    const [isModalTrialConfirmation, setModalTrialConfirmation] = useState(false);
    const handleModalTrialConfirmation = () => {

        if(userTokenState == undefined){

            const tokenUser = localStorage.getItem('userToken');
            setUserTokenState(tokenUser);
            setModalTrialConfirmation(true);

            // console.log("Yes, user token is undefined !");

        } else {

            setModalTrialConfirmation(true)
        };
    };

    

    /*

        `````````````````````````
        CONFRIM OKE TRIAL

        `````````````````````````
    */

    const [trialExpiryTimestamp, setTrialExpiryTimestamp] = useState(undefined);

    const handleTrialOke = () => {

        if(userTokenState !== undefined){

            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
                
            };
          
            axios.defaults.headers.common = header;    

            axios
                .put(URL_API + `/group/membership/trial`)
                .then(function(response){

                    // closeModalAnggota();
                    console.log("Response Original : ", response)// 2019-07-18 01:45:20

                    if(response.status == 200 ){

                        setTrialExpiryTimestamp(response.data.data.trial_expiry_timestamp);
                        Redirect(ToCompletionProfile);
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);
                    if(error.response !== undefined){

                        if(error.response.status == 422 ){
    
                            alert("Error 422 : " + error.response.statusText + " !");
                        };

                    } else {

                        alert('Oops, something went wrong !')
                    };

                    
                })

        } else { console.log("Tidak dapat user Token, why ?")}

    };

    /*
        ```````````````````````````````
        HANDLE MODAL UPGRADE/BERBAYAR

        ``````````````````````````````
    */

    const [paramsData, setParamsData] = useState();

    const [isBerbayar, setBerbayar] = useState(false);// GANTI KE setBerbayar

    const handleBuyNow = () => {

        setLoader(true);
        
        //*
        const userTokenConst = localStorage.getItem('userToken');
        setUserTokenState(userTokenConst);

        const header =  {     

            'Accept': "application/json",
            'Content-Type' : "application/json",
            'Authorization' : "bearer " + userTokenConst

        };
        
        axios.defaults.headers.common = header;    
            
        axios
            .get(URL_API + `/group/membership/premium`)
            .then(function(response){

                setLoader(false);

                console.log("Response Original : ", response);
                // setFetchedData(response.data.data);
                if(response.status == 200){
                    
                    let params = {
                        membership_type_id : activeBoxPict.id,
                        membership_duration_id: activeButton.id,
                        bank_id: 'c7333fd9-1c18-4613-870c-c8c9f24b52d3',
                        user_count: valueSliderMahkota > 1 ? valueSliderMahkota :  activeBoxPict.user,
                        updated_at: response.data.data.updated_at
                    };

                    console.log("Params : ", params);   
                    setParamsData(params); 
                    setBerbayar(true);

                    // setParamsData({...paramsData, updated_at: response.data.data.updated_at})
                };
            })
            .catch(function(error){
                
                setLoader(false);

                console.log("Error : ", error.response)
                if(error.response !== undefined){

                    if(error.response.status == 422){
                        alert(error.response.data.info.message + " !");
    
                    };
                    if(error.response.status == 500){
                        alert("Error 500 : " + error.response.statusText + " !");
    
                    };
                    
                } else {
                    
                    alert('Oops, something went wrong !')
                }

            });
    };

    /*
        ```````````````````````````
        HANDLE LOADER WHEN BERBAYAR

        ```````````````````````````
    */
    const [ loader, setLoader ] = useState(false);

    
    return (

        <MuiThemeProvider theme={theme}>

            <br />
            <br />
            <br />

            <Hidden only='xs'>
                <Paper elevation={0}>
                    <Grid container 
                        direction="row"
                        justify="center"
                        alignItems="center"
                        spacing={32}
                    >  
                        <Grid item xs={4}></Grid>
                        <Grid item xs={4}>
                            <Breadcrumbs 
                                className={classes.breadcrumb}
                                separator={<NavigateNextIcon fontSize="medium" />} 
                                aria-label="Breadcrumb">
                                
                                <Typography color="primary" className={classes.title}><b>Keanggotaan</b></Typography>
                                <Typography color="inherit" className={classes.title}>Profil</Typography>
                                <Typography color="inherit" className={classes.title}>Struktur Perusahaan</Typography>
                            </Breadcrumbs>
                        </Grid>
                        <Grid item xs={4} style={{textAlign: 'center'}}></Grid>
                    </Grid>
                </Paper>
            </Hidden>
            <br />
            <br />

            <Paper className={classes.root} elevation={0}>
                <Grid container 
                    // alignItems="flex-start"
                    // alignItems='center'
                    // justify='center'
                >  
                    <Grid item xs={12} style={{textAlign: 'center'}}>
                        <Typography variant="h5" className={classes.title}>
                            <b>Pilih Rencana</b>
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>

            <Grid container > 
                <Grid 
                    item 
                    // xs={6} 
                    xs={12} 
                    sm={6} 
                    style={{textAlign: 'center'}}
                >
                    <Paper className={classes.subRootTrial} elevation={4}>

                        <br />
                        <Typography variant="h6" className={classes.titleTrial}>
                            Trial
                        </Typography>

                        <Typography variant="h4" className={classes.titleGratis}>
                            GRATIS
                        </Typography>

                        <br />
                        <Typography variant="subtitle2" className={classes.titleTrial}>
                            Nikmati dan rasakan manfaat Aplikasi selama 1 bulan
                        </Typography>

                        
                        <Icon className={classes.icon} >
                            check_circle
                        </Icon>
                        <span className={classes.titleIconChecklist}>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Full Update
                        </span>

                        <br />
                        <br />
                        <Icon className={classes.icon} >
                            check_circle
                        </Icon>
                        <span className={classes.titleIconChecklist}>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tersedia 3 User
                        </span>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        
                        <Button 
                            // onClick={() => setModalTrialConfirmation(true)}
                            onClick={handleModalTrialConfirmation}
                            variant='outlined' className={classes.button} 
                            fullWidth
                        >
                            Mulai Trial
                        </Button>
                    </Paper>
                    <br />
                    <br />
                    <br />
                    <br />
                </Grid>

                <Grid 
                    item 
                    // xs={6}
                    xs={12} 
                    sm={6} 
                    style={{textAlign: 'center'}}
                >
                    <Paper className={classes.subRootBerbayar} elevation={4}>
                        
                        <br />
                        <Typography variant="h6" className={classes.titleBerbayar}>
                            Menyesuaikan Harga
                        </Typography>

                        { 
                            duration.length > 0 && duration.map((item, i) => {

                                return (
                                    <Button 
                                        key={i} 
                                        // onClick={ () => setActiveButton(item.value) }
                                        onClick={ () => handleActiveButtonDuration(item) }
                                        variant={activeButton.value === item.value ? 'contained' : 'outlined'}
                                        className={activeButton.value === item.value ? classes.buttonToggle : null}
                                        variant='outlined' 
                                        size='small' 
                                        style={{margin: 7}}
                                    >
                                        {item.value}
                                    </Button>
                                )
                            })
                        }

                        <br />
                        <br />
                        <Typography variant="caption" className={classes.titlePotonganHarga}>
                            {
                                activeButton.discount !== 0 ? (

                                    "Potongan " + activeButton.discount + "%"

                                    ) : null
                            }
                            {/* Potongan {activeButton.discount}% */}
                        </Typography>

                        <br />
                        <Typography variant="h6" className={classes.titleStrikePrice}>
                            {
                                 activeButton.discount !== 0 ? (

                                    //  <strike>Rp {numeral((activeBoxPict.price * valueSliderMahkota)).format('0,0')}</strike>
                                    <strike>Rp {numeral((activeBoxPict.price * valueSliderMahkota * activeButton.duration)).format('0,0')}</strike>

                                 ) : null
                            }
                        </Typography>

                        <Typography variant="h4" className={classes.titlePrice}>
                            <span style={{fontSize: 14}}>Rp</span> {numeral(nominalAfterDiscount * activeButton.duration).format('0,0')}
                        </Typography>
                        
                        <br />
                        <Typography variant="h6" className={classes.titleStrikePrice}>
                            {/* Basic 4 User */}
                            {activeBoxPict.choose} &nbsp;
                            {
                                valueSliderMahkota > 0 ? (
                                    numeral(valueSliderMahkota).format('0,0') + " User"

                                ) : 
                                (
                                    (activeBoxPict.user === 101) ? ("> 101 User") :  activeBoxPict.user + ' User'
                                    
                                )
                            }
                            {/* {activeBoxPict.user === 101 ? "> 101" : activeBoxPict.user} User */}
                        </Typography>

                        {/* 
                        <Slider
                            max={200}
                            min={1}
                            value={valueSliderMahkota}
                            aria-labelledby="slider-mahkota"
                            onChange={handleChangeSliderMahkota}
                            classes={{
                                root: classes.sliderMahkota,
                                thumbIconWrapper: classes.thumbIconWrapperMahkota,
                                track: classes.trackSliderMahkota,
                            }}
                            thumb={
                                <img
                                    alt="slider thumb icon"
                                    src={IconSliderPacketMahkota}
                                    className={classes.thumbIconMahkota}
                                />
                            }
                           
                        /> */}

                            <br />
                            <PrettoSlider 
                                valueLabelDisplay="auto" 
                                aria-label="slider-mahkota" 
                                // defaultValue={10} 
                                ThumbComponent = {
                                    AirbnbThumbComponent
                                }
                                max={200}
                                min={1}
                                value={valueSliderMahkota}
                                // defaultValue={valueSliderMahkota}
                                aria-labelledby="slider-mahkota"
                                onChange={handleChangeSliderMahkota}
                                style={{margin: 8}}
                                
                           
                            />
                            
                        <br />
                        <br />
                        
                        {
                            packets.length > 0 && packets.map((item, i) => {

                                return (
                                    
                                    <Box 
                                        key={i} 
                                        component="div" 
                                        m={1} 
                                        className={classes.figureBoxPictMahkota}
                                        onClick={() => handleActiveBoxPict(item)}
                                        // style={{backgroundColor: '#fdc9c8'}}
                                        style={{backgroundColor: activeBoxPict.value === item.value ? '#fdc9c8' : null}}
                                    >
                                        <figure  className={classes.figurePictMahkota}>
                                            <img 
                                                src={item.image} 
                                                styles={{height: 45, width: 45}} 
                                                alt={item.choose}
                                            />
                                            <figcaption style={{fontSize: 12}}>
                                                <b>{item.choose}</b>
                                            </figcaption>
                                        </figure>
                                        {/* {
                                            activeBoxPict.price === item.price && (

                                                <Typography variant='caption' className={classes.title} >
                                                    {numeral(item.price).format('0,0')} /User
                                                </Typography>
                                            )
                                        } */}

                                        {/* <Typography variant='caption' className={classes.title} style={{color:  activeBoxPict.price === item.price ? 'black' : 'white'}} >
                                            <b><i>Jumlah User : {item.user}</i></b>
                                        </Typography>
                                        <Typography variant='caption' className={classes.title} style={{color:  activeBoxPict.price === item.price ? 'black' : 'white'}} >
                                            <b><i>Rp {numeral(item.price).format('0,0')}</i></b>
                                        </Typography> */}



                                        <IconButton style={{
                                            color:  activeBoxPict.price === item.price ? '#fdc9c8' : 'white',
                                            borderColor: 'red',
                                            borderWidth: 1,

                                            
                                        }}>
                                            <i className='material-icons' style={{
                                                    fill: activeBoxPict.price === item.price ? 'red' : 'white'
                                            }}>
                                                info
                                            </i>
                                        </IconButton>
                                     
                                       

                                    </Box>
                                )
                            })
                        }
                        <br />
                        <br />
                        <Icon className={classes.iconFullUpdateMahkota} >
                            check_circle
                        </Icon>
                        <span className={classes.titleIconChecklistMahkota}>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Full Update
                        </span>
                        <br />
                        <br />
                        <Button 
                            onClick={handleBuyNow}
                            variant='outlined' 
                            className={classes.button} 
                            fullWidth
                        >
                            {
                                loader !== true ? 'Beli Sekarang' : (

                                    <CircularProgress size={20} style={{color: 'white'}} />
                                )
                            }
                            
                        </Button>
                    </Paper>
                    <br />
                    <br />
                    <br />
                    <br />
                </Grid>
            </Grid>
            


            {/*     
                ``````````````````````````````````
                DIALOG MODAL HUBUNGI ADMINISTRATOR

                ``````````````````````````````````
            */}

            <DialogHubungiAdministrator 
                classes = {classes}
                isResponseError = { isResponseError }
                setResponseError = { setResponseError }
                infoResponse400 = { infoResponse400}
                infoResponse422 = { infoResponse422 }
                infoResponse404 = { infoResponse404 }
            />

            {/*     
                `````````````````````````
                MODAL TRIAL CONFIRMATION

                `````````````````````````
            */}
            <Dialog
                open={isModalTrialConfirmation}
                onClose={() => setModalTrialConfirmation(false)}
                aria-labelledby="alert-dialog-title-trial-confirmation"
                aria-describedby="alert-dialog-description-trial-confirmation"
            >
                <DialogTitle id="alert-dialog-title-trial-confirmation" style={{textAlign: "center"}}>
                    <br />
                    <img src={Group_724} className={classes.media} alt="Pict Trial Modal Confirmation" /> 
                </DialogTitle>
                <DialogContent style={{textAlign: "center"}}>
                    <DialogContentText id="alert-dialog-description-trial-confirmation" >
                        <Typography variant='h6' className={classes.title}>
                            <b>Gunakan Performate dalam 30 hari ?</b>
                        </Typography>
                        <Typography variant='caption' className={classes.title}>
                            <b>Kamu tidak perlu lisensi untuk pilihan ini</b>
                        </Typography>
                       
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "center", justifyContent:'center'}}>

                    <Button 
                        onClick={() => setModalTrialConfirmation(false)} 
                        variant='outlined' 
                        size='small'
                        className={classes.buttonModalCancel}    
                    >
                        Batal
                    </Button>
                    
                     <Button 
                        // onClick={() => Redirect(ToCompletionProfile)}
                        onClick= {handleTrialOke}
                        variant='contained' 
                        color="primary" 
                        size='small'
                        className={classes.buttonModal}
                    >  
                        Ya
                    </Button>
                </DialogActions>
                <br />
                <br />
            </Dialog>    

            <DialogModalTransferOptionBerbayar 
                classes={classes} 
                isBerbayar={isBerbayar} 
                setBerbayar={setBerbayar}
                userTokenState={userTokenState}
                nominalAfterDiscount={nominalAfterDiscount}
                setNominalAfterDiscount = { setNominalAfterDiscount}
                paramsData={paramsData}
                setParamsData={setParamsData}
            />

            {/* 
            
                NEXT, BIKIN DIALOG MODAL RESPONSE 422 - "Anda sudah memakai paket ini !"

            */}
        
        </MuiThemeProvider>
    );

};

export default withStyles(styles)(ViewCompletionMembershipOptions);

const duration = [
    {
        value: '1 Bulan',
        discount: 0,
        id: '59bef0c5-f5dc-423b-8fc6-9d5f11e618fc',
        duration: 1
        
    },
    {
        value: '3 Bulan',
        discount: 10,
        id:'d1e942f1-8705-4095-9afa-b2bd40daf264',
        duration: 3

      
    },
    {
        value: '6 Bulan',
        discount: 15,
        id: '4f63f815-bab7-4c6c-95c2-c0e1c619085d',
        duration: 6

    },
    {
        value: '1 Tahun',
        discount: 20,
        id:'fd685d2b-7f4f-4963-961d-529d30fce305',
        duration: 12

    },
  ];

  const packets = [
    {
        id: 'b28f5f75-9836-40be-b762-7d577a115464',
        value: 'Basic',
        choose: 'Basic',
        image: PictPaketBasic,
        user: 1,
        price: 75000
    },
    {
        id: 'b5adb87a-4f22-4a5f-a285-d98daf59bbfd',
        value: 'Silver',
        choose: 'Silver',
        image: PictPaketSilver,
        user: 11,
        price: 65000
    },
    {
        id: 'ab7d2c39-db0d-4a50-8f13-50183ca7dbec',
        value: 'Gold',
        choose: 'Gold',
        image: PictPaketGold,
        user: 51,
        price: 50000
    },
    {
        id: '8c7d1cef-719f-4653-9495-16dfcb2c2aee',
        value: 'Platinum',
        choose: 'Platinum',
        image: PictPaketPlatinum,
        user: 101,
        price: 40000
    }
  ];