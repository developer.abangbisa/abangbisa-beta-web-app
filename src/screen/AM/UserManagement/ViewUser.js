import React, { useCallback, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel

} from '@material-ui/core';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import LightTooltip from '../../../components/LightTooltip';


import SearchIcon from '@material-ui/icons/Search';
import DoneIcon from '@material-ui/icons/Done'; 
import DeleteIcon from '@material-ui/icons/Delete';  
import CloseIcon from '@material-ui/icons/Close';  

import axios from 'axios';

import DialogModalInviteAnggota from './ComponentViewUser/DialogModalInviteAnggota';
import EnhancedTableToolbar from './ComponentViewUser/EnhancedTableToolbar';

// import ModalDetail from './ComponentViewUser/ModalDetail';
import DialogModalDetail from './ComponentViewUser/DialogModalDetail';

import ButtonColorGrey from '../../../components/ButtonColorGrey';

import IconPreviewGrid from '../../../assets/images/SVG/Group_1002.svg';
import IconPreviewList from '../../../assets/images/SVG/Group_1001_red.svg';
import IconFilter from '../../../assets/images/SVG/Group_1250.svg';
import IconFilterNew from '../../../assets/images/SVG/Group_1117.svg';
import PictDefaultDashboard from '../../../assets/images/Mask_Group_2.png';


import { useGetHttp } from '../../../utilities-hook/useGetHttp';
import { URL_API } from '../../../constants/config-api';
import Redirect from '../../../utilities/Redirect';
import { ToDashboard} from '../../../constants/config-redirect-url';

import { styles } from './Style/StyleUser';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    },
    overrides: {

        MuiList: {
          root: {
            width: 54,
            // marginLeft: 14
          }
        },
        MuiListItemText: {
          root: {
            fontSize: 17
          }
        },
        MuiSwitch: {
    
          
          '&$checked': {
            color: 'grey'
          },
          iconChecked: {
              color: '#c1322d'
          },
          checked: {}
        },
      }
});


/*

    ```````````````````````````````````
    functions utilities TABLE COMPONENT

    ```````````````````````````````````
*/

let counter = 0;  

function desc(a, b, orderBy) {

    if (b[orderBy] < a[orderBy]) {
      return -1;
    };

    if (b[orderBy] > a[orderBy]) {
      return 1;
    };

    return 0;

};  

function stableSort(array, cmp) {

    // console.log("stableSort : ", array)

    if(array !== null && array !== undefined){

      const stabilizedThis = array.map((el, index) => [el, index]);

      stabilizedThis.sort((a, b) => {
      
        const order = cmp(a[0], b[0]);
      
        if (order !== 0) return order;
      
        return a[1] - b[1];
      });
  
      return stabilizedThis.map(el => el[0]);
    };

    return [];

};
  
function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
};

const rows = [
    { id: 'name', align: 'center', disablePadding: true, label: 'Nama User' },
    { id: 'email', align: 'right', disablePadding: false, label: 'Email' },
    { id: 'role', align: 'center', disablePadding: false, label: 'Role' },
    { id: 'status', align: 'center', disablePadding: false, label: 'Status' },
    { id: 'buttondetail', align: 'center', disablePadding: false, label: '' },
    // { id: 'editdanhapus', align: 'center', disablePadding: false, label: '' },

  ];


const ViewUser = props => {

    const { classes } = props;

    /*
        Delete

    */
   const handleDelete = () => console.log("Delete...")

   /* 
     ``````````````````````````
     - Get List status USER

     - GET LIST STATUS INVITED
     
     - Table functional

     ``````````````````````````
   */

    const [ data, setData ] = useState([]);
    const [ jumlahUser, setJumlahUser ] = useState('');

    const [ dataUserInvited, setDataUserInvited ] = useState([]);
    const [ jumlahUserInvited, setJumlahUserInvited ] = useState('');

    useEffect(() => {
        
        const userToken = localStorage.getItem('userToken');
    
        if(userToken !== null){

            const headers =  {

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Access-Control-Allow-Origin': '*',
                'crossorigin':true,
                'crossDomain': true,
                'Authorization': 'bearer ' + userToken
            };
        
            axios.defaults.headers.common = headers;    


            /* USERS READY */
            axios
                .get(URL_API + `/account-management/user?options[filter][0][0]=status_id&options[filter][0][1]=eq&options[filter][0][2]=3`)
                .then((response) => {
                    
                    console.log("Original response - USERS : ",response);
                    
                    if(response.status == 200){
                        
                        if(response.data.data !== null){
                            
                            setJumlahUser(response.data.data.length);

                            if(response.data.data.length > 0){
    
                                setData(response.data.data);
                            
                            };
                        };
                    };
                })
                .catch((error) => {

                    console.log("Error response : ",error.response);
                    
                });

            /* USERS INVITED */
            axios
                .get(URL_API + `/account-management/user?options[filter][0][0]=status_id&options[filter][0][1]=eq&options[filter][0][2]=1`)
                .then((response) => {
                    
                    console.log("Original response - USERS INVITED : ",response);

                    if(response.status == 200){
                        
                        if(response.data.data !== null){
                            
                            setJumlahUserInvited(response.data.data.length);

                            if(response.data.data.length > 0){
    
                                setDataUserInvited(response.data.data);
                            
                            };
                        };
                    };
                    
                })
                .catch((error) => {

                    console.log("Error response : ",error.response);
                    
                });

        } else { console.log("Ga dapet User Token !")}

    }, []);

    //    const [loading, data, setFetchedData ] = useGetHttp(URL_API + `/account-management/user`, []);


   const [order, setOrder] = useState('asc');
   const [orderBy, setOrderBy] = useState('calories');
   const [selected, setSelected] = useState([]);

   const [page, setPage] = useState(0);
   const [rowsPerPage, setRowsPerPage] = useState(10);
 
   function handleRequestSort(event, property) {

     const isDesc = orderBy === property && order === 'desc';
     setOrder(isDesc ? 'asc' : 'desc');
     setOrderBy(property);
   
   };
 
   function handleSelectAllClick(event) {

     if (event.target.checked) {
       const newSelecteds = data.map(n => n.id);
       setSelected(newSelecteds);
       return;
     };

     setSelected([]);
   };
 
   function handleClick(event, id) {

     const selectedIndex = selected.indexOf(id);

     let newSelected = [];
 
     if (selectedIndex === -1) {

       newSelected = newSelected.concat(selected, id);

     } else if (selectedIndex === 0) {

       newSelected = newSelected.concat(selected.slice(1));

     } else if (selectedIndex === selected.length - 1) {

       newSelected = newSelected.concat(selected.slice(0, -1));

     } else if (selectedIndex > 0) {

       newSelected = newSelected.concat(
         selected.slice(0, selectedIndex),
         selected.slice(selectedIndex + 1),
       );
     }
 
     setSelected(newSelected);
   };
 
   function handleChangePage(event, newPage) {
     setPage(newPage);
   }
 
   function handleChangeRowsPerPage(event) {
     setRowsPerPage(event.target.value);
   }
 
   const isSelected = id => selected.indexOf(id) !== -1;
 
   const emptyRows = rowsPerPage - Math.min(rowsPerPage, (data !== null && data !== undefined  ? data.length : 0 ) - page * rowsPerPage);


   /*

       ````
       TAB

       ````
   */

   const [tabIndex, setTabIndex] = useState(0);
   const handleTab = (tabIndex) => setTabIndex(tabIndex);

    /*

        ``````````````````````````````````
        MODAL DETAIL USER HAS BEEN AN USER  

        ```````````````````````````````````
    */

    const [isChooseDetail, setChooseDetail ] = useState(false);
    const [ dataDetailUser, setDataDetailuser ] = useState(null);
    // const handleCloseModalEdit = () => setChooseDetail(false);

    const handleModalDetailUpdateUser = (e, data) => {

        e.preventDefault();
        
        setDataDetailuser(data)
        
        setChooseDetail(true);



        console.log("Data User : ", data );

    };

    /*

       ````````````
       MODAL INVITE

       ````````````
    */

    const [isModalInvite, setModalInvite] = useState();

    return (
            <MuiThemeProvider theme={theme}>
            <br />
            <Paper className={classes.root}  elevation={0}>

                <Grid  
                    container
                    // spacing={10}
                    direction="row"
                    justify="flex-end"
                    alignItems="center"
                >   
                    <Grid item sm={9} style={{paddingTop: 0}}>

                        <Typography variant='h6' className={classes.title} style={{marginLeft: 42}}>
                                    Pengaturan User                              
                        </Typography>
                    </Grid>
                    <Grid item sm={3} style={{textAlign: 'right'}}>
                        <img src={IconPreviewList} className={classes.iconPreviewList} />
                        <img src={IconPreviewGrid} className={classes.iconPreviewGrid} />
                    </Grid>
                </Grid>
                <br />
            
                {/* 
                
                    ``````````````````````````
                        Search, Button Invite

                    ``````````````````````````
                */}

                <Grid  
                    container
                    spacing={8}
                    direction="row"
                    justify="center"
                    alignItems="center"
                >   
                    <Grid item sm={4}></Grid>
                    <Grid item sm={5} style={{textAlign: 'right'}}>
                        <Paper className={classes.paper} elevation={1}>
                            <IconButton className={classes.iconButton} aria-label="Search">
                                <SearchIcon />
                            </IconButton>
                            <InputBase className={classes.input} placeholder="Cari ..." />
                            <Divider className={classes.divider} />
                        </Paper>
                    </Grid>
                    <Grid item sm={1} style={{textAlign: 'right'}}>
                        <Tooltip title="Filter" placement="right-start">
                            <img src={IconFilterNew} className={classes.iconFilter} />
                        </Tooltip>
                    </Grid>
                    <Grid item sm={2} style={{textAlign: 'left'}}>
                        <Button 
                            onClick={() => setModalInvite(true)}
                            variant="contained" 
                            color="secondary" 
                            className={classes.button} 
                            style={{marginRight: 24}}
                        >
                            + Invite User
                        </Button>
                    </Grid>
                </Grid>

                
                {/* 
                
                    `````````````````````````````````
                        Row Tab, List, & Grid Image, Table

                    ``````````````````````````````````
                */}
                <Grid  
                    container
                    spacing={8}
                    direction="row"
                    justify="center"
                    alignItems="center"
                >   
                    <Grid item sm={12} style={{paddingTop: 1}}>
                    
                        <Tabs style={{backgroundColor: 'white', paddingLeft: 0}} selectedIndex={tabIndex} onSelect={tabIndex => handleTab(tabIndex)} >
                            <TabList style={{border: 0, paddingLeft: 0}}>
                                <Tab>
                                    <List dense >
                                        <ListItem style={{paddingLeft: 0}}>
                                            <Avatar style={{backgroundColor: '#d1354a', marginRight: 5, fontSize: 14, width: 20, height: 20, textAlign: 'left'}}>
                                                {jumlahUser !== '' ? jumlahUser : ''}
                                            </Avatar>
                                            <ListItemText primary=" User" style={{fontSize: 17, fontWeight: 'bold', paddingLeft: 0 }} />
                                        </ListItem>
                                    </List>            
                                </Tab>
                                <Tab>
                                    <List dense >
                                        <ListItem style={{paddingLeft: 0}}>
                                            <Avatar style={{backgroundColor: '#d1354a', marginRight: 5, fontSize: 14, width: 20, height: 20, textAlign: 'left'}}>
                                                {jumlahUserInvited !== '' ? jumlahUserInvited : ''}
                                            </Avatar>
                                            <ListItemText primary=" Invited" style={{fontSize: 17, fontWeight: 'bold', paddingLeft: 0 }} />
                                        </ListItem>
                                    </List>      
                                </Tab>
                            </TabList>
                            
                            {/*  

                                `````````````````
                                Panel, User Content Table

                                `````````````````
                            */}

                            <TabPanel style={{backgroundColor: 'white'}}>


                                <br />
                                <br />
                                <div className={classes.tableWrapper}>

                                    {
                                        data == null && (

                                            <Grid  
                                                container
                                                spacing={40}
                                                direction="row"
                                                justify="center"
                                                alignItems="center"
                                            >
                                                
                                                <Grid item sm={12} style={{textAlign: 'center'}}> 
                                                <CircularProgress size={32} style={{marginTop: 64, color: 'red'}} />
                                                </Grid>
                                                
                                            </Grid>
                                        )
                                    }

                                    <Table className={classes.table} aria-labelledby="tableTitle">

                                        {
                                            data !== null && data !== undefined && data.length > 0 ? (

                                                <EnhancedTableHead
                                                    numSelected={selected.length}
                                                    order={order}
                                                    orderBy={orderBy}
                                                    onSelectAllClick={handleSelectAllClick}
                                                    onRequestSort={handleRequestSort}
                                                    rowCount={data !== null ? data.length : 0}
                                                /> 
                                            ) : null
                                        }


                                        <TableBody>

                                            {
                                                stableSort(data, getSorting(order, orderBy))
                                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                    .map( n => {

                                                        console.log("N : ", n);
          
                                                        const isItemSelected = isSelected(n.id);

                                                        return (

                                                            <TableRow
                                                                hover
                                                                onClick={event => handleClick(event, n.id)}
                                                                role="checkbox"
                                                                aria-checked={isItemSelected}
                                                                tabIndex={-1}
                                                                key={n.id}
                                                                selected={isItemSelected}
                                                            >
                                                                <TableCell padding="checkbox">

                                                                    {/* {
                                                                        n.masterRole.length > 0 ? (
                        
                                                                            <LightTooltip open={false} title={n.masterRole[0].name == 'superadmin' ? 'Superadmin tidak bisa di delete !' : '' } placement="right-start">
                                                                                <Checkbox disabled checked={isItemSelected} />
                                                                            </LightTooltip>
                        
                                                                        ) : (
                        
                                                                            <Checkbox checked={isItemSelected} />

                                                                        )
                                                                    } */}
                                                                    <Checkbox checked={isItemSelected} />

                                                                </TableCell>

                                                                <TableCell className={classes.title} component="th" scope="row" >
                                                                    {/* {n.member.first_name} */}
                                                                    {
                                                                        n.masterRole.length > 0 ? (

                                                                            <LightTooltip open={true} title={n.masterRole[0].name == 'superadmin' ? 'Superadmin' : '' } placement="right-start">
                                                                                <span> 
                                                                                    {n.member.first_name !== null ? n.member.first_name : n.member.email }
                                                                                </span>
                                                                            </LightTooltip>

                                                                        ) : (

                                                                            <span>
                                                                                {n.member.first_name !== null ? n.member.first_name : n.member.email }
                                                                            </span>
                                                                        )
                                                                    }
                                                                </TableCell>
                                                                <TableCell className={classes.title}>{n.email}</TableCell>
                                                                <TableCell className={classes.title}>
                                                                    {
                                                                        n.masterRole.length > 0  ? n.masterRole.map((item, i) => {

                                                                            return (
                                                                                <span key={i}>
                                                                                    { item.name }

                                                                                </span>
                                                                            )
                                                                        }) : (
                                                                            <span>-</span>
                                                                        )

                                                                        
                                                                    }

                                                                </TableCell>
                                                                <TableCell>           

                                                                    {
                                                                        n.status_id == 3 ? (

                                                                            <Chip  
                                                                                icon={<DoneIcon style={{color: 'white'}} />}
                                                                                label="Aktif" 
                                                                                className={classes.chipVerifified} 
                                                                                style={{backgroundColor: '#16e616', color: 'white'}}
                                                                                
                                                                            />

                                                                        ): (

                                                                            <Chip  
                                                                                icon = {

                                                                                    <CloseIcon style={{color: 'white', fontSize: 17}} />
                                                                                }
                                                                                label="Tidak Aktif" 
                                                                                className={classes.chipVerifified} 
                                                                                style={{backgroundColor: '#cc0707', color: 'white'}}
                                                                                
                                                                            />
                                                                        ) 
                                                                    }

                                                                </TableCell>
                                                                <TableCell className={classes.title} align='left'>

                                                                    <ButtonColorGrey 
                                                                        title='Detail' 
                                                                        // handleClick={() => setChooseDetail(true)} 
                                                                        handleClick={(e) => handleModalDetailUpdateUser(e, n)} 
                                                                        
                                                                    />
                                                                </TableCell>
                                                            </TableRow>

                                                        );
                                                    })
                                            }

                                        </TableBody>
                                    </Table>
                                </div>
                                            
                                {/* ROW OF DELETE  */}
                                <EnhancedTableToolbar numSelected={selected.length} />

                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25]}
                                    component="div"
                                    count={data !== null && data !== undefined ? data.length : 0}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'Previous Page',
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'Next Page',
                                    }}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                />
                                
                            </TabPanel>

                            {/*  

                                `````````````````````````````
                                Panel, Invited Content Table

                                `````````````````````````````
                            */}

                            <TabPanel style={{backgroundColor: 'white'}}> 
                               
                     


                                <br />
                                <br />
                                <div className={classes.tableWrapper}>

                                    {
                                        data == null && (

                                            <Grid container>
                                                <Grid item xs={12} style={{textAlign: 'center'}}>
            
                                                    <img 
                                                        src={PictDefaultDashboard} 
                                                        alt='Belum ada data yang bisa di tampilkan !'
                                                        className={classes.imagePictDefault} 
                                                    />
            
                                                    <br />
                                                    <br />
                                                    <Typography variant='h6' className={classes.title}>
                                                        Belum ada data yang bisa ditampilkan 
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        )
                                    }

                                    <Table className={classes.table} aria-labelledby="tableTitle">

                                        {
                                            dataUserInvited !== null && dataUserInvited !== undefined && dataUserInvited.length > 0 ? (

                                                <EnhancedTableHead
                                                    numSelected={selected.length}
                                                    order={order}
                                                    orderBy={orderBy}
                                                    onSelectAllClick={handleSelectAllClick}
                                                    onRequestSort={handleRequestSort}
                                                    rowCount={dataUserInvited !== null ? dataUserInvited.length : 0}
                                                /> 
                                            ) : null
                                        }


                                        <TableBody>

                                            {
                                                stableSort(dataUserInvited, getSorting(order, orderBy))
                                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                    .map(n => {

                                                        const isItemSelected = isSelected(n.id);

                                                        return (

                                                            <TableRow
                                                                hover
                                                                onClick={event => handleClick(event, n.id)}
                                                                role="checkbox"
                                                                aria-checked={isItemSelected}
                                                                tabIndex={-1}
                                                                key={n.id}
                                                                selected={isItemSelected}
                                                            >
                                                                <TableCell padding="checkbox">
                                                                    <Checkbox checked={isItemSelected} />
                                                                </TableCell>

                                                                <TableCell component="th" scope="row" padding="none">
                                                                    {n.name}
                                                                </TableCell>
                                                                <TableCell>{n.email}</TableCell>
                                                                <TableCell>***</TableCell>
                                                                <TableCell>
                                                                
                                                                    {
                                                                        n.status_id == 3 ? (

                                                                            <Chip  
                                                                                icon={<DoneIcon style={{color: 'white'}} />}
                                                                                label="Aktif" 
                                                                                className={classes.chipVerifified} 
                                                                                style={{backgroundColor: '#16e616', color: 'white'}}
                                                                                
                                                                            />

                                                                        ): (
                                                                            <Chip  
                                                                                icon={<CloseIcon style={{color: 'white',  fontSize: 17}} />}
                                                                                label="Belum di konfirmasi" 
                                                                                className={classes.chipVerifified} 
                                                                                style={{backgroundColor: '#cc0707', color: 'white'}}
                                                                                
                                                                            />
                                                                        ) 
                                                                    }

                                                                </TableCell>
                                                                <TableCell align='left'>

                                                                    <ButtonColorGrey 
                                                                        title='Detail' 
                                                                        handleClick={() => setChooseDetail(true)} 
                                                                        // handleClick={handleDetailUpdate} 
                                                                        
                                                                    />
                                                                </TableCell>
                                                            </TableRow>

                                                        );
                                                    })
                                            }

                                        </TableBody>
                                    </Table>
                                </div>
                                            
                                {/* ROW OF DELETE  */}
                                <EnhancedTableToolbar numSelected={selected.length} />

                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25]}
                                    component="div"
                                    count={data !== null && data !== undefined ? data.length : 0}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'Previous Page',
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'Next Page',
                                    }}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                />
                                
                            </TabPanel>
                        </Tabs>
                    </Grid>    

                   
                </Grid>
            </Paper>

            {/* DIALOG MODAL DETAIL */}

            {/* 
                <ModalDetail 
                    isChooseDetail={isChooseDetail}
                    handleCloseModalEdit={handleCloseModalEdit}
                /> 
            */}

            <DialogModalDetail 
                classes = { classes }
                isChooseDetail={isChooseDetail}
                setChooseDetail = { setChooseDetail }    
                dataDetailUser = { dataDetailUser }        
            />

            {/* MODAL INVITE */}
            <DialogModalInviteAnggota 
                
                classes={ classes }
                isModalInvite={ isModalInvite }
                setModalInvite={ setModalInvite }
                // listAnggota={listAnggota}
            />


        </MuiThemeProvider>

    )
};

export default withStyles(styles)(ViewUser);



function EnhancedTableHead(props) {

    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;

    const createSortHandler = property => event => {

      onRequestSort(event, property);
    };
  
    return (

      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {rows.map(

            row => (
              <TableCell
                key={row.id}
                numeric={row.numeric}
                padding={row.disablePadding ? 'none' : 'default'}
                sortDirection={orderBy === row.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.id}
                    direction={order}
                    onClick={createSortHandler(row.id)}
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this,
            )}
            </TableRow>
      </TableHead>
    );
  };
  