import React,{Component, useState, useEffect} from 'react';
import { 
    Grid, CardHeader, withStyles, CardContent, Card, CircularProgress, 
    Typography, Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, 
    List, ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction, Radio, Checkbox, TextField, MenuItem,
    FormControl, InputLabel, Select, Input, Fab
} from '@material-ui/core';

import axios from 'axios';

import DialogLanjutInviteAnggota from './DialogLanjutInviteAnggota';
// import CheckboxCustom from './CheckboxCustom';

import GearPicture from '../../../../assets/images/Group_1214.png';
import PictInfo from '../../../../assets/images/icon-info-24px.svg';
import AvatarDummy from '../../../../assets/images/Avatar_dummy.png';
import { URL_API } from '../../../../constants/config-api';
import { useGetHttp } from  '../../../../utilities-hook/useGetHttp';
// import { useCheckboxHandler } from  '../../../../utilities-hook/useCheckboxHandler';
// import Capitalize from  '../../../../utilities/Capitalize';

import { fade } from '@material-ui/core/styles';



const DialogModalInviteAnggota = props => {

    const { classes, isModalInvite, setModalInvite} = props;

    const [ userTokenState, setUserTokenState ] = useState('');

    /*
        ``````````````````
        GET LIST ROLE NOW

        ``````````````````
    */

    // const [ listRoleNow, setListRoleNow ] = useState([]);
    


    const [ loading, fetchedData, setFetchedData ] = useGetHttp(URL_API + `/account-management/user/batch/create`, [isModalInvite]);
    
    let listAnggota = [];

    if(fetchedData !== null && fetchedData !== undefined){
        if(fetchedData.memberCollections !== undefined){

            // console.log("fetchedData : ", fetchedData.memberCollections);
            listAnggota = [...fetchedData.memberCollections];
        };
    };

    /*
        ```````````````````````````````
        HANDLE CHOOSE USER IN CHECKBOX

        ```````````````````````````````
    */

    const [ selected, setSelected ] = useState([]);    

    // const handleClickCheckbox = (e, id) => {

    const handleClickCheckbox = (e, item) => {

        console.log("Item checkbox - single choose: ", item);

        const selectedIndex = selected.indexOf(item.id);

        let newSelected = [];
  
        if (selectedIndex === -1) {
  
          newSelected = newSelected.concat(selected, item.id);
  
        } else if (selectedIndex === 0) {
  
          newSelected = newSelected.concat(selected.slice(1));
  
        } else if (selectedIndex === selected.length - 1) {
  
          newSelected = newSelected.concat(selected.slice(0, -1));
  
        } else if (selectedIndex > 0) {
  
          newSelected = newSelected.concat(
            selected.slice(0, selectedIndex),
            selected.slice(selectedIndex + 1),
          );
        };
  
        console.log("Item checkbox - munltiple choose:: : ", newSelected);

        setSelected(newSelected);
    };

    const isSelected = id => selected.indexOf(id) !== -1;

    // useEffect(() => {

    //     if(isModalInvite == true ){

    //         const userToken = localStorage.getItem('userToken');
    
    //         if(userToken !== null){

    //             setUserTokenState(userToken);
    
    //             const headers =  {
    
    //                 'Accept': "application/json",
    //                 'Content-Type' : "application/json",
    //                 'Access-Control-Allow-Origin': '*',
    //                 'crossorigin':true,
    //                 'crossDomain': true,
    //                 'Authorization': 'bearer ' + userToken
    //             };
            
    //             axios.defaults.headers.common = headers;    
    //             axios
    //                 .get(URL_API + `/account-management/user/batch/create`)
    //                 .then((response) => {
                        
    //                     console.log("Original response : ",response);
                        
    //                     if(response.status == 200){
                            
    //                         const templateListRole = [];
                    
    //                         if(response.data.data !== undefined) {
            
    //                             Object.getOwnPropertyNames(response.data.data.roleOptions).forEach((val, idx, array) => {
    //                                     const data = {
    //                                         key: val,
    //                                         value: response.data.data.roleOptions[val]
    //                                     };			
    //                                     templateListRole.push(data);
    //                                 }
    //                             )
    //                         };  

    //                         setListRoleNow(templateListRole)
                            
    //                     };
                        
    //                 })
    //                 .catch((error) => {
    
    //                     console.log("Error response : ",error.response);
                        
    //                 });
    
    //         } else { console.log("Ga dapet User Token !")}

    //     };

    // },[isModalInvite]);

    /*
        ``````````````````````````
        HANDLE SIMPAN DATA & MODAL LANJUT DIALOG

        ``````````````````````````
    */

    const [ isModalLanjut, setModalLanjut ] = useState(false);

    const handleSimpanDataAndDialogLanjut = () => {

        console.log("selected checbox oke", selected);

        setModalLanjut(true);


    };

    return (

        <div>
            <Dialog
                open={isModalInvite}
                onClose={() => setModalInvite(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description" 
                // fullWidth           
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>

                    <Grid container>

                        <Grid item sm={8}>
                            <Typography variant='h6' className={classes.title}>
                                <b>Pilih Anggota</b>
                            </Typography>
                        </Grid>

                        <Grid item sm={4}>
                           
                        </Grid>
                    </Grid>
                  
                </DialogTitle>
                <DialogContent style={{textAlign: "left"}}>
                
                    <Grid container>
                        <Grid item sm={12}>
                            <List className={classes.listRoot}>

                                {
                                    listAnggota.length > 0 ? listAnggota.map((item, i) => {

                                        const isItemSelected = isSelected(item.id);

                                        return (     

                                            <ListItem 
                                                // dense 
                                                // key={n.id}
                                                key={i}
                                                style={{paddingLeft: 0, marginRight: 24}}
                                                button 
                                                aria-checked={isItemSelected}
                                                selected={isItemSelected}
                                                onClick={(e) => handleClickCheckbox(e, item)}
                                                
                                            >
                                                <Checkbox checked = { isItemSelected } />
                                                
                                                <img src={AvatarDummy} alt="Gear Picture" className={classes.imageAvatar} />
                                                &nbsp;&nbsp;

                                                <ListItemText 
                                                    className={classes.listItemtext}
                                                    primary={
                                                        <Typography variant='subtitle2' className={classes.title}>
                                                            <b>{item.first_name} {item.last_name}</b>
                                                        </Typography>
                                                    } 
                                                    secondary={
                                                        <Typography variant='subtitle2' className={classes.title}>
                                                            {item.email}
                                                        </Typography>
                                                    } 
                                                />

                                            </ListItem>
                                            
                                        )

                                    }) : (

                                        <Typography variant='subtitle2' className={classes.title}>
                                            <b>Anda belum menambahkan Anggota !</b>
                                        </Typography>
                                    )
                                }
                            </List>   
                        </Grid>
                        {/*                         
                            <Grid item sm={3} >
                                <TextField
                                    id="outlined-select-provinsi"
                                    select
                                    label="Pilih Role : "
                                    className={classes.textField}
                                    value={roleNowSelected.name}
                                    onChange={handleChangeDropdownMultipleDropdown('name')}
                                    SelectProps={{
                                        MenuProps: {
                                            className: classes.menu,
                                        },
                                    }}
                                    margin="normal"
                                    variant="outlined"
                                    style={{width: 150, marginRight: 16}}
                                    // fullWidth
                                >
                                    {

                                        listRoleNow.length > 0 && listRoleNow.map ((option, i) => (

                                                <MenuItem 
                                                    key={i} 
                                                    value={option} 
                                                    style={{width: 150}}
                                                >
                                                    {option.value}
                                                </MenuItem>
                                            )
                                        )
                                    }
                                    
                                </TextField>
                            </Grid> */}
                    </Grid>
                                     
                    <DialogContentText id="alert-dialog-description">
                        <Typography variant='subtitle1' className={classes.titleModal}>
                            
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "center", justifyContent:'center '}}>
                    <Button 
                        // onClick={(e) => chooseOption(e)}
                        // onClick={() => setModalLanjut(true)}
                        onClick = {handleSimpanDataAndDialogLanjut}
                        variant='contained' 
                        className={classes.button}
                        fullWidth
                    >  
                        Lanjut
                    </Button>
                </DialogActions>
                <br />
            </Dialog>

            <DialogLanjutInviteAnggota 
                classes = { classes }
                // listAnggota = { listAnggota }
                isModalLanjut = { isModalLanjut}
                setModalLanjut = { setModalLanjut }
                selected = { selected }
                // listRoleNow = { listRoleNow }

            />
        </div>
    )

};

export default DialogModalInviteAnggota;
 
