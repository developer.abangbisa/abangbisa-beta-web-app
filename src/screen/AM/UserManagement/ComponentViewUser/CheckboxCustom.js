import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from '@material-ui/core';

const CheckboxCustom = ({ type = 'checkbox',dataset, name, checked = false, onChange ,id  }) => {

    return (

        <Checkbox type={type} name={name} checked={checked} onChange={onChange} id={id}   />

    );
};

CheckboxCustom.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};

export default CheckboxCustom;