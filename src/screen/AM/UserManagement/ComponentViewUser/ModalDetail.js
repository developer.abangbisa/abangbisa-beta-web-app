import React, {useEffect, useState} from 'react';
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Row,Modal } from 'react-bootstrap';
import { List, ListItem, ListItemText, ListItemAvatar, ListItemSecondaryAction, Tooltip } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';

import Button from '../../../../components/Button';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }    
});

const styles = theme => ({

  
    button: {
        width: '400px',
        height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
    }
});

const ModalDetail  = props => { 

    const [dense, setDense ] = useState(false)
    
    return (
        <Modal show={props.isChooseDetail} onHide={props.handleCloseModalEdit}  centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Detail User
                </Modal.Title>
            </Modal.Header> 
            <Modal.Body>
                <MuiThemeProvider theme={theme}>
                    <List dense={dense}>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                                primary="Doel"
                                // secondary={secondary ? 'Secondary text' : null}
                                secondary='adoel.cs@gmail.com'
                            />
                            <ListItemSecondaryAction>
                                <Tooltip title="Edit" placement="left">
                                    <IconButton aria-label="Edit">
                                        <i className="material-icons">
                                            edit
                                        </i>
                                    </IconButton>
                                </Tooltip>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>
                </MuiThemeProvider>
            </Modal.Body>
            <Modal.Footer>
                <Row style={{marginRight: 3}}>
                    <IconButton aria-label="Edit">
                        <DeleteIcon style={{color: '#c1322d', marginRight: 24 }} />
                    </IconButton>
                    <Button 
                        title='Simpan' 
                        handleClick={console.log("User klik button Detail !!!")}
                    />  
                </Row>
            </Modal.Footer>
        </Modal>
    )};

export default withStyles(styles) (ModalDetail);