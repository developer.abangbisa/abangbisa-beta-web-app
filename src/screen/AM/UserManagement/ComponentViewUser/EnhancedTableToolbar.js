import React, { useState, useEffect} from 'react';
import axios from 'axios';

import { makeStyles } from '@material-ui/styles';
import classNames from 'classnames';

import { Toolbar, Typography, Tooltip,  } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import { URL_API } from '../../../../constants/config-api';
import Redirect from '../../../../utilities/Redirect';

const useToolbarStyles = makeStyles(theme => ({

    root: {},
    highlight:
    //   theme.palette.type === 'light'
    //     ? {
    //         color: theme.palette.secondary.main,
    //         backgroundColor: lighten(theme.palette.secondary.light, 0.85),
    //       }
    //     : {
    //         color: theme.palette.text.primary,
    //         backgroundColor: theme.palette.secondary.dark,
    //       },
    {
        color: 'grey',
        backround: 'white'
    },
    spacer: {
      flex: '1 1 100%',
    },
    actions: {
        //   color: theme.palette.text.secondary,
        color: 'grey'
    },
    title: {
      flex: '0 0 auto',
      marginRight: 57

    }

  }));

const EnhancedTableToolbar = props => {

    const classes = useToolbarStyles();
    const { numSelected, idRole } = props;

    const handleDeleteMasterUser = () => {
      
      alert("Oops, something went wrong !")
      const userToken = localStorage.getItem('userToken');
        
      if(userToken !== undefined){

        // console.log("Delete : ", idRole);
        // idRole.length > 0 && idRole.map((item, i) => {

        //   console.log(item)

        //   const header =  {       
        //       'Accept': "application/json",
        //       'Content-Type' : "application/json",
        //       'Authorization' : "bearer " + userToken,
  
        //   };
  
        //   axios.defaults.headers.common = header;    
  
        //   axios
        //       .delete(URL_API + `/account-management/master-role/${item.id}`)
        //       .then(function(response){
  
        //           console.log("Response Original : ", response);
        //           window.location.reload();
                 
        //       })
        //       .catch(function(error){
                  
        //           console.log("Error : ", error.response)

        //           const newErrorData = { ...error.response.data};
        //           console.log("newErrorData : ", newErrorData);

        //           if(newErrorData.info.message !== null){
        //             if(newErrorData.info.status == 422){

        //               alert("Opps, something went wrong ! ",newErrorData.info.message);

        //             }
        //           };
             
        //       })

        // });
      

      } else { console.log("No Access Token available!")};

    };

    return (
      <Toolbar
        className=
            {
                classNames(classes.root, {
                        [classes.highlight]: numSelected > 0,
                      }
                )
            }

        style={{minHeight: 0, paddingLeft: 3, marginLeft: 7}}
      >

        <div className={classes.actions}>
          {
              numSelected > 0 ? (

                <Tooltip title="Hapus">
                    <IconButton 
                      onClick={() => handleDeleteMasterUser()}
                      aria-label="Delete"
                    >
                        <DeleteIcon style={{color:'#c1322d'}} />
                    </IconButton>
                </Tooltip>

            ) : (
                <Tooltip title="Filter list">
                    <IconButton aria-label="Filter list">
                        {/* <FilterListIcon /> */}
                    </IconButton>
                </Tooltip>
            )
          }
        </div>
       


        <div className={classes.spacer} />

        <div className={classes.title}>

          {
              numSelected > 0 ? (

                    <Typography color="inherit" variant="subtitle1">
                        {numSelected} Terpilih
                    </Typography>

                ) : (

                    <Typography variant="h6" id="tableTitle">
                        {/* Pengaturan Role */}
                    </Typography>
                )
          }
        </div>
      </Toolbar>
    );
  };

  export default EnhancedTableToolbar;