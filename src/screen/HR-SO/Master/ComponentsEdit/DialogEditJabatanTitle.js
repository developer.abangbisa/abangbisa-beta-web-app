import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

import Snackbeer from '../../../../components/Snackbeer';

const DialogEditJabatanTitle = props => {

    const { 
        
            classes,
            isModalEdit,
            setModalEdit,
            dataDetailEdit,
           
        } = props;

    // console.log("dataDetailEdit : ", dataDetailEdit);

    /*
        ``````````````````````````````
        HANDLE CHANGE EDIT JENIS UNIT

        ``````````````````````````````
    */

    const [ jabatanTitle, setJabatanTitle ] = useState('');
    const handleChangeEdit = (e) => setJabatanTitle(e.target.value);

    /*
        ```````````````````````
        HANDLE SIMPAN PERUBAHAN

        ````````````````````````
    */
    const [ isModalResponse200, setModalResponse200 ] = useState(false);
    const [ loader, setLoader ] = useState(false);

    const userToken = localStorage.getItem('userToken');
    
    const handleSimpanPerubahan = (e) => {

        e.preventDefault();
        setLoader(true);
        
        let data = {
            
            MasterStructurePositionTitle : {
                
                updated_at: dataDetailEdit.updated_at,
                name: jabatanTitle,
                
            },
            // _method: 'patch'
            
        };
        
        console.log("Data detail edit : ", data);
        
        
        if(userToken !== undefined){
            
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
                
            };

            axios.defaults.headers.common = header;    

            axios
                .put(URL_API + `/human-resource/master-structure-position-title/${dataDetailEdit.id}`, data)
                .then(function(response){

                    console.log("Response Original : ", response)
                    setLoader(false);
                    
                    if(response.status == 200 ){
                        window.location.reload();
                        // setModalResponse200(true);
                        // setModalEdit(false)
                    };              
                })
                .catch(function(error){
                    
                    setLoader(false);

                    alert('Whoops something went wrong !');
                    console.log("Error : ", error.response)
                    
                })

        } else { console.log("No Access Token available!")};

    };


    return (

        // <div>
            <Dialog
                open={isModalEdit}
                onClose={() => setModalEdit(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                    <Typography variant='subtitle1' className={classes.title}>
                        <b>Ubah Data Jabatan</b>
                    </Typography>
                </DialogTitle>
                <DialogContent>

                    {/* <List className={classes.list}> */}

                    <Typography variant='subtitle1' className={classes.titleForm}>
                        <b>Jabatan</b>
                    </Typography>
                              
                    <TextField
                        id="outlined-bare"
                        onChange= {handleChangeEdit}
                        value={jabatanTitle}
                        className={classes.textField}
                        placeholder={dataDetailEdit !== null ? dataDetailEdit.name : ''}
                        variant="outlined"
                        // error={infoError == true && npwp == '' ? true : false}
                        // helperText={infoError == true && npwp == '' ? "Wajib di isi" : ' '}
                        // required={true} 
                        // name='nip'
                        // color='primary'
                        // onKeyDown={handleEnterPress}
                        // disabled= {isLockedStatusState == 1 ? true : false}
                        // fullWidth
                    />
                        
                    <DialogContentText id="alert-dialog-description">
                        <Typography variant='h6'>
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                    <Button 
                        onClick={() => setModalEdit(false)} 
                        variant='outlined' 
                        size='small'
                        className={classes.buttonModalCancel}    
                    >
                        Batal
                    </Button>
                    
                    <Button 
                        onClick= {(e) => handleSimpanPerubahan(e)}
                        variant='contained' 
                        color="primary" 
                        size='small'
                        className={classes.buttonModal}s
                    >  
                        {
                            loader == true ? (

                                <CircularProgress size={20} style={{color: 'white'}} />

                            ) : ' Simpan Perubahan'
                        }
                    </Button>
                </DialogActions>
                <br />
                <br />
            </Dialog>

            /* <Snackbeer
                classes={classes}
                isModalResponse200= {isModalResponse200}
                setModalResponse200 = {setModalResponse200}
                messages = 'Perubahan data berhasil di simpan !'
            /> */
        //</div> 
    )   
};

export default DialogEditJabatanTitle;
