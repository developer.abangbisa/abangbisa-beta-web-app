import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Icon

} from '@material-ui/core';

import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const DialogJabatan = props => {

    const { 
        
            classes,
            setModalJabatan,
            isModalJabatan,
        
        } = props;

    /*
        `````````````````````
        HANDLE NAMA UNIT ONLY

        `````````````````````
    */
    const [ namaJabatan, setNamaJabtan ] = useState('');
    const handleChangenamaJabatan = (e) => setNamaJabtan(e.target.value)

    /*
        ````````````````````````````````````````````````
        HANDLE SIMPAN NAMA UNIT ONLY

        ````````````````````````````````````````````````
    */

    const handleSimpan = () => {

            //*context
            let data = {
        
                MasterStructurePositionTitle : {

                    name : namaJabatan,
                }
            };
  
            console.log("Data : ", data);
      
            const userToken = localStorage.getItem('userToken');
                
              if(userToken !== undefined ){
            
                const header =  {       
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
    
                };

                axios.defaults.headers.common = header;    

                axios
                    .post(URL_API + '/human-resource/master-structure-position-title', data)
                    .then(function(response){
                        
                        console.log("Response Original : ", response);
                        setModalJabatan(false)

                        if(response.status == 200 ){

                            window.location.reload();

                            if(response.data.data !== undefined){
                        
                            };
                        };
                    })
                    .catch(function(error){

                        alert('Whoops, something went wrong !')
                        console.log("Error : ", error.response)
                    
                    })
    
              } else { console.log("No Access Token available!")};
    };

    return (

        <Dialog
            open={isModalJabatan}
            onClose={() => setModalJabatan(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Tambah Jabatan baru</b>
            </Typography>
        </DialogTitle>
        <DialogContent>

            <Typography variant='subtitle1' className={classes.titleForm}>
                <b>Nama Jabatan</b>
            </Typography>

            <TextField
                id="outlined-bare"
                onChange= {handleChangenamaJabatan}
                // value={nameAnggotaKeluarga}
                className={classes.textField}
                placeholder=''
                variant="outlined"
            />

            <DialogContentText id="alert-dialog-description"></DialogContentText>

        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            <Button 
                onClick={() => setModalJabatan(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button>
            
            <Button 
                // onClick={() => Redirect(ToHrEmployeeKepegawaianInfoPegawai)}
                onClick= {handleSimpan}
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Tambah
            </Button>
        </DialogActions>
        <br />
        <br />
        </Dialog>
    )   
};

export default DialogJabatan;
