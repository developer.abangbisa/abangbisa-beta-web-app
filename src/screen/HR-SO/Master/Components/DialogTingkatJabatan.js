import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, Icon

} from '@material-ui/core';

import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const DialogTingkatJabatan = props => {

    const { 
        
            classes,
            setModalTingkatJabatan,
            isModalTingkatJabatan,
        
        } = props;

    /*
        `````````````````````
        HANDLE NAMA UNIT ONLY

        `````````````````````
    */
    const [ tingkatJabatan, setTingkatJabatan ] = useState('');
    const handleChangeTingkatJabatan = (e) => setTingkatJabatan(e.target.value)

    /*
        ````````````````````````````````````````````````
        HANDLE SIMPAN NAMA UNIT ONLY

        ````````````````````````````````````````````````
    */

    const handleSimpan = () => {

            //*context
            let data = {
        
                MasterStructurePositionLevel : {

                    name : tingkatJabatan,
                }
            };
  
            console.log("Data : ", data);
      
            const userToken = localStorage.getItem('userToken');
                
              if(userToken !== undefined ){
            
                const header =  {       
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
    
                };

                axios.defaults.headers.common = header;    

                axios
                    .post(URL_API + '/human-resource/master-structure-position-level', data)
                    .then(function(response){

                        // closeModalAnggota();
                        console.log("Response Original : ", response);
                        setModalTingkatJabatan(false)
                    
                        if(response.status == 200 ){

                            window.location.reload();

                            if(response.data.data !== undefined){
                        
                            };
                        };
                    })
                    .catch(function(error){

                        alert('Whoops, something went wrong !')
                        console.log("Error : ", error.response)
                    
                    })
    
              } else { console.log("No Access Token available!")};
    };

    return (

        <Dialog
            open={isModalTingkatJabatan}
            onClose={() => setModalTingkatJabatan(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Tambah Tingkat Jabatan baru</b>
            </Typography>
        </DialogTitle>
        <DialogContent>

            <Typography variant='subtitle1' className={classes.titleForm}>
                <b>Nama Tingkat Jabatan</b>
            </Typography>

            <TextField
                id="outlined-bare"
                onChange= {handleChangeTingkatJabatan}
                // value={nameAnggotaKeluarga}
                className={classes.textField}
                placeholder='Contoh : General Manager'
                variant="outlined"
            />

            <DialogContentText id="alert-dialog-description"></DialogContentText>

        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            <Button 
                onClick={() => setModalTingkatJabatan(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button>
            
            <Button 
                // onClick={() => Redirect(ToHrEmployeeKepegawaianInfoPegawai)}
                onClick= {handleSimpan}
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Tambah
            </Button>
        </DialogActions>
        <br />
        <br />
        </Dialog>
    )   
};

export default DialogTingkatJabatan;
