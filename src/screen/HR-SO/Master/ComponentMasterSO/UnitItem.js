import React, { Component, useEffect, useState, createRef } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Fab, Icon

} from '@material-ui/core';

import EditIcon from '@material-ui/icons/Edit';

import DialogDeleteNamaUnit from '../ComponentsDelete/DialogDeleteNamaUnit';
import DialogEditNamaUnit from '../ComponentsEdit/DialogEditNamaUnit';


const UnitItem = props => {

    const { classes, data } = props;

    /*
        `````````````
        HANDLE DELETE

        `````````````
    */

    const [ isOpenModalDelete, setOpenModalDelete ] = useState(false);
    

        
    /*
        ```````````````````````````````````
        HANDLE MODAL UPDATE NAMA UNIT ONLY

        ```````````````````````````````````
    */

   const [ isModalEdit, setModalEdit ] = useState(false);
   const [ dataDetailEdit, setDataDetailEdit ] = useState(null);

   const handleEdit = (e, item) => {

       e.preventDefault();

       setModalEdit(true);
       setDataDetailEdit(item)

       console.log("Edit : ", item);

   };

    return (

        <span>
            <Chip
                
                style={{fontFamily: 'Nunito', marginLeft: 40, marginTop:8, backgroundColor: '#ffbc39', color: 'white', fontWeight: 'bold'}}                        
                label={data.name}
                onDelete = { () => setOpenModalDelete(true) }
                icon = {
                    <Tooltip title="Edit" placement="bottom">
                        <IconButton onClick={ (e) => handleEdit(e, data)}>
                            <EditIcon style={{fontSize: 17, color: 'white', cursor: 'pointer' }} />
                        </IconButton>
                    </Tooltip>
                }
            />

            <DialogDeleteNamaUnit 
                classes = {classes }
                isOpenModalDelete = { isOpenModalDelete }
                setOpenModalDelete = { setOpenModalDelete }
                data = { data }
            />

            <DialogEditNamaUnit 
                classes = {classes }
                dataDetailEdit = { dataDetailEdit}
                isModalEdit = { isModalEdit }
                setModalEdit = { setModalEdit } 
            />
        </span>
    )
};


export default UnitItem;

/*

    ``````````````````````````````````````````````````````````````
    THIS COMPONENT IS USED WHEN WE WANT TO USE '@react-hook/hover'

    ``````````````````````````````````````````````````````````````
*/

// <span ref={hoverRef} > 
//    <Chip                                       
//         // onDelete={(e) => handleDeleteComponent(e, {bind: childAgain}, i)}
//         style={{fontFamily: 'Nunito', marginLeft: 40, backgroundColor: '#ffbc39', color: 'white', fontWeight: 'bold'}}                        
//         label={data.name}
//         onDelete = {

//             isHovering ? (

//                 () => alert("Deleting...")
                
//             ) : null
//         }
        
//         icon = {
//             isHovering ? (
//                 <Tooltip title="Edit" placement="bottom">
//                     <IconButton
//                         onClick={ () => alert('Editing...')}
//                     >
//                         <EditIcon style={{fontSize: 17, color: 'white', cursor: 'pointer'}} />
//                     </IconButton>
//                 </Tooltip>
//             ) : null
//         }
//     />
// </span>