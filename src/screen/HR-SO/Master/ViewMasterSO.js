import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Fab, Icon

} from '@material-ui/core';

import {red, grey, green, amber} from '@material-ui/core/colors';

// import axios from 'axios';
// import useHover from '@react-hook/hover';
// import EditIcon from '@material-ui/icons/Edit';

import DialogTambahUnit from './Components/DialogTambahUnit';

import Unit from './ComponentMasterSO/Unit';
import TingkatJabatan from './ComponentMasterSO/TingkatJabatan';
import JabatanTitle from './ComponentMasterSO/JabatanTitle';

const theme = createMuiTheme({
    
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }
});

const styles = theme => ({

    root: {
        
        padding: theme.spacing(5, 2),
        marginTop: theme.spacing(4),
        width: 575,
        borderRadius: 7
    },
    button: {
        
        // width: '503px',
        // height: '42px',s
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        // marginBottom: theme.spacing(4),
        color: 'white',
        textTransform: 'capitalize'
    },
    buttonTambah: {
        background: '#f8b314',
        borderRadius: 5,
        border:0,
        fontFamily:'Nunito',
        color: 'white',
        textTransform: 'capitalize',
        '&:hover': {
            background: '#ffb200'
        }
    },
    buttonDetail: {
        textTransform: 'capitalize',
        fontFamily:'Nunito',

    },
     
    buttonAction: {

        textTransform : 'capitalize'
    },
    title : {
        
        fontFamily: 'Nunito'
    },
    titleSub1 : {

        fontFamily: 'Nunito',
        marginLeft: theme.spacing(5)

    },
    titleSub2: {

        fontFamily: 'Nunito',
        marginLeft: theme.spacing(5),
        color: 'grey',
        textDecorationLine: 'underline',
        textDecorationStyle: 'dashed',
        // textDecorationStyle: 'dotted',
        textUnderlinePosition: 'under',
        letterSpacing: 3
        // text-decoration-skip: ink;
        // text-underline-position: under
        // text-decoration-line: underline;
        // text-decoration-style: solid;
    },
    titleForm: {
        fontFamily: 'Nunito',
        marginLeft : theme.spacing(3),
        // marginTop: theme.spacing(2),
    },
    fab: {
        // margin: theme.spacing(1),
        fontFamily: 'Nunito',
        textTransform: 'capitalize',
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        color: 'white',
        padding: 0,
        fontSize: 12,
        fontWeight: 'bold'


    },
    hoverIconPlus: {
        marginLeft: 16, 
    },
    hoverIconPlusLeave: {
        marginLeft: 16, 
        backgroundColor: '#e0e0e08a'

    },

    textField: {
        minWidth: 425,
        marginLeft: theme.spacing(3),
        marginBottom: theme.spacing(1),
    },

    /*
        ```````````````````
        MODAL, BUTTON MODAL

        ```````````````````
    */
    buttonModal: {
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        // backgroundColor: 'cyan',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize'

    },
    buttonModalCancel: {
        fontFamily:'Nunito',
        textTransform: 'capitalize'
    },

    /*
        ``````````
        MODAL EDIT

        ``````````
    */

    rootPaper: {

        marginLeft: theme.spacing(3),

        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 425,
    },
    input: {

        // marginLeft: 8,
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
      },
      divider: {
        width: 1,
        height: 28,
        margin: 4,
      },
      icon: {
        margin: 2 * 2,
      },
      iconHover: {
        margin: 2 * 2,
        color: red[800],
        '&:hover': {
          color: grey[500],
          
        },
      },
      iconHoverAdd: {
        margin: 2 * 2,
        color: green[800],
        // '&:hover': {
        //   color: grey[800],
        // },
      },

      iconHoverAddDisabled: {
        margin: 2 * 2,
        color: grey[800]
      },

      warning: {
        backgroundColor: amber[700],
      },

      success: {
          backgroundColor: green[500],
      },
});

const ViewMasterSO = props => {
    
    const { classes } = props;

    /*
        ``````````````````````````````````````````````
        HANDLE TAMBAH JENIS UNIT & NAMA UNIT SEKALIGUS

        ``````````````````````````````````````````````        
    */

    const [ isModalTambahJenisUnitAndNamaUnit, setModalTambahJenisUnitAndNamaUnit ] = useState(false);



    
        
    return (

        <MuiThemeProvider theme={theme}>
            <Grid container>
                <Grid item xs={12}>
                    <br />
                    <br />
                    <Typography variant='h6' className={classes.titleSub1}>
                        <b>Unit</b> &nbsp;&nbsp;&nbsp;&nbsp;

                        <Fab 
                            onClick={() => setModalTambahJenisUnitAndNamaUnit(true)}
                            variant="extended" aria-label="Tambah" className={classes.fab} 
                            size='small'
                        >
                            + Tambah Unit Baru
                        </Fab>
                    </Typography>

                    <br />  
                    <Unit 
                        classes={classes}
                    />
                </Grid>

                <Grid item xs={12}>
                    <br />
                    <Typography variant='h6' className={classes.titleSub1}>
                        <b>Tingkat Jabatan</b>
                    </Typography>

                    <br />
                    <TingkatJabatan 
                        classes = {classes}
                    />
                </Grid>

                <Grid item xs={12}>
                    <br />
                    <br />
                    <br />
                    <Typography variant='h6' className={classes.titleSub1}>
                        <b>Jabatan</b>
                    </Typography>

                    <br />
                    <JabatanTitle 
                        classes={classes}
                    />
                </Grid>

                <DialogTambahUnit 
                    classes={ classes }
                    isModalTambahJenisUnitAndNamaUnit= { isModalTambahJenisUnitAndNamaUnit }
                    setModalTambahJenisUnitAndNamaUnit = { setModalTambahJenisUnitAndNamaUnit }
                />


                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </Grid>

        </MuiThemeProvider>
        
    )
};

export default withStyles(styles)(ViewMasterSO);