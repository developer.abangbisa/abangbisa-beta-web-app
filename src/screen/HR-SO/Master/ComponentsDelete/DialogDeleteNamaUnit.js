
import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import axios from 'axios';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
// import Redirect from 'react-router/Redirect';


import { URL_API } from '../../../../constants/config-api';

const DialogDeleteNamaUnit = (props) => {
    
    const { 
    
        classes, 
        isOpenModalDelete,
        setOpenModalDelete,
        data
    } = props;
    
    const handleDelete = () => {

        const userToken = localStorage.getItem('userToken');
        
        if(userToken !== undefined ){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .delete(URL_API + `/human-resource/master-structure-unit/${data.id}`)
                .then(function(response){

                    console.log("Response Original : ", response)
                    window.location.reload();
                    
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
               
                })

        } else { console.log("No Access Token available!")};
    };
    
    return (

        <Dialog
            open={isOpenModalDelete}
            onClose={() => setOpenModalDelete(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
                <Typography variant='h6' className={classes.title}>
                </Typography>
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
            
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.title} style={{color: 'black'}}>
                        <b>Apakah Anda yakin ingin menghapus <i>{data.name}</i> ?</b>
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    onClick={handleDelete}
                    variant='contained' 
                    className={classes.button}
                    // fullWidth
                >  
                    Yakin
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

export default DialogDeleteNamaUnit;
