import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';

import Select from 'react-select';


import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const suggestions = [

    { label: 'Afghanistan' },
    { label: 'Aland Islands' },
    { label: 'Albania' },
    { label: 'Algeria' },
    { label: 'American Samoa' },
    { label: 'Andorra' },
    { label: 'Angola' },
    { label: 'Anguilla' },
    { label: 'Antarctica' },
    { label: 'Antigua and Barbuda' },
    { label: 'Argentina' },
    { label: 'Armenia' },
    { label: 'Aruba' },
    { label: 'Australia' },
    { label: 'Austria' },
    { label: 'Azerbaijan' },
    { label: 'Bahamas' },
    { label: 'Bahrain' },
    { label: 'Bangladesh' },
    { label: 'Barbados' },
    { label: 'Belarus' },
    { label: 'Belgium' },
    { label: 'Belize' },
    { label: 'Benin' },
    { label: 'Bermuda' },
    { label: 'Bhutan' },
    { label: 'Bolivia, Plurinational State of' },
    { label: 'Bonaire, Sint Eustatius and Saba' },
    { label: 'Bosnia and Herzegovina' },
    { label: 'Botswana' },
    { label: 'Bouvet Island' },
    { label: 'Brazil' },
    { label: 'British Indian Ocean Territory' },
    { label: 'Brunei Darussalam' },

  ].map(suggestion => ({

    value: suggestion.label,
    label: suggestion.label,

  }));

const DialogTambahPosisition_version_mas_tri = props => {

    const { 
            classes,
            isModalTambah,
            setModalTambah,
            // handleChangeNamaAnggotaKeluarga,
            handleSimpanData
        
        } = props;
    
    /*
        ```````````````````
        DATA UNTUK DROPDOWN

        ```````````````````
    */

    const [ listAtasan, setListAtasan ] = useState([]);
    const [ listNamaUnit, setListNamaUnit ] = useState([]);
    const [ listJabatan, setListJabatan ] = useState([]);
    const [ listTingkatJabatan, setListTingkatJabatan ] = useState([]);
    const [ listJenisUnit, setListJenisUnit ] = useState([]);

    const [ listUmumPosisiJabatan, setListUmumPosisiJabatan ] = useState([]);

    const [ listAncestor, setListAncestor ] = useState([]);
    
    /*
        ````````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    const [ userTokenState, setUserTokenState ] = useState('');
    const userToken = localStorage.getItem('userToken');

    useEffect(() => {

        setUserTokenState(userToken);

        if(userToken !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/human-resource/structure-position/create`)
                .then(function(response){

                    console.log("Response Original : ", response);
                    // setListJenisUnitAndNamaUnit(response.data.data); 

                    if(response.status == 200 ){

                        //*
                        const structuresListAtasan = [];
            
                        if(response.data.data !== null) {

                            if(response.data.data.parentCollections !== undefined){
                                
                                setListAtasan(response.data.data.parentCollections);
                            };  
                        };  
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        };

    },[]);

    /*
        ``````````````````````````
        HANDLE CHANGE PILIH ATASAN

        ``````````````````````````
    */
    const [ isJenisUnitShow, setJenisUnitShow ] = useState(false);
    const [ parentId, setParentId ] = useState('');

    const [ atasan, setAtasan ] = useState({

        name: ''
    });
    
    const handleChangePilihAtasan = name => e => {

        setAtasan({ ...atasan, [name]: e.target.value });

        // console.log(e.target.value.id);
        setParentId(e.target.value.id);

        if(userTokenState !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/human-resource/structure-position/create?options[filter][parent_id]=${e.target.value.id}`)
                .then(function(response){

                    console.log("Response Original : ", response);

                    if(response.status == 200 ){

                        if(response.data.data !== null){

                            setJenisUnitShow(true);
                            setListJenisUnit(response.data.data.structureUnitTypeCollections);
                            setListAncestor(response.data.data.ancestorCollections);  

                        }
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        
        };
    };

    /*
        ``````````````````````````````
        HANDLE CHANGE PILIH JENIS UNIT

        ``````````````````````````````
    */

    const [ isNamaUnitShow, setNamaUnitShow ] = useState(false);
    const [ jenisUnitId, setJenisUnitId ] = useState('');

    const [ jenisUnit, setJenisUnit ] = useState({

        name: ''
    });
    
    const handleChangeJenisUnit = name => e => {

        setJenisUnit({ ...jenisUnit, [name]: e.target.value });

        console.log(e.target.value);
        setJenisUnitId(e.target.value);

        if(userTokenState !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/human-resource/structure-position/create?options[filter][parent_id]=${parentId}&options[filter][structure_unit_type_id]=${e.target.value}`)
                .then(function(response){

                    console.log("Response Original : ", response);

                    if(response.status == 200 ){

                        if(response.data.data !== null){

                            setNamaUnitShow(true);
                            setListNamaUnit(response.data.data.structureUnitCollections);

                            setListTingkatJabatan(response.data.data.structurePositionLevelCollections);
                            setListJabatan(response.data.data.structurePositionTitleCollections);

                            // setListUmumPosisiJabatan();

                            let structuresListUmumPosisiJabatan = [];

                            if(response.data.data.classificationOptions !== undefined){

                                Object.getOwnPropertyNames(response.data.data.classificationOptions).forEach((val, idx, array) => {
                                        
                                    const data = {
                                            key: val,
                                            value: response.data.data.classificationOptions[val]
                                        };			
    
                                        structuresListUmumPosisiJabatan.push(data);
                                    }
                                );
                                                                
                                setListUmumPosisiJabatan(structuresListUmumPosisiJabatan);

                            };
                            
                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        };
    };


    /*
        ``````````````````````````````
        HANDLE CHANGE PILIH NAMA UNIT

        ``````````````````````````````
    */

    const [ isTingkatJabatanShow, setTingkatJabatanShow ] = useState(false);

    const [ namaUnit, setNamaUnit ] = useState({

        name: ''
    });
    
    const handleChangeNamaUnit = name => e => {

        setNamaUnit({ ...namaUnit, [name]: e.target.value });

        setTingkatJabatanShow(true);
    };

    /*
        ```````````````````````````````````
        HANDLE CHANGE PILIH TINGKAT JABATAN

        ```````````````````````````````````
    */

    const [ isNamaJabatan, setNamaJabatan ] = useState(false);

    const [ tingkatJabatan, setTingkatJabatan ] = useState({

        name: ''
    });
    
    const handleChangeTingkatJabatan = name => e => {

        setTingkatJabatan({ ...tingkatJabatan, [name]: e.target.value });

        setNamaJabatan(true)

    };


    /*
        ```````````````````````````````````
        HANDLE CHANGE PILIH TINGKAT JABATAN

        ```````````````````````````````````
    */

    const [ jabatan, setJabatan ] = useState({

        name: ''
    });
    
    const handleChangeJabatan = name => e => {

        setJabatan({ ...jabatan, [name]: e.target.value });

    };



    /*
        ``````````````````````````````````
        HANDLE CHANGE PILIH CLASSIFICATION

        ``````````````````````````````````
    */

    const [ umumPosisiJabatan, setUmumPosisiJabatan ] = useState({

        name: ''
    });
    
    const handleChangeUmumPosisiJabatan = name => e => {

        setUmumPosisiJabatan({ ...umumPosisiJabatan, [name]: e.target.value });

    };

    // const [ namaUnit, setNamaUnit] = useState('');
    const [ isKodePosisi, setKodePosisi ] = useState(false); 
    const [ kodePosisiValue, setKodePosisiValue ] = useState('');
    
    /*
        `````````````````````````
        HANDLE POST TAMBAH POSISI

        `````````````````````````
    */

    const handleTambahPosisi = () => {

        let data = {

            StructurePosition: {
                structure_unit_id: namaUnit.name,
                structure_position_level_id: tingkatJabatan.name,
                structure_position_title_id: jabatan.name,
                parent_id: atasan.name.id, //* Posisi atasan
                classification_id: umumPosisiJabatan.name, //* Umum, Normal, etc
                code: kodePosisiValue
           
            }
        };

        console.log("Data Tambah Posisi : ", data);

        if(userTokenState !== undefined){
   
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    
 
            axios
                .post(URL_API + `/human-resource/structure-position`, data)
                .then(function(response){
 
                    console.log("Response Original   : ", response);
                    window.location.reload();
 
                })
                .catch(function(error){

                    if(error.response !== undefined){

                        if(error.response.status == 500){
                            
                            alert(error.response.data.message);
                        }
                    };
                    
                    console.log("Error : ", error.response)
                })
        };
    };

    /*
        ``````````
        SUGGESTION

        ``````````
    */
    const [single, setSingle] = React.useState(null);

    function handleChangeSingle(value) {

        setSingle(value);
    };

    function inputComponent({ inputRef, ...props }) {
    
        return <div ref={inputRef} {...props} />;
    }

    function Control(props) {

        const {

            children,
            innerProps,
            innerRef,
            selectProps: { classes, TextFieldProps },
        } = props;


  
        return (
            <TextField
                fullWidth
                InputProps={{
                inputComponent,
                inputProps: {
                    className: classes.input,
                    ref: innerRef,
                    children,
                    ...innerProps,
                },
                }}
                {...TextFieldProps}
            />
        );
    };

    function NoOptionsMessage(props) {
    
        return (
          <Typography
            color="textSecondary"
            className={props.selectProps.classes.noOptionsMessage}
            {...props.innerProps}
          >
            {props.children}
          </Typography>
        );
      }
      
      function Placeholder(props) {

        const { selectProps, innerProps = {}, children } = props;

        return (

          <Typography color="textSecondary" className={selectProps.classes.placeholder} {...innerProps}>
            {children}
          </Typography>
        );
      }

      function SingleValue(props) {

        return (
          <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
            {props.children}
          </Typography>
        );
      };

      function Menu(props) {

        return (
          <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
            {props.children}
          </Paper>
        );
      }
     
    
    const components = {
        Control,
        Menu,
        // MultiValue,
        NoOptionsMessage,
        Option,
        Placeholder,
        SingleValue,
        // ValueContainer,
      };

    /*
        ````````````
        REACT-SELECT

        ````````````
    */

    const options = [

        { value: 'SALES', label: 'SALES' },
        { value: 'MARKETING', label: 'MARKETING' },
        { value: 'CEO', label: 'CEO' },
    ];

    const [ selectedOption, setSelectedOption ] = useState(null);

    const handleChangeSelectAutoComplete = (data) => {
        
        // e.preventDefault();
        // setSelectedOption(selectedOption )
        console.log('SELECTED OPTION : ', data);
    };
    
    return (

        <Dialog
            open={isModalTambah}
            onClose={() => setModalTambah(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Tambah Posisi</b>
            </Typography>
        </DialogTitle>
        <DialogContent>
            <List className={classes.list}>

                <ListItem >  

                    <TextField
                        id="outlined-select-provinsi"
                        select
                        label="Pilih Atasan : "
                        className={classes.textField}
                        value={atasan.name}
                        onChange={handleChangePilihAtasan('name')}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="normal"
                        variant="outlined"
                    >
                        {

                            listAtasan.length > 0 && listAtasan.map ((option, i) => (

                                    <MenuItem key={i} value={option}>
                                        {option.structure_position_title_name}
                                    </MenuItem>
                                )
                            )
                        }
                    </TextField>
                </ListItem>

                {

                    isJenisUnitShow == true && (

                        <span>
                            {/* <ListItem  alignItems='center'>  
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm} >
                                            <b>Atasan-nya :  &nbsp;</b>
                                            
                                        </Typography>
                                    } 
                                />
                                <TextField  
                                    id="outlined-bare"
                                    // onChange={handleChangeNamaBelakang}
                                    // value={ namaBelakang }
                                    placeholder={atasan.name.structure_position_title_name}
                                    className={classes.textFieldPlaceHolder}
                                    inputProps={{className: classes.titleTextPlaceHolder}} 
                                    // disabled={isTextFieldDisabled !== true ? true : false}                        
                                    disabled= {true}
                                />
                            </ListItem> */}

                            <ListItem  alignItems='center'>  
                                
                                <ListItemText 
                                    primary={
                                        <Typography variant='subtitle1' className={classes.titleForm} >
                                            <b>
                                                {atasan.name.structure_unit_type_name !== null ? atasan.name.structure_unit_type_name : "Divisi" }
                                                &nbsp;
                                            </b>

                                        </Typography>
                                    } 
                                />
                                <TextField  
                                    id="outlined-bare"
                                    // onChange={handleChangeNamaBelakang}
                                    // value={namaBelakang}
                                    placeholder={atasan.name.structure_unit_name !== null ? atasan.name.structure_unit_name : "-"}
                                    className={classes.textFieldPlaceHolder}
                                    inputProps={{className: classes.titleTextPlaceHolder}} 
                                    // disabled={isTextFieldDisabled !== true ? true : false}                        
                                    disabled= {true}
                                />
                            </ListItem>
                        </span>
                    )
                }


                {
                    isJenisUnitShow == true && (
                   
                        <ListItem>  
                            <TextField
                                id="outlined-select-list-jenis-unit"
                                select
                                label="Pilih Jenis Unit : "
                                // label={"Pilih " + item.name}
                                className={classes.textField}
                                value={jenisUnit.name}
                                onChange={handleChangeJenisUnit('name')}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                margin="normal"
                                variant="outlined"
                            >
                                {
        
                                    listJenisUnit.length > 0 && listJenisUnit.map ((option, i) => (
        
                                            <MenuItem key={i} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        )
                                    )
                                }
                            </TextField>
                        </ListItem>
                                    
                    )
                }

                {

                    isNamaUnitShow == true && (

                        <ListItem >  
                            <TextField
                                id="outlined-select-provinsi"
                                select
                                label="Pilih Unit : "
                                className={classes.textField}
                                value={namaUnit.name}
                                onChange={handleChangeNamaUnit('name')}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                margin="normal"
                                variant="outlined"
                            >
                                {
        
                                    listNamaUnit.length > 0 && listNamaUnit.map ((option, i) => (
        
                                            <MenuItem key={i} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        )
                                    )
                                }
                            </TextField>
                        </ListItem>
                    )
                }

                {
                    isTingkatJabatanShow == true && (
                        <ListItem >  
                            <TextField
                                id="outlined-select-provinsi"
                                select
                                label="Pilih Tingkat Jabatan : "
                                className={classes.textField}
                                value={tingkatJabatan.name}
                                onChange={handleChangeTingkatJabatan('name')}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                margin="normal"
                                variant="outlined"
                            >
                                {

                                    listTingkatJabatan.length > 0 && listTingkatJabatan.map ((option, i) => (

                                            <MenuItem key={i} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        )
                                    )
                                }

                            </TextField>
                        </ListItem>

                    )
                }

                {
                    isNamaJabatan == true && (

                        <ListItem>  
                            <TextField
                                id="outlined-select-provinsi"
                                select
                                label="Pilih Nama Jabatan : "
                                className={classes.textField}
                                value={jabatan.name}
                                onChange={handleChangeJabatan('name')}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                margin="normal"
                                variant="outlined"
                            >
                                {
        
                                    listJabatan.length > 0 && listJabatan.map ((option, i) => (
        
                                            <MenuItem key={i} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        
                                        )
                                    )
                                }
                            </TextField>
                        </ListItem>
                    )
                }

                {
                    isNamaJabatan == true && (

                        <ListItem >  
                            <TextField
                                id="outlined-select-provinsi"
                                select
                                label="Kategori Posisi : "
                                className={classes.textField}
                                value={umumPosisiJabatan.name}
                                onChange={handleChangeUmumPosisiJabatan('name')}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                margin="normal"
                                variant="outlined"
                            >
                                {

                                    listUmumPosisiJabatan.length > 0 && listUmumPosisiJabatan.map ((option, i) => (

                                            <MenuItem key={i} value={option.key}>
                                                {option.value}
                                            </MenuItem>
                                        )
                                    )
                                }
                            </TextField>
                        </ListItem> 
                    )

                }

                
                {
                    isKodePosisi !== true ? (

                        <Typography 
                            onClick={() => setKodePosisi(true)}
                            variant='subtitle2' className={classes.titleForm} style={{color: '#cc0707', cursor: 'pointer', marginLeft: 16}}>
                            Tambah Kode Posisi ? 
                        </Typography>

                    ) : (
                        <div>
                            {/* <TextField
                                label='Kode Posisi'
                                id="outlined-bare"
                                onChange= {(e) => setKodePosisiValue(e.target.value)}
                                className={classes.textField}
                                style={{marginLeft: 16}}
                                variant="outlined"
                                
                            />
                            <Typography 
                                onClick={() => setKodePosisi(false)}
                                variant='subtitle2' className={classes.titleForm} style={{color: '#cc0707', cursor: 'pointer', marginLeft: 16}}>
                                Batalkan kode posisi
                            </Typography> */}


                            {/* <Select
                                classes={classes}
                                styles={selectStyles}
                                inputId="react-select-single"
                                TextFieldProps={{
                                    label: 'Country',
                                    InputLabelProps: {
                                    htmlFor: 'react-select-single',
                                    shrink: true,
                                    },
                                }}
                                placeholder="Search a country (start with a)"
                                options={suggestions}
                                components={components}
                                value={single}
                                onChange={handleChangeSingle}
                            /> */}
                             <Select
                                // value={selectedOption}
                                options={options}
                                // onChange={() => handleChangeSelectAutoComplete(options)}
                            />
                        </div>
                    )
                }
               

            </List>

            <DialogContentText id="alert-dialog-description">
                <Typography variant='h6'>
                </Typography>
            </DialogContentText>
        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            <Button 
                onClick={() => setModalTambah(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button>
            
            <Button 
                // onClick={() => Redirect(ToHrEmployeeKepegawaianInfoPegawai)}
                onClick= {handleTambahPosisi}
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Tambah Posisi 
            </Button>
        </DialogActions>
        <br />
        <br />
        </Dialog>
    )   
};

export default DialogTambahPosisition_version_mas_tri;

const selectStyles = {
    input: base => ({
      ...base,
    //   color: theme.palette.text.primary,
    color: 'grey',
      '& input': {
        font: 'inherit',
      },
    }),    
  };

