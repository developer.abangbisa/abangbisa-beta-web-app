import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel, NoSsr

} from '@material-ui/core';

import Select from 'react-select';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const DialogTambahPosition = props => {

    const { 

            classes,
            isModalTambah,
            setModalTambah,
            handleSimpanData,
            theme
        
        } = props;

    const selectStyles = {

        input: base => ({
            ...base,
            color: theme.palette.text.primary,
            '& input': {
                font: 'inherit',
            },
        }),
        // menu: provided => ({ ...provided, zIndex: 999999 }),
        // menuList: provided => ({ ...provided, zIndex: 999999 })

    };


const useStyles = makeStyles(theme => ({

    root: {
      flexGrow: 1,
        //   height: 252,
      minWidth: 290,
    },
    input: {
      display: 'flex',
      padding: 0,
      height: 'auto',
    },
    valueContainer: {
      display: 'flex',
      flexWrap: 'wrap',
      flex: 1,
      alignItems: 'center',
      overflow: 'hidden',
    },
    chip: {
      margin: theme.spacing(0.5, 0.25),
    },

    noOptionsMessage: {
      padding: theme.spacing(1, 2),
    },
    singleValue: {
      fontSize: 16,
    },
    placeholder: {
      position: 'absolute',
      left: 2,
      bottom: 6,
      fontSize: 16,
    },
    paper: {
      position: 'absolute',
      zIndex: 1,
      marginTop: theme.spacing(1),
      left: 0,
      right: 0,
    },
    divider: {

      height: theme.spacing(2),
    },

  }));
    
  const classesMakeStyle = useStyles();

    /*
        ```````````````````
        DATA UNTUK DROPDOWN

        ```````````````````
    */

    const [ listAtasan, setListAtasan ] = useState([]);
    const [ listNamaUnit, setListNamaUnit ] = useState([]);
    const [ listNamaJabatan, setListNamajabatan ] = useState([]);
    const [ listTingkatJabatan, setListTingkatJabatan ] = useState([]);
    const [ listJenisUnit, setListJenisUnit ] = useState([]);

    const [ listUmumPosisiJabatan, setListUmumPosisiJabatan ] = useState([]);

    const [ listAncestor, setListAncestor ] = useState([]);
    
    /*
        ````````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    const [ userTokenState, setUserTokenState ] = useState('');
    const userToken = localStorage.getItem('userToken');

    useEffect(() => {

        setUserTokenState(userToken);

        if(userToken !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/human-resource/structure-position/create`)
                .then(function(response){

                    console.log("Response Original : ", response);

                    if(response.status == 200 ){

                        let structuresListAtasan = [];
            
                        if(response.data.data !== null) {

                            if(response.data.data.parentCollections !== undefined){

                                Object.getOwnPropertyNames(response.data.data.parentCollections).forEach((val, idx, array) => {
                                        
                                    // console.log("val : ", val);

                                    const data = {

                                        value: response.data.data.parentCollections[val].id,
                                        label: response.data.data.parentCollections[val].structure_position_title_name,
                                        general: response.data.data.parentCollections[val]
 
                                        
                                    };			

                                    structuresListAtasan.push(data);

                                });
                                                                
                                setListAtasan(structuresListAtasan);
                            };  
                        };  
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        };

    },[]);

    /*
        ``````````````````````````
        HANDLE CHANGE PILIH ATASAN

        ``````````````````````````
    */
    const [ isJenisUnitShow, setJenisUnitShow ] = useState(false);
    const [ parentId, setParentId ] = useState('');

    // const [ atasan, setAtasan ] = useState({

    //     name: ''
    // });

    const [atasan, setAtasan] = useState(null);

    function handleChangePilihAtasan(data) {
 
        console.log("Value : ", data);
         
        setAtasan(data);
        setParentId(data.value)

        if(userTokenState !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/human-resource/structure-position/create?options[filter][parent_id]=${data.value}`)
                .then(function(response){

                    console.log("Response Original : ", response);

                    let structuresListJenisUnit = [];

                    if(response.status == 200 ){

                        // if(response.data.data !== null){

                        //     setJenisUnitShow(true);
                        //     setListJenisUnit(response.data.data.structureUnitTypeCollections);
                        //     setListAncestor(response.data.data.ancestorCollections);  

                        // }

                        if(response.data.data !== null){

                            setJenisUnitShow(true);
                            

                            if(response.data.data.structureUnitTypeCollections !== undefined){

                                Object.getOwnPropertyNames(response.data.data.structureUnitTypeCollections).forEach((val, idx, array) => {
                                        
                                    // console.log("val : ", val);

                                    const data = {

                                        value: response.data.data.structureUnitTypeCollections[val].id,
                                        label: response.data.data.structureUnitTypeCollections[val].name,
                                        // general: response.data.data.structureUnitTypeCollections[val]
 
                                        
                                    };
                                    
                                    // console.log("data ", data);

                                    if(data.value !== undefined){

                                        structuresListJenisUnit.push(data);

                                    } else {

                                        let datas = {
                                            value: 'id80-jenis-unit-no-options',
                                            label: ''
                                        }

                                        structuresListJenisUnit.push(datas);

                                    };
                                    
                                });
                                                                
                                setListJenisUnit(structuresListJenisUnit);
                            };


                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        
        };
    };
 

    /*
        ``````````````````````````````
        HANDLE CHANGE PILIH JENIS UNIT

        ``````````````````````````````
    */

    const [ isNamaUnitShow, setNamaUnitShow ] = useState(false);
    const [ jenisUnitId, setJenisUnitId ] = useState('');

    // const [ jenisUnit, setJenisUnit ] = useState({

    //     name: ''
    // });

    const [jenisUnit, setJenisUnit] = useState(null);

    const handleChangeJenisUnit = (data) => {

        console.log("Value Jenis Unit: ", data);
         
        setJenisUnit(data);

        if(userTokenState !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/human-resource/structure-position/create?options[filter][parent_id]=${parentId}&options[filter][structure_unit_type_id]=${data.value}`)
                .then(function(response){

                    console.log("Response Original : ", response);

                    if(response.status == 200 ){

                        if(response.data.data !== null){

                            setNamaUnitShow(true);
                           
                           if(response.data.data.structureUnitCollections !== undefined){

                                /*
                                    ``````````````
                                    LIST NAMA UNIT
                                    
                                    ``````````````                                
                                */
                                let structureListNamaUnit = [];

                                Object.getOwnPropertyNames(response.data.data.structureUnitCollections).forEach((val, idx, array) => {

                                    let data = {
                                        value: response.data.data.structureUnitCollections[val].id,
                                        label: response.data.data.structureUnitCollections[val].name
                                    };			
    
                                    console.log("Data Nama Unit : ", data);


                                    if(data.value !== undefined){

                                        structureListNamaUnit.push(data)

                                    } else {

                                        let datas = {
                                            value: 'id80-nama-unit-no-options',
                                            label: ''
                                        }

                                        structureListNamaUnit.push(datas);

                                    };
                                    
                                
                                })
                                        
                                setListNamaUnit(structureListNamaUnit);


                                /*
                                    ````````````````````
                                    LIST TINGKAT JABATAN
                                    
                                    ````````````````````                          
                                */

                                let structureListTingkatJabatan = [];
                                
                                Object.getOwnPropertyNames(response.data.data.structurePositionLevelCollections).forEach((val, idx, array) => {
    
                                    let data = {

                                        value: response.data.data.structurePositionLevelCollections[val].id,
                                        label: response.data.data.structurePositionLevelCollections[val].name
                                    };			
    
                                    console.log("Data Tingkat Jabatan : ", data);
    
                                    if(data.value !== undefined){
    
                                        structureListTingkatJabatan.push(data)
    
                                    } else {
    
                                        let datas = {
                                            value: 'id80-tingkat-jabatan-no-options',
                                            label: ''
                                        };
    
                                        structureListTingkatJabatan.push(datas);
    
                                    };
                                    
                                
                                })
    
                                setListTingkatJabatan(structureListTingkatJabatan);


                                /*
                                    `````````````````
                                    LIST NAMA JABATAN
                                    
                                    `````````````````
                                */

                                let structureListNamaJabatan = [];
                                
                                Object.getOwnPropertyNames(response.data.data.structurePositionTitleCollections).forEach((val, idx, array) => {
    
                                    let data = {

                                        value: response.data.data.structurePositionTitleCollections[val].id,
                                        label: response.data.data.structurePositionTitleCollections[val].name
                                    };			
    
                                    console.log("Data Nama Jabatan : ", data);
    
                                    if(data.value !== undefined){
    
                                        structureListNamaJabatan.push(data)
    
                                    } else {
    
                                        let datas = {
                                            value: 'id80-list-nama-jabatan-no-options',
                                            label: ''
                                        };
    
                                        structureListNamaJabatan.push(datas);
    
                                    };
                                    
                                });

                               setListNamajabatan(structureListNamaJabatan);

                            };



                            /*
                                ```````````````````````````````
                                LIST KLASIFIKASI POSISI JABATAN 
                                
                                ```````````````````````````````
                            */


                            let structuresListUmumKlasifikasiPosisiJabatan = [];

                            if(response.data.data.classificationOptions !== undefined){

                                Object.getOwnPropertyNames(response.data.data.classificationOptions).forEach((val, idx, array) => {
                                        
                                    const data = {

                                            value: val,
                                            label: response.data.data.classificationOptions[val]
                                        };			
    
                                        structuresListUmumKlasifikasiPosisiJabatan.push(data);

                                    
                                });
                                                                
                                setListUmumPosisiJabatan(structuresListUmumKlasifikasiPosisiJabatan);

                            };
                            
                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        };
    };
    

    /*
        ``````````````````````````````
        HANDLE CHANGE PILIH NAMA UNIT

        ``````````````````````````````
    */

    const [ isTingkatJabatanShow, setTingkatJabatanShow ] = useState(false);

    const [ namaUnit, setNamaUnit ] = useState(null);
    
    const handleChangeNamaUnit = (data) => {

        console.log("Data nama unit : ", data );
        setNamaUnit(data);

        setTingkatJabatanShow(true);
    };

    /*
        ```````````````````````````````````
        HANDLE CHANGE PILIH TINGKAT JABATAN

        ```````````````````````````````````
    */

    const [ isNamaJabatan, setNamaJabatan ] = useState(false);
    const [ tingkatJabatan, setTingkatJabatan ] = useState(null);
    
    const handleChangeTingkatJabatan = (data) => {

        console.log("Value Tingkat Jabatan : ", data);
        setTingkatJabatan(data);


    };

    /*
        ```````````````````````````````````
        HANDLE CHANGE PILIH NAMA JABATAN

        ```````````````````````````````````
    */
    
    const [ jabatan, setJabatan ] = useState(null)
    const handleChangeNamaJabatan = (data) => {

        console.log("Value Nama Jabatan : ", data);

        setJabatan(data);
    };


    // const [ jabatan, setJabatan ] = useState({

    //     name: ''
    // });
    
    // const handleChangeJabatan = name => e => {

    //     setJabatan({ ...jabatan, [name]: e.target.value });

    // };

    /*
        ``````````````````````````````````
        HANDLE CHANGE PILIH CLASSIFICATION

        ``````````````````````````````````
    */

    const [ umumPosisiJabatan, setUmumPosisiJabatan ] = useState(null);
    
    const handleChangeUmumPosisiJabatan = (data) =>  {

        console.log("Value klasifikasi posisi : ", data);
        
        setUmumPosisiJabatan(data)
    };

    // const [ namaUnit, setNamaUnit] = useState('');
    const [ isKodePosisi, setKodePosisi ] = useState(false); 
    const [ kodePosisiValue, setKodePosisiValue ] = useState('');
    
    /*
        `````````````````````````
        HANDLE POST TAMBAH POSISI

        `````````````````````````
    */

    const handleTambahPosisi = () => {

        let data = {

            StructurePosition: {
                structure_unit_type_id: jenisUnit.value,
                structure_unit_id: namaUnit.value,
                structure_position_level_id: tingkatJabatan.value,
                structure_position_title_id: jabatan.value,
                parent_id: atasan.value, //* Posisi atasan
                classification_id: umumPosisiJabatan.value, //* Umum, Normal, etc
                // code: kodePosisiValue
           
            }
        };

        console.log("Data Tambah Posisi : ", data);

        if(userTokenState !== null){
   
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    
 
            axios
                .post(URL_API + `/human-resource/structure-position`, data)
                .then(function(response){
 
                    console.log("Response Original   : ", response);
                    window.location.reload();
 
                })
                .catch(function(error){

                    if(error.response !== undefined){

                        if(error.response.status == 500){
                            
                            alert(" Error 500 : " + error.response.data.message);
                        }

                        if(error.response.status == 400){

                            alert(" Error 400 : " + error.response.data.message);
                        }
                    };
                    
                    console.log("Error : ", error.response)
                })
        };
    };

    /*
        ``````````
        SUGGESTION

        ``````````
    */

    const options = [

        { value: 'SALES', label: 'SALES' },
        { value: 'MARKETING', label: 'MARKETING' },
        { value: 'CEO', label: 'CEO' },
    ];

    const [ selectedOption, setSelectedOption ] = useState(null);

    const handleChangeSelectAutoComplete = (data) => {
        
        // e.preventDefault();
        // setSelectedOption(selectedOption )
        console.log('SELECTED OPTION : ', data);
    };

    //*************************************** */
    
  /*
        ``````````
        SUGGESTION

        ``````````
    */
   const components = {

    Control,
    Menu,
    // MultiValue,
    NoOptionsMessage,
    Option,
    Placeholder,
    SingleValue,
    ValueContainer,
  };


    
    return (

        <Dialog
            open={isModalTambah}
            onClose={() => setModalTambah(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            fullWidth
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Tambah Posisi</b>
            </Typography>
        </DialogTitle>
        <DialogContent>

            {/* <div className={classesMakeStyle.root}> */}
            <NoSsr>
                <Select
                    classesMakeStyle={classesMakeStyle}
                    options={ listAtasan }
                    styles={ selectStyles }
                    inputId="react-select-single"
                    TextFieldProps={{
                        label: 'Pilih atasan : ',
                        InputLabelProps: {
                        htmlFor: 'react-select-single',
                        shrink: true,
                        },
                    }}

                    placeholder="..."
                    components={components}
                    value={atasan}
                    onChange={handleChangePilihAtasan}
                    // onChange={() => handleChangeSingle(listAtasan)}

                />

            </NoSsr>

            { 
                isJenisUnitShow == false && (
                    <span>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />                       
                    </span>
                )
            }
                    
            {/* </div> */}

            {
                isJenisUnitShow == true && (

                    <List>
                        <ListItem  alignItems='center'>  
                            <ListItemText 
                                primary={
                                    <Typography variant='subtitle1' className={classes.titleForm} >
                                        <b>
                                            {atasan.general.structure_unit_type_name !== null ? atasan.general.structure_unit_type_name + " : ": "Divisi : " }
                                            &nbsp;
                                        </b>

                                    </Typography>
                                } 
                            />
                            <TextField  
                                id="outlined-bare"
                                placeholder={atasan.general.structure_unit_name !== null ? atasan.general.structure_unit_name : "-"}
                                className={classes.textFieldPlaceHolder}
                                inputProps={{className: classes.titleTextPlaceHolder}} 
                                disabled= {true}
                            />
                        </ListItem>
                        <br />

                    </List>
                )
            }

            {
                isJenisUnitShow == true && (
                
                    <NoSsr>
                        <Select
                            classesMakeStyle={classesMakeStyle}
                            options={ listJenisUnit }
                            styles={ selectStyles }
                            inputId="pilih-jenis-unit"
                            TextFieldProps={{
                                label: 'Pilih Jenis Unit : ',
                                InputLabelProps: {
                                htmlFor: "pilih-jenis-unit",
                                shrink: true,
                                },
                            }}
        
                            placeholder="..."
                            components={components}
                            value={jenisUnit}
                            onChange={handleChangeJenisUnit}        
                        />
                        <br />
                    </NoSsr>      
                )
            }

            {
                isNamaUnitShow == true && (
                
                    <NoSsr>
                        <Select
                            classesMakeStyle={classesMakeStyle}
                            options={ listNamaUnit }
                            styles={ selectStyles }
                            inputId="pilih-nama-unit"
                            TextFieldProps={{
                                label: 'Pilih Nama Unit : ',
                                InputLabelProps: {
                                    htmlFor: "pilih-nama-unit",
                                    shrink: true,
                                },
                            }}
        
                            placeholder="..."
                            components={components}
                            value={namaUnit}
                            onChange={handleChangeNamaUnit}        
                        />
                        <br />
                    </NoSsr>      
                )
            }


            {
                isTingkatJabatanShow == true && (
                
                    <NoSsr>
                        <Select
                            classesMakeStyle={classesMakeStyle}
                            options={ listTingkatJabatan }
                            styles={ selectStyles }
                            inputId="pilih-tingkat-jabatan"
                            TextFieldProps={{
                                label: 'Pilih Tingkat Jabatan : ',
                                InputLabelProps: {
                                    htmlFor: "pilih-tingkat-jabatan",
                                    shrink: true,
                                },
                            }}
        
                            placeholder="..."
                            components={components}
                            value={tingkatJabatan}
                            onChange={handleChangeTingkatJabatan}        
                        />
                        <br />

                        <Select
                            classesMakeStyle={classesMakeStyle}
                            options={ listNamaJabatan }
                            styles={ selectStyles }
                            inputId="pilih-nama-jabatan"
                            TextFieldProps={{
                                label: 'Pilih Nama Jabatan : ',
                                InputLabelProps: {
                                    htmlFor: "pilih-nama-jabatan",
                                    shrink: true,
                                },
                            }}
        
                            placeholder="..."
                            components={components}
                            value={jabatan }
                            onChange={handleChangeNamaJabatan}        
                        />
                        <br />

                        <Select
                            classesMakeStyle={classesMakeStyle}
                            options={ listUmumPosisiJabatan }
                            styles={ selectStyles }
                            inputId="pilih-klasifikasi-posisi"
                            TextFieldProps={{
                                label: 'Kategori Posisi : : ',
                                InputLabelProps: {
                                    htmlFor: "pilih-klasifikasi-posisi",
                                    shrink: true,
                                },
                            }}
        
                            placeholder="..."
                            components={components}
                            value={umumPosisiJabatan }
                            onChange={handleChangeUmumPosisiJabatan}        
                        />
                        <br />


                    </NoSsr>  
                    
                    
                )
            }



            <DialogContentText id="alert-dialog-description">
                <Typography variant='h6'>
                </Typography>
            </DialogContentText>
        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            <Button 
                onClick={() => setModalTambah(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button>
            
            <Button 
                onClick= {handleTambahPosisi}
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Tambah Posisi 
            </Button>

        </DialogActions>
        <br />
    </Dialog>
    )   
};

export default DialogTambahPosition;

function inputComponent({ inputRef, ...props }) {

    return <div ref={inputRef} {...props} />;
};

function Control(props) {

    const {

        children,
        innerProps,
        innerRef,
        selectProps: { classesMakeStyle, TextFieldProps },
    } = props;

    return (
        <TextField
            fullWidth
            InputProps={{
            inputComponent,
            inputProps: {
                className: classesMakeStyle.input,
                ref: innerRef,
                children,
                ...innerProps,
            },
            }}
            {...TextFieldProps}
        />
    );
};

function NoOptionsMessage(props) {

    return (
        <Typography
        color="textSecondary"
        className={props.selectProps.classesMakeStyle.noOptionsMessage}
        {...props.innerProps}
        >
        {props.children}
        </Typography>
    );
};
     
function Placeholder(props) {

    const { selectProps, innerProps = {}, children } = props;

    return (

        <Typography color="textSecondary" className={selectProps.classesMakeStyle.placeholder} {...innerProps}>
        {children}
        </Typography>
    );
};

function SingleValue(props) {

    return (
        <Typography className={props.selectProps.classesMakeStyle.singleValue} {...props.innerProps}>
        {props.children}
        </Typography>
    );

};

function Menu(props) {

    return (
        <Paper square className={props.selectProps.classesMakeStyle.paper} {...props.innerProps}>
            {props.children}
        </Paper>
    );
};
    

function Option(props) {

    return (
        <MenuItem
            ref={props.innerRef}
            selected={props.isFocused}
            component="div"
            style={{
                fontWeight: props.isSelected ? 500 : 400,
            }}
            {...props.innerProps}
        >
         {props.children}
        </MenuItem>
    );
};


function ValueContainer(props) {
  
    return <div className={props.selectProps.classesMakeStyle.valueContainer}>{props.children}</div>;

};
      