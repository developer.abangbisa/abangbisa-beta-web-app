import React, { useCallback, useEffect, useState, useContext} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead
    
} from '@material-ui/core';

import SearchIcon from '@material-ui/icons/Search';
import NonActiveIcon from '@material-ui/icons/Close';
import axios from 'axios';
import moment from 'moment';

// import DialogModalTambahRole from './ComponetViewRole/DialogModalTambahRole';
import IconPreviewGrid from '../../../assets/images/SVG/Group_1002.svg';
import IconPreviewList from '../../../assets/images/SVG/Group_1001_red.svg';
import IconFilterNew from '../../../assets/images/SVG/Group_1117.svg';
import IconSettingMasterStructure from '../../../assets/images/Subtraction_27.png';
import IconChart from '../../../assets/images/Group_1002.png';

import ButtonColorGrey from '../../../components/ButtonColorGrey';
import EnhancedTableToolbar from './Components/EnhancedTableToolbar';

import DialogTambahPosition from './ComponentTambah/DialogTambahPosition';
// import DialogTambahPosition from './ComponentTambah/DialogTambahPosisition_version_mas_tri';


import DialogModalDetail from './Components/DialogModalDetail';

import { useGetHttp } from './Hook/useGetHttp';
import { URL_API } from '../../../constants/config-api';
import Redirect from '../../../utilities/Redirect';
import { ToRoleDetail, ToSOMaster, ToOrganigram } from '../../../constants/config-redirect-url';
import { Steps, Hints} from 'intro.js-react';
import { styles } from './Style/StyleTable';
import 'intro.js/introjs.css';
 
const theme = createMuiTheme({
  
    palette: {

        primary: {
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }

});

const rows = [

    { id: 'name_jabatan', align: 'justify', disablePadding: false, label: 'Nama Jabatan' },
    { id: 'structure_unit', align: 'justify', disablePadding: false, label: 'Struktur Unit' },
    { id: 'nama_karyawan', align: 'justify', disablePadding: false, label: 'Nama Karyawan' },
    // { id: 'atasan', align: 'left', disablePadding: false, label: 'Atasan' },
    { id: 'kode_posisi', align: 'left', disablePadding: false, label: 'Kode Posisi' },
    { id: 'jenis_hierarki', align: 'justify', disablePadding: false, label: 'Jenis Hirarki' },
    { id: 'cta', align: 'justify', disablePadding: false, label: '' }

];

const ViewSOTable = props => {

    const { classes } = props;

    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('calories');
    const [selected, setSelected] = React.useState([]);
    
    /* 
    
      ````````````````````
      COMPONENT DID MOUNT

      ````````````````````
      
    */

    const enabledIntroStatus = localStorage.getItem('intro_enabled');

    const [ data, setData ] = useState([]);
    const userToken = localStorage.getItem('userToken');
    const [ userTokenState, setUserTokenState ] = useState('');

    /*
        `````
        INTRO

        `````
    */

    const [ stepsEnabled, setStepsEnabled ] = useState(false);
    const [ initialStep ] = useState(0);
    const [ steps ] = useState([
      {
        element: '.introInIconGear',
        intro: 'Klik <i>Icon Pengaturan </i> untuk kembali ke <u><i>Master Struktur Organisasi</i></u>',
      },
      {
        element: '.introTambahPosisi',
        intro: 'Dan sekarang Anda bisa menambahkan posisi untuk jabatan yang Anda inginkan melalui tombol ini ! ',
      },
    ]);


    const handleDoneJanganTampilkanPesan = () => {

      localStorage.setItem('intro_enabled', false);

    };

   useEffect(() => {

      /*
        ``````
        TASK-1
        ``````
      */
      if(enabledIntroStatus == 'false'){

        setStepsEnabled(false);

      } else {

        setStepsEnabled(true);
      }
      

      /*
        ``````
        TASK-2
        ``````
      */


       setUserTokenState(userToken);

       if(userToken !== undefined){
   
           const header =  {       
               'Accept': "application/json",
               'Content-Type' : "application/json",
               'Authorization' : "bearer " + userToken,
           };
       
           axios.defaults.headers.common = header;    

           axios
               .get(URL_API + `/human-resource/structure-position`)
               .then(function(response){

                   console.log("Response Original POSITION   : ", response);
                   setData(response.data.data);

               })
               .catch(function(error){
                   
                   console.log("Error : ", error.response)
               })
       };

    },[]);

    // const [data] = useState(inisiateDummy)
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
  
    function handleRequestSort(event, property) {

      const isDesc = orderBy === property && order === 'desc';
      setOrder(isDesc ? 'asc' : 'desc');
      setOrderBy(property);
    
    };
  
    function handleSelectAllClick(event) {

      if (event.target.checked) {
        
        const newSelecteds = data.map(n => n.id);
        setSelected(newSelecteds);
        return;
      };

      setSelected([]);
    }
  
    const [idRole, setIdRole ] = useState([]);

    /*

      ````````````````
        Get ID Role

      ````````````````
    */

    function handleClick(event, id) {

      let ids = [];
      console.log("Id :", id);
      const newIds = [...ids, {id: id}]

      setIdRole(newIds);

      /*

        ````````````````
          
          *********

        ````````````````
      */

      const selectedIndex = selected.indexOf(id);

      let newSelected = [];

      if (selectedIndex === -1) {

        newSelected = newSelected.concat(selected, id);

      } else if (selectedIndex === 0) {

        newSelected = newSelected.concat(selected.slice(1));

      } else if (selectedIndex === selected.length - 1) {

        newSelected = newSelected.concat(selected.slice(0, -1));

      } else if (selectedIndex > 0) {

        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      };

      setSelected(newSelected);

    };
  
    function handleChangePage(event, newPage) {

      setPage(newPage);
    };
  
    function handleChangeRowsPerPage(event) {

      setRowsPerPage(event.target.value);
    };
  
    const isSelected = id => selected.indexOf(id) !== -1;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, (data !== null && data !== undefined  ? data.length : 0 ) - page * rowsPerPage);

    /* 
      ```````````````````
      Row Tab List & Grid

      ```````````````````
    */
    const [ isModalTambahRole, setModalTambahRole ] = useState(false);

    /* 
      ````````````````````
      HANDLE BUTTON DETAIL 

      ````````````````````
    */

    const [isModalDetail, setModalDetail] = useState(false);
    const [ dataDetail, setDataDetail ] = useState(null);

    const handleButtonDetail = (e, data) => {
    
      // console.log("Data Detail clicked : ", data);

      setModalDetail(true);
      setDataDetail(data);

    };


    /* 
      ````````````````````
      HANDLE MODAL TAMBAH

      ````````````````````
    */
    const [ isModalTambah, setModalTambah ] = useState(false);

    /*
      ``````````````````
      HANDLE CHECKed BOX

      ``````````````````
    */
    const [idSO, setIdSO ] = useState('');
    const handleCheckBox = (e, id) => {

      e.preventDefault();

      let ids = [];
      console.log("Id :", id);
      const newIds = [...ids, {id: id}]

      setIdSO(newIds);

      /*

        ````````````````
          
          *********

        ````````````````
      */

      // setIdSelected(data.id)

      const selectedIndex = selected.indexOf(id);

      let newSelected = [];

      if (selectedIndex === -1) {

        newSelected = newSelected.concat(selected, id);

      } else if (selectedIndex === 0) {

        newSelected = newSelected.concat(selected.slice(1));

      } else if (selectedIndex === selected.length - 1) {

        newSelected = newSelected.concat(selected.slice(0, -1));

      } else if (selectedIndex > 0) {

        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1),
        );
      };

      setSelected(newSelected);


    };

    return (

        <MuiThemeProvider theme={theme}>

          <Steps
            enabled={stepsEnabled}
            steps={steps}
            initialStep={initialStep}
            onExit={handleDoneJanganTampilkanPesan}
          />
  
            <br />
            <Paper className={classes.root}  elevation={0}>

            {/* 
            
                ```````````````````
                Row Tab List & Grid

                ```````````````````
            */}

            <Grid  
                container
                spacing={8}
                direction="row"
                justify="flex-end"
                alignItems="center"
            >   
                <Grid item sm={9}>
                    <List dense >
                        <ListItem button>
                        <ListItemText 
                            // primary="Pengaturan Role"
                            primary={
                                <Typography variant='h6' className={classes.title}>
                                    <b>Struktur Organisasi</b>
                                </Typography>
                            } 
                         />

                        </ListItem>
                    </List>
                </Grid>
                <Grid item sm={3} style={{textAlign: 'center'}}>
                      
                    <IconButton
                      className='introInIconGear'
                      onClick={ () => Redirect(ToSOMaster)}
                    >
                      <Tooltip title="Pengaturan Master Struktur Organisasi" placement="left-start">
                        <img src={IconSettingMasterStructure} className={classes.iconPreviewSettingGear} />
                      </Tooltip>
                    </IconButton>
                            
                    <Button 
                      onClick={() => Redirect(ToOrganigram)}
                      variant='outlined' size='small' className={classes.buttonStatusActive} >Organigram
                          <img src={IconChart} className={classes.iconPreviewChart} />
                      </Button>
                </Grid>

            </Grid>

            {/* 
            
                ````````````````````````````````````
                    Row Button Tambah Role & Filter 

                ````````````````````````````````````
                
            */}

            <br />
            <Grid  
                container
                spacing={0}
                direction="row"
                justify="flex-end"
            >   
                <Grid item sm={6} style={{textAlign: 'left'}}>
                    <Typography variant='subtitle2' className={classes.title} style={{marginLeft: 16}}>
                        <b>Daftar Posisi</b>
                    </Typography>
                </Grid>
                <Grid item sm={3} style={{textAlign: 'right'}}>
                    <Paper className={classes.paper} elevation={1}>
                        <IconButton className={classes.iconButton} aria-label="Search">
                            <SearchIcon />
                        </IconButton>
                        <InputBase className={classes.input} placeholder="Cari posisi..." />
                        <Divider className={classes.divider} />
                    </Paper>
                </Grid>
                <Grid item sm={1} style={{textAlign: 'right'}}>
                    <Tooltip title="Filter" placement="right-start">
                        <img src={IconFilterNew} className={classes.iconFilter} />
                    </Tooltip>
                </Grid>
                <Grid 
                  item 
                  sm={2} 
                  className='introTambahPosisi'
                  style={{textAlign: 'center'}}
                >
                    <Button 
                      onClick={() => setModalTambah(true)}
                      variant="contained" 
                      className={classes.button} 
                      style={{marginTop: 10, marginRight: 14}}>
                          + Tambah Posisi  
                    </Button>
                </Grid>
            </Grid>
            <br />

            {/* 
            
                `````````````````
                    TABLE

                `````````````````
            */}

            <div className={classes.tableWrapper}>

                {
                    data.length == 0 && (
                        <Grid  
                            container
                            // spacing={10}
                            direction="row"
                            justify="center"
                            alignItems="center"
                        >
                            
                            <Grid item sm={12} style={{textAlign: 'center'}}> 
                                <CircularProgress size={32} style={{marginTop: 64, color: 'red'}} />
                            </Grid>
                            
                        </Grid>
                    )
                }

                <Table className={classes.table} aria-labelledby="tableTitle">

                    {
                        data !== null && data !== undefined && data.length > 0 ? (

                            <EnhancedTableHead
                                numSelected={selected.length}
                                order={order}
                                orderBy={orderBy}
                                onSelectAllClick={handleSelectAllClick}
                                onRequestSort={handleRequestSort}
                                rowCount={data !== null ? data.length : 0}
                            /> 
                        ) : null
                    }
                

                    <TableBody>

                        {
                            stableSort(data, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map(n => {

                                    // console.log("n : ", n);
                                    
                                    const isItemSelected = isSelected(n.id);
                                    
                                    return (

                                        <TableRow
                                            hover
                                            // onClick={event => handleClick(event, n.id)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={n.id}
                                            selected={isItemSelected}
                                            // style={{textAlign: 'center'}}
                                        >
                                            <TableCell padding="checkbox">
                                              <Checkbox 
                                                // checked={n.id == idSelected ? true : false} 
                                                onChange={(e) => handleCheckBox(e,n.id)}
                                                checked={isItemSelected}
                                                
                                              />
                                                {/* {
                                                  n.is_locked !== 1 ? (

                                                      <Checkbox checked={isItemSelected} />

                                                      ) : (
                                                        
                                                        <Checkbox disabled checked /> 

                                                      )
                                                } */}
                                            </TableCell>

                                            <TableCell 
                                              align='justify'  
                                              component="th" 
                                              scope="row" 
                                              // padding="inset"
                                              inset='true'
                                              style={{fontFamily: 'Nunito', fontWeight: 'bold'}}
                                            >
                                                {n.structure_position_title_name}
                                            </TableCell>

                                            <TableCell 
                                              align='left' 
                                              style={{fontFamily: 'Nunito'}}
                                            >
                                                {/* {moment(n.created_at).format('DD MMMM YYYY')} */}
                                                {n.structure_unit_name}
                                            </TableCell>
                                            
                                            <TableCell align='left'>
                                                {n.member_first_name !== null ? n.member_first_name + " " + n.member_last_name : '-'}
                                            </TableCell>


                                                
                                            {/* <TableCell align='left'>
                                                n.parent_id
                                                - 
                                                
                                            </TableCell> */}

                                            <TableCell align='left'>
                                                { n.code !== null ? n.code : '-' }
                                            </TableCell>
                                            
                                            <TableCell align='left'>
                                                {n.classification.name}
                                            </TableCell>

                                            <TableCell align='center'> 
                                                {/* 
                                                  <IconButton>
                                                      <i className='material-icons' >
                                                          more_vert
                                                      </i>
                                                  </IconButton> 
                                                S*/}
                                                <Button 
                                                  size='small'
                                                  variant='outlined' 
                                                  // className={classes.button}
                                                  onClick={(e) => handleButtonDetail(e, n )}
                                                  className={classes.buttonTable}
                                                >
                                                  Detail
                                                </Button>
                                                

                                            </TableCell>
                                        </TableRow>

                                    );
                                })
                        }
                        {/* {
                            emptyRows > 0 && (
                                <TableRow style={{ height: 49 * emptyRows }}>
                                <TableCell colSpan={6} />
                                </TableRow>
                            )
                        } */}
                        </TableBody>
                    </Table>
                </div>

                {/* ROW OF DELETE  */}
                <EnhancedTableToolbar numSelected={selected.length} id={idSO} />
        
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={data !== null && data !== undefined ? data.length : 0}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                    'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                    'aria-label': 'Next Page',
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />

            {/* <DialogModalTambahRole 
              classes= {classes}
              isModalTambahRole={isModalTambahRole}
              setModalTambahRole={setModalTambahRole}
              setFetchedData= {setFetchedData}
              data= {data}
            /> */}
            </Paper>

            
            <DialogTambahPosition
              classes = { classes }
              isModalTambah = { isModalTambah }
              setModalTambah = { setModalTambah }
              theme = { theme }
            />

            <DialogModalDetail
              classes = { classes }
              isModalDetail = { isModalDetail }
              setModalDetail = { setModalDetail }
              dataDetail = {dataDetail}

            />
        </MuiThemeProvider>
    )
};

export default withStyles(styles)(ViewSOTable);


function EnhancedTableHead(props) {

    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  
    const createSortHandler = property => event => {
  
      onRequestSort(event, property);
    };
  
    return (
  
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>

          {
            rows.map (
  
              row => (
  
                <TableCell
                  align={row.align}
                  key={row.id}
                  numeric={row.numeric}
                  padding={row.disablePadding ? 'none' : 'default'}
                  sortDirection={orderBy === row.id ? order : false}

                  style={{fontFamily: 'Nunito', fontWeight: 'bold', color: 'black'}}
                >
                  <Tooltip
                    title="Sort"
                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                    enterDelay={300}
                  >
                    <TableSortLabel
                      active={orderBy === row.id}
                      direction={order}
                      onClick={createSortHandler(row.id)}
                    >
                      {row.label}
                    </TableSortLabel>
                  </Tooltip>
                </TableCell>
  
              ), this
            )
          }
        </TableRow>
      </TableHead>
    );
  };


// let counter = 0;    

// function createData(name, calories, fat, carbs, protein) {

//   counter += 1;
//   return { id: counter, name, calories, fat, carbs, protein };

// };

function desc(a, b, orderBy) {

    if (b[orderBy] < a[orderBy]) {
      return -1;
    };
  
    if (b[orderBy] > a[orderBy]) {
      return 1;
    };
  
    return 0;
  
  };  
  
  function stableSort(array, cmp) {
  
      if(array !== null && array !== undefined){
  
        const stabilizedThis = array.map((el, index) => [el, index]);
  
        stabilizedThis.sort((a, b) => {
        
          const order = cmp(a[0], b[0]);
        
          if (order !== 0) return order;
        
          return a[1] - b[1];
  
        });
    
        return stabilizedThis.map(el => el[0]);
      };
  
      return [];
  
  };
    
  function getSorting(order, orderBy) {
    
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
      
  };
      