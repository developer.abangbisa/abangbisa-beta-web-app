const data = [

    {
        id: '123direksi',
        nama_jabatan: 'CEO',
        struktur_unit: 'Direksi CEO',
        nama_karyawan: 'Bapak Deny',
        atasan: '-',
        kode_posisi: '-',
        jenis_hierarki: 'Umum'
    },
    {
        id: '123hrd',
        nama_jabatan: 'Manajer HRD',
        struktur_unit: 'Divisi HRD',
        nama_karyawan: 'Bapak Wardoyo',
        atasan: 'CEO',
        kode_posisi: '-',
        jenis_hierarki: 'Umum'
    },
    {
        id: '123finance',
        nama_jabatan: 'Manajer Finance',
        struktur_unit: 'Divisi Finance',
        nama_karyawan: 'Galih',
        atasan: 'CEO',
        kode_posisi: '-',
        jenis_hierarki: 'Umum'
    },
    {
        id: '123sales',
        nama_jabatan: 'Manajer Sales',
        struktur_unit: 'Divisi Sales',
        nama_karyawan: '(Vacancy)',
        atasan: 'CEO',
        kode_posisi: '-',
        jenis_hierarki: 'Umum'
    },
    {
        nama_jabatan: 'Sales-MAN',
        struktur_unit: 'Divisi Sales',
        nama_karyawan: '(Vacancy)',
        atasan: 'Manajer Sales',
        kode_posisi: '-',
        jenis_hierarki: 'Umum'
    }

];


export default data;