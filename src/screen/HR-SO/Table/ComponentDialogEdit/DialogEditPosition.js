import React, { Component, useEffect, useState } from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 
    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, IconButton, InputBase, Divider,
    Tooltip, List, ListItem, ListItemText, Avatar, Table, TableBody, TableRow, TableCell,
    Chip, TablePagination, TableHead, TableSortLabel, Radio, FormLabel

} from '@material-ui/core';


import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';


const DialogEditPosition = props => {

    const { 
            classes,
            isModalEdit,
            setModalEdit,
            // handleChangeNamaAnggotaKeluarga,
            // handleSimpanData,
            dataDetail
        
        } = props;
    
    /*
        ```````````````````
        DATA UNTUK DROPDOWN

        ```````````````````
    */

    const [ listAtasan, setListAtasan ] = useState([]);
    const [ listNamaUnit, setListNamaUnit ] = useState([]);
    const [ listJabatan, setListJabatan ] = useState([]);
    const [ listTingkatJabatan, setListTingkatJabatan ] = useState([]);

    const [ listUmumPosisiJabatan, setListUmumPosisiJabatan ] = useState([]);


    /*
        ````````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    const [ userTokenState, setUserTokenState ] = useState('');
    const userToken = localStorage.getItem('userToken');

    const [ updatedAt, setUpdatedAt ] = useState('');

    useEffect(() => {

        setUserTokenState(userToken);

        if(userToken !== undefined){
    
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
            };
        
            axios.defaults.headers.common = header;    

            axios
                .get(URL_API + `/human-resource/structure-position/create`)
                .then(function(response){

                    console.log("Response Original : ", response);
                    // setListJenisUnitAndNamaUnit(response.data.data);

                    if(response.status == 200 ){

                        //*
                        const structuresListAtasan = [];
            
                        if(response.data.data !== undefined) {

                            if(response.data.data.parentCollections !== undefined){
                                
                                setListAtasan(response.data.data.parentCollections);
                            };  
                            

                            //**
                            const structuresListNamaUnit = [];

                            if(response.data.data.structureUnitOptions !== undefined){

                                Object.getOwnPropertyNames(response.data.data.structureUnitOptions).forEach((val, idx, array) => {
                                        
                                    const data = {
                                            key: val,
                                            value: response.data.data.structureUnitOptions[val]
                                        };			
    
                                        structuresListNamaUnit.push(data);
                                    }
                                );
                                                                
                                setListNamaUnit(structuresListNamaUnit);

                            };


                            //***
                            const structuresListJabatan = [];
                            
                            if(response.data.data.structurePositionTitleOptions !== undefined){

                                Object.getOwnPropertyNames(response.data.data.structurePositionTitleOptions).forEach((val, idx, array) => {
                                        
                                    const data = {
                                            key: val,
                                            value: response.data.data.structurePositionTitleOptions[val]
                                        };			
    
                                        structuresListJabatan.push(data);
                                    }
                                );
                                                                
                                setListJabatan(structuresListJabatan);

                            };

                            //****
                            const structuresListTingkatJabatan = [];

                            
                            if(response.data.data.structurePositionLevelOptions !== undefined){

                                Object.getOwnPropertyNames(response.data.data.structurePositionLevelOptions).forEach((val, idx, array) => {
                                        
                                    const data = {
                                            key: val,
                                            value: response.data.data.structurePositionLevelOptions[val]
                                        };			
    
                                        structuresListTingkatJabatan.push(data);
                                    }
                                );
                                                  
                                setListTingkatJabatan(structuresListTingkatJabatan);

                            };

                            //*****
                            const structuresListUmumPosisiJabatan = [];

                            if(response.data.data.classificationOptions !== undefined){

                                Object.getOwnPropertyNames(response.data.data.classificationOptions).forEach((val, idx, array) => {
                                        
                                    const data = {
                                            key: val,
                                            value: response.data.data.classificationOptions[val]
                                        };			
    
                                        structuresListUmumPosisiJabatan.push(data);
                                    }
                                );
                                                                
                                setListUmumPosisiJabatan(structuresListUmumPosisiJabatan);

                            };

                            
                        };  

                    };

                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })


                /*
                    ``````````````
                    GET UPDATED AT

                    ``````````````
                */
                if(dataDetail !== null) {
                    if(dataDetail.parent_id !== null){

                        axios
                         .get(URL_API + `/human-resource/structure-position/${dataDetail.parent_id}/patch`)
                         .then(function(response){
                            
                            console.log("Response Original LABEL PATCH : ", response);
                            if(response.status == 200){

                                setUpdatedAt(response.data.data.updated_at)

                            };
                            
                         })
                         .catch(function(error){
                             
                             console.log("Error : ", error.response)
                         })
                 
                    };
                }
        };

    },[isModalEdit]);
    /*
        `````````
        FORM DATA

        `````````
    */

    const [ atasan, setAtasan ] = useState({

        name: ''
    });
    
    const handleChangePilihAtasan = name => e => {

        setAtasan({ ...atasan, [name]: e.target.value });

    };

    const [ namaUnit, setNamaUnit ] = useState({

        name: ''
    });
    
    const handleChangeNamaUnit = name => e => {

        setNamaUnit({ ...namaUnit, [name]: e.target.value });

    };

    const [ jabatan, setJabatan ] = useState({

        name: ''
    });
    
    const handleChangeJabatan = name => e => {

        setJabatan({ ...jabatan, [name]: e.target.value });

    };

    const [ tingkatJabatan, setTingkatJabatan ] = useState({

        name: ''
    });
    
    const handleChangeTingkatJabatan = name => e => {

        setTingkatJabatan({ ...tingkatJabatan, [name]: e.target.value });

    };

    const [ umumPosisiJabatan, setUmumPosisiJabatan ] = useState({

        name: ''
    });
    
    const handleChangeUmumPosisiJabatan = name => e => {

        setUmumPosisiJabatan({ ...umumPosisiJabatan, [name]: e.target.value });

    };

    // const [ namaUnit, setNamaUnit] = useState('');
    const [ isKodePosisi, setKodePosisi ] = useState(false); 
    const [ kodePosisiValue, setKodePosisiValue ] = useState('');

    
    /*
        ``````````````````
        HANDLE EDIT POSISI

        ``````````````````
    */

    const handleEdit = () => {

        let data = {

            StructurePosition: {
                updated_at: updatedAt,
                structure_unit_id: namaUnit.name,
                structure_position_level_id: tingkatJabatan.name,
                structure_position_title_id: jabatan.name,
                code: kodePosisiValue,
                member_id: ''
           
            },
            _method: 'patch'

        };

        console.log("Data Edit Posisi : ", data);

        if(userTokenState !== undefined){
   
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
            };
        
            axios.defaults.headers.common = header;    
 
            axios
                .post(URL_API + `/human-resource/structure-position/${dataDetail !== null ? dataDetail.parent_id : ''}`, data)
                .then(function(response){
 
                    console.log("Response Original EDIT POSITION   : ", response);
                    window.location.reload();
 
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response)
                })
        };
    };

    return (

        <Dialog
            open={isModalEdit}
            onClose={() => setModalEdit(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
            <Typography variant='subtitle1' className={classes.title}>
                <b>Edit Posisi</b>
            </Typography>
        </DialogTitle>
        <DialogContent>
            <List className={classes.list}>

                <ListItem >  
                    <TextField
                        id="outlined-select-provinsi"
                        select
                        label="Pilih Atasan : "
                        className={classes.textField}
                        value={atasan.name}
                        onChange={handleChangePilihAtasan('name')}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="normal"
                        variant="outlined"
                    >
                        {

                            listAtasan.length > 0 && listAtasan.map ((option, i) => (

                                    // <MenuItem key={i} value={option.key}>
                                    //     {option.value}
                                    // </MenuItem>
                                    <MenuItem key={i} value={option.id}>
                                        {option.structure_position_title_name}
                                    </MenuItem>
                                )
                            )
                        }
                    </TextField>
                </ListItem>

                 <ListItem >  
                    <TextField
                        id="outlined-select-provinsi"
                        select
                        label="Pilih Unit : "
                        className={classes.textField}
                        value={namaUnit.name}
                        onChange={handleChangeNamaUnit('name')}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="normal"
                        variant="outlined"
                    >
                        {

                            listNamaUnit.length > 0 && listNamaUnit.map ((option, i) => (

                                    <MenuItem key={i} value={option.key}>
                                        {option.value}
                                    </MenuItem>
                                )
                            )
                        }
                    </TextField>
                </ListItem>


                <ListItem >  
                    <TextField
                        id="outlined-select-provinsi"
                        select
                        label="Pilih Jabatan : "
                        className={classes.textField}
                        value={jabatan.name}
                        onChange={handleChangeJabatan('name')}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="normal"
                        variant="outlined"
                    >
                        {

                            listJabatan.length > 0 && listJabatan.map ((option, i) => (

                                    <MenuItem key={i} value={option.key}>
                                        {option.value}
                                    </MenuItem>
                                 
                                )
                            )
                        }
                    </TextField>
                </ListItem>

                <ListItem >  
                    <TextField
                        id="outlined-select-provinsi"
                        select
                        label="Pilih Tingkat Jabatan : "
                        className={classes.textField}
                        value={tingkatJabatan.name}
                        onChange={handleChangeTingkatJabatan('name')}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="normal"
                        variant="outlined"
                    >
                        {

                            listTingkatJabatan.length > 0 && listTingkatJabatan.map ((option, i) => (

                                    <MenuItem key={i} value={option.key}>
                                        {option.value}
                                    </MenuItem>
                                )
                            )
                        }

                    </TextField>
                </ListItem>

                <ListItem >  
                    <TextField
                        id="outlined-select-provinsi"
                        select
                        label="Posisi Jabatan : "
                        className={classes.textField}
                        value={umumPosisiJabatan.name}
                        onChange={handleChangeUmumPosisiJabatan('name')}
                        SelectProps={{
                            MenuProps: {
                                className: classes.menu,
                            },
                        }}
                        margin="normal"
                        variant="outlined"
                    >
                        {

                            listUmumPosisiJabatan.length > 0 && listUmumPosisiJabatan.map ((option, i) => (

                                    <MenuItem key={i} value={option.key}>
                                        {option.value}
                                    </MenuItem>
                                )
                            )
                        }
                    </TextField>
                </ListItem>

                
                {
                    isKodePosisi !== true ? (

                        <Typography 
                            onClick={() => setKodePosisi(true)}
                            variant='subtitle2' className={classes.titleForm} style={{color: '#cc0707', cursor: 'pointer', marginLeft: 16}}>
                            Tambah Kode Posisi ? 
                        </Typography>

                    ) : (
                        <div>
                            <TextField
                                label='Kode Posisi'
                                id="outlined-bare"
                                onChange= {(e) => setKodePosisiValue(e.target.value)}
                                // value={namaUnit}
                                className={classes.textField}
                                style={{marginLeft: 16}}
                                variant="outlined"
                                
                            />
                            <Typography 
                                onClick={() => setKodePosisi(false)}
                                variant='subtitle2' className={classes.titleForm} style={{color: '#cc0707', cursor: 'pointer', marginLeft: 16}}>
                                Batalkan kode posisi
                            </Typography>
                        </div>
                    )
                }

            </List>

            <DialogContentText id="alert-dialog-description">
                <Typography variant='h6'>
                </Typography>
            </DialogContentText>
        </DialogContent>
        <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
            <Button 
                onClick={() => setModalEdit(false)} 
                variant='outlined' 
                size='small'
                className={classes.buttonModalCancel}    
            >
                Batal
            </Button>
            
            <Button 
                // onClick={() => Redirect(ToHrEmployeeKepegawaianInfoPegawai)}
                onClick= {handleEdit}
                variant='contained' 
                color="primary" 
                size='small'
                className={classes.buttonModal}
            >  
                Simpan perubahan
            </Button>
        </DialogActions>
        <br />
        <br />
        </Dialog>
    )   
};

export default DialogEditPosition;

