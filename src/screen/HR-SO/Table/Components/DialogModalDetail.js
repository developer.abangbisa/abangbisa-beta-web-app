
import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, Chip, Avatar, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";

import axios from 'axios';
// import Redirect from 'react-router/Redirect';

import DialogEditPosition from '../ComponentDialogEdit/DialogEditPosition';

import AvatarDummy from '../../../../assets/images/Avatar_dummy.png';
import { URL_API } from '../../../../constants/config-api';

import { ToHrEmployeeProfileDetailSeeDetail } from '../../../../constants/config-redirect-url';

const DialogModalDetail = (props) => {
    
    const { 
    
        classes, 
        isModalDetail,
        setModalDetail,
        dataDetail

    } = props;

    console.log("Data Detail : : ", dataDetail !== null ? dataDetail : 'null');
    
    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */

    const userToken = localStorage.getItem('userToken'); 
    const [ detail, setDetail ] = useState(null);

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    const [ isModalEdit, setModalEdit ] = useState(false);
    const [ hideButtonEdit, setHideButtonEdit ] = useState(false);

    useEffect(() => {

        
        if(dataDetail !== null){
            if(dataDetail.parent_id !== null){

                if(userToken !== undefined){
            
                    const header =  {       
                        'Accept': "application/json",
                        'Content-Type' : "application/json",
                        'Authorization' : "bearer " + userToken,
                    };
                
                    axios.defaults.headers.common = header;    
        
                    axios
                        .get(URL_API + `/human-resource/structure-position/${dataDetail.parent_id}`)
                        .then(function(response){
        
                            console.log("Response Original POSITION DETAIL : ", response);
                            setDetail(response.data.data);
                        })
                        .catch(function(error){
                            
                            console.log("Error : ", error.response)
                        })
                };
            
            } else { 
                
                alert("Parent ID is null !");
                setModalDetail(false);

            }; 
        };

        /* 
            ````````````````````
            TO KNOW CURRENT PAGE
            
            ````````````````````
        */

        const currentLocation = window.location.pathname;

        if(currentLocation === ToHrEmployeeProfileDetailSeeDetail){

            setHideButtonEdit(true)
               
        };


    }, [isModalDetail]);

    const handleDialogEdit = () => {

        console.log("dataDetail : ", dataDetail);
        setModalEdit(true);
        
    };
    
    return (

        <div>
            <Dialog
                open={isModalDetail}
                onClose={() => setModalDetail(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                // maxWidth={"sm"}
                fullWidth
                
            >
                <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                    <Typography variant='h6' className={classes.title}>
                        <b>Detail Posisi  {dataDetail !== null ?  dataDetail.structure_position_title_name : "" } </b>
                    </Typography>        
                </DialogTitle>
                <DialogContent style={{textAlign: "left"}}>

                    <Chip
                        avatar={<Avatar alt="You" src={AvatarDummy}/>}
                        label={

                            <Typography variant='h6' className={classes.title} style={{color: 'white'}} >
                                { dataDetail !== null ? dataDetail.structure_position_title_name : '-' }
                            </Typography>

                        }
                        size='large'
                        // onDelete={handleDelete}
                        className={classes.chip}
                        style={{fontFamily: 'Nunito', backgroundColor: '#d2d2d2', borderColor: 'white'}}
                        variant="outlined"
                    />
                    
                    <br />
                    <br />
                    <Grid container >
                        <Grid item xs={6}>
                            <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                                Atasan :
                            </Typography>
                            <Typography variant='subtitle1' className={classes.title} >
                                <b>{detail !== null ? detail.member_first_name : '-'}</b>
                            </Typography>
                            <Typography variant='h6' className={classes.title}>
                                {detail !== null ? detail.structure_position_title_name : '-'}
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                         
                        </Grid>
                    </Grid>

                    <br />
                    <Grid container>
                        <Grid item xs={6}>
                            <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                                Jenis Unit :
                            </Typography>
                            <Typography variant='subtitle1' className={classes.title}>
                                <b>{detail !== null ? detail.structure_unit_type_name : '-'}</b>
                            </Typography>
                            {/* <Typography variant='h6' className={classes.title}>
                                {detail !== null ? detail.structure_position_title_name : '-'}
                            </Typography> */}
                        </Grid>
                        <Grid item xs={6}>
                            <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                                Nama Unit :
                            </Typography>
                            <Typography variant='subtitle1' className={classes.title}>
                                <b>{detail !== null ? detail.structure_unit_name : '-'}</b>
                            </Typography>
                        </Grid>
                    </Grid>

                    <br />
                    <Grid container>
                        <Grid item xs={6}>
                            <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                            Posisi Jabatan :
                            </Typography>
                            <Typography variant='subtitle1' className={classes.title}>
                                <b>{detail !== null ? detail.classification.name : '-'}</b>
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                                Kode Posisi :
                            </Typography>
                            <Typography variant='subtitle1' className={classes.title}>
                                <b>{detail !== null ? detail.code : '-'}</b>
                            </Typography>
                        </Grid>
                    </Grid>

                    <DialogContentText id="alert-dialog-description">
                        {/* 
                            <Typography variant='subtitle1' className={classes.title} style={{color: 'black'}}>
                                <b>Apakah Anda yakin ingin menghapus <i>{dataDetail !== null ? dataDetail.name : ''}</i> ?</b>
                            </Typography> 
                        */}

                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{alignItems: "right", justifyContent:'flex-end'}}>
                    <Button 
                        size='small'
                        onClick={() => setModalDetail(false)}
                        variant='outlined' 
                        className={classes.buttonModalCancel}    
                        // style={{marginRight: 16, textTransform: 'uppercase'}}
                        // fullWidth
                    >  
                        Kembali
                    </Button>

                    {
                        hideButtonEdit == false && (

                            <Button 
                                size='small'
                                onClick={handleDialogEdit}
                                variant='contained' 
                                className={classes.button}
                                style={{marginRight: 16, textTransform: 'uppercase'}}
                            >  
                                Klik di sini untuk edit posisi
                            </Button>
                        )
                    }
                </DialogActions>
                <br />
            </Dialog>

            <DialogEditPosition
                classes = { classes }
                dataDetail = { dataDetail }
                setModalEdit = { setModalEdit }
                isModalEdit = { isModalEdit }
            />
            
        </div>
    )
};

export default DialogModalDetail;
