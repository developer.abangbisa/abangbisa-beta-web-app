import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider, fade} from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, AppBar, Toolbar
    
} from '@material-ui/core';

import { ThemeProvider } from "@material-ui/styles";

import axios from 'axios';

import DateFnsUtils from "@date-io/date-fns";
import MomentUtils from "@date-io/moment";
import moment from 'moment';
import clsx from 'clsx';

import { DatePicker, MuiPickersUtilsProvider, } from "@material-ui/pickers";
// import { DateRangePicker, DateRange } from "@matharumanpreet00/react-daterange-picker";
import DialogDateRange from './Components/DialogDateRange';

import styled from 'styled-components';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import LensIcon from '@material-ui/icons/LensOutlined';
import DoneIcon from '@material-ui/icons/Done';

import { StyledMenu, StyledMenuItem } from '../../../components/StyledMenuDropdown';
import ViewYou from './You/ViewYou';
import ViewAll from './All/ViewAll';

import { URL_API } from '../../../constants/config-api';
import PictPIALA from '../../../assets/images/Group_2667.png';

import 'moment/locale/id';

const theme = createMuiTheme({
  
    palette: {

        primary: {
            
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }

});

const styles = theme => ({

    root: {
      
        width: '100%',
        marginTop: 1 * 3
  
    },
    layoutMarginLeftAndTop: {

        marginLeft: 72, 
        marginTop: 56
    },
    layoutMarginTop: {

        // marginLeft: 72, 
        marginTop: 56
    },
    layoutMarginTopNew: {
        marginTop: theme.spacing(10)

    },
    layoutMarginLeftList: {

        marginLeft: theme.spacing(4), 
        marginTop: theme.spacing(1)
      
    },

    button: {

        // width: '503px',
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'

    },
    buttonDisabled: {
        fontFamily:'Nunito',
        textTransform: 'capitalize',
        color: 'grey',
        fontWeight: 'bold'
    },
    title: {

        fontFamily: 'Nunito'
    },
    titleListChoose: {

        fontFamily: 'Nunito',
        cursor: 'pointer'

    },
    titleListChooseAktif: {

        fontFamily: 'Nunito',
        color: '#d6d3d3', 
        cursor: 'pointer'
    },

    timeFrameIcon: {
        fontFamily: 'Nunito',
        color: 'grey', //#4aa9fb
        textTransform: 'capitalize',
        // marginRight: theme.spacing(4),
        // marginTop: theme.spacing(9),
        // backgroundColor: '#cfe8fd'
    },
    timeFrameIcontDateRangeText: {
        fontFamily: 'Nunito',
        color: '#4aa9fb',
        textTransform: 'capitalize',
        backgroundColor: '#edf6ff'
    },

    timeFrameIconInModal: {

        fontFamily: 'Nunito',
        color: 'grey',
        textTransform: 'capitalize',
        '&:hover': {
            color: '#edcfd8',//#edcfd8, #cc0707
        },
        borderWidth: 0
    },

    /*
        ```````
        SEARCH

        ```````
    */
    search: {

        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        // borderRadius: '5',
        backgroundColor: fade(theme.palette.common.black, 0.1),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.black, 0.35),
            },

        // backgroundColor: grey,
        //     '&:hover': {
        //         backgroundColor: green,
        //     },
        marginRight: theme.spacing(2),
        // marginLeft: 0,
        width: '50%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
                width: 'auto',
            },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: 'white'
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
        color: 'grey'
        // color: '#cc0707'
    },
    /*
        ````````````````````````````
        PAPER, CIRCULAR PROGRESS BAR

        ````````````````````````````        
    */
    paperList: {
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        padding: theme.spacing(0.2),
        
    },
    paperListKebakaran: {
        
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        padding: theme.spacing(0.2),
        background: '#edcfd8',
        borderBottom: '0.5em solid red'
        // borderLeft: '0.1em solid #c3c0c0de',


    },
    circularProgressBar : {
        
        width: 60, 
        height: 60,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(0.3),
        marginBottom: theme.spacing(2)
    },
    /*
        ````````````
        MODAL CREATE 

        ````````````
    */
    textField: {
        minWidth: 425,
        [theme.breakpoints.only('xs')]: {
                   
            minWidth: 200,
        } 
        
    },

    /*
        `````````````````````````````
        FOTO REAL USER NAME, USERNAME

        `````````````````````````````
    */
    userRealFoto: {
        // margin: 10,
        width: 48,
        height: 48,
        borderRadius: 24
    },
    imageAvatar: {

        width: 50,
        height: 40
    },


    /*
        ````
        TAB 

        ````
    */
    tabList: {
        borderWidth: 1, 
        paddingLeft: 0, 
        borderColor: '#d9dada', 
        borderStyle: 'solid'
    },
    tabListWithoutBorder: {
        borderWidth: 0, 
        paddingLeft: 0, 
        borderColor: '#d9dada', 
        borderStyle: 'solid'
    },

    /*

        ``````````
        DATE RANGE

        ``````````
    */
    dateRangePerbaruiSampul: {
        fontFamily: 'Nunito',
        color: 'grey',
        textTransform: 'capitalize',
        // marginRight: theme.spacing(4),
        // marginTop: theme.spacing(9),

    },

    /*

        ````````````
        YOU - ACTIVE

        ````````````
    */
    moreDetail: {

        fontFamily: 'Nunito',
        color: 'black', //#4aa9fb
        textTransform: 'capitalize',
        fontSize: 11,
        cursor: 'pointer',
        paddingTop:0,
        paddingBottom:0, 
        paddingRight: 3,
        paddingLeft: 3,
        backgroundColor: 'yellow'

    },
});


class LocalizedUtils extends MomentUtils {

    getDatePickerHeaderText(date) {
        return this.format(date, "DD MMMM YYYY", { locale: this.locale });
    }
};

const ViewGoal = props => {

    const { classes } = props;

    /*
       ````
       TAB

       ````
    */
    const [paramsUrlGoal, setParamsUrlGoal ] = useState('options[filter][scope]=self');

    const [tabIndex, setTabIndex] = useState(1);// 1 = You, 0 = All
    const handleTab = (tabIndex) => {

        setTabIndex(tabIndex);

        if(tabIndex == 1){

            setParamsUrlGoal('options[filter][scope]=self');
        } else {

            setParamsUrlGoal('');

        };
    };
    
    /*
        ````````````````````````````
        COMPONENT DID MOUNT with TAB

        ````````````````````````````
    */
    const userToken = localStorage.getItem('userToken');

    const [ userTokenState, setUserTokenState ] = useState('');
    const [ collectionGoalList, setCollectionGoalList ] = useState([

        {

            calculatedValue : {
                end_date: null,
                expected_value: null,
                expected_value_percent: null,
                gap_value: null,
                gap_value_percent: null,
                result_value: null,
                result_value_percent: null,
                start_date: null,
                target_value: null
            },
            status_id: null,
            name : null,
            period: {
                id: null,
                name: null
            },
            start_date: null,
            created_at: null,
            end_date: null,

            owner : {
                structure_unit_type_name : null,
                structure_unit_name: null,
                member_first_name: null,
                member_last_name: null,
                structure_position_title_name: null
            },
            time: {
                remaining_days : null,

            }
        }
    ]);

    const [ loader, setLoader ] = useState(false);
    useEffect(() => {

        const abortController = new AbortController();
        const signal = abortController.signal;

        setLoader(true);

        setUserTokenState(userToken);

        // let isSubscribed = true

        if(userToken !== undefined ){
        
            const header =  {   

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    
            //* GOL SELF - ONLY OWNER GOAL USER LOGIN
            axios
                .get(URL_API + `/swd/goal?${paramsUrlGoal}`,  { signal: signal })
                .then(function(response){

                    setLoader(false);
                    console.log("Response Original : ", response);

                    if(response.status == 200){


                        if(response.data.data !== null){
                            setCollectionGoalList(response.data.data);
                            
                        };
                    };
                })
                .catch(function(error){
                    
                    setLoader(false);
                    console.log("Error : ", error.response);

                })

                //CLEAN UP
                return function cleanup() {
                    abortController.abort();
                };

        } else { console.log("No Access Token available!")};


        
    }, [tabIndex, paramsUrlGoal]);
    // }, []);


    /*
       ```````````````````
       DROPDOWN ALL STATUS
       ```````````````````
   */
    const [anchorElAllStatus, setAnchorElAllStatus] = useState(null);

    function handleClickAllStatus(event) {

        setAnchorElAllStatus(event.currentTarget);
    };
  
    function handleCloseAllStatus() {

        setAnchorElAllStatus(null);
    };

    /*
        ```````````````````
        DROPDOWN TIME FRAME
        ```````````````````
    */
    const [anchorElTimeFrame, setAnchorElTimeFrame] = useState(null);

    const [ textValueTimeFrame, setTextValueTimeFrame ] = useState('Time Frame');

    function handleClickTimeFrame(event) {

        setAnchorElTimeFrame(event.currentTarget);
    };
    
    function handleCloseTimeFrame() {

        setAnchorElTimeFrame(null);
    };

    /*
        ````````
        DUE DATE

        ````````
    */

    const [locale, setLocale] = useState("id");


    const [selectedDueDate, setSelectedDueDate] = useState(null);
    const [ isShowDueDateComponent, setShowDueDateComponent ] = useState(false);
    const [ isShowDueDateComponentDirectly, setShowDueDateComponentDirectly ] = useState(false);

    const handleDueDate = () => {

        handleCloseTimeFrame();
        setShowDueDateComponent(true);
        // setShowDueDateComponentDirectly(true);

        setShowDateRangeComponent(false);
        setShowTextDateRange(false);

        setTextValueTimeFrame('Due Date');

       
        

    };

    /*
        ``````````
        DATE RANGE

        ``````````
    */
    const [ isShowDateRangeComponent, setShowDateRangeComponent ] = useState(false);
    const [ isShowTextDateRange, setShowTextDateRange ] = useState(false);

    const handleDateRange = () => {

        handleCloseTimeFrame();
        setShowDueDateComponent(false);
        // setShowDueDateComponentDirectly(false);

        // setShowTextDateRange(true);
        setShowDateRangeComponent(true);

        setTextValueTimeFrame('Date Range')

    };

    const [ openDateRange, setOpenDateRange ]  = useState(true);
    const [ dateRange, setDateRange ] = useState({});
    
    const handleChangeDateRange = (data) => {

        console.log("Data Date Range : ", data);
        setDateRange(data);
        
        setShowTextDateRange(true);
        setShowDateRangeComponent(false)
        
    };


    
    /*
        ```````````````````
        Filtering Due Date

        ``````````````````
    */

    //const handleChangeDueDate = () => {};

    useEffect(() => {


        if(selectedDueDate !== null){

            //*RUN!
            setLoader(true );
    
            if(userTokenState !== undefined ){
            
                const header =  {   
    
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userTokenState,
        
                };
    
                axios.defaults.headers.common = header;    
    
                axios
                    .get(URL_API + `/swd/goal?options[filter][due_date]=${moment(selectedDueDate).format('YYYY-MM-DD')}`)
                    .then(function(response){
    
                        setLoader(false);
                        setShowDueDateComponent(false)
                        
                        console.log("Response Original Due Date : ", response);
    
                        if(response.status == 200){
    
                            if(response.data.data !== null){
                                setCollectionGoalList(response.data.data);
                            
                            };
                        };
                    })
                    .catch(function(error){
                        
                        setLoader(false);
                        setShowDueDateComponent(false)
                        console.log("Error : ", error.response);
    
                    })
    
            } else { console.log("No Access Token available!")};
        }
    },[selectedDueDate]);

    const startDate = localStorage.getItem('start_date');
    const endDate = localStorage.getItem('end_date');

    useEffect(() => {

        if(startDate !== null && endDate !== null){

            //*RUN!
            setLoader(true );

            if(userTokenState !== undefined ){
            
                const header =  {   
    
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userTokenState,
        
                };
    
                axios.defaults.headers.common = header;    
    
                axios
                    .get(URL_API + `/swd/goal?options[filter][start_date]=${moment(startDate).format('YYYY-MM-DD')}&options[filter][end_date]=${moment(endDate).format('YYYY-MM-DD')}`)

                    .then(function(response){
    
                        setLoader(false);
                        setShowDateRangeComponent(false)
                        
                        console.log("Response Original Due Date : ", response);
    
                        if(response.status == 200){
    
                            if(response.data.data !== null){
                                setCollectionGoalList(response.data.data);

                                localStorage.removeItem('start_date');
                                localStorage.removeItem('end_date');
                            
                            };
                        };
                    })
                    .catch(function(error){
                        
                        setLoader(false);
                        setShowDateRangeComponent(false);                        
                        console.log("Error : ", error.response);

                        localStorage.removeItem('start_date');
                        localStorage.removeItem('end_date');
    
                    })
    
            } else { console.log("No Access Token available!")};

        };

    },[startDate, endDate]);

    return (

        <MuiThemeProvider theme={theme} >
            
            <Grid container >
                <Grid item sm={7} style={{textAlign: 'right'}} >

                </Grid>

                <Grid item sm={5} style={{textAlign: 'right'}} > 

                    {/* 

                        `````````````````
                        STATUS BUTTON ALL PROGRESS
                        
                        `````````````````                        
                    */} 
                    

                    {
                        isShowTextDateRange == true && (

                            <Button 
                                variant='outlined' 
                                size='small' 
                                className={clsx(classes.timeFrameIcontDateRangeText, classes.layoutMarginTopNew)} 
                                style={{marginRight: 8, borderWidth: 0}}
                                onClick={() => setShowDateRangeComponent(true)}
                            >
                                <b>{dateRange !== {} ? moment(dateRange.startDate).format("DD MMMM YYYY") : "-"} -  { dateRange !== {} ? moment(dateRange.endDate).format("DD MMMM YYYY") : "-"}</b>
                            </Button>
                        )
                    }

        
                    <Button 
                        variant='outlined' 
                        size='small' 
                        className={clsx(classes.timeFrameIcon, classes.layoutMarginTopNew)} 
                        style={{marginRight: 8, borderWidth: 0}}
                        onClick={handleClickAllStatus}
                    >
                        &nbsp;
                        
                           <img src ={PictPIALA} style={{width: 17, height: 14, marginRight: 8 }}/>
                            
                        <b>All Progress</b>
                    </Button>
                    <StyledMenu
                        id="customized-menu"
                        anchorEl={anchorElAllStatus}
                        keepMounted
                        open={Boolean(anchorElAllStatus)}
                        onClose={handleCloseAllStatus}
                    >
                        <StyledMenuItem disabled>                         
                            <ListItemText 
                                
                                primary = {

                                    <Typography variant='subtitle2' className={classes.title} >
                                        <b>All Progress</b>
                                    </Typography>
                                    
                                }  
                            />
                            
                            <ListItemIcon>
                                <i className="material-icons" style={{color: 'green', fontSize: 17, marginLeft: 16}}>
                                    check_circle_outline
                                </i>
                            </ListItemIcon>

                        </StyledMenuItem>

                        <StyledMenuItem>
                            <ListItemText 
                                primary = {

                                    <Typography variant='subtitle2' className={classes.title} >
                                        <b>Achieved</b>
                                    </Typography>
                                } 
                            />
                        </StyledMenuItem>

                        <StyledMenuItem>
                            <ListItemText 
                                primary = {

                                    <Typography variant='subtitle2' className={classes.title} >
                                        <b>Not Achieved</b>
                                    </Typography>
                                } 
                            />
                        </StyledMenuItem>

                    </StyledMenu>
                                
                    {/* 

                        `````````````````
                        TIME FRAME BUTTON

                        `````````````````                        
                    */}
                    <Button 

                        onClick={ handleClickTimeFrame }
                        variant='outlined' 
                        size='small' 
                        className={clsx(classes.timeFrameIcon, classes.layoutMarginTopNew)}
                        style={{marginRight: 8}}
                    >
                        <i className="material-icons" style={{color: 'grey', fontSize: 14}}>
                            calendar_today
                        </i>

                        &nbsp;
                        <b>{ textValueTimeFrame }</b>
                    </Button>

                    <StyledMenu
                        id="customized-menu"
                        anchorEl={anchorElTimeFrame}
                        keepMounted
                        open={Boolean(anchorElTimeFrame)}
                        onClose={handleCloseTimeFrame}
                    >
                        <StyledMenuItem
                            onClick={handleDueDate}
                        >                         
                            <ListItemText 
                                
                                primary = {

                                    <Typography variant='subtitle2' className={classes.title}>
                                        <b>Due Date</b>
                                    </Typography>
                                }  
                            />

                        </StyledMenuItem>

                        <StyledMenuItem
                            onClick= { handleDateRange }
                        >
                            <ListItemText 
                                
                                primary = {

                                    <Typography variant='subtitle2' className={classes.title} >
                                        <b>Date Range</b>
                                    </Typography>
                                } 
                            />

                        </StyledMenuItem>
                    </StyledMenu>                  
                </Grid>
            </Grid>

            {/* 
                `````````````````````````````````
                COMPONENT DATE PICKER, DATE RANGE

                `````````````````````````````````
            */}
            <Grid container >
                <Grid item sm={4} style={{textAlign: 'right'}}>


                </Grid>
                <Grid item sm={8} style={{textAlign: 'right'}}>
  
                    {
                        isShowDueDateComponent == true && ( //https://stackoverflow.com/questions/17493309/how-do-i-change-the-language-of-moment-js

                            <MuiPickersUtilsProvider utils={LocalizedUtils} locale={locale}>
                                <ThemeProvider theme={themePicker}>
                                    <Fragment>
                                        <DatePicker
                                            value={selectedDueDate}
                                            onChange={setSelectedDueDate}
                                            animateYearScrolling
                                            variant="inline"// dialog, static, inline
                                            disableToolbar={false}
                                            // orientation="landscape"
                                            // format
                                            // TextFieldComponent =
                                            // ToolbarComponent 
                                            label="Pilih tanggal"
                                            style={{marginLeft: 16}}
                                        />
                                    </Fragment>
                                </ThemeProvider>
                            </MuiPickersUtilsProvider>
                         )
                    } 

                    {
                        
                         isShowDateRangeComponent == true && (

                            <DialogDateRange 
                                classes = { classes }
                                isShowDateRangeComponent = { isShowDateRangeComponent }
                                setShowDateRangeComponent = { setShowDateRangeComponent }
                            />
                         )
                     }

                </Grid>         
            </Grid>

            {/* 
                ````
                Tab

                ````
            */}
            <Grid container style={{marginTop: '-124px'}}>   
                <Grid item sm={12}>
                    <Tabs
                        className={classes.layoutMarginLeftAndTop}  
                        style={{backgroundColor: 'white', paddingLeft: 0}} 
                        selectedIndex={tabIndex} onSelect={tabIndex => handleTab(tabIndex)} 
                    >
                        <TabList className={isShowDueDateComponent == true ? classes.tabListWithoutBorder : classes.tabList }>
                            <Tab>
                                <Typography variant='subtitle2' className={classes.title}>
                                    All
                                </Typography>
                            </Tab>

                            <Tab>
                                <Typography variant='subtitle2' className={classes.title}>
                                    You
                                </Typography>
                            </Tab>
                        </TabList>

                        <TabPanel> {/*  style={{backgroundColor: 'white'}}  */}
                            <ViewAll 
                                classes = { classes }
                                isShowDateRangeComponent = { isShowDateRangeComponent }
                                collectionGoalList = { collectionGoalList }
                                setCollectionGoalList = { setCollectionGoalList }
                                loader = {loader }
                                setLoader = { setLoader }
                            />
                        </TabPanel>

                        <TabPanel  > 
                            <ViewYou 
                                classes = { classes }
                                isShowDateRangeComponent = { isShowDateRangeComponent }
                                collectionGoalList = { collectionGoalList }
                                setCollectionGoalList = { setCollectionGoalList }
                                loader = {loader }
                                setLoader = { setLoader }

                            />
                        </TabPanel>
                    </Tabs>
                </Grid>
            </Grid>

            {/* 
                `````````````````````````````````
                COMPONENT DATE PICKER, DATE RANGE

                `````````````````````````````````
            */}
        </MuiThemeProvider>
        
    );
};

export default withStyles(styles)(ViewGoal);

const themePicker = createMuiTheme({

    palette: {

        primary: {
            
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    },

    typography: {
        fontFamily: 'Nunito',
        textTransform: 'capitalize'
    },
    textfield: {
        width: 200
    }
  })