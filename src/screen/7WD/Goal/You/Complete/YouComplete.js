import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Chip
    
} from '@material-ui/core';

import moment from 'moment';
import clsx from 'clsx';
import { CircularProgressbarWithChildren, buildStyles, CircularProgressbar } from 'react-circular-progressbar';
import DoneIcon from '@material-ui/icons/Done';

import DialogCreate from '../../Components/DialogCreate';
import LightTooltip from '../../../../../components/LightTooltip';
import Circle from '../../../../../components/Circle';


import TruncateText from '../../../../../utilities/TruncateText';
import TruncateTextShort from '../../../../../utilities/TruncateTextShort';

import Redirect from '../../../../../utilities/Redirect';
import { ToGoalDetail, ToGoalDetailTabComplete } from '../../../../../constants/config-redirect-url';
const numeral = require('numeral');

const YouComplete = props => {

    const { classes, collectionGoalList, loader, setCollectionGoalList } = props;

    const [ find, setFind ] = useState(null);
    
    useEffect(() => {


        
        let finding = collectionGoalList.find(function(element) {
        
            return element.status_id == 4;
        })

        setFind(finding);

        // if(find == undefined){

        //     alert("Anda belum memiliki Goal yang sudah complete !")
        //     window.location.reload();
        // };
        

    }, [])

    /*  
        ``````````````````
        HANDLE GOAL DETAIL TAB COMPLETE

        ``````````````````
    */
    const handleDetail = (e, data) => {

        e.preventDefault();
        console.log(data);

        localStorage.setItem('goal_detail', JSON.stringify(data) )
        Redirect(ToGoalDetailTabComplete);

    };

    return (

        <Fragment>
            <Grid container style={{backgroundColor: '#f4f7fc'}}>
                <Grid item xs={12}>

                    <br />

                    {
                        loader == true && (

                            <Grid  
                                container
                                spacing={0}
                                direction="row"
                                justify="center"
                                alignItems="center"
                            >
                                <CircularProgress size={32} style={{marginTop: 72, color: 'red'}} />

                            </Grid>
                        )
                    }

                    {
                        collectionGoalList.length == 0 && loader == false && (

                            (
                            
                                <Paper elevation={1} className={classes.paperList} >
                                    <Grid container>
                                        <Grid item xs={12} style={{textAlign: 'center'}}>
                                            <Typography variant='subtitle1' className={classes.title} style={{color: 'grey', marginTop: 16, marginBottom: 16}}>
                                                <b>Tidak ada Goal yang dapat di tampilkan saat ini ;(</b>
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            )
                        )
                    }

                    {
                        find == undefined &&  loader == false && (

                            <Paper elevation={1} className={classes.paperList} >
                                <Grid container>
                                    <Grid item xs={12} style={{textAlign: 'center'}}>
                                        <Typography variant='subtitle1' className={classes.title} style={{color: 'grey', marginTop: 16, marginBottom: 16}}>
                                            <b>Anda belum memiliki Goal yang sudah <i>complete</i> !</b>
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Paper>
                            
                        )
                    }


                    {
                        collectionGoalList.length > 0 && collectionGoalList.map((item,i) => {

                            if(item.status_id == 4){

                                // console.log("Item  : ", item)
                                
                                return(
    
                                    <Paper 
                                        key={i} 
                                        elevation={1} 
                                        className={
                                            
                                            item.calculatedValue.gap_value_percent > item.calculatedValue.result_value_percent ||
                                            item.calculatedValue.gap_value_percent == item.calculatedValue.result_value_percent ? 
    
                                                classes.paperListKebakaran : classes.paperList
                                        
                                        } 
                                        style={{marginBottom: 16}}
                                    >
                                        <Grid container alignItems="flex-end" >
    
                                            <div className={classes.circularProgressBar}>

                                                <Circle 
                                                    classes = { classes }
                                                    item = { item }
                                                    index = {i}
                                                    
                                                />
                                                {/* {
                                                     item.calculatedValue.expected_value > item.calculatedValue.result_value ? (

                                                        <CircularProgressbarWithChildren
                                                            value={item.calculatedValue.result_value_percent}
                                                            styles={buildStyles({
                                                                pathColor: 'red',  //*PENCAPAIAN : Green or Red
                                                                trailColor: '#eee',
                                                                strokeLinecap: 'butt',//butt
                                                            })}
                                                        >
                                                        
                                                            <CircularProgressbar 
                                                                value={item.calculatedValue.gap_value_percent}  
                                                                styles={buildStyles({
                                                                    // pathColor:'yellow', //*GAP : Yellow
                                                                    pathColor:'red', //*GAP : Yellow
                                                                    trailColor: 'transparent',
                                                                    strokeLinecap: 'butt',
                                                                    textColor: 'black',
                                                                })}
                                                                text={`${item.calculatedValue.result_value_percent}%`}
                                                            />
                                                        
                                                        </CircularProgressbarWithChildren>

                                                     ) : (

                                                        <CircularProgressbarWithChildren
                                                            value={item.calculatedValue.result_value_percent}
                                                            styles={buildStyles({
                                                                pathColor: 'rgba(61, 255, 41, 0.87)',  //*PENCAPAIAN : Green or Red
                                                                trailColor: '#eee',
                                                                strokeLinecap: 'butt',//butt
                                                            })}
                                                        >
                                                            
                                                            <CircularProgressbar 
                                                                value={item.calculatedValue.gap_value_percent}  
                                                                styles={
                                                                    buildStyles({
                                                                        pathColor: 'rgba(61, 255, 41, 0.87)',
                                                                        trailColor: 'transparent',
                                                                        strokeLinecap: 'butt',
                                                                        textColor: 'black',
                                                                    })
                                                                }
                                                                text={`${item.calculatedValue.result_value_percent}%`}
                                                            />
                                                        </CircularProgressbarWithChildren>
                                                     )
                                                } */}
                                                
                                                
                                            </div>
                                    
                                            <List className={classes.layoutMarginLeftList} style={{width: 200}}>
                                                <ListItem style={{paddingLeft: 0, marginRight: 24}}>
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                <b>{item.name }</b>
                                                            </Typography>
                                                        } 
                                                        secondary={
                                                            
                                                            <Typography variant='subtitle2' className={classes.title}>
    
                                                                {// *PERIOD
                                                                    item.period !== null && (
                                                                        item.period.name
                                                                    )
                                                                }
        
                                                                {// *DATE RANGE
                                                                    item.start_date !== null && item.end_date !== null && item.period == null && (
                                                                        <LightTooltip title={moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY')} placement="right-start">
                                                                            <span>
                                                                                {TruncateTextShort(moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY'))}
                                                                            </span>
                                                                        </LightTooltip>
                                                                    )
                                                                }
    
                                                                {// *DUE DATE
    
                                                                    item.start_date == null && item.end_date !== null && item.period == null && (
                                                                    
                                                                        moment(item.end_date).format('DD MMMM YYYY')
                                                                        
                                                                    )
    
                                                                }
                                                            </Typography>
                                                        } 
                                                    />
                                                
                                                </ListItem>
                                            </List>
                
                                            <List style={{width: 172}}>
                                                <ListItem 
                                                    style={{
                                                        marginRight: 24, 
                                                        marginBottom: 8, 
                                                        paddingLeft: 0,
                                                        // borderLeft: '0.1em solid #c3c0c0de',
                                                    }}
                                                >
                                                    <ListItemIcon 
                                                        style={{
                                                            marginLeft: 8
                                                            
                                                        }}
                                                    >
                                                        <IconButton
                                                            style={{backgroundColor: '#aed9ff', padding: '7px'}}
                                                        >
                                                            <i className="material-icons" style={{color: '#4aa9fb'}}>
                                                                people
                                                            </i>
                                                        </IconButton>
                                                    </ListItemIcon>
                                                    
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                                                                // style={{marginRight: 32}}
                                                            >
                                                                {/* <b>{item.owner.structure_unit_type_name } {item.owner.structure_unit_name}</b> */}
                                                                <b>{
                                                                    TruncateTextShort(
                                                                        item.owner !== null ? item.owner.structure_unit_type_name : '-' + " " + item.owner !== null ? item.owner.structure_unit_name : '-'
                                                                    )
                                                                }</b>
    
                                                            </Typography>
                                                        } 
                                                    
                                                    />
                                                
                                                </ListItem>
                                            </List>
                
                                            <List>
                                                <ListItem 
                                                    style={{
                                                        marginRight: 24, 
                                                        paddingLeft: 0,
                                                        // borderLeft: '0.1em solid #c3c0c0de',
                                                    }}
                                                >
                
                                                    <ListItemIcon style={{marginLeft: 8}}>
                                                        <IconButton style={{backgroundColor: '#bdffde', padding: '8px'}}>
                                                            <DoneIcon style={{color: '#36d686', fontSize: 21}} />
                                                        </IconButton>
                                                    </ListItemIcon>
                                                    
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                
                                                            >
                                                                <b>Posisi aman</b>
                                                            </Typography>
                                                        } 
                
                                                        secondary={
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                {numeral(item.calculatedValue.expected_value_percent).format('0,0')}%
                                                            </Typography>
                                                        } 
                                                    
                                                    />
                                                </ListItem>
                                            </List>
                
                                            <List>
                                                <ListItem 
                                                    style={{
                                                        marginRight: 24, 
                                                        paddingLeft: 0,
                                                        // borderLeft: '0.1em solid #c3c0c0de',
                                                    }}
                                                >
                
                                                    <ListItemIcon style={{marginLeft: 8}}>
                                                        <IconButton style={{backgroundColor: 'rgb(255, 222, 172)', padding: '11px'}}>
                                                            <i className="material-icons" style={{color: 'orange', fontSize: 14}}>
                                                                calendar_today
                                                            </i>
                                                        </IconButton>
                                                    </ListItemIcon>
                                                    
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                                                            >
                                                                <b>Sisa waktu</b>
                                                            </Typography>
                                                        } 
                
                                                        secondary={
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                {item.time.remaining_days} hari
    
                                                                {/* {
                                                                    item.time.remaining_days == 0 ? 
    
                                                                        moment(item.end_date).fromNow() : 
                                                                            
                                                                            item.time.remaining_days +  " hari lagi"
                                                                } */}
                                                            </Typography>
                                                        } 
                                                    />
                                                
                                                </ListItem>
                                            </List>
                
                                            <List className={classes.layoutMarginLeftList}>
                                                <ListItem alignItems='center' style={{paddingLeft: 0, marginRight: 24}}>
                                                    <ListItemText 
                                                        noWrap
                                                        className={classes.listItemtext}
                                                        // primary = {
                                                        //     <LightTooltip title={`Terakhir update ${moment(item.created_at).format('DD MMMM YYYY h:mm:ss a')}`} placement="left-start">
                                                        //         <Typography  variant='subtitle2' className={classes.title}>
                                                        //             {TruncateText(`Terakhir update ${moment(item.created_at).format('DD MMMM YYYY h:mm:ss a')}`)}
                                                        //         </Typography>
                                                        //     </LightTooltip>
                                                        // } 
                                                        primary={
    
                                                            <LightTooltip title='Klik di sini ! Untuk melihat lebih detail Goal ini' placement="left-start">
                                                                {/* 
                                                                <Typography 
                                                                    style={{cursor: 'pointer', marginBottom: 16}}
                                                                    variant='subtitle2' 
                                                                    className={classes.title} 
                                                                    onClick={(e) => handleDetail(e, item)}
                                                                >
                                                                    <b>More Detail</b>
                                                                </Typography> 
                                                                */}
                                                                <Button 
                                                                    onClick={(e) => handleDetail(e, item)}                                                                    
                                                                    variant='outlined' 
                                                                    size='small' 
                                                                    className={classes.moreDetail}
                                                                >
                                                                
                                                                    <b>More Detail</b>
                                                                </Button>
                                                            </LightTooltip>
                                                        } 
                                                    />
                                                
                                                </ListItem>
                                            </List>
                                        
                                        </Grid>
                
                                    </Paper>
                                );

                            }; 
                            // else {

                            //     return   (
                                    
                            //         <Fragment>
                            //             <Paper elevation={1} className={classes.paperList} key={i}>
                            //                 <Grid container>
                            //                     <Grid item xs={12} style={{textAlign: 'center'}}>
                            //                         <Typography variant='subtitle1' className={classes.title} style={{color: 'grey', marginTop: 16, marginBottom: 16}}>
                            //                             <b>Anda belum memiliki Goal yang sudah <i>complete</i> !</b>
                            //                         </Typography>
                            //                     </Grid>
                            //                 </Grid>
                            //             </Paper>
                            //             <br />
                            //         </Fragment>
                            //     )
                            // }



                        }) 
                    }
                    
                </Grid>
            </Grid>
        </Fragment>
    )
};

export default YouComplete;