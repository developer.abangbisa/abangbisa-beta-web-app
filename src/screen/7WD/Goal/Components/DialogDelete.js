
import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';
import { ToGoal } from '../../../../constants/config-redirect-url';
import Redirect from '../../../../utilities/Redirect';

const DialogDelete = (props) => {
    
    const { 
            classes, 
            isModalDeleteGoalMaster, 
            setModalDeleteGoalMaster, 
            // goalResultId, 
            goalDetailState,
            // goalResultNameDescription, 
            // collectionGoalResultList, 
            // setCollectionGoalResultList 
        
    } = props;


    const [ userTokenState, setUserTokenState ] = useState('');
    
    useEffect(() => {
        
        if(isModalDeleteGoalMaster == true){
            
            const userToken = localStorage.getItem('userToken');
            setUserTokenState(userToken)
        };
        
    }, [isModalDeleteGoalMaster]);
    
    const handleDelete = () => {

        // console.log("goalResultNameDescription : ", goalResultNameDescription);
    
        if(userTokenState !== '' ){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .delete(URL_API + `/swd/goal/${goalDetailState.id}`)
                .then(function(response){

                    console.log("Response Original : ", response);
                    // setCollectionGoalResultList(collectionGoalResultList.filter(item => item.id !== goalResultId));
                    // setModalDeleteGoalMaster(false);
                    Redirect(ToGoal);
                  
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);
                    setModalDeleteGoalMaster(false);
                    alert('Whoops, something went wrong !')

               
                })

        } else { console.log("No Access Token available!")};
        
    };
    
    return (
        <Dialog
            open={isModalDeleteGoalMaster}
            onClose={() => setModalDeleteGoalMaster(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
              
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.title} style={{color: 'black'}}>
                        <b>Apakah Anda yakin ingin menghapus Goal : <i>{goalDetailState.name}</i> ?</b>
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    onClick={handleDelete}
                    variant='contained' 
                    className={classes.buttonModalDelete}
                    // fullWidth
                >  
                    Yakin
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

export default DialogDelete;
