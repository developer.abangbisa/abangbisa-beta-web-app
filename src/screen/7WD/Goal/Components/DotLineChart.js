
import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider, fade} from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, ListItemSecondaryAction, Avatar, IconButton, InputBase, Divider, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, AppBar, Toolbar, Chip
    
} from '@material-ui/core';

import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Area, AreaChart } from 'recharts';
import moment from 'moment';
const numeral = require('numeral');


const data = [
    {
      name: '1', 
      // uv: 4000, 
      pv: 2400, 
      amt: 2400,
    },
    {
      name: '2', 
      // uv: 3000, 
      pv: 1398, 
      amt: 2210,
    },
    {
      name: '3', 
      // uv: 2000,
      pv: 12800, 
      amt: 2290,
    },
    {
      name: '4', 
      // uv: 2780, 
      pv: 3908, 
      amt: 2000,
    },
    {
      name: '5', 
      // uv: 1890, 
      pv: 4800, 
      amt: 2181,
    },
    {
      name: '6', 
      // uv: 2390, 
      pv: 3800, 
      amt: 2500,
    },
    {
      name: '7', 
      // uv: 3490, 
      pv: 2000000000000, 
      amt: 2100,
    },
  ];




const DotLineChart = props => {
  
  const { classes, totalDaysState, collectionGoalResultList } = props;
  const [ dataGoalResult, setDataGoalResult ] = useState([]);

  // checked.split('.').join("");

  useEffect(() => {
    
      let dataGoalResultLet = [];

      if(collectionGoalResultList.length > 0){
    
        collectionGoalResultList.map((item, i) => {

          // console.log("Item : ", numeral(item.result_value).format('0[.]00000'));
          
          // let test = item.result_value.split('.').join(""); 

          let items = {
          
            name: moment(item.result_date).format('DD MMMM YYYY'),
            // Pencapaian: parseInt(item.result_value.split('.').join("")) 
            Pencapaian: parseInt(numeral(item.result_value).format('0[.]00000'))

          };
    
          dataGoalResultLet.push(items);
    
        })

        console.log("dataGoalResultLet : ", dataGoalResultLet);
        const sortedArray  = dataGoalResultLet.sort((a,b) => new moment(a.name).format('YYYYMMDD') - new moment(b.name).format('YYYYMMDD'))
        
        // setDataGoalResult(dataGoalResultLet);
        setDataGoalResult(sortedArray);
        
      };

  },[collectionGoalResultList])
  


    // let data = [];

    // if(totalDaysState > 0){

    //   let foo = new Array(totalDaysState).fill(null)

    //   foo.length > 0 && foo.map((boo, i) => {  

    //     let item = {

    //       name: i + 1,
    //       pv: i * 3000,
    //       amt: i * 1100
    //     };

    //     data.push(item)
    //   })
    // };

    
     
    return (


        <Fragment>
          {
            collectionGoalResultList.length > 0 && (

             
                <AreaChart width={730} height={250} data={dataGoalResult} margin={{ top: 10, right: 30, left: 0, bottom: 8 }}>
                  <defs>
                    <linearGradient id="colorPencapaian" x1="0" y1="0" x2="0" y2="1">
                      <stop offset="5%" stopColor="#d1354a" stopOpacity={0.8}/>
                      <stop offset="95%" stopColor="#d1354a" stopOpacity={0}/>
                    </linearGradient>
                  </defs>

                  {/* <XAxis dataKey="name" tick={{fontSize: 10}} /> */}

                  <XAxis dataKey="name" tick={<CustomizedAxisTick />} />
                  <YAxis />
                  <CartesianGrid strokeDasharray="3 3" />
                  <Tooltip />
                  <Area type="monotone" dataKey="Pencapaian" stroke="#d1354a" fillOpacity={1} fill="url(#colorPencapaian)" />
                  {/* <Area type="monotone" dataKey="pv" stroke="#d1354a" fillOpacity={1} fill="url(#Pencapaian)" /> */}

              </AreaChart>

            ) 
          } 

      </Fragment>
    )

    
      
    
};


export default DotLineChart;


const CustomizedAxisTick = props => {

  const { x, y, stroke, payload } = props;

  // console.log("Payload : ", payload);
  
  return (
    <g transform={`translate(${x},${y})`}>
      <text 
        x={0} 
        y={0} 
        dy={16} 
        textAnchor="end" 
        fill="white" 
        transform="rotate(-35)"
        fontSize={10}
        fontFamily='Nunito'
        
      >
        { payload.value }
      </text>
    </g>
  );
}



const CustomizedDot = (props) => {

  const { cx, cy, stroke, payload, value } = props;

  if (value > 2500) {

    return (

      <IconButton
          style={{backgroundColor: 'grey', padding: '6px'}}
      >
          <i className="material-icons" style={{color: '#d1354a'}}>
              radio_button_checked
          </i>
      </IconButton>
    );
  };

  return (
    <IconButton
        style={{backgroundColor: 'white', padding: '6px'}}
    >
        <i className="material-icons" style={{color: 'green'}}>
            radio_button_checked
        </i>
    </IconButton>
  );
};
