
// import React,{Component, useState, useEffect, useContext, useRef} from 'react';
// import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
// import axios from 'axios';
// import { URL_API } from '../../../../constants/config-api';
// import { ToGoal } from '../../../../constants/config-redirect-url';
// import Redirect from '../../../../utilities/Redirect';

import React,{Component, useState, useEffect, useContext, useRef, Fragment} from 'react';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Radio, Chip
    
} from '@material-ui/core';

import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { lightBlue } from "@material-ui/core/colors";

import axios from 'axios';

import idLocale from "date-fns/locale/id";
import DateFnsUtils from "@date-io/date-fns";
import MomentUtils from "@date-io/moment";
import moment from 'moment';
import clsx from 'clsx';

import { DatePicker, MuiPickersUtilsProvider, } from "@material-ui/pickers";
// import { DateRangePicker, DateRange } from "@matharumanpreet00/react-daterange-picker";
import { StyledMenu, StyledMenuItem } from '../../../../components/StyledMenuDropdown';
import DialogError from '../../../../components/DialogError';


import { URL_API } from '../../../../constants/config-api';
import Capitalize from '../../../../utilities/Capitalize';
import AvatarDummy from '../../../../assets/images/Avatar_dummy.png';
import DialogDateRange from '../Components/DialogDateRange';

import { ToSOTable } from '../../../../constants/config-redirect-url';
import Redirect from '../../../../utilities/Redirect';

import 'moment/locale/id';

class LocalizedUtils extends MomentUtils {

    getDatePickerHeaderText(date) {
        return this.format(date, "DD MMMM YYYY", { locale: this.locale });
    }
};
  
const DialogEdit = (props) => {
    
    const { 
            classes, 
            isModalEditGoalMaster, 
            setModalEditGoalMaster, 
            goalDetailState,
            fotoQuery, 
            userLoginName, 
            collectionGoalList, 
            setCollectionGoalList 
       
    } = props;

    // console.log('goalDetailState : ', goalDetailState);

    /*
        ````````
        USE REFF

        ````````
    */
    let textInputReff = useRef(null);

    const userToken = localStorage.getItem('userToken');

    const [ userTokenState, setUserTokenState ] = useState('');
    const [ idGoal, setIdGoal ] = useState('');

    useEffect(() => {
        
        if(isModalEditGoalMaster == true){
            
            const userToken = localStorage.getItem('userToken');
            setUserTokenState(userToken);

            setBigGoal(goalDetailState.name);

            setIdGoal(goalDetailState.id);

            setLoader(false);
            setDisabledButton(false);



            //*Focus in Text Field
            setTimeout(() => {

                textInputReff.current.focus();
                
            }, 100);

        };
        
    }, [isModalEditGoalMaster]);

    /*
        ```````````````````
        COMPONENT DID MOUNT 

        ```````````````````
    */
//    const userToken = localStorage.getItem('userToken');

   const [ startDateRangeState, setStartDateRangeState ] = useState(null);
   const [ endDateRangeState, setEndDateRangeState ] = useState(null);

   const startDateRange = localStorage.getItem('start_date');
   const endDateRange = localStorage.getItem('end_date');

//    const [ userTokenState, setUserTokenState ] = useState('');
   const [ positionList, setPositionList ] = useState([]);
   const [ calculationMethodList, setCalculationMethodList ] = useState([]);

   const [periodCollections, setPeriodCollections] = useState([]);

   const [ memberPositionList, setMemberPositionList ] = useState([]);

   //*Enable Dialog INFO 
   useEffect(() => {

       setUserTokenState(userToken);
       setLoader(false);
       setDisabledButton(false)

       if(userToken !== undefined ){
       
           const header =  {   

               'Accept': "application/json",
               'Content-Type' : "application/json",
               'Authorization' : "bearer " + userToken,
   
           };

           axios.defaults.headers.common = header;    

           axios
            //    .get(URL_API + `/swd/goal/${idGoal}/update`)
                .get(URL_API + `/swd/goal/create?mode=simple`)
                .then(function(response){

                   console.log("Response Original Label : ", response);

                   if(response.status == 200){

                       setCalculationMethodList(response.data.data.calculationMethodCollections);
                       setPeriodCollections(response.data.data.periodCollections);
                       setMemberPositionList(response.data.data.ownerCollections)

                   };
                })
                .catch(function(error){
                   
                   console.log("Error : ", error.response);

                });

       } else { console.log("No Access Token available!")};

   }, []);

   useEffect(() => {

       // console.log("startDateRangeState : ", startDateRange);
       // console.log("endDateRangeState : ", endDateRange);
       setStartDateRangeState(startDateRange);
       setEndDateRangeState(endDateRange)

   }, [ startDateRange, endDateRange]);

   /*
       ``````````````````````
       HANDLE CHANGE BIG GOAL

       ```````````````````
   */
   const [ bigGoal, setBigGoal ] = useState('');
   const handleChangeBIGGoal = (e) => {

       // console.log("BIG GOAL : ", e.target.value);

       setBigGoal(e.target.value);
   };

   /*
       `````````````````````````
       HANDLE CHANGE DESCRIPTION

       `````````````````````````
   */
   const [ description, setDescription ] = useState('');
   const handleChangeDescription = (e) => {

       e.preventDefault();
       setDescription(e.target.value);

   };  

   /*
       ``````````````````````````
       HANDLE CHANGE NILAI TARGET

       ``````````````````````````
   */
   const [ nilaiTarget, setNilaiTarget ] = useState('');
   const handleChangeNilaiTarget = (e) => {

       e.preventDefault();
       setNilaiTarget(e.target.value);

   };

   /*
       `````````````````
       DROPDOWN POSITION

       `````````````````
   */
   const [ anchorElTimePosition, setAnchorElTimePosition ] = useState(null);

   const [ textValuePosition, setTextValuePosition ] = useState('');
   const [ ownerId, setOwnerId ] = useState('');

   function handleClickDropdownPosition(event) {

       setAnchorElTimePosition(event.currentTarget);

   };
   
   function handleCloseDropdownPosition() {

       setAnchorElTimePosition(null);
   };

   const handleChoosePosition = (e, data) => {

       handleCloseDropdownPosition();

       console.log("Position selected : ", data);
       setTextValuePosition(data.structure_position_title_name);
       setOwnerId(data.id);

   };

   /*
       ``````````````````````````````````
       HANDLE SELECTED CALCULATION METHOD 

       `````````````````````````````````
   */
   const [selectedValueCalcMethod, setSelectedValueCalcMethod] = useState('');

   const handleChangeRadioButton = (e, data) => {

       e.preventDefault();
       // console.log("Data : ", data);

       setSelectedValueCalcMethod(data)

  };

   /*
       ```````````````````
       DROPDOWN TIME FRAME
       ```````````````````
   */

   const [locale, setLocale] = useState("id");
   const [anchorElTimeFrame, setAnchorElTimeFrame] = useState(null);

   const [ textValueTimeFrame, setTextValueTimeFrame ] = useState('Time Frame');

   function handleClickTimeFrame(event) {

       setAnchorElTimeFrame(event.currentTarget);
   };
   
   function handleCloseTimeFrame() {

       setAnchorElTimeFrame(null);
   };

   /*
       ```````````````
       DUE DATE OPTION

       ```````````````
   */
   // const [selectedDueDate, setSelectedDueDate] = useState(new Date());
   const [selectedDueDate, setSelectedDueDate] = useState('');

   
   const [ isShowDueDateComponent, setShowDueDateComponent ] = useState(false);
   const [ isShowDueDateComponentDirectly, setShowDueDateComponentDirectly ] = useState(false);

   const handleChooseDueDate = () => {

       // setEndDateRangeState(new Date()) //*Baris kode ini untuk validasi DISABLED Button saja !

       handleCloseTimeFrame();
       setShowDueDateComponent(true);
       setShowDueDateComponentDirectly(true);

       setShowDateRangeComponent(false);
       setShowTextDateRange(false);

       setTextValueTimeFrame('Due Date');

       localStorage.removeItem('start_date');
       localStorage.removeItem('end_date');


   };

   /*
       `````````````````
       DATE RANGE OPTION

       `````````````````
   */

   const [ isShowDateRangeComponent, setShowDateRangeComponent ] = useState(false);
   const [ isShowTextDateRange, setShowTextDateRange ] = useState(false);

   const handleChooseRange = () => {

       // setSelectedDueDate(new Date());//*Baris kode ini untuk validasi DISABLED Button saja !
   
       handleCloseTimeFrame();
       setShowDueDateComponent(false);
       setShowDueDateComponentDirectly(false);

       setShowDateRangeComponent(true);
       setShowTextDateRange(true);

       setTextValueTimeFrame('Date Range');

   };

   /*
       ``````````````
       PERIODE OPTION

       ``````````````
   */

  const [ isShowPeriodComponent, setShowPeriodComponent ] = useState(false);
  const [ isShowTextPeriod, setShowTextPeriod ] = useState(false);

   const handleChoosePeriod = (event) => {


       setAnchorElPeriod(event.currentTarget);


       //*
       handleCloseTimeFrame();
       setShowDueDateComponent(false);
       setShowDueDateComponentDirectly(false);

       setShowDateRangeComponent(false);
       setShowTextDateRange(false);

       setTextValueTimeFrame('Period');

       localStorage.removeItem('start_date');
       localStorage.removeItem('end_date');


       setShowPeriodComponent(true);
       setSelectedDueDate(null)


   };



   /*
       ````````````````````````````
       PERIODE OPTION LIST DROPDOWN

       ````````````````````````````
   */
   //*
   const [anchorElPeriod, setAnchorElPeriod] = useState(null);
   // const [ textValueTimeFrame, setTextValueTimeFrame ] = useState('Time Frame');

   function handleClickPeriod(event) { //*Fungsi setAnchor di sini sudah di pakai langsung di 'handleChoosePeriod()'

       setAnchorElPeriod(event.currentTarget);
   };
   
   function handleClosePeriod() {

       setAnchorElPeriod(null);
   };


   const [ periodId, setPeriodId ] = useState('');
   const [ periodName, setPeriodName ] = useState('');
   const handleChoosePeriodId = (e, item) => {

       e.preventDefault();


       console.log("Item : ",item);

       setPeriodId(item.id);
       setPeriodName(item.name)

       handleClosePeriod();

       setShowTextPeriod(true)

   };

    /*
       ```````````````````
       HANDLE DIALOG ERROR

       ```````````````````
    */

    const [ isOpenDialogError, setOpenDialogError ] = useState(false);


   /*
       `````````````````````````
       HANDLE BUTTON SUBMIT GOAL

       `````````````````````````
   */

   const [ loader, setLoader ] = useState(false);
   const [ disabledButton, setDisabledButton ] = useState(false);

   const handleSUBMITGoal = () => {

       setLoader(true);
       setDisabledButton(true);

       let data = { 
           
           Goal : {
               
               name: bigGoal,
               owner_id: memberPositionList.length > 0 && ownerId == '' ?  memberPositionList[0].id : ownerId,
               calculation_method_id: selectedValueCalcMethod.id,
               period_id: periodId !== '' ? periodId : null ,
               start_date: startDateRangeState !== null ? startDateRangeState : null,
               end_date: endDateRangeState !== null ? endDateRangeState :  moment(selectedDueDate).format('YYYY-MM-DD'),
               target_value: nilaiTarget,
               description: description
           }
       };

       if(data.Goal.period_id == null){

           delete data.Goal.period_id;
       };

       if(data.Goal.period_id !== null && data.Goal.start_date == null && data.Goal.end_date == 'Invalid date' ){

           delete data.Goal.start_date;

           delete data.Goal.end_date;
       };

       if(data.Goal.start_date == null){

           delete data.Goal.start_date;
       };

       console.log("Data SUBMIT : ", data);

    //    alert('API Edit Goal is UNDER MAINTENANCE...')
    setOpenDialogError(true);

    //    if(userTokenState !== ''){
       
    //        const header =  {   

    //            'Accept': "application/json",
    //            'Content-Type' : "application/json",
    //            'Authorization' : "bearer " + userTokenState,
   
    //        };

    //        axios.defaults.headers.common = header;    

    //        axios
    //            .post(URL_API + '/swd/goal?mode=simple', data)
    //            .then(function(response){

    //                setLoader(false);
    //                console.log("Response Original : ", response);
                   
    //                if(response.status == 200){
                       
    //                    // window.location.reload();
    //                    setModalEditGoalMaster(false);
    //                    setCollectionGoalList([...collectionGoalList, response.data.data]);
    //                };
    //            })
    //            .catch(function(error){
                   
    //                setLoader(false);

    //                if(error.response !== undefined){
    //                    if(error.response.status == 400){

    //                        // alert('Error 400 :' + JSON.parse(error.response.data.info.errors.end_date))
    //                        if(error.response.data.info.errors.end_date){

    //                            alert('Error 400 :' + error.response.data.info.errors.end_date)
    //                        }
    //                    };

    //                } else {

    //                    alert("Whoops, something went wrong !");
    //                }

    //                console.log("Error : ", error.response);


    //            })

    //    } else { console.log("No Access Token available!")};
   };
    
    return (
        <Dialog
            open={isModalEditGoalMaster}
            onClose={() => setModalEditGoalMaster(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            // fullWidth
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "center"}}>
                <Typography variant='h6' className={classes.title}>
                    <b>Edit Goal  
                        {/* <span onClick={(e) => handleClickPeriod(e)}>&raquo;&raquo;</span> */}
                    </b>
                </Typography>
            </DialogTitle>

            <DialogContent style={{textAlign: "left"}}>
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.title} >
                        <b>What is your BIG ambitious Goal ?</b>
                    </Typography>
                    <TextField
                        inputRef={ textInputReff }
                        id="outlined-bare"
                        className={classes.textField}
                        onChange= {handleChangeBIGGoal}
                        // color='primary'
                        // onKeyDown={handleEnterPress}
                        value= {bigGoal}
                        // placeholder = {goalDetailState.name}
                        // value = {goalDetailState.name}
                        variant="outlined"
                        fullWidth
                    />
                </DialogContentText>

                <Grid container>
                    <Grid item xs={6}>
                        <List>
                            <ListItem style={{paddingLeft: 0}}>
                                <ListItemIcon>                                                
                                    {
                                        fotoQuery !== '' ? (
                                                                        
                                            //*<img src={URL_API + '/' +fotoQuery+ "&token=" + userToken}  className={classes.userRealFoto} />
                                            <img src={URL_API + '/my-profile/photo' + "?token=" + userToken}  className={classes.userRealFoto} />


                                        ) : (

                                            <img src={AvatarDummy} alt="Gear Picture" className={classes.imageAvatar} />
                                        )
                                    }

                                </ListItemIcon>                                
                                
                                <ListItemText 
                                    className={classes.listItemtext}
                                    primary = {
                                        <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                            Goal owner
                                        </Typography>
                                    } 
                                    secondary={
                                        <Typography variant='subtitle2' className={classes.title}>
                                            {/* <b>{userLoginName !== '' ? userLoginName : goalDetailState.owner.member_first_name }</b> */}
                                            <b>{goalDetailState.owner.member_first_name }</b>
                                        </Typography>
                                    } 
                                />
                            
                            </ListItem>
                        </List>
                    </Grid>
                    
                    <Grid item xs={6}>

                        {/* 

                            `````````````````
                            TIME FRAME BUTTON

                            `````````````````                        
                        */}

                        <Button 
                            onClick={ handleClickTimeFrame }
                            variant='outlined' 
                            size='small' 
                            className={classes.timeFrameIconInModal}
                            style={{marginTop: 24}}
                        >
                            <IconButton style={{background: '#edcfd8'}}>
                                <i className="material-icons" style={{color: 'white', fontSize: 14}}>
                                    calendar_today
                                </i>
                            </IconButton>
                            &nbsp;
                            <b>{ textValueTimeFrame }</b>
                            
                        </Button>

                        <StyledMenu
                            id="customized-menu"
                            anchorEl={anchorElTimeFrame}
                            keepMounted
                            open={Boolean(anchorElTimeFrame)}
                            onClose={handleCloseTimeFrame}
                        >
                            <StyledMenuItem
                                onClick={handleChooseDueDate}
                            >                         
                                <ListItemText 
                                    
                                    primary = {

                                        <Typography variant='subtitle2' className={classes.title}>
                                            <b>Due Date</b>
                                        </Typography>
                                    }  
                                />

                            </StyledMenuItem>

                            <StyledMenuItem
                                onClick= { handleChooseRange }
                            >
                                <ListItemText 
                                    
                                    primary = {

                                        <Typography variant='subtitle2' className={classes.title} >
                                            <b>Date Range</b>
                                        </Typography>
                                    } 
                                />

                            </StyledMenuItem>

                            <StyledMenuItem
                                // onClick={handleClickPeriod}
                                onClick={handleChoosePeriod}
                            >
                                <ListItemText 
                                    primary = {

                                        <Typography variant='subtitle2' className={classes.title} >
                                            <b>Period</b>
                                        </Typography>
                                    } 
                                />
                            </StyledMenuItem>
                        </StyledMenu>  
                        
                        {/* 

                            ````````````````
                            MENU LIST PERIOD

                            ````````````````
                        */}

                        <StyledMenu
                            id="customized-menu-period"
                            anchorEl={anchorElPeriod}
                            keepMounted
                            open={Boolean(anchorElPeriod)}
                            onClose={handleClosePeriod}
                            style={{marginTop: '-160px'}}
                        >
                            {

                                isShowPeriodComponent == true && periodCollections.length > 0 ? periodCollections.map((item, i) => (

                                    <StyledMenuItem
                                        key={i}
                                        onClick={(e) => handleChoosePeriodId(e, item)}
                                    >                         
                                        <ListItemText 
                                            
                                            primary = {

                                                <Typography variant='subtitle2' className={classes.title}>
                                                    <b>{item.name}</b>
                                                </Typography>
                                            }  
                                        />
                                    </StyledMenuItem>

                                )) : (
                                    <StyledMenuItem disabled>                         
                                        <ListItemText 
                                            
                                            primary = {

                                                <Typography variant='subtitle2' className={classes.title}>
                                                    <b>Anda belum memiliki periode ;(</b>
                                                </Typography>
                                            }  
                                        />
                                    </StyledMenuItem>
                                )
                            }
                        </StyledMenu> 


                        {
                            isShowDueDateComponent == true && ( //https://stackoverflow.com/questions/17493309/how-do-i-change-the-language-of-moment-js

                                <MuiPickersUtilsProvider utils={LocalizedUtils} locale={locale}>
                                    <ThemeProvider theme={theme}>
                                        <Fragment>
                                            <DatePicker
                                                // value={moment(selectedDueDate).format('DD MMMM YYYY')}
                                                // value={moment(selectedDueDate).format('YYYY-MM-DD')}
                                                value={selectedDueDate}
                                                onChange={setSelectedDueDate}
                                                animateYearScrolling
                                                open={isShowDueDateComponentDirectly}
                                                onOpen={() => setShowDueDateComponentDirectly(true)}
                                                onClose={() => setShowDueDateComponentDirectly(false)}
                                                variant="dialog"// dialog, static, inline
                                                disableToolbar={false}
                                                // orientation="landscape"
                                                // format
                                                // TextFieldComponent =
                                                // ToolbarComponent 
                                                label=""
                                                style={{marginLeft: 16}}
                                            />
                                        </Fragment>
                                    </ThemeProvider>
                                </MuiPickersUtilsProvider>
                            )
                        } 

                        {
                            isShowDateRangeComponent == true && (
                                
                                <Fragment>

                                    <DialogDateRange 

                                        classes = { classes }
                                        userTokenState = { userTokenState }
                                        isShowDateRangeComponent = { isShowDateRangeComponent }
                                        setShowDateRangeComponent = { setShowDateRangeComponent }

                                        // startDateInDateRange = { startDateInDateRange }
                                        // endDateInDateRange = { endDateInDateRange }
                                        
                                        // setStartDateInDateRange = { setStartDateInDateRange }
                                        // setEndDateInDateRange = { setEndDateInDateRange }
                                    />

                                
                                </Fragment>
                            )
                        }

                        
                    </Grid>
                </Grid>

                <Grid container>
                    <Grid item xs={12} style={{textAlign: 'right'}}>

                        {
                            isShowTextDateRange == true && (

                                <Button 
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIcontDateRangeText} 
                                    style={{marginRight: 8, borderWidth: 0}}
                                    onClick={() => setShowDateRangeComponent(true)}
                                >
                                    <b>{ moment(startDateRangeState).format('DD MMMM YYYY') } - { moment(endDateRangeState).format('DD MMMM YYYY') }</b>
                                </Button>
                            )
                        }

                        {
                            isShowTextPeriod == true && (

                                <Button 
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIcontDateRangeText} 
                                    style={{marginRight: 8, borderWidth: 0}}
                                    onClick={(e) => handleClickPeriod(e)}
                                >
                                <b>{periodName}</b>
                            </Button>
                            )
                        }
                    </Grid>
                </Grid>

                <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                    <b>Deskripsi (Optional)</b>
                </Typography>

                <TextField
                    onChange={handleChangeDescription}
                    style={{marginTop: 0}}
                    id="outlined-multiline-static-description"
                    // label="Multiline"
                    multiline
                    rows="4"
                    // defaultValue="Default Value"
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    fullWidth
                />

                <br />
                <Grid container>
                    <Grid item xs={12}>
                        {
                            memberPositionList.length == 0 && (


                                <Button 
                                    onClick={ () => Redirect(ToSOTable) }
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIconInModal}
                                    style={{marginTop: 24}}
                                >
                                    <IconButton style={{background: '#edcfd8'}}>
                                        <i className="material-icons" style={{color: 'white', fontSize: 14}}>
                                            person
                                        </i>
                                    </IconButton>
                                    &nbsp;
                                    <b>
                                        Belum ada posisi yang di tentukan untuk membuat Goal ;(
                                    </b>
                                
                                </Button>
                            
                            )
                        }
                        
                        {

                            memberPositionList.length > 0 && (

                                <Button 
                                    onClick={ handleClickDropdownPosition }
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIconInModal}
                                    style={{marginTop: 24}}
                                >
                                    <IconButton style={{background: '#edcfd8'}}>
                                        <i className="material-icons" style={{color: 'white', fontSize: 14}}>
                                            person
                                        </i>
                                    </IconButton>
                                    &nbsp;
                                    <b>

                                    
                                        { 
                                            memberPositionList.length > 0 && textValuePosition == ''

                                                ? memberPositionList[0].structure_position_title_name : 
                                                    textValuePosition 
                                        } 
                                    </b>
                                
                                </Button>
                            )
                        }


                        <StyledMenu
                            id="customized-menu"
                            anchorEl={anchorElTimePosition}
                            keepMounted
                            open={Boolean(anchorElTimePosition)}
                            onClose={handleCloseDropdownPosition}
                        >

                            {
                                memberPositionList.length > 0 && memberPositionList.map((item, i) => {
                                    
                                    return (
                                        <StyledMenuItem key={i} onClick={(e) => handleChoosePosition(e, item)}>                         
                                            <ListItemText 
                                                
                                                primary = {

                                                    <Typography variant='subtitle2' className={classes.title}>
                                                        <b>{item.structure_position_title_name}</b>
                                                    </Typography>
                                                }  
                                            />

                                        </StyledMenuItem>
                                    )
                                })
                            }            
                        </StyledMenu>

                    </Grid>
                </Grid>
                
                <br />
                <br />
                <Grid container>
                    <Grid item xs={6}>
                        <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                            <b>Start value : </b>
                        </Typography>
                        <Typography variant='subtitle1' className={classes.title}>
                            <b>0</b>
                        </Typography>
                    </Grid>

                    <Grid item xs={6}>
                        <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                            <b>Nilai Target : </b>
                        </Typography>
                        <TextField
                            id="outlined-bare"
                            // className={classes.textField}
                            onChange= {handleChangeNilaiTarget}
                            // color='primary'
                            // onKeyDown={handleEnterPress}
                            variant="outlined"
                            fullWidth
                        />
                    </Grid>
                </Grid>

                <br />
                <br />
                <Typography variant='subtitle1' className={classes.title} style={{color: 'grey'}}>
                    <b>Input Method :</b>
                </Typography>
                <List>

                    {
                        calculationMethodList.length > 0 && calculationMethodList.map((item, i) => (

                            <Fragment
                                key={i}
                            >
                                <Radio    
                                    checked={selectedValueCalcMethod === item}
                                    onChange={(e) => handleChangeRadioButton(e, item)}
                                    value={item.id}
                                    name={item.name}
                                />
                                <span style={{fontFamily: 'Nunito', color: 'grey'}}>
                                    {Capitalize(item.name)} &nbsp;&nbsp;&nbsp;&nbsp;
                                </span>
                            </Fragment>
                            // </ListItem>
                        ))
                    }
                </List>
                    
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>

                {
                        // memberPositionList.length == 0 ||
                        // textValuePosition == '' || 
                        // selectedDueDate == '' ||
                        // startDateRangeState == null ||
                        // endDateRangeState == null
                        bigGoal == '' ||
                        nilaiTarget == '' ||
                        selectedValueCalcMethod == ''
                        

                    ? (
                        <Button 
                            // onClick={handleSUBMITGoal}
                            variant='contained' 
                            className={classes.buttonDisabled}
                            fullWidth
                            disabled
                        >  
                            Simpan 
                        </Button>

                    ) : (

                        <Button 
                            onClick={handleSUBMITGoal}
                            variant='contained' 
                            className={classes.button}
                            fullWidth
                            // disableRipple={disabledButton == true ? true : false}
                            disabled={disabledButton == true ? true : false}

                        >  

                            {       
                                loader == true ? (

                                    <CircularProgress size={20} style={{color: 'white'}} />

                                ) : '  Simpan Perubahan !'
                            }
                        
                        </Button>
                    )


                }
            </DialogActions>
            <br />
            
            <DialogError 
                classes= { classes }
                isOpenDialogError = { isOpenDialogError }
                setOpenDialogError = { setOpenDialogError } 
                textErrorInformation = 'API Edit Goal is UNDER MAINTENANCE...'
            />


        </Dialog>
    )
};

export default DialogEdit;


const theme = createMuiTheme({

    palette: {

        primary: {
            
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    },

    typography: {
        fontFamily: 'Nunito',
        textTransform: 'capitalize'
    },
    textfield: {
        width: 200
    }
  })