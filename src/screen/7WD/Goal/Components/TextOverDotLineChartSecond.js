

import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider, fade} from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, ListItemSecondaryAction, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, AppBar, Toolbar, Chip
    
} from '@material-ui/core';

import PictResultValue from '../../../../assets/images/Group_2620.png';

const numeral = require('numeral');



const TextOverDotLineChartSecond = props => {

    const { classes, goalDetailState } = props;

    return (


        <List>

            <ListItem>
                <ListItemIcon 
                    style={{
                        marginLeft: 8
                        
                    }}
                >
                    <img src={PictResultValue} style={{width: 40, height: 40 }} />
                </ListItemIcon>
                            
                <ListItemText 
                    className={classes.listItemtext}
                    primary = {
                        <Typography 
                            variant='subtitle2' 
                            className={classes.title}
                            // style={{marginRight: 32}}
                        >
                            <b>
                                {numeral(goalDetailState.calculatedValue.result_value).format('0,0')}
                                &nbsp;
                                ({numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')}%)
                            </b>
                        </Typography>
                    } 

                    secondary = {        
                        <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                            Goal Result                           
                        </Typography>
                    }
                />

            </ListItem>
        </List>
    )
};

export default TextOverDotLineChartSecond;
