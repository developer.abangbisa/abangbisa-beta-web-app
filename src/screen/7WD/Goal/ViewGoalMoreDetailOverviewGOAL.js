import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider, fade} from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, ListItemSecondaryAction, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, AppBar, Toolbar, Chip, Collapse
    
} from '@material-ui/core';

import axios from 'axios'
import moment from 'moment';
import clsx from 'clsx';

import { CircularProgressbarWithChildren, buildStyles, CircularProgressbar } from 'react-circular-progressbar';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import DotLineChart from './Components/DotLineChart';
import { StyledMenu, StyledMenuItem } from '../../../components/StyledMenuDropdown';
import CircleGoalDetail from '../../../components/CircleGoalDetail';

import TextOverDotLineChart from './Components/TextOverDotLineChart';
import TextOverDotLineChartSecond from './Components/TextOverDotLineChartSecond';

import { URL_API } from '../../../constants/config-api';


import AvatarDummy from '../../../assets/images/Avatar_dummy.png';
import PictResultValue from '../../../assets/images/Group_2620.png';
import PictGAP from '../../../assets/images/Group_2619.png';
import PictDone from '../../../assets/images/Group_2614.png';

const numeral = require('numeral');

const ViewGoalMoreDetailOverviewGOAL = props => {

    const { 
        
        classes,
        goalDetailState,
        fotoQuery,
        userToken,
        userLoginName,
        memberPositionList,
        handleDropdownOpen,
        handleDropdownClose,
        anchorEl,
        handleDialogEditGoalMaster,
        handleDialogDeleteGoalMaster,
        collectionGoalResultList,
        handleCollapse,
        openCollapse,
        handleDropdownCloseGoalResult,
        handleDropdownOpenGoalResult,
        anchorElListGoalResult,
        handleDialogEdit,
        handleDialogDelete

    } = props;
    
    return (

        <Grid container >
            <Grid item xs={3} style={{textAlign: 'left'}}>  
                <Paper 
                    elevation= {1} 
                    className = {

                        goalDetailState.calculatedValue.gap_value_percent > goalDetailState.calculatedValue.result_value_percent ||
                        goalDetailState.calculatedValue.expected_value_percent > goalDetailState.calculatedValue.result_value_percent
                        
                        ?
                            classes.paperColumPertamaRowFirstKebakaran : 

                                classes.paperColumPertamaRowFirst
                    
                    }
                >
                    <List>
                        <ListItem style={{paddingLeft: 0, marginRight: 24}}>
                            
                            <IconButton
                                aria-haspopup="true"
                                color="inherit"
                                
                            >                        
                                {
                                    fotoQuery !== '' ? (
                                                                    
                                        <img src={URL_API + '/' +fotoQuery+ "&token=" + userToken}  className={classes.userRealFoto} />

                                    ) : (

                                        <img src={AvatarDummy} alt="Gear Picture" className={classes.imageAvatar} />

                                    )
                                }
                            </IconButton>
                            &nbsp;&nbsp;

                            <ListItemText 
                                className={classes.listItemtext}
                                primary = {

                                    <Typography variant='subtitle2' className={classes.title}>
                                        <b>{userLoginName}</b>
                                    </Typography>
                                } 
                                secondary = {

                                    memberPositionList.length > 0 && memberPositionList.map((item, i) => (

                                        <Chip  
                                            key={i}
                                            label={item.structure_position_title_name} 
                                            style={{
                                                backgroundColor: '#f4f7fc', 
                                                color: 'grey', 
                                                fontFamily: 'Nunito',
                                                marginTop: 8
                                            }}
                                            size='small'
                                            
                                        />
                                    ))
                                } 
                            />

                            <ListItemSecondaryAction>
                                <IconButton 
                                    onClick={handleDropdownOpen}
                                    style={{marginTop: '-64px', marginRight: '-16px'}}>
                                    <i className='material-icons'>
                                        more_horiz
                                    </i>
                                </IconButton>

                                <StyledMenu
                                    id="customized-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={Boolean(anchorEl)}
                                    onClose={handleDropdownClose}
                                >
                                    <StyledMenuItem
                                        onClick={handleDialogEditGoalMaster}
                                    >                         
                                        <ListItemText 
                                            
                                            primary = {

                                                <Typography variant='subtitle2' className={classes.title}>
                                                    <b>Edit</b>
                                                </Typography>
                                            }  
                                        />

                                    </StyledMenuItem>
                                    <StyledMenuItem
                                        onClick={handleDialogDeleteGoalMaster}
                                    >                         
                                        <ListItemText 
                                            
                                            primary = {

                                                <Typography variant='subtitle2' className={classes.title}>
                                                    <b>Delete</b>
                                                </Typography>

                                            }  
                                        />

                                    </StyledMenuItem>
                                </StyledMenu>
                            </ListItemSecondaryAction>
                        
                        </ListItem>

                        <ListItem>
                            <Grid container direction='row' justify='center' alignItems='center'>
                                <div className={classes.circularProgressBarInMoreDetail}>

                                    <CircleGoalDetail 
                                        classes = { classes }
                                        goalDetailState = { goalDetailState }
                                    />

                                </div>                                   
                            </Grid>                            
                        </ListItem>
                        <ListItem>
                            
                            <Grid container direction='row' alignItems='center' justify='center'>
                                <Typography variant='subtitle1' className={classes.title} >
                                    <b>{goalDetailState.name}</b>
                                </Typography>
                            </Grid>
                        </ListItem>

                        <ListItem>
                            <ListItemIcon 
                                style={{ marginLeft: 8 }}
                            >
                                <IconButton style={{backgroundColor: '#aed9ff', padding: '6px'}}>
                                    <i className="material-icons" style={{color: '#4aa9fb'}}>
                                        people
                                    </i>
                                </IconButton>
                            </ListItemIcon>
                                            
                            <ListItemText 
                                className={classes.listItemtext}
                                primary = {
                                    <Typography 
                                        variant='subtitle2' 
                                        className={classes.title}
                                    >
                                        <b>{goalDetailState.owner.structure_unit_type_name } {goalDetailState.owner.structure_unit_name}</b>
                                    </Typography>
                                } 
                            
                            />
                        </ListItem>

                        <ListItem>
                            <ListItemIcon 
                                style={{
                                    marginLeft: 8
                                    
                                }}
                            >
                                <IconButton
                                    style={{backgroundColor: '#bdffde', padding: '6px'}}
                                >
                                    <i className="material-icons" style={{color: '#36d686'}}>
                                        play_arrow
                                    </i>
                                </IconButton>
                            </ListItemIcon>
                                            
                            <ListItemText 
                                className={classes.listItemtext}
                                primary = {
                                    <Typography 
                                        variant='subtitle2' 
                                        className={classes.title}
                                    >
                                        Start Value
                                    </Typography>
                                } 

                                secondary = {
                                                    
                                    <Typography variant='subtitle2' className={classes.title}>
                                        <b>{numeral(goalDetailState.calculatedValue.start_value).format('0,0')}</b>
                                    </Typography>
                                }
                            
                            />
                        </ListItem>
                        
                        <ListItem>
                            <ListItemIcon 
                                style={{
                                    marginLeft: 8
                                    
                                }}
                            >
                                <IconButton
                                    style={{backgroundColor: '#aed9ff', padding: '6px'}}
                                >
                                    <i className="material-icons" style={{color: '#4aa9fb'}}>
                                        radio_button_checked
                                    </i>
                                </IconButton>
                            </ListItemIcon>
                                            
                            <ListItemText 
                                className={classes.listItemtext}
                                primary = {
                                    <Typography 
                                        variant='subtitle2' 
                                        className={classes.title}
                                    >
                                        Target value
                                    </Typography>
                                } 

                                secondary = {  
                                        
                                    <Typography variant='subtitle2' className={classes.title}>
                                        <b>{numeral(goalDetailState.target_value).format('0,0')}</b>
                                    </Typography>
                                }
                            
                            />
                        </ListItem>

                        <ListItem>
                            <ListItemIcon 
                                style={{
                                    marginLeft: 8
                                    
                                }}
                            >
                                <img src={PictResultValue} style={{width: 40, height: 40 }} />
                            </ListItemIcon>
                                            
                            <ListItemText 
                                className={classes.listItemtext}
                                primary = {
                                    <Typography 
                                        variant='subtitle2' 
                                        className={classes.title}
                                    >
                                        Goal Result
                                    </Typography>
                                } 

                                secondary = {        
                                    <Typography variant='subtitle2' className={classes.title}>
                                        <b>
                                            {numeral(goalDetailState.calculatedValue.result_value).format('0,0')}
                                            &nbsp;
                                            ({numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')}%)
                                        </b>
                                    </Typography>
                                }
                            
                            />
                        </ListItem>
                        
                        <ListItem>
                            <ListItemIcon 
                                style={{
                                    marginLeft: 8
                                    
                                }}
                            >
                                <img src={PictGAP} style={{width: 40, height: 40 }} />
                            </ListItemIcon>
                                            
                            <ListItemText 
                                className={classes.listItemtext}
                                primary = {
                                    <Typography 
                                        variant='subtitle2' 
                                        className={classes.title}
                                    >
                                        Gap
                                    </Typography>
                                } 

                                secondary = {      
                                    
                                    goalDetailState.calculatedValue.gap_value < 0 ? (

                                        <Typography variant='subtitle2' className={classes.title}>
                                            0
                                        </Typography> 

                                    ) : (

                                        <Typography variant='subtitle2' className={classes.title}>
                                            <b>{numeral(goalDetailState.calculatedValue.gap_value).format('0,0')}</b>
                                            &nbsp;
                                            <b>({numeral(goalDetailState.calculatedValue.gap_value_percent).format('0,0')}%)</b>
                                        </Typography>
                                    )

                                }
                            />
                        </ListItem>
                        
                        <ListItem>
                            <ListItemIcon 
                                style={{
                                    marginLeft: 8
                                    
                                }}
                            >
                                <img src={PictDone} style={{width: 40, height: 40 }} />
                            </ListItemIcon>
                                            
                            <ListItemText 
                                className={classes.listItemtext}
                                primary = {
                                    <Typography 
                                        variant='subtitle2' 
                                        className={classes.title}
                                    >
                                        Posisi Aman
                                    </Typography>
                                } 

                                secondary = {     

                                    <Typography variant='subtitle2' className={classes.title}>
                                        <b>{numeral(goalDetailState.calculatedValue.expected_value_percent).format('0,0')} %</b>
                                    </Typography>
                                }
                            />
                        </ListItem>
                    </List>
                </Paper>
                <br />
                <br />
            </Grid>

            <Grid item xs={9} style={{textAlign: 'left'}} >  

                <Paper elevation={1} className={classes.paperColumnDuaRowFirst}>
                    <Typography variant='subtitle1' className={classes.titleInLineChart} >
                        <b>Overview History</b>
                    </Typography>
                    
                    <Grid container>
                        <Grid item sm={4}>
                            <TextOverDotLineChart 
                                classes = {classes }
                                goalDetailState = { goalDetailState }
                            />
                        </Grid>

                        <Grid item sm={4}>
                            <TextOverDotLineChartSecond
                                classes = {classes }
                                goalDetailState = { goalDetailState }
                            />
                        </Grid>
                        <Grid item sm={4}></Grid>
                    </Grid>
                    <br />
                
                    <DotLineChart 
                        classes = {classes }
                        collectionGoalResultList = { collectionGoalResultList }
                    />

                </Paper>

                <br />
                <Grid container>
                    <Grid item sm={6}> 
                        {
                            collectionGoalResultList.length > 0 && (

                                <Typography variant='h6' className={classes.titleInLineChart} >
                                    <b>Latest Goal Result</b>
                                </Typography>

                            )
                        }
                    </Grid>

                    <Grid item sm={6} style={{textAlign: 'right'}}> 

                    </Grid>
                </Grid>


                <br />

                {
                    collectionGoalResultList.length == 0 && goalDetailState.status_id !== '4' && (

                        <Paper className={classes.paperColumnDuaRowFirst}>
                            <Grid container>
                                <Grid item xs={12} style={{textAlign: 'center'}}>
                                    <Typography variant='subtitle2' className={classes.title} style={{color: 'grey', margin: 8}}>
                                        Anda belum memiliki Goal Result ;(
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Paper>
                    )
                }

                {
                    collectionGoalResultList.length > 0 && (

                        <Paper className={classes.paperColumnDuaRowFirst}>

                
                            <Grid container>
                                <Grid item xs={12}>

                                    <List style={{width: 120}}>

                                        {
                                            collectionGoalResultList.length > 0 && (

                                                <ListItem button onClick={handleCollapse}>
                                                    <ListItemText 
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                                                                style={{color: 'grey'}}
                                                            >
                                                                <b>Record</b>
                                                            </Typography>
                                                        } 
                                                    />
                                        
                                                    {
                                                        openCollapse   ? <ExpandLess /> : <ExpandMore />
                                                    }
                                                </ListItem>
                                            )
                                        }

                                        <Collapse in={openCollapse} timeout="auto" unmountOnExit>

                                                

                                                {
                                                    collectionGoalResultList.length > 0 ? collectionGoalResultList.map((item, i) => {


                                                        if(goalDetailState.calculationMethod.id == '2' ){
                                                            

                                                            return (

                                                                <List style={{width: '760%'}} key={i}>
                                                                    <ListItem >
                                                                        <ListItemText 
                                                                            primary = {
                                                                                <Typography 
                                                                                    variant='subtitle1' 
                                                                                    className={classes.title}
                                                                                    style={{color: '#55dc87'}}
                                                                                >
                                                                                    <b>+ {numeral(item.result_value).format('0,0')}</b>
                                                                                </Typography>
                                                                            } 

                                                                            secondary = {       

                                                                                <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                                                                    {item.description !== null ? item.description : '-'} 
                                                                                    <br />
                                                                                    <i style={{fontSize: 12}}>({moment(item.result_date).format('DD MMMM YYYY')})</i>
                                                                                </Typography>
                                                                            }
                                                                        />
                                                                    </ListItem>
                                                                    <Divider />

                                                                    <ListItemSecondaryAction>
                                                        
                                                                        <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                                                                
                                                                            {moment(item.updated_at).format('DD MMMM YYYY h:mm:ss a')}

                                                                            <IconButton
                                                                                onClick={(e) => handleDropdownOpenGoalResult(e, item)}
                                                                        
                                                                            >
                                                                                <i className='material-icons'>
                                                                                    more_vert
                                                                                </i>
                                                                            </IconButton>
                                                                        </Typography>

                                                                            <StyledMenu
                                                                                id="customized-menu-goal-result"
                                                                                anchorEl={anchorElListGoalResult}
                                                                                keepMounted
                                                                                open={Boolean(anchorElListGoalResult)}
                                                                                onClose={handleDropdownCloseGoalResult}
                                                                            >
                                                                                <StyledMenuItem
                                                                                    onClick ={ (e) => handleDialogEdit(e, item)}
                                                                                
                                                                                >                         
                                                                                    <ListItemText 
                                                                                        
                                                                                        primary = {

                                                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                                                <b>Edit</b>
                                                                                            </Typography>
                                                                                        }  
                                                                                    />

                                                                                </StyledMenuItem>
                                                                                <StyledMenuItem
                                                                                    onClick ={ (e) => handleDialogDelete(e, item)}

                                                                                >                         
                                                                                    <ListItemText 
                                                                                        primary = {

                                                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                                                <b>Delete</b>
                                                                                            </Typography>
                                                                                        }  
                                                                                    />

                                                                                </StyledMenuItem>
                                                                            </StyledMenu>
                                                                    
                                                                    </ListItemSecondaryAction>
                                                                </List>
                                                            )
                                                        };


                                                        if(goalDetailState.calculationMethod.id == '1' ){ 

                                                            return (

                                                                <List style={{width: '760%'}} key={i}>
                                                                    <ListItem >
                                                                        <ListItemText 
                                                                            primary = {
                                                                                <Typography 
                                                                                    variant='subtitle1' 
                                                                                    className={classes.title}
                                                                                    style={{color: '#55dc87'}}
                                                                                >
                                                                                    You updated goal result to <b> {numeral(item.result_value).format('0,0')}</b>
                                                                                </Typography>
                                                                            } 

                                                                            secondary = {       

                                                                                <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                                                                    { item.description !== null ? item.description : '-' } 
                                                                                    <br />
                                                                                    <i style={{fontSize: 12}}>({moment(item.result_date).format('DD MMMM YYYY')})</i>
                                                                                </Typography>
                                                                            }
                                                                        />
                                                                    </ListItem>
                                                                    <Divider />

                                                                    <ListItemSecondaryAction>
                                                        
                                                                        <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                                                                
                                                                            {moment(item.updated_at).format('DD MMMM YYYY h:mm:ss a')}

                                                                            <IconButton
                                                                                onClick={(e) => handleDropdownOpenGoalResult(e, item)}                                                                            
                                                                            >
                                                                                <i className='material-icons'>
                                                                                    more_vert
                                                                                </i>
                                                                            </IconButton>
                                                                        </Typography>

                                                                            <StyledMenu
                                                                                id="customized-menu-goal-result"
                                                                                anchorEl={anchorElListGoalResult}
                                                                                keepMounted
                                                                                open={Boolean(anchorElListGoalResult)}
                                                                                onClose={handleDropdownCloseGoalResult}
                                                                            >
                                                                                <StyledMenuItem
                                                                                    onClick ={ (e) => handleDialogEdit(e, item)}

                                                                                >                         
                                                                                    <ListItemText 
                                                                                        
                                                                                        primary = {

                                                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                                                <b>Edit</b>
                                                                                            </Typography>
                                                                                        }  
                                                                                    />

                                                                                </StyledMenuItem>
                                                                                <StyledMenuItem
                                                                                    onClick ={ (e) => handleDialogDelete(e, item)}

                                                                                >                         
                                                                                    <ListItemText 
                                                                                        primary = {

                                                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                                                <b>Delete</b>
                                                                                            </Typography>
                                                                                        }  
                                                                                    />

                                                                                </StyledMenuItem>
                                                                            </StyledMenu>
                                                                    
                                                                    </ListItemSecondaryAction>
                                                                </List>
                                                            )
                                                        };


                                                    }) : null
                                                }
                                            
                                        </Collapse>
                                    </List>
                                </Grid>
                            </Grid>
                        </Paper>
                    )
                }
                <br />
                <br />
                <br />

            </Grid>
        </Grid>
    );


};

export default ViewGoalMoreDetailOverviewGOAL;

