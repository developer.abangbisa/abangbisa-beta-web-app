import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider, fade} from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, ListItemSecondaryAction, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, AppBar, Toolbar, Chip, Collapse
    
} from '@material-ui/core';

import axios from 'axios'
import moment from 'moment';
import clsx from 'clsx';

import { BrowserRouter as Router, Route, Switch, Redirect  } from "react-router-dom";
import { RoutedTabs, NavTab } from "react-router-tabs";

// import { CircularProgressbarWithChildren, buildStyles, CircularProgressbar } from 'react-circular-progressbar';

// import ExpandLess from '@material-ui/icons/ExpandLess';
// import ExpandMore from '@material-ui/icons/ExpandMore';

// import DotLineChart from './Components/DotLineChart';
// import { StyledMenu, StyledMenuItem } from '../../../components/StyledMenuDropdown';
// import CircleGoalDetail from '../../../components/CircleGoalDetail';

// import TextOverDotLineChart from './Components/TextOverDotLineChart';
// import TextOverDotLineChartSecond from './Components/TextOverDotLineChartSecond';

import SearchIcon from '@material-ui/icons/Search';
import ViewGoalMoreDetailOverviewGOAL from './ViewGoalMoreDetailOverviewGOAL';


import DialogCreate from './ComponentsGoalResult/DialogCreate';
import DialogEdit from './ComponentsGoalResult/DialogEdit';
import DialogDelete from './ComponentsGoalResult/DialogDelete';
import DialogDeleteGoalMaster from './Components/DialogDelete';
import DialogEditGoalMaster from './Components/DialogEdit';

import RedirectCustom from '../../../utilities/RedirectCustom';
import { ToGoal, ToGoalDetail } from '../../../constants/config-redirect-url';
import { RouteToOverviewGoal, RouteToMeasureActivities } from '../../../constants/config-redirect-url-with-router';

import { URL_API } from '../../../constants/config-api';

// import AvatarDummy from '../../../assets/images/Avatar_dummy.png';
// import PictResultValue from '../../../assets/images/Group_2620.png';
// import PictGAP from '../../../assets/images/Group_2619.png';
// import PictDone from '../../../assets/images/Group_2614.png';

// import Admins from './Admins';
// import Moderator from './Moderator';
// const numeral = require('numeral');
import ViewMA from '../../7WD/MeasureActivities/ViewMA';

const theme = createMuiTheme({
  
    palette: {

        primary: {
            
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }

});

const styles = theme => ({

    root: {
      
        width: '100%',
        marginTop: 1 * 3
  
    },
    layoutMarginLeftAndTop: {

        marginLeft: 72, 
        marginTop: 56
    },
    layoutMarginTop: {

        // marginLeft: 72, 
        marginTop: 56
    },
    layoutMarginTopNew: {

        marginTop: theme.spacing(10)
    },
    layoutMarginLeftList: {

        marginLeft: theme.spacing(4), 
        marginTop: theme.spacing(1)
      
    },
    layoutMarginLeftListChooseTab: {

        // marginLeft: theme.spacing(5), 
        marginTop: theme.spacing(4)
      
    },
    title: {
        fontFamily: 'Nunito'
    },
    titleInLineChart: {
        fontFamily: 'Nunito',
        marginLeft: theme.spacing(4), 
        marginTop: theme.spacing(4)

    },

    button: {

        // width: '503px',
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        // marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'

    },
    buttonDisabled: {
        fontFamily:'Nunito',
        textTransform: 'capitalize',
        color: 'grey',
        fontWeight: 'bold'


    },
    timeFrameIcontDateRangeText: {
        fontFamily: 'Nunito',
        color: '#4aa9fb',
        textTransform: 'capitalize',
        backgroundColor: '#edf6ff'
    },

    /*
        ```````````
        DIALOG EDIT

        ```````````
    */
    titleListChoose: {

        fontFamily: 'Nunito',
        cursor: 'pointer'

    },
    titleListChooseAktif: {

        fontFamily: 'Nunito',
        color: '#d6d3d3', 
        cursor: 'pointer'
    },

    bottomBorderActive: {

        borderBottom: '0.2em solid #cc0707', 
        borderRadius: 2 
    },

    timeFrameIcon: {
        fontFamily: 'Nunito',
        color: 'grey', //#4aa9fb
        textTransform: 'capitalize',
        // marginRight: theme.spacing(4),
        // marginTop: theme.spacing(9),
        // backgroundColor: '#cfe8fd'
    },

    timeFrameIconInModal: {

        fontFamily: 'Nunito',
        color: 'grey',
        textTransform: 'capitalize',
        '&:hover': {
            color: '#edcfd8',//#edcfd8, #cc0707
        },
        borderWidth: 0
    },

    /*       
        ****************************** 
    */
    
    paperColumPertamaRowFirst: {

        marginLeft: theme.spacing(11),
        padding: theme.spacing(0.2),

    },
    paperColumPertamaRowFirstKebakaran: {

        marginLeft: theme.spacing(11),
        padding: theme.spacing(0.2),
        background: '#edcfd8',
        borderLeft: '0.5em solid red'

    },
    paperColumnDuaRowFirst: {

        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        padding: theme.spacing(0.2),
    },
    
    /*
        `````````````````````````````
        FOTO REAL USER NAME, USERNAME

        `````````````````````````````
    */
    userRealFoto: {

        // margin: 10,
        width: 48,
        height: 48,
        borderRadius: 24
    },
    imageAvatar: {

        width: 50,
        height: 40
    },

    /*
        ````````
        CIRCULAR

        ````````
    */
    circularProgressBarInMoreDetail : {
        
        // width: 60, 
        // height: 60,
        width: 100, 
        height: 100,
        
    },
    circularProgressBarInPaperDetail : {
        
        width: 60, 
        height: 60,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(2)
    },

    buttonModalDelete: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        marginRight: theme.spacing(4),
        color: 'white'
    },

    /*
        ``````````
        MODAL EDIT 

        ``````````
    */
    textField: {
        
        minWidth: 425   
    },

    /*
        ```````
        SEARCH

        ```````
    */
    search: {

        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.black, 0.1),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.black, 0.35),
        },
        // borderRadius: '5',
        // backgroundColor: grey,
        //     '&:hover': {
        //         backgroundColor: green,
        //     },
        // marginRight: theme.spacing(2),
        // marginLeft: 0,
        marginTop: theme.spacing(4),
        width: '50%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
                width: 'auto',
            },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: 'white'
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
        color: 'grey'
        // color: '#cc0707'
    },
});

const ViewGoalMoreDetail = props => {

    const { classes, match } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    const userToken = localStorage.getItem('userToken');
    const statusUserLogin = localStorage.getItem('status_user_login');
    const statusUserLoginAfterParse = JSON.parse(statusUserLogin);

    const [ userTokenState, setUserTokenState ] = useState('');

    const [ fotoQuery, setFotoQuery ] = useState('');
    const [ userLoginName, setUserLoginName ] = useState('');
    const [ memberPositionList, setMemberPositionList ] = useState([]);

    const goalDetail = localStorage.getItem('goal_detail');
    const goalDetailAfterParse = JSON.parse(goalDetail);
    const [ goalDetailState, setGoalDetailState ] = useState(

        {

            id: null,
            period: {
                id: null,
                name: null,
                start_date: null,
                end_date: null
            },
            start_date: null,
            end_date: null,

            calculatedValue: {

                result_value_percent: null,
                gap_value_percent: null,
                result_value: null,
                gap_value_percent: null,
                expected_value_percent: null
            },
            name: null,
            owner : {

                structure_unit_type_name: null,
                structure_unit_name : null

            },
            calculatedValue: {

                target_value: null,

            },
            calculationMethod: {

                id: null
            },
            time: {
                remaining_days: null
            },
            start_value: null,
            status_id: null
        }
    );

    const [ collectionGoalResultList, setCollectionGoalResultList ] = useState([]);
    const [ showButtonGoalResult, setShowButtonGoalResult ] = useState(false);

    // const [ totalDaysState, setTotalDaysState ] = useState(null);
    // const [ dataCircleInPaperSecond, setDataCircleInPaperSecond ] = useState(null);

    useEffect(() => {

        setUserTokenState(userToken);
        setUserLoginName(statusUserLoginAfterParse.member_first_name + " " + statusUserLoginAfterParse.member_last_name)
        setFotoQuery(statusUserLoginAfterParse.member_photo_url);
        setMemberPositionList(statusUserLoginAfterParse.member_position);
        
        // console.log("Match : ", match);

        if(userToken !== undefined ){
        
            const header =  {   

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            //*GOL RESULT LIST SELF - ONLY OWNER GOAL USER LOGIN
            axios
                .get(URL_API + `/swd/goal/${goalDetailAfterParse.id}/result`)
                .then(function(response){

                    console.log("Response Original : ", response);

                    if(response.status == 200){
                        if(response.data.data !== null){

                            setCollectionGoalResultList(response.data.data);

                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);
                });

            //*GOL DETAIL
            axios
                .get(URL_API + `/swd/goal/${goalDetailAfterParse.id}`)
                .then(function(response){

                    console.log("Response Original GOAL DETAIL : ", response);

                    if(response.status == 200){
                        if(response.data.data !== null){

                            //*Get foto
                            if(response.data.data.member !== null){
                                if(response.data.data.member.self.rel !== null){
                                    if(response.data.data.member.self.rel.photo_url !== null){

                                        setFotoQuery(response.data.data.member.self.rel.photo_url )
                                    };
                                };
                            };
                            
                            setGoalDetailState(response.data.data);

                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);

                })

        } else { console.log("No Access Token available!")};



        /*
            ```````````````````````````
            TO KNOW CURRENT SHOW BUTTON

            ```````````````````````````
        */
        const currentLocation = window.location.pathname;

        if(currentLocation == ToGoalDetail + RouteToOverviewGoal){
            // console.log("currentLocation : ", currentLocation);
            // console.log("currentLocationWew : ", ToGoalDetail + RouteToOverviewGoal);

            setShowButtonGoalResult(true);
        };

    },[]);


    /*
        ``````````````````````````````````````````````````
        HANDLE UPDATE GOAL DETAIL AFTER CREATE GOAL RESULT

        ``````````````````````````````````````````````````
    */

    const [ isFireGoalDetail, setFireGoalDetail ] = useState(false);

    useEffect(() => {
        
        if(isFireGoalDetail == true ){

            if(userTokenState !== undefined ){
        
                const header =  {   
    
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userTokenState
                };
    
                axios.defaults.headers.common = header;    
    
                axios
                    .get(URL_API + `/swd/goal/${goalDetailAfterParse.id}`)
                    .then(function(response){
    
                        console.log("Response Original GOAL DETAIL OTOMATIS UPDATE: ", response);
    
                        if(response.status == 200){
                            if(response.data.data !== null){
    
                                setGoalDetailState(response.data.data);
    
                            };
                        };
                    })
                    .catch(function(error){
                        
                        console.log("Error : ", error.response);
    
                    });

                //*GOL RESULT LIST SELF - ONLY OWNER GOAL USER LOGIN
                axios
                    .get(URL_API + `/swd/goal/${goalDetailAfterParse.id}/result`)
                    .then(function(response){

                        console.log("Response Original : ", response);

                        if(response.status == 200){
                            if(response.data.data !== null){

                                setCollectionGoalResultList(response.data.data);

                            };
                        };
                    })
                    .catch(function(error){
                        
                        console.log("Error : ", error.response);

                    });
    
            } else { console.log("No Access Token available!")};
        };

    },[isFireGoalDetail])

    /*
        `````````````````````````
        DROPDOWN GOAL RESULT LIST
        `````````````````````````
    */

    const [goalResultId, setGoalResultId] = useState('');
    const [goalResultNameDescription, setGoalResultNameDescription] = useState('');
    const [goalResultDataDetail, setGoalResultDataDetail] = useState('');


    const [anchorElListGoalResult, setAnchorElListGoalResult] = useState(null);

    const handleDropdownOpenGoalResult = (event, item) => {

        event.preventDefault();
        setAnchorElListGoalResult(event.currentTarget);
        
        console.log('handleDropdownOpenGoalResult : ', item);


        setGoalResultId(item.id);
        setGoalResultNameDescription(item.description)
        setGoalResultDataDetail(item)
    };

    function handleDropdownCloseGoalResult() {

        setAnchorElListGoalResult(null);
    };

    /*
        ```````````````````
        DROPDOWN TIME FRAME
        ```````````````````
    */
    const [anchorEl, setAnchorEl] = useState(null);

    // const [ textValueTimeFrame, setTextValueTimeFrame ] = useState('Time Frame');
    function handleDropdownOpen(event) {

        setAnchorEl(event.currentTarget);

        // console.log('handleDropdownOpen : ', item)
    };
    
    function handleDropdownClose() {

        setAnchorEl(null);
    };

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL DELETE GOAL MASTER

        ``````````````````````````````````````
    */
    const [ isModalDeleteGoalMaster, setModalDeleteGoalMaster ] = useState(false);
    const handleDialogDeleteGoalMaster = () => {

        handleDropdownClose();
        setModalDeleteGoalMaster(true);
    };

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL EDIT GOAL MASTER

        ``````````````````````````````````````
    */
    const [ isModalEditGoalMaster,  setModalEditGoalMaster ] = useState(false);
    
    const handleDialogEditGoalMaster = () => {

        handleDropdownClose();
        setModalEditGoalMaster(true);
    };

    /*
        ```````````````
        HANDLE COLLAPSE

        ```````````````
    */
    const [openCollapse, setOpenCollapse] = useState(true);

    function handleCollapse() {

        setOpenCollapse(!openCollapse);
    };

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL CREATE GOAL RESULT

        ``````````````````````````````````````
    */
    const [ isModalCreate, setModalCreate ] = useState(false);

    const handleDialogModalGoalResult = () => {

        setModalCreate(true);
        setOpenCollapse(true);
    };    

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL DELETE GOAL RESULT

        ``````````````````````````````````````
    */
    const [ isModalDelete, setModalDelete ] = useState(false);

    const handleDialogDelete = (e, data) => {

        e.preventDefault();

        handleDropdownCloseGoalResult()
        setModalDelete(true);
        // console.log("handleDialogDelete : ", data.description);

    };

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL EDIT GOAL RESULT

        ``````````````````````````````````````
    */
    const [ isModalEdit, setModalEdit ] = useState(false);

    const handleDialogEdit = (e, data) => {
       
        e.preventDefault();

        handleDropdownCloseGoalResult()        
        setModalEdit(true);

    };

    /*
        ````````````````````````````
        HANDLE TAB ACTIVE & COMPLETE
        
        ````````````````````````````
    */
    //    const [ isYouActive, setYouActive ] = useState(true); 
    //    const [ isYouComplete, setYouComplete ] = useState(false);

   const [ dataActiveOrComplete, setDataActiveOrComplete] = useState (
       {
            id: 1,
            label: 'Overview Goal',
            to : ToGoalDetail + RouteToOverviewGoal
       }
   );

   const handleClickListChoose = (e, data) => {

        e.preventDefault();
        console.log("Data : ", data);

        setDataActiveOrComplete(data)

        if(data.id == 1){

            setShowButtonGoalResult(true);
            

       } 
       else {

            setShowButtonGoalResult(false);

       }
   };

    return (

        <MuiThemeProvider theme={theme} >
            <Paper 
                elevation={0} 
                square 
                className={classes.layoutMarginLeftAndTop}
            >
                <Grid container >
                    <Grid item sm={6} md={6} style={{textAlign: 'left'}} >  
                        <Typography variant='subtitle2' className={classes.title} style={{marginTop: 16, marginLeft: 8, color: 'grey'}} >
                            <Tooltip title='Kembali untuk melihat semua Goal' placement="right">
                                <IconButton onClick={() => RedirectCustom(ToGoal)}>
                                    <i className='material-icons' style={{ cursor: 'pointer'}}>
                                        keyboard_backspace
                                    </i>
                                </IconButton>    
                            </Tooltip>
                        </Typography>
                    </Grid>

                    <Grid item sm={3} md={3} style={{textAlign: 'right'}} >

                         {  
                            //*PERIOD
                            goalDetailState.period !== null && (
                                
                                <Fragment>
                                    <Chip  
                                        label={ goalDetailState.period.name}
                                        style={{backgroundColor: '#f4f7fc', color: 'grey', fontFamily: 'Nunito', marginTop: 24, }}
                                        size='small'
                                    />


                                </Fragment>
                            )
                        }
                    </Grid>

                    <Grid item sm={3} md={3} style={{textAlign: 'left'}} >

                         {//*DATE RANGE
                            goalDetailState.start_date !== null && goalDetailState.end_date !== null && goalDetailState.period == null && (
                                    
                                <Button 
                                    style={{marginTop: 24,borderWidth: 0, marginLeft : 16}}
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIcontDateRangeText}
                                >
                                    <b>{ moment(goalDetailState.start_date).format('DD MMMM YYYY') + " - " + moment(goalDetailState.end_date).format('DD MMMM YYYY') }</b>
                                </Button>
                                
                            )
                        }

                        {// *DUE DATE

                            goalDetailState.start_date == null && goalDetailState.end_date !== null && goalDetailState.period == null && (
                                
                                <Button 
                                    style={{marginTop: 24,borderWidth: 0, marginLeft : 16}}
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIcontDateRangeText}
                                >
                                    <b>{moment(goalDetailState.end_date).format('DD MMMM YYYY')}</b>
                                </Button>
                            )
                        }

                          {// *PERIOD

                            goalDetailState.period !== null  && (
                                                            
                                <Button 
                                    style={{marginTop: 24,borderWidth: 0, marginLeft : 16}}
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIcontDateRangeText}
                                    // onClick={() => setShowDateRangeComponent(true)}
                                >
                                    <b>{moment(goalDetailState.period.start_date).format('DD MMMM YYYY') + " - " + moment(goalDetailState.period.end_date).format('DD MMMM YYYY')}</b>                                    
                                </Button>
                                
                            )
                        }
                    </Grid>
                </Grid>
            </Paper>


            {/* 
                `````````````````````````````````
                TAB OVERVIEW & MEASURE ACTIVITIES

                `````````````````````````````````
            */}
            <Router >
                <Grid container >
                
                    {
                        listChoose.length > 0 && listChoose.map((item, i) => {

                            return (
                                <Fragment key={i}>
                                    <Grid item sm={2} md={2} style={{textAlign: 'right'}} >
                                        <Button 
                                            variant='outlined'
                                            onClick={ (e) => handleClickListChoose(e, item) }
                                            size='small'
                                            className = {
                                                dataActiveOrComplete.id == item.id ?
                                                clsx(classes.titleListChoose, classes.layoutMarginLeftListChooseTab) : 
                                                clsx(classes.titleListChooseAktif, classes.layoutMarginLeftListChooseTab)
                                            }
                                            style = {{ 

                                                borderColor: 'none', 
                                                borderWidth: 0, 
                                                textTransform: 'Capitalize',
                                                marginLeft: '-16px'
                                            }}
                                        >
                                            <Typography 
                                                variant='subtitle2' 
                                                className={classes.title}
                                                // onClick={ (e) => handleClickListChoose(e, item) }
                                            >
                                                <b 
                                                    className = {

                                                        dataActiveOrComplete.id == item.id ? (

                                                            classes.bottomBorderActive

                                                        ) : null
                                                    }
                                                >

                                                    <NavTab
                                                        to={item.to}
                                                        style={{
                                                            textDecorationLine: 'none',
                                                            color: `${dataActiveOrComplete.id == item.id ? 'black' : 'grey'}`
                                                        }}
                                                    >
                                                        { item.label }
                                                    </NavTab>
                                                </b>
                                            </Typography>
                                        </Button>

                                        {/* <NavTab to="/goal-detail/overview-goal">Overview Goal &nbsp;</NavTab>
                                        <NavTab to="/goal-detail/measure-activities">Measure Activities</NavTab> */}
                                    
                                    </Grid>
                                </Fragment>
                            )
                        })
                    }

                    <Grid item sm={4} md={4}></Grid> 

                    <Grid item sm={2} md={2} style={{textAlign: 'right'}} > 

                        {
                            showButtonGoalResult == false && (

                                <div className={classes.search}>
                                    <div className={classes.searchIcon}>
                                        <SearchIcon />
                                    </div>
                                    <InputBase

                                        // onChange={(e) => handleSearch(e.target.value)}
                                        placeholder="Search MA..."
                                        classes={{

                                            root: classes.inputRoot,
                                            input: classes.inputInput
                                        }}

                                        inputProps={{ 'aria-label': 'Search'}}
                                    />
                                </div>
                            )
                        }
                    </Grid>
                    
                    <Grid item sm={2} md={2} style={{textAlign: 'right'}} > 

                        {
                            goalDetailState.status_id !== '4' && (

                                <Button 
                                    onClick={showButtonGoalResult == true ? handleDialogModalGoalResult : () => alert('Tambah MA')}
                                    variant='contained' 
                                    className={classes.button}
                                    style={{
                                        marginRight: 16,
                                        marginTop: 32,
                                        // paddingRight: 8
                                    }}
                                    size='small'
                                >  
                                    <IconButton style={{padding: 7 }}>
                                        <i className='material-icons' style={{fontSize: 12, color: 'white'}}>
                                            add
                                        </i>
                                    </IconButton>  

                                        { showButtonGoalResult !== true ? 'Buat Measure Activity' : 'Tambah Goal Result baru' }                              
                                        
                                </Button>
                            )
                        }

                        
                    </Grid>
                </Grid> 
                <br />

                {
                    /*
                        ```````````````````````````
                        SWITCH ROUTER for VIEW PAGE 

                            - Overview Goal

                            - Measure Activiities

                        ```````````````````````````
                    */
                }

                <Switch>
                    <Route
                        exact
                        path={`${match.path}`}
                        render={() => <Redirect replace to={`${match.path}${RouteToOverviewGoal}`} />}
                    />
                    <Route 
                        path={`${match.path}${RouteToOverviewGoal}`} 
                        // component = { // ViewGoalMoreDetailOverviewGOAL
                        render = {

                            (props) => (

                                <ViewGoalMoreDetailOverviewGOAL
                                    {...props }
                                    classes = { classes }
                                    goalDetailState = { goalDetailState }
                                    fotoQuery = { fotoQuery }
                                    userToken = { userToken }
                                    userLoginName = { userLoginName }
                                    memberPositionList = { memberPositionList }
                                    handleDropdownOpen = { handleDropdownOpen }
                                    handleDropdownClose = { handleDropdownClose }
                                    anchorEl = { anchorEl }
                                    handleDialogEditGoalMaster = { handleDialogEditGoalMaster }
                                    handleDialogDeleteGoalMaster = { handleDialogDeleteGoalMaster }
                                    collectionGoalResultList = { collectionGoalResultList }
                                    handleCollapse = { handleCollapse }
                                    openCollapse = { openCollapse }
                                    handleDropdownCloseGoalResult = { handleDropdownCloseGoalResult }
                                    handleDropdownOpenGoalResult = { handleDropdownOpenGoalResult }
                                    anchorElListGoalResult = { anchorElListGoalResult }
                                    handleDialogEdit = { handleDialogEdit }
                                    handleDialogDelete = { handleDialogDelete }
                                
                                /> )
                        
                        } 

                    />

                    <Route 
                        path={`${match.path}${RouteToMeasureActivities}`} 
                        render = { (props) => (

                                <ViewMA 
                                    {...props}
                                    classes = { classes }
                                />
                            )}
                        // component={Moderator} 
                    />

                </Switch>
            </Router>
        
           

            {/* 
                ``````````````````````
                DIALOG for GOAL RESULT

                ``````````````````````        
            */}

            <DialogCreate 
                classes = { classes }
                isModalCreate = { isModalCreate }
                setModalCreate = {setModalCreate }
                collectionGoalResultList = { collectionGoalResultList }
                setCollectionGoalResultList = { setCollectionGoalResultList }
                goalDetailState = {goalDetailState}
                setFireGoalDetail = { setFireGoalDetail }
            />

            <DialogEdit 
                classes = { classes }
                isModalEdit = { isModalEdit }
                setModalEdit = {setModalEdit }
                collectionGoalResultList = { collectionGoalResultList }
                setCollectionGoalResultList = { setCollectionGoalResultList }
                goalDetailState = {goalDetailState}
                setFireGoalDetail = { setFireGoalDetail }
                goalResultDataDetail = { goalResultDataDetail }
            />

            <DialogDelete 
                classes = { classes }
                isModalDelete = { isModalDelete }
                setModalDelete = { setModalDelete }
                collectionGoalResultList = { collectionGoalResultList }
                setCollectionGoalResultList = { setCollectionGoalResultList }
                goalResultId = { goalResultId }
                goalDetailState = {goalDetailState}
                goalResultNameDescription = { goalResultNameDescription }
            />

            <DialogDeleteGoalMaster
                classes = { classes }
                isModalDeleteGoalMaster = { isModalDeleteGoalMaster }
                setModalDeleteGoalMaster = { setModalDeleteGoalMaster }
                goalDetailState = { goalDetailState }
            />

            <DialogEditGoalMaster
                classes = { classes }
                setModalEditGoalMaster = { setModalEditGoalMaster }
                isModalEditGoalMaster = { isModalEditGoalMaster }
                goalDetailState = { goalDetailState }
            />

          
        </MuiThemeProvider>
    )
};


export default withStyles(styles)(ViewGoalMoreDetail);

const listChoose = [

    {
        id: 1,
        label: 'Overview Goal',
        // to: 'overview-goal'
        to : ToGoalDetail + RouteToOverviewGoal

    },

    {
        id: 2,
        label: 'Measure Activities',
        // to:'measure-activities'
        to: ToGoalDetail + RouteToMeasureActivities

    }
];


//* 1: Total, 2: Increment