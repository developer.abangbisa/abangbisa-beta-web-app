
import React,{Component, useState, useEffect, useContext, useRef} from 'react';
import { Container, Paper, Typography, Grid, TextField, Checkbox, FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions} from '@material-ui/core';
import axios from 'axios';
import { URL_API } from '../../../../constants/config-api';

const DialogDelete = (props) => {
    
    const { 
            classes, 
            isModalDelete, 
            setModalDelete, 
            goalResultId, 
            goalDetailState,
            goalResultNameDescription, 
            collectionGoalResultList, 
            setCollectionGoalResultList 
        
    } = props;


    const [ userTokenState, setUserTokenState ] = useState('');
    
    useEffect(() => {
        
        if(isModalDelete == true){
            
            const userToken = localStorage.getItem('userToken');
            setUserTokenState(userToken)
        };
        
    }, [isModalDelete]);
    
    const handleDelete = () => {

        console.log("goalResultNameDescription : ", goalResultNameDescription);

       
        
        if(userTokenState !== '' ){
        
            const header =  {       
                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
    
            };

            axios.defaults.headers.common = header;    

            axios
                .delete(URL_API + `/swd/goal/${goalDetailState.id}/result/${goalResultId}`)
                .then(function(response){

                    console.log("Response Original : ", response);
                    setCollectionGoalResultList(collectionGoalResultList.filter(item => item.id !== goalResultId));
                    setModalDelete(false);
                  
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);
                    setModalDelete(false);
                    alert('Whoops, something went wrong !')

               
                })

        } else { console.log("No Access Token available!")};
        
    };
    
    return (
        <Dialog
            open={isModalDelete}
            onClose={() => setModalDelete(false)}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            
        >
            <DialogTitle id="alert-dialog-title" style={{textAlign: "left"}}>
              
            </DialogTitle>
            <DialogContent style={{textAlign: "center"}}>
                <DialogContentText id="alert-dialog-description">
                    <Typography variant='subtitle1' className={classes.title} style={{color: 'black'}}>
                        <b>Apakah Anda yakin ingin menghapus <i>Goal Result</i> ini ?</b>
                    </Typography>
                </DialogContentText>
            </DialogContent>
            <DialogActions style={{alignItems: "center", justifyContent:'center'}}>
                <Button 
                    onClick={handleDelete}
                    variant='contained' 
                    className={classes.buttonModalDelete}
                    // fullWidth
                >  
                    Yakin
                </Button>
            </DialogActions>
            <br />
        </Dialog>
    )
};

export default DialogDelete;
