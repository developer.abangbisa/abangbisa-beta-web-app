import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Chip
    
} from '@material-ui/core';

import axios from 'axios';
import moment from 'moment';
import clsx from 'clsx';
import {  useDebouncedCallback } from "use-debounce";


import SearchIcon from '@material-ui/icons/Search';
import DoneIcon from '@material-ui/icons/Done';

import { CircularProgressbarWithChildren, CircularProgressbar, buildStyles } from 'react-circular-progressbar';

import DialogCreate from '../Components/DialogCreate';
import AllActive from '../All/Active/AllActive';
import AllComplete from '../All/Complete/AllComplete';

// import YouActive from '../You/Active/YouActive';
// import YouComplete from '../You/Complete/YouComplete';

import LightTooltip from '../../../../components/LightTooltip';

import TruncateText from '../../../../utilities/TruncateText';
import TruncateTextShort from '../../../../utilities/TruncateTextShort';

import { URL_API } from '../../../../constants/config-api';

import AvatarDummy from '../../../../assets/images/Avatar_dummy.png';
import OrganigramGoal from '../../../../assets/images/Group_2491.png';
import PictREFRESH from '../../../../assets/images/Group_2725.png';


import 'react-circular-progressbar/dist/styles.css';
import '../Style/custom.css';

const numeral = require('numeral');

const ViewYou = props => {

    const { classes, isShowDateRangeComponent, setCollectionGoalList, collectionGoalList, loader, setLoader } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */

    const userToken = localStorage.getItem('userToken');
    const statusUserLogin = localStorage.getItem('status_user_login');
    const statusUserLoginAfterParse = JSON.parse(statusUserLogin);

    const [ userTokenState, setUserTokenState ] = useState('');

    const [ fotoQuery, setFotoQuery ] = useState('');
    const [ userLoginName, setUserLoginName ] = useState('');
    const [ memberPositionList, setMemberPositionList ] = useState([]);

    // const [ loader, setLoader ] = useState(false);

    useEffect(() => {

        setUserTokenState(userToken);
        setUserLoginName(statusUserLoginAfterParse.member_first_name + " " + statusUserLoginAfterParse.member_last_name)
        setFotoQuery(statusUserLoginAfterParse.member_photo_url);
        setMemberPositionList(statusUserLoginAfterParse.member_position);

      },[])

    /*
        `````````````````
        CIRCULAR PROGRESS
        
        `````````````````        
    */
    const percentage = 66;

    
    /*
        ```````````````````
        HANDLE DIALOG MODAL CREATE 
        
        ```````````````````      
    */
    const [ isModalCreate, setModalCreate ] = useState(false);

    const handleDialogCreate = () => {

        setModalCreate(true)

    };

    /*
        ````````````````````````````
        HANDLE TAB ACTIVE & COMPLETE
        
        ````````````````````````````
    */
    const [ isYouActive, setYouActive ] = useState(true); 
    const [ isYouComplete, setYouComplete ] = useState(false);

    const [ dataActiveOrComplete, setDataActiveOrComplete] = useState(
        {
            id: 1,
            label: 'Active'
        }
    );

    const handleClickListChoose = (e, data) => {

        e.preventDefault();

        console.log("Data : ", data);
        setDataActiveOrComplete(data);

        if(data.id == 1){
            
            setYouActive(true);
            setYouComplete(false);

        } else {

            setYouComplete(true);
            setYouActive(false);

        }
    };

    /*
        ```````````````````````````
        HANDLE REFRES LIST ALL GOAL
        
        ```````````````````````````
    */
    const handleRefresh = () => {

        setLoader(true );

        if(userTokenState !== undefined ){
        
            const header =  {   

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userTokenState,
    
            };

            axios.defaults.headers.common = header;    

            //* GOL SELF - ONLY OWNER GOAL USER LOGIN
            axios
                .get(URL_API + `/swd/goal`)
                .then(function(response){

                    setLoader(false);
                    console.log("Response Original : ", response);

                    if(response.status == 200){

                        if(response.data.data !== null){
                            if(response.data.data.length > 0){
                                
                                setCollectionGoalList(response.data.data);
                                // setCollectionGoalListDummy(response.data.data);


                            }
                        };
                    };
                })
                .catch(function(error){

                    setLoader(false);
                    console.log("Error : ", error.response);

                })

        } else { console.log("No Access Token available!")};
    }


    /*
        ```````````````````
        Search  GOAL

        ``````````````````
    */
    const [ handleSearch, cancel ] = useDebouncedCallback(
            
        useCallback( value => {

            setLoader(true)

            console.log("I want to know goal : ", value);

            if(userToken !== undefined ){
            
                const header =  {   

                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
        
                };

                axios.defaults.headers.common = header;    

                axios
                    .get(URL_API + `/swd/goal?options[filter][search]=${value}`)
                    .then(function(response){

                        setLoader(false);
                        console.log("Response Original Goal : ", response);

                        if(response.status == 200){    
                            if(response.data.data !== null){
                                setCollectionGoalList(response.data.data);

                            };
                        };
                    })
                    .catch(function(error){
                        
                        setLoader(false);
                        console.log("Error : ", error.response);

                    })

            } else { console.log("No Access Token available!")};

        
        }, []), 1000,

        { maxWait: 5000 }
    );

    return (
        <Fragment>
           <Grid container>
                <br />
                <br />
                <br />
                <Grid item sm={12}>
                    <List className={classes.layoutMarginLeftList}>
                        <ListItem style={{paddingLeft: 0, marginRight: 24}}>
                            
                        <IconButton
                            aria-haspopup="true"
                            color="inherit"
                            
                        >                        
                            {
                                fotoQuery !== '' ? (
                                                                
                                    // <img src={URL_API + '/' +fotoQuery+ "&token=" + userToken}  className={classes.userRealFoto} />
                                    <img src={URL_API + '/my-profile/photo' + "?token=" + userToken}  className={classes.userRealFoto} />


                                ) : (

                                    <img src={AvatarDummy} alt="Gear Picture" className={classes.imageAvatar} />

                                )
                            }
                        </IconButton>
                            &nbsp;&nbsp;

                            <ListItemText 
                                className={classes.listItemtext}
                                primary = {

                                    <Typography variant='subtitle2' className={classes.title}>
                                        <b>{userLoginName}</b>
                                    </Typography>
                                } 
                                secondary={

                                    memberPositionList.length > 0 && memberPositionList.map((item, i) => (

                                        <Chip  
                                            // icon={<DoneIcon style={{color: 'white'}} />}
                                            label={item.structure_position_title_name} 
                                            style={{backgroundColor: '#f4f7fc', color: 'grey', fontFamily: 'Nunito'}}
                                            size='small'
                                            
                                        />
                                    ))
                                } 
                            />
                        
                        </ListItem>
                    </List>
                </Grid>
            </Grid>
            
            {
                isShowDateRangeComponent == false && (

                    <Grid container>


                        {
                            listChoose.length > 0 && listChoose.map((item, i) => {

                                return (
                                    <Fragment key={i}>
                                          <Grid item sm={1}>
                                                <Typography 
                                                    variant='subtitle2' 
                                                    className={
                                                        dataActiveOrComplete.id == item.id ?
                                                            clsx(classes.titleListChoose, classes.layoutMarginLeftList) : 
                                                                clsx(classes.titleListChooseAktif, classes.layoutMarginLeftList)
                                                    } 
                                                    // onClick={() => setYouActive(true)}
                                                    onClick={(e) => handleClickListChoose(e, item)}
                                                >
                                                    <b>{item.label}</b>
                                                </Typography>
                                            </Grid>
                                    </Fragment>
                                )
                            })
                        }
                        
                        <Grid item sm={7} style={{textAlign: 'right'}}>

                            <IconButton
                                onClick={handleRefresh}
                            >
                                <img src={PictREFRESH} style={{width: 17, height: 17}} />
                            </IconButton>

                            <IconButton>
                                <i className="material-icons" style={{color: '#4aa9fb'}}>
                                    view_headline   
                                </i>
                            </IconButton>

                            <IconButton>
                                <img src={OrganigramGoal} style={{width: 16, height: 16}} />
                            </IconButton>

                        </Grid>
                        <Grid item sm={3}>
                            <div className={classes.search}>
                                <div className={classes.searchIcon}>
                                    <SearchIcon />
                                </div>
                                <InputBase
                                
                                    onChange={(e) => handleSearch(e.target.value)}
                                    placeholder="Search Goal..."
                                    classes={{

                                        root: classes.inputRoot,
                                        input: classes.inputInput
                                    }}

                                    inputProps={{ 'aria-label': 'Search'}}
                                />
                            </div>
                        </Grid>
                    </Grid>
                )
            }

            <Grid container  style={{backgroundColor: '#f4f7fc'}}>
                <Grid item xs={12} style={{textAlign: 'right'}}>

                    {
                        isYouActive == true && (

                            <Button 
                                // onClick={handleDelete}
                                onClick = { () => setModalCreate(true)}
                                variant='contained' 
                                className={classes.button}
                                style={{marginRight: 24, marginBottom: 16, marginTop: 16}}
                            >  
                                Buat Goal baru
                            </Button>
                        )
                    }
                </Grid>
            </Grid>

            {
                isYouActive == true ? (

                    <AllActive 

                        classes = { classes }
                        isModalCreate = { isModalCreate }
                        setModalCreate = { setModalCreate }
                        fotoQuery = { fotoQuery}
                        userLoginName = { userLoginName }
                        memberPositionList = { memberPositionList }
                        collectionGoalList = { collectionGoalList }
                        setCollectionGoalList = { setCollectionGoalList }
                        loader = { loader}
                    />
                ) : null
            }

            {
                isYouComplete == true ? (
                    
                    <AllComplete 
                        classes = {classes }
                        isModalCreate = { isModalCreate }

                        setModalCreate = { setModalCreate } 
                        fotoQuery = { fotoQuery } 
                        userLoginName = { userLoginName } 
                        memberPositionList = { memberPositionList } 
                        collectionGoalList = { collectionGoalList } 
                        setCollectionGoalList = { setCollectionGoalList } 
                        loader = { loader }
                    />

                ) : null
            }
           
        </Fragment>
    )
};

export default ViewYou;

const listChoose = [
    {
        id: 1,
        label: 'Active'
    },

    {
        id: 2,
        label: 'Complete'
    },
]