import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Chip
    
} from '@material-ui/core';

import moment from 'moment';
import clsx from 'clsx';


import { CircularProgressbarWithChildren, CircularProgressbar, buildStyles } from 'react-circular-progressbar';

import DoneIcon from '@material-ui/icons/Done';

import DialogCreate from '../../Components/DialogCreate';


import LightTooltip from '../../../../../components/LightTooltip';

import TruncateText from '../../../../../utilities/TruncateText';
import TruncateTextShort from '../../../../../utilities/TruncateTextShort';
import Redirect from '../../../../../utilities/Redirect';
import { ToGoalDetailALL } from '../../../../../constants/config-redirect-url';

import { URL_API } from '../../../../../constants/config-api';
import AvatarDummy from '../../../../../assets/images/Avatar_dummy.png';
import OrganigramGoal from '../../../../../assets/images/Group_2491.png';

const numeral = require('numeral');


const AllActive = props => {

    // const { classes, isShowDateRangeComponent, setCollectionGoalList, collectionGoalList } = props;
    const { classes, isModalCreate, setModalCreate, fotoQuery, userLoginName, memberPositionList, collectionGoalList, setCollectionGoalList, loader } = props;

        /*  
        ``````````````````
        HANDLE GOAL DETAIL

        ``````````````````
    */
    const handleDetail = (e, data) => {

        e.preventDefault();
        console.log(data);

        localStorage.setItem('goal_detail', JSON.stringify(data) )
        Redirect(ToGoalDetailALL);

    };

    return(

        <Fragment>
            <Grid container style={{backgroundColor: '#f4f7fc'}}>
                <Grid item xs={12} >

                    {
                        loader == true && (

                            <Grid  
                                container
                                spacing={0}
                                direction="row"
                                justify="center"
                                alignItems="center"
                            >
                                <CircularProgress size={32} style={{marginTop: 72, color: 'red'}} />

                            </Grid>
                        )
                    }

                    {
                        collectionGoalList.length == 0 && loader == false && (

                            (
                            
                                <Paper elevation={1} className={classes.paperList}>
                                    <Grid container>
                                        <Grid item xs={12} style={{textAlign: 'center'}}>
                                            <Typography variant='subtitle1' className={classes.title} style={{color: 'grey', marginTop: 16, marginBottom: 16}}>
                                                <b>Tidak ada Goal yang dapat di tampilkan saat ini ;(</b>
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            )
                        )
                    }

                    {
                        collectionGoalList.length > 0 && collectionGoalList.map((item,i) => {

                            
                            // if(moment(new Date()).isAfter(item.end_date)){//* Condition ketika End Date sudah terlewati !
                            
                            //     console.log(item.name);
                            // };
                            
                            if(item.status_id == 2 || item.status_id == 3 ){
                                
                                console.log("Log from ALL Active : ", item);//*Condition status id is NOT WORK !

                                return(
    
                                    <Paper 
                                        elevation={1} 
                                        className={
                                            
                                            item.calculatedValue.gap_value_percent > item.calculatedValue.result_value_percent ||
                                            item.calculatedValue.expected_value_percent > item.calculatedValue.result_value_percent
                                            ? 
    
                                                classes.paperListKebakaran : classes.paperList
                                        
                                        } 
                                        key={i} 
                                        style={{marginBottom: 16}}>
                                        <Grid container alignItems="flex-end">
    
                                            <div className={classes.circularProgressBar}>


                                               {/* 
                                                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                                                    - INI SCENARIO KALAU ANTARA "GAP" LEBIH BESAR DARI "GOAL RESULT" ==> MERAH untuk "trail GOAL RESULT/Pencapaiannya" 
                                                    
                                                    - INI SCENARIO KALAU "POSISI AMAN" LEBIH BESAR DARI "GOAL RESULT" ==> YELLOW untuk "trail GAP-nya"
                                                    
                                                    `````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                                                    
                                                */}

{
                                                    item.calculatedValue.expected_value_percent > item.calculatedValue.result_value_percent && (

                                                        <CircularProgressbarWithChildren
                                                            value={numeral(item.calculatedValue.expected_value_percent).format('0,0')}
                                                            // value={5}
                                                            styles= {

                                                                    buildStyles({
                                                                        pathColor: 'yellow',  
                                                                        trailColor: '#eee',
                                                                        strokeLinecap: 'butt',//butt
                                                                    })
                                                            }
                                                        >
                                                            <CircularProgressbar
                                                                value={numeral(item.calculatedValue.result_value_percent).format('0,0')} 
                                                                // value={20} 
                                                                styles = {

                                                                    item.calculatedValue.gap_value_percent > item.calculatedValue.result_value_percent ||
                                                                    item.calculatedValue.expected_value_percent > item.calculatedValue.result_value_percent
                                                                    ?

                                                                        buildStyles({

                                                                            pathColor:'red',                                                             
                                                                            trailColor: 'transparent',
                                                                            strokeLinecap: 'butt',
                                                                            textColor: 'black'
                                                                        }) : 

                                                                            buildStyles({

                                                                                pathColor:'rgba(61, 255, 41, 0.87)', //*Green                                                          
                                                                                trailColor: 'transparent',
                                                                                strokeLinecap: 'butt',
                                                                                textColor: 'black',
                                                                            })
                                                                }

                                                                text={`${numeral(item.calculatedValue.result_value_percent).format('0,0')}%`}
                                                            />
                                                        </CircularProgressbarWithChildren>
                                                    )
                                                }


                                                 {/* 
                                                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````
                                                    
                                                    - INI SCENARIO KALAU "GOAL RESULT" LEBIH BESAR DARI "POSISI AMAN" ==> GREEN untuk "trail GOAL RESULT-nya" 

                                                        (Warna GREEN mendahului warna YELLOW GAP)
                                                    
                                                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````
                                                    
                                                */}
                                                
                                                    {

                                                        item.calculatedValue.result_value_percent > item.calculatedValue.expected_value_percent ||
                                                        item.calculatedValue.result_value_percent == item.calculatedValue.expected_value_percent  ? (
                                                            // item.calculatedValue.result_value_percent > item.calculatedValue.expected_value_percent ? (

                                                            <CircularProgressbarWithChildren
                                                                value = { numeral(item.calculatedValue.result_value_percent).format('0,0') }
                                                                // value = { numeral(item.calculatedValue.expected_value_percent).format('0,0') }

                                                                styles= {

                                                                        buildStyles({
                                                                            pathColor: 'rgba(61, 255, 41, 0.87)', 
                                                                            // pathColor:'yellow',    
                                                                            trailColor: '#eee',
                                                                            strokeLinecap: 'butt',//butt
                                                                        })
                                                                }
                                                            >

                                                                <CircularProgressbar
                                                                    value={numeral(item.calculatedValue.expected_value_percent).format('0,0')} 
                                                                    // value={numeral(item.calculatedValue.result_value_percent).format('0,0')} 
                                                                    styles = {

                                                                        // item.calculatedValue.result_value_percent > item.calculatedValue.expected_value_percent ||
                                                                        // item.calculatedValue.result_value_percent == item.calculatedValue.expected_value_percent  ? 

                                                                        

                                                                        //     buildStyles({

                                                                        //         pathColor:'rgba(61, 255, 41, 0.87)',                                                             
                                                                        //         trailColor: 'transparent',
                                                                        //         strokeLinecap: 'butt',
                                                                        //         textColor: 'black'
                                                                        //     }) : 

                                                                        //         buildStyles({

                                                                        //             pathColor:'red', //*Green                                                          
                                                                        //             trailColor: 'transparent',
                                                                        //             strokeLinecap: 'butt',
                                                                        //             textColor: 'black',
                                                                        //         })

                                                                        buildStyles({

                                                                            pathColor:'yellow',   
                                                                            // pathColor: 'rgba(61, 255, 41, 0.87)',                                                           
                                                                            trailColor: 'transparent',
                                                                            strokeLinecap: 'butt',
                                                                            textColor: 'black'
                                                                        })                                                                     
                                                                    }

                                                                    text={`${numeral(item.calculatedValue.result_value_percent).format('0,0')}%`}
                                                                />

                                                            </CircularProgressbarWithChildren>
                                                        
                                                        ) : null
                                                        
                                                        // (

                                                        //     <CircularProgressbarWithChildren
                                                        //         value = { numeral(item.calculatedValue.result_value_percent).format('0,0') }
                                                        //         styles= {

                                                        //                 buildStyles({
                                                        //                     pathColor: 'red',     
                                                        //                     trailColor: '#eee',
                                                        //                     strokeLinecap: 'butt',//butt
                                                        //                 })
                                                        //         }
                                                        //     >

                                                        //         <CircularProgressbar
                                                        //             value={numeral(item.calculatedValue.gap_value_percent).format('0,0')} 
                                                        //             styles = {

                                                        //                 buildStyles({

                                                        //                     // pathColor:'yellow',   
                                                        //                     pathColor: 'rgba(61, 255, 41, 0.87)',                                                           
                                                        //                     trailColor: 'transparent',
                                                        //                     strokeLinecap: 'butt',
                                                        //                     textColor: 'black'
                                                        //                 })                                                                     
                                                        //             }

                                                        //             text={`${numeral(item.calculatedValue.result_value_percent).format('0,0')}%`}
                                                        //         />

                                                        //     </CircularProgressbarWithChildren>

                                                        // )
                                                    }

                                            </div>
                                    
                                            <List className={classes.layoutMarginLeftList} style={{width: 320}}>
                                                <ListItem style={{paddingLeft: 0, marginRight: 24}}>
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                <b>{item.name }</b>
                                                            </Typography>
                                                        } 
                                                        secondary={
                                                            
                                                            <Typography variant='subtitle2' className={classes.title}>
    
                                                                {// *PERIOD
                                                                    item.period !== null && (
                                                                        item.period.name
                                                                    )
                                                                }
        
                                                                {// *DATE RANGE
                                                                    item.start_date !== null && item.end_date !== null && item.period == null && (
                                                                        <LightTooltip title={moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY')} placement="right-start">
                                                                            <span>
                                                                                {/* {TruncateTextShort(moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY'))} */}
                                                                                {moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY')}

                                                                            </span>
                                                                        </LightTooltip>
                                                                    )
                                                                }
    
                                                                {// *DUE DATE
    
                                                                    item.start_date == null && item.end_date !== null && item.period == null && (
                                                                    
                                                                        moment(item.end_date).format('DD MMMM YYYY')
                                                                        
                                                                    )
    
                                                                }
                                                            </Typography>
                                                        } 
                                                    />
                                                
                                                </ListItem>
                                            </List>
                
                                            <List>
                                                <ListItem 
                                                    style={{
                                                        marginRight: 24, 
                                                        // marginBottom: 8, 
                                                        paddingLeft: 0,
                                                        // borderLeft: '0.1em solid #c3c0c0de',
                                                    }}
                                                >
                                                    <ListItemIcon 
                                                        style={{
                                                            marginLeft: 8
                                                            
                                                        }}
                                                    >
                                                        <IconButton style={{backgroundColor: '#dbdbdb', padding: '7px'}}>
                                                            {/* <img src={AvatarDummy} style={{width: 48, height: 40 }} /> */}
                                                            <i className="material-icons" style={{color: '#c4c5c4'}}>
                                                                person
                                                            </i>
                                                        </IconButton>   
                                                    </ListItemIcon>
                                                    
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                                                                // style={{marginRight: 32}}
                                                            >
                                                                <b>
                                                                    {item.owner !== null ? item.owner.member_first_name : '-'} 
                                                                    {item.owner !== null ? item.owner.member_last_name : '-'}
                                                                </b>
                                                                {/* <b>{TruncateTextShort(item.owner.member_first_name + " " + item.owner.member_last_name)}</b> */}
    
                                                            </Typography>
                                                        } 
                                                        secondary={
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                {item.owner ? item.owner.structure_position_title_name : '-'}
                                                            </Typography>
                                                        } 
                                                    />
                                                
                                                </ListItem>
                                            </List>
                
                                            <List>
                                                <ListItem 
                                                    style={{
                                                        marginRight: 24, 
                                                        paddingLeft: 0,
                                                        // borderLeft: '0.1em solid #c3c0c0de',
                                                    }}
                                                >
                
                                                    <ListItemIcon style={{marginLeft: 8}}>
                                                        <IconButton style={{backgroundColor: '#bdffde', padding: '8px'}}>
                                                            <DoneIcon style={{color: '#36d686', fontSize: 21}} />
                                                        </IconButton>
                                                    </ListItemIcon>
                                                    
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                
                                                            >
                                                                <b>Posisi aman</b>
                                                            </Typography>
                                                        } 
                
                                                        secondary={
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                {numeral(item.calculatedValue.expected_value_percent).format('0,0')}%
                                                            </Typography>
                                                        } 
                                                    
                                                    />
                                                </ListItem>
                                            </List>
                
                                           
                                            <List className={classes.layoutMarginLeftList}>
                                                <ListItem alignItems='center' style={{paddingLeft: 0, marginRight: 24}}>
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        // primary = {
                                                        //     <LightTooltip title={`Terakhir update ${moment(item.created_at).format('DD MMMM YYYY h:mm:ss a')}`} placement="left-start">
                                                        //         <Typography  variant='subtitle2' className={classes.title}>
                                                        //             {TruncateText(`Terakhir update ${moment(item.created_at).format('DD MMMM YYYY h:mm:ss a')}`)}
                                                        //         </Typography>
                                                        //     </LightTooltip>
                                                        // } 
                                                        primary = {
    
                                                            <LightTooltip title='Klik di sini ! Untuk melihat lebih detail Goal ini' placement="left-start">
                                                                {/* 
                                                                    <Typography 
                                                                        variant='subtitle2' 
                                                                        className={classes.title} 
                                                                        style={{cursor: 'pointer', marginBottom: 16, color: 'grey'}}
                                                                        onClick={(e) => handleDetail(e, item)}
                                                                    >
                                                                        <b>More Detail</b>
                                                                    </Typography> 
                                                                */}
                                                                <Button 
                                                                    onClick={(e) => handleDetail(e, item)}                                                                    
                                                                    variant='outlined' 
                                                                    size='small' 
                                                                    className={classes.moreDetail}
                                                                    style={{cursor: 'pointer', marginBottom: 8, color: 'grey'}}

                                                                >
                                                                    <b>More Detail</b>
                                                                </Button>
                                                            </LightTooltip>
                                                        } 
                                                    />
                                                </ListItem>
                                            </List>
                                        
                                        </Grid>
                
                                    </Paper>
                                );

                            }



                        }) 
                    }

                </Grid>
            </Grid>
            
            <DialogCreate 
                classes = { classes }
                isModalCreate = { isModalCreate }
                setModalCreate = { setModalCreate }
                fotoQuery = { fotoQuery}
                userLoginName = { userLoginName }
                memberPositionList = { memberPositionList }
                setCollectionGoalList = { setCollectionGoalList}
                collectionGoalList = {collectionGoalList }
            />
        </Fragment>
    )

};

export default AllActive;