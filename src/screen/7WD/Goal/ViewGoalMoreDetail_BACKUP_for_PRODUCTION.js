import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider, fade} from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, ListItemSecondaryAction, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, AppBar, Toolbar, Chip, Collapse
    
} from '@material-ui/core';

import axios from 'axios'
import moment from 'moment';
import clsx from 'clsx';

import { CircularProgressbarWithChildren, buildStyles, CircularProgressbar } from 'react-circular-progressbar';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import DotLineChart from './Components/DotLineChart';
import { StyledMenu, StyledMenuItem } from '../../../components/StyledMenuDropdown';
import CircleGoalDetail from '../../../components/CircleGoalDetail';

import TextOverDotLineChart from './Components/TextOverDotLineChart';
import TextOverDotLineChartSecond from './Components/TextOverDotLineChartSecond';

import DialogCreate from './ComponentsGoalResult/DialogCreate';
import DialogEdit from './ComponentsGoalResult/DialogEdit';
import DialogDelete from './ComponentsGoalResult/DialogDelete';
import DialogDeleteGoalMaster from './Components/DialogDelete';
import DialogEditGoalMaster from './Components/DialogEdit';

import Redirect from '../../../utilities/Redirect';
import { ToGoal } from '../../../constants/config-redirect-url';
import { URL_API } from '../../../constants/config-api';

import AvatarDummy from '../../../assets/images/Avatar_dummy.png';
import PictResultValue from '../../../assets/images/Group_2620.png';
import PictGAP from '../../../assets/images/Group_2619.png';
import PictDone from '../../../assets/images/Group_2614.png';

const numeral = require('numeral');

const theme = createMuiTheme({
  
    palette: {

        primary: {
            
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }

});

const styles = theme => ({

    root: {
      
        width: '100%',
        marginTop: 1 * 3
  
    },
    layoutMarginLeftAndTop: {

        marginLeft: 72, 
        marginTop: 56
    },
    layoutMarginTop: {

        // marginLeft: 72, 
        marginTop: 56
    },
    layoutMarginTopNew: {

        marginTop: theme.spacing(10)
    },
    layoutMarginLeftList: {

        marginLeft: theme.spacing(4), 
        marginTop: theme.spacing(1)
      
    },
    title: {
        fontFamily: 'Nunito'
    },
    titleInLineChart: {
        fontFamily: 'Nunito',
        marginLeft: theme.spacing(4), 
        marginTop: theme.spacing(4)

    },

    button: {

        // width: '503px',
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        // marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'

    },
    buttonDisabled: {
        fontFamily:'Nunito',
        textTransform: 'capitalize',
        color: 'grey',
        fontWeight: 'bold'


    },
    timeFrameIcontDateRangeText: {
        fontFamily: 'Nunito',
        color: '#4aa9fb',
        textTransform: 'capitalize',
        backgroundColor: '#edf6ff'
    },

    /*
        ```````````
        DIALOG EDIT

        ```````````
    */
    titleListChoose: {

        fontFamily: 'Nunito',
        cursor: 'pointer'

    },
    titleListChooseAktif: {

        fontFamily: 'Nunito',
        color: '#d6d3d3', 
        cursor: 'pointer'
    },

    timeFrameIcon: {
        fontFamily: 'Nunito',
        color: 'grey', //#4aa9fb
        textTransform: 'capitalize',
        // marginRight: theme.spacing(4),
        // marginTop: theme.spacing(9),
        // backgroundColor: '#cfe8fd'
    },

    timeFrameIconInModal: {

        fontFamily: 'Nunito',
        color: 'grey',
        textTransform: 'capitalize',
        '&:hover': {
            color: '#edcfd8',//#edcfd8, #cc0707
        },
        borderWidth: 0
    },

    /*
        ****************************** 
        ******************************         
        ****************************** 
    */
    
    paperColumPertamaRowFirst: {
        marginLeft: theme.spacing(11),
        padding: theme.spacing(0.2),

    },
    paperColumPertamaRowFirstKebakaran: {
        marginLeft: theme.spacing(11),
        padding: theme.spacing(0.2),
        background: '#edcfd8',
        borderLeft: '0.5em solid red'

    },
    paperColumnDuaRowFirst: {

        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        padding: theme.spacing(0.2),
    },
    
    /*
        `````````````````````````````
        FOTO REAL USER NAME, USERNAME

        `````````````````````````````
    */
    userRealFoto: {

        // margin: 10,
        width: 48,
        height: 48,
        borderRadius: 24
    },
    imageAvatar: {

        width: 50,
        height: 40
    },

    /*
        ````````
        CIRCULAR

        ````````
    */
    circularProgressBarInMoreDetail : {
        
        // width: 60, 
        // height: 60,
        width: 100, 
        height: 100,
        
    },
    circularProgressBarInPaperDetail : {
        
        width: 60, 
        height: 60,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(2)
    },

    buttonModalDelete: {

        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        marginRight: theme.spacing(4),
        color: 'white'
    },


    /*
        ``````````
        MODAL EDIT 

        ``````````
    */
    textField: {
        
        minWidth: 425   
    }
});

const ViewGoalMoreDetail = props => {

    const { classes } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    const userToken = localStorage.getItem('userToken');
    const statusUserLogin = localStorage.getItem('status_user_login');
    const statusUserLoginAfterParse = JSON.parse(statusUserLogin);

    const [ userTokenState, setUserTokenState ] = useState('');

    const [ fotoQuery, setFotoQuery ] = useState('');
    const [ userLoginName, setUserLoginName ] = useState('');
    const [ memberPositionList, setMemberPositionList ] = useState([]);

    const goalDetail = localStorage.getItem('goal_detail');
    const goalDetailAfterParse = JSON.parse(goalDetail);
    const [ goalDetailState, setGoalDetailState ] = useState(

        {

            id: null,
            period: {
                id: null,
                name: null,
                start_date: null,
                end_date: null
            },
            start_date: null,
            end_date: null,

            calculatedValue: {

                result_value_percent: null,
                gap_value_percent: null,
                result_value: null,
                gap_value_percent: null,
                expected_value_percent: null
            },
            name: null,
            owner : {

                structure_unit_type_name: null,
                structure_unit_name : null

            },
            calculatedValue: {

                target_value: null,

            },
            calculationMethod: {

                id: null
            },
            time: {
                remaining_days: null
            },
            start_value: null,
            status_id: null
        }
    );

    const [ collectionGoalResultList, setCollectionGoalResultList ] = useState([]);

    const [ totalDaysState, setTotalDaysState ] = useState(null);
    const [ dataCircleInPaperSecond, setDataCircleInPaperSecond ] = useState(null);

    useEffect(() => {

        setUserTokenState(userToken);
        setUserLoginName(statusUserLoginAfterParse.member_first_name + " " + statusUserLoginAfterParse.member_last_name)
        setFotoQuery(statusUserLoginAfterParse.member_photo_url);
        setMemberPositionList(statusUserLoginAfterParse.member_position);

        if(userToken !== undefined ){
        
            const header =  {   

                'Accept': "application/json",
                'Content-Type' : "application/json",
                'Authorization' : "bearer " + userToken,
    
            };

            axios.defaults.headers.common = header;    

            //*GOL RESULT LIST SELF - ONLY OWNER GOAL USER LOGIN
            axios
                .get(URL_API + `/swd/goal/${goalDetailAfterParse.id}/result`)
                .then(function(response){

                    console.log("Response Original : ", response);

                    if(response.status == 200){
                        if(response.data.data !== null){

                            setCollectionGoalResultList(response.data.data);

                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);

                });

            //*GOL DETAIL
            axios
                .get(URL_API + `/swd/goal/${goalDetailAfterParse.id}`)
                .then(function(response){

                    console.log("Response Original GOAL DETAIL : ", response);

                    if(response.status == 200){
                        if(response.data.data !== null){

                            //*Get foto
                            if(response.data.data.member !== null){
                                if(response.data.data.member.self.rel !== null){
                                    if(response.data.data.member.self.rel.photo_url !== null){

                                        setFotoQuery(response.data.data.member.self.rel.photo_url )
                                    };
                                };
                            };

                            // setCollectionGoalResultList(response.data.data);
                            setGoalDetailState(response.data.data);

                        };
                    };
                })
                .catch(function(error){
                    
                    console.log("Error : ", error.response);

                })

        } else { console.log("No Access Token available!")};

    },[]);

    /*
        ```````````````````````````````````````
        HANDLE LIST GOAL RESULT KALAU BERUBAH !

        ```````````````````````````````````````
    */

    // useEffect(() => {


    //     if(userToken !== undefined ){
        
    //         const header =  {   

    //             'Accept': "application/json",
    //             'Content-Type' : "application/json",
    //             'Authorization' : "bearer " + userToken,

    //         };

    //         axios.defaults.headers.common = header;    

    //     } else { console.log("No Access Token available!")};

    // },[collectionGoalResultList]);

    /*
        ``````````````````````````````````````````````````
        HANDLE UPDATE GOAL DETAIL AFTER CREATE GOAL RESULT

        ``````````````````````````````````````````````````
    */

    const [ isFireGoalDetail, setFireGoalDetail ] = useState(false);

    useEffect(() => {
        
        if(isFireGoalDetail == true ){

            if(userTokenState !== undefined ){
        
                const header =  {   
    
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userTokenState,
        
                };
    
                axios.defaults.headers.common = header;    
    
                axios
                    .get(URL_API + `/swd/goal/${goalDetailAfterParse.id}`)
                    .then(function(response){
    
                        console.log("Response Original GOAL DETAIL OTOMATIS UPDATE: ", response);
    
                        if(response.status == 200){
                            if(response.data.data !== null){
    
                                setGoalDetailState(response.data.data);
    
                            };
                        };
                    })
                    .catch(function(error){
                        
                        console.log("Error : ", error.response);
    
                    });

                //*GOL RESULT LIST SELF - ONLY OWNER GOAL USER LOGIN
                axios
                    .get(URL_API + `/swd/goal/${goalDetailAfterParse.id}/result`)
                    .then(function(response){

                        console.log("Response Original : ", response);

                        if(response.status == 200){
                            if(response.data.data !== null){

                                setCollectionGoalResultList(response.data.data);

                            };
                        };
                    })
                    .catch(function(error){
                        
                        console.log("Error : ", error.response);

                    });
    
            } else { console.log("No Access Token available!")};

        }


    },[isFireGoalDetail])

    /*
        `````````````````````````
        DROPDOWN GOAL RESULT LIST
        `````````````````````````
    */

    const [goalResultId, setGoalResultId] = useState('');
    const [goalResultNameDescription, setGoalResultNameDescription] = useState('');
    const [goalResultDataDetail, setGoalResultDataDetail] = useState('');


    const [anchorElListGoalResult, setAnchorElListGoalResult] = useState(null);

    const handleDropdownOpenGoalResult = (event, item) => {

        event.preventDefault();
        setAnchorElListGoalResult(event.currentTarget);
        
        console.log('handleDropdownOpenGoalResult : ', item);


        setGoalResultId(item.id);
        setGoalResultNameDescription(item.description)
        
        setGoalResultDataDetail(item)
    };

    function handleDropdownCloseGoalResult() {

        setAnchorElListGoalResult(null);
    };

    /*
        ```````````````````
        DROPDOWN TIME FRAME
        ```````````````````
    */
    const [anchorEl, setAnchorEl] = useState(null);

    const [ textValueTimeFrame, setTextValueTimeFrame ] = useState('Time Frame');

    function handleDropdownOpen(event) {

        setAnchorEl(event.currentTarget);

        // console.log('handleDropdownOpen : ', item)
    };
    
    function handleDropdownClose() {

        setAnchorEl(null);
    };

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL DELETE GOAL MASTER

        ``````````````````````````````````````
    */
    const [ isModalDeleteGoalMaster, setModalDeleteGoalMaster ] = useState(false);
    const handleDialogDeleteGoalMaster = () => {

        handleDropdownClose();
        setModalDeleteGoalMaster(true);
    };

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL EDIT GOAL MASTER

        ``````````````````````````````````````
    */

    const [ isModalEditGoalMaster,  setModalEditGoalMaster ] = useState(false);
    
    const handleDialogEditGoalMaster = () => {

        handleDropdownClose();
        setModalEditGoalMaster(true);
    };

    /*
        ```````````````
        HANDLE COLLAPSE

        ```````````````
    */
    const [openCollapse, setOpenCollapse] = useState(true);

    function handleCollapse() {

        setOpenCollapse(!openCollapse);
    };

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL CREATE GOAL RESULT

        ``````````````````````````````````````
    */
    const [ isModalCreate, setModalCreate ] = useState(false);

    const handleDialogModalGoalResult = () => {

        setModalCreate(true);
        setOpenCollapse(true);
    };    

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL DELETE GOAL RESULT

        ``````````````````````````````````````
    */
    const [ isModalDelete, setModalDelete ] = useState(false);

    const handleDialogDelete = (e, data) => {

        e.preventDefault();

        handleDropdownCloseGoalResult()
        setModalDelete(true);
        // console.log("handleDialogDelete : ", data.description);

    };

    /*
        ``````````````````````````````````````
        HANDLE DIALOG MODAL EDIT GOAL RESULT

        ``````````````````````````````````````
    */
    const [ isModalEdit, setModalEdit ] = useState(false);

    const handleDialogEdit = (e, data) => {
       
        e.preventDefault();

        handleDropdownCloseGoalResult()        
        setModalEdit(true);

    };

    return (

        <MuiThemeProvider theme={theme} >
            <Paper 
                elevation={0} 
                square 
                className={classes.layoutMarginLeftAndTop}
            >
                <Grid container >
                    <Grid item sm={6} md={6} style={{textAlign: 'left'}} >  
                        <Typography variant='subtitle2' className={classes.title} style={{marginTop: 16, marginLeft: 8, color: 'grey'}} >
                            <Tooltip title='Kembali untuk melihat semua Goal' placement="right">
                                <IconButton onClick={() => Redirect(ToGoal)}>
                                    <i className='material-icons' style={{ cursor: 'pointer'}}>
                                        keyboard_backspace
                                    </i>
                                </IconButton>    
                            </Tooltip>
                        </Typography>
                    </Grid>

                    <Grid item sm={3} md={3} style={{textAlign: 'right'}} >

                         {  
                            //*PERIOD
                            goalDetailState.period !== null && (
                                
                                <Fragment>
                                    <Chip  
                                        label={ goalDetailState.period.name}
                                        style={{backgroundColor: '#f4f7fc', color: 'grey', fontFamily: 'Nunito', marginTop: 24, }}
                                        size='small'
                                    />


                                </Fragment>
                            )
                        }
                    </Grid>

                    <Grid item sm={3} md={3} style={{textAlign: 'left'}} >

                         {//*DATE RANGE
                            goalDetailState.start_date !== null && goalDetailState.end_date !== null && goalDetailState.period == null && (
                                    
                                <Button 
                                    style={{marginTop: 24,borderWidth: 0, marginLeft : 16}}
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIcontDateRangeText}
                                >
                                    <b>{ moment(goalDetailState.start_date).format('DD MMMM YYYY') + " - " + moment(goalDetailState.end_date).format('DD MMMM YYYY') }</b>
                                </Button>
                                
                            )
                        }

                        {// *DUE DATE

                            goalDetailState.start_date == null && goalDetailState.end_date !== null && goalDetailState.period == null && (
                                
                                <Button 
                                    style={{marginTop: 24,borderWidth: 0, marginLeft : 16}}
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIcontDateRangeText}
                                >
                                    <b>{moment(goalDetailState.end_date).format('DD MMMM YYYY')}</b>
                                </Button>
                                
                                
                            )
                        }

                          {// *PERIOD

                            goalDetailState.period !== null  && (
                                                            
                                <Button 
                                    style={{marginTop: 24,borderWidth: 0, marginLeft : 16}}
                                    variant='outlined' 
                                    size='small' 
                                    className={classes.timeFrameIcontDateRangeText}
                                    // onClick={() => setShowDateRangeComponent(true)}
                                >
                                    <b>{moment(goalDetailState.period.start_date).format('DD MMMM YYYY') + " - " + moment(goalDetailState.period.end_date).format('DD MMMM YYYY')}</b>                                    
                                </Button>
                                
                                
                            )
                        }
                    </Grid>
                </Grid>
            </Paper>


            {/* 
                `````````````````````````````````
                TAB OVERVIEW & MEASURE ACTIVITIES

                `````````````````````````````````
            */}
            {/* <Grid container >
                <Grid item sm={6} md={6} style={{textAlign: 'center'}} > 

                </Grid>

                <Grid item sm={6} md={6} style={{textAlign: 'right'}} > 

                    {
                        goalDetailState.status_id !== '4' && (

                            <Button 
                                onClick={handleDialogModalGoalResult}
                                variant='contained' 
                                className={classes.button}
                                style={{
                                    marginRight: 16,
                                    marginTop: 32,
                                    // paddingRight: 8
                                }}
                                size='small'
                            >  
                                <IconButton style={{padding: 7 }}>
                                    <i className='material-icons' style={{fontSize: 12, color: 'white'}}>
                                        add
                                    </i>
                                </IconButton>                                
                                    Tambah baru
                            </Button>
                        )
                    }
                </Grid>
            </Grid>  */}

            <br />
            <Grid container >
                <Grid item xs={3} style={{textAlign: 'left'}}>  
                    <Paper 
                        elevation= {1} 
                        className = {

                            goalDetailState.calculatedValue.gap_value_percent > goalDetailState.calculatedValue.result_value_percent ||
                            goalDetailState.calculatedValue.expected_value_percent > goalDetailState.calculatedValue.result_value_percent
                            
                            ?
                                classes.paperColumPertamaRowFirstKebakaran : 

                                    classes.paperColumPertamaRowFirst
                        
                        }
                    >
                        <List>
                            <ListItem style={{paddingLeft: 0, marginRight: 24}}>
                                
                                <IconButton
                                    aria-haspopup="true"
                                    color="inherit"
                                    
                                >                        
                                    {
                                        fotoQuery !== '' ? (
                                                                        
                                            <img src={URL_API + '/' +fotoQuery+ "&token=" + userToken}  className={classes.userRealFoto} />

                                        ) : (

                                            <img src={AvatarDummy} alt="Gear Picture" className={classes.imageAvatar} />

                                        )
                                    }
                                </IconButton>
                                &nbsp;&nbsp;

                                <ListItemText 
                                    className={classes.listItemtext}
                                    primary = {

                                        <Typography variant='subtitle2' className={classes.title}>
                                            <b>{userLoginName}</b>
                                        </Typography>
                                    } 
                                    secondary={

                                        memberPositionList.length > 0 && memberPositionList.map((item, i) => (

                                            <Chip  
                                                key={i}
                                                label={item.structure_position_title_name} 
                                                style={{
                                                    backgroundColor: '#f4f7fc', 
                                                    color: 'grey', 
                                                    fontFamily: 'Nunito',
                                                    marginTop: 8
                                                }}
                                                size='small'
                                                
                                            />
                                        ))
                                    } 
                                />

                                <ListItemSecondaryAction>
                                    <IconButton 
                                        onClick={handleDropdownOpen}
                                        style={{marginTop: '-64px', marginRight: '-16px'}}>
                                        <i className='material-icons'>
                                            more_horiz
                                        </i>
                                    </IconButton>

                                    <StyledMenu
                                        id="customized-menu"
                                        anchorEl={anchorEl}
                                        keepMounted
                                        open={Boolean(anchorEl)}
                                        onClose={handleDropdownClose}
                                    >
                                        <StyledMenuItem
                                            onClick={handleDialogEditGoalMaster}
                                        >                         
                                            <ListItemText 
                                                
                                                primary = {

                                                    <Typography variant='subtitle2' className={classes.title}>
                                                        <b>Edit</b>
                                                    </Typography>
                                                }  
                                            />

                                        </StyledMenuItem>
                                        <StyledMenuItem
                                            onClick={handleDialogDeleteGoalMaster}
                                            // onClick={() => setModalDelete(true)}
                                        >                         
                                            <ListItemText 
                                                
                                                primary = {

                                                    <Typography variant='subtitle2' className={classes.title}>
                                                        <b>Delete</b>
                                                    </Typography>
                                                }  
                                            />

                                        </StyledMenuItem>
                                    </StyledMenu>
                                </ListItemSecondaryAction>
                            
                            </ListItem>

                            <ListItem>
                                <Grid container direction='row' justify='center' alignItems='center'>
                                    <div className={classes.circularProgressBarInMoreDetail}>

                                        <CircleGoalDetail 
                                            classes = { classes }
                                            goalDetailState = { goalDetailState }
                                            // index = {i}
                                        />

                                                {/* 
                                                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                                                    - INI SCENARIO KALAU ANTARA "GAP" LEBIH BESAR DARI "GOAL RESULT" ==> MERAH untuk "trail GOAL RESULT/Pencapaiannya" 
                                                    
                                                    - INI SCENARIO KALAU "POSISI AMAN" LEBIH BESAR DARI "GOAL RESULT" ==> YELLOW untuk "trail GAP-nya"
                                                    
                                                    `````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                                                    
                                                */}

                                                {/* {
                                                    goalDetailState.calculatedValue.expected_value_percent > goalDetailState.calculatedValue.result_value_percent && (

                                                        <CircularProgressbarWithChildren
                                                            value={numeral(goalDetailState.calculatedValue.expected_value_percent).format('0,0')}//*
                                                            // value={numeral(goalDetailState.calculatedValue.gap_value_percent).format('0,0')}
                                                            styles= {

                                                                buildStyles({
                                                                    pathColor: 'yellow',  
                                                                    trailColor: '#eee',
                                                                    strokeLinecap: 'butt',//butt
                                                                })
                                                            }
                                                        >
                                                            <CircularProgressbar
                                                                value={numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')} 
                                                                styles = {

                                                                    goalDetailState.calculatedValue.gap_value_percent > goalDetailState.calculatedValue.result_value_percent ||
                                                                    goalDetailState.calculatedValue.expected_value_percent > goalDetailState.calculatedValue.result_value_percent
                                                                    
                                                                    ?

                                                                        buildStyles({

                                                                            pathColor:'red',                                                             
                                                                            trailColor: 'transparent',
                                                                            strokeLinecap: 'butt',
                                                                            textColor: 'black'
                                                                        }) : 

                                                                            buildStyles({

                                                                                pathColor:'rgba(61, 255, 41, 0.87)', //*Green                                                          
                                                                                trailColor: 'transparent',
                                                                                strokeLinecap: 'butt',
                                                                                textColor: 'black',
                                                                            })
                                                                }

                                                                text={`${numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')}%`}
                                                            />
                                                        </CircularProgressbarWithChildren>
                                                    )
                                                } */}

                                                {/* 
                                                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````
                                                    
                                                    - INI SCENARIO KALAU "GOAL RESULT" LEBIH BESAR DARI "POSISI AMAN" ==> GREEN untuk "trail GOAL RESULT-nya" 

                                                        (Warna GREEN mendahului warna YELLOW GAP)
                                                    
                                                    ``````````````````````````````````````````````````````````````````````````````````````````````````````````
                                                    
                                                */}

                                                {/* {

                                                    goalDetailState.calculatedValue.result_value_percent > goalDetailState.calculatedValue.expected_value_percent ||
                                                    goalDetailState.calculatedValue.result_value_percent == goalDetailState.calculatedValue.expected_value_percent  ? (

                                                        <CircularProgressbarWithChildren
                                                            value = { numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0') }
                                                            styles= {

                                                                    buildStyles({
                                                                        pathColor: 'rgba(61, 255, 41, 0.87)',  
                                                                        trailColor: '#eee',
                                                                        strokeLinecap: 'butt',//butt
                                                                    })
                                                            }
                                                        >

                                                            <CircularProgressbar
                                                                value={numeral(goalDetailState.calculatedValue.expected_value_percent).format('0,0')} 
                                                                styles = {

                                                                    buildStyles({

                                                                        pathColor:'yellow',                                                             
                                                                        trailColor: 'transparent',
                                                                        strokeLinecap: 'butt',
                                                                        textColor: 'black'
                                                                    })
                                                                }

                                                                text={`${numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')}%`}
                                                            />

                                                        </CircularProgressbarWithChildren>
                                                    
                                                    ) : null
                                                } */}

                                        
                                    </div>                                   
                                </Grid>                            
                            </ListItem>
                            <ListItem>
                                
                                <Grid container direction='row' alignItems='center' justify='center'>
                                    <Typography variant='subtitle1' className={classes.title} >
                                        <b>{goalDetailState.name}</b>
                                    </Typography>
                                </Grid>
                            </ListItem>

                            <ListItem>
                                <ListItemIcon 
                                    style={{ marginLeft: 8 }}
                                >
                                    <IconButton style={{backgroundColor: '#aed9ff', padding: '6px'}}>
                                        <i className="material-icons" style={{color: '#4aa9fb'}}>
                                            people
                                        </i>
                                    </IconButton>
                                </ListItemIcon>
                                                
                                <ListItemText 
                                    className={classes.listItemtext}
                                    primary = {
                                        <Typography 
                                            variant='subtitle2' 
                                            className={classes.title}
                                        >
                                            <b>{goalDetailState.owner.structure_unit_type_name } {goalDetailState.owner.structure_unit_name}</b>
                                        </Typography>
                                    } 
                                
                                />
                            </ListItem>

                            <ListItem>
                                <ListItemIcon 
                                    style={{
                                        marginLeft: 8
                                        
                                    }}
                                >
                                    <IconButton
                                        style={{backgroundColor: '#bdffde', padding: '6px'}}
                                    >
                                        <i className="material-icons" style={{color: '#36d686'}}>
                                            play_arrow
                                        </i>
                                    </IconButton>
                                </ListItemIcon>
                                                
                                <ListItemText 
                                    className={classes.listItemtext}
                                    primary = {
                                        <Typography 
                                            variant='subtitle2' 
                                            className={classes.title}
                                            // style={{marginRight: 32}}
                                        >
                                            Start Value
                                        </Typography>
                                    } 

                                    secondary = {
                                                        
                                        <Typography variant='subtitle2' className={classes.title}>
                                            <b>{numeral(goalDetailState.calculatedValue.start_value).format('0,0')}</b>
                                        </Typography>
                                    }
                                
                                />
                            </ListItem>
                            
                            <ListItem>
                                <ListItemIcon 
                                    style={{
                                        marginLeft: 8
                                        
                                    }}
                                >
                                    <IconButton
                                        style={{backgroundColor: '#aed9ff', padding: '6px'}}
                                    >
                                        <i className="material-icons" style={{color: '#4aa9fb'}}>
                                            radio_button_checked
                                        </i>
                                    </IconButton>
                                </ListItemIcon>
                                                
                                <ListItemText 
                                    className={classes.listItemtext}
                                    primary = {
                                        <Typography 
                                            variant='subtitle2' 
                                            className={classes.title}
                                            // style={{marginRight: 32}}
                                        >
                                            Target value
                                        </Typography>
                                    } 

                                    secondary = {        
                                        <Typography variant='subtitle2' className={classes.title}>
                                            <b>{numeral(goalDetailState.target_value).format('0,0')}</b>
                                        </Typography>
                                    }
                                
                                />
                            </ListItem>

                            <ListItem>
                                <ListItemIcon 
                                    style={{
                                        marginLeft: 8
                                        
                                    }}
                                >
                                    <img src={PictResultValue} style={{width: 40, height: 40 }} />
                                </ListItemIcon>
                                                
                                <ListItemText 
                                    className={classes.listItemtext}
                                    primary = {
                                        <Typography 
                                            variant='subtitle2' 
                                            className={classes.title}
                                            // style={{marginRight: 32}}
                                        >
                                            Goal Result
                                        </Typography>
                                    } 

                                    secondary = {        
                                        <Typography variant='subtitle2' className={classes.title}>
                                            <b>
                                                {numeral(goalDetailState.calculatedValue.result_value).format('0,0')}
                                                &nbsp;
                                                ({numeral(goalDetailState.calculatedValue.result_value_percent).format('0,0')}%)
                                            </b>
                                        </Typography>
                                    }
                                
                                />
                            </ListItem>
                            
                            <ListItem>
                                <ListItemIcon 
                                    style={{
                                        marginLeft: 8
                                        
                                    }}
                                >
                                    <img src={PictGAP} style={{width: 40, height: 40 }} />
                                </ListItemIcon>
                                                
                                <ListItemText 
                                    className={classes.listItemtext}
                                    primary = {
                                        <Typography 
                                            variant='subtitle2' 
                                            className={classes.title}
                                            // style={{marginRight: 32}}
                                        >
                                            Gap
                                        </Typography>
                                    } 

                                    secondary = {      
                                        
                                        goalDetailState.calculatedValue.gap_value < 0 ? (

                                            <Typography variant='subtitle2' className={classes.title}>
                                                0
                                            </Typography> 

                                        ) : (

                                            <Typography variant='subtitle2' className={classes.title}>
                                                <b>{numeral(goalDetailState.calculatedValue.gap_value).format('0,0')}</b>
                                                &nbsp;
                                                <b>({numeral(goalDetailState.calculatedValue.gap_value_percent).format('0,0')}%)</b>
                                            </Typography>
                                        )

                                    }
                                />
                            </ListItem>
                            
                            <ListItem>
                                <ListItemIcon 
                                    style={{
                                        marginLeft: 8
                                        
                                    }}
                                >
                                    <img src={PictDone} style={{width: 40, height: 40 }} />
                                </ListItemIcon>
                                                
                                <ListItemText 
                                    className={classes.listItemtext}
                                    primary = {
                                        <Typography 
                                            variant='subtitle2' 
                                            className={classes.title}
                                            // style={{marginRight: 32}}
                                        >
                                            Posisi Aman
                                        </Typography>
                                    } 

                                    secondary = {     

                                        <Typography variant='subtitle2' className={classes.title}>
                                            <b>{numeral(goalDetailState.calculatedValue.expected_value_percent).format('0,0')} %</b>
                                        </Typography>
                                    }
                                />
                            </ListItem>
                        </List>
                    </Paper>
                    <br />
                    <br />
                </Grid>

                <Grid item xs={9} style={{textAlign: 'left'}} >  

                    <Paper elevation={1} className={classes.paperColumnDuaRowFirst}>
                        <Typography variant='subtitle1' className={classes.titleInLineChart} >
                            <b>Overview History</b>
                        </Typography>
                        
                        <Grid container>
                            <Grid item sm={4}>
                                <TextOverDotLineChart 
                                    classes = {classes }
                                    goalDetailState = { goalDetailState }
                                />
                            </Grid>

                            <Grid item sm={4}>
                                <TextOverDotLineChartSecond
                                    classes = {classes }
                                    goalDetailState = { goalDetailState }
                                />
                            </Grid>
                            <Grid item sm={4}></Grid>
                        </Grid>
                        <br />

                        {/* 
                            ``````````````
                            DOT LINE CHART

                            ``````````````                   
                        */}
                    
                        <DotLineChart 
                            // totalDaysState = { totalDaysState }
                            classes = {classes }
                            collectionGoalResultList = { collectionGoalResultList }
                        />

                    </Paper>

                    <br />
                    <Grid container>
                        <Grid item sm={6}> 
                            {
                                collectionGoalResultList.length > 0 && (

                                    <Typography variant='h6' className={classes.titleInLineChart} >
                                        <b>Latest Goal Result</b>
                                    </Typography>

                                )
                            }
                        </Grid>

                        <Grid item sm={6} style={{textAlign: 'right'}}> 

                            
                            {
                                goalDetailState.status_id !== '4' && (

                                    <Button 
                                        onClick={handleDialogModalGoalResult}
                                        variant='contained' 
                                        className={classes.button}
                                        style={{marginRight: 16,marginTop: 32}}
                                    >  
                                        Buat Goal Result
                                    </Button>
                                )
                            }

                        </Grid>
                    </Grid>

                    {/* 
                        ````````
                        PAPER-02
                        ````````
                    
                    */}

                    <br />

                    {
                        collectionGoalResultList.length == 0 && goalDetailState.status_id !== '4' && (

                            <Paper className={classes.paperColumnDuaRowFirst}>
                                <Grid container>
                                    <Grid item xs={12} style={{textAlign: 'center'}}>
                                        <Typography variant='subtitle2' className={classes.title} style={{color: 'grey', margin: 8}}>
                                            Anda belum memiliki Goal Result ;(
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Paper>
                        )
                    }

                    {
                        collectionGoalResultList.length > 0 && (

                            <Paper className={classes.paperColumnDuaRowFirst}>
                                <Grid container>
                                    <Grid item xs={12}>
        
                                        <List style={{width: 120}}>
        
                                            {
                                                collectionGoalResultList.length > 0 && (
        
                                                    <ListItem button onClick={handleCollapse}>
                                                        <ListItemText 
                                                            primary = {
                                                                <Typography 
                                                                    variant='subtitle2' 
                                                                    className={classes.title}
                                                                    style={{color: 'grey'}}
                                                                >
                                                                    <b>Record</b>
                                                                </Typography>
                                                            } 
                                                        />
                                            
                                                        {
                                                            openCollapse   ? <ExpandLess /> : <ExpandMore />
                                                        }
                                                    </ListItem>
                                                )
                                            }

                                            <Collapse in={openCollapse} timeout="auto" unmountOnExit>

                                                    
        
                                                    {
                                                        collectionGoalResultList.length > 0 ? collectionGoalResultList.map((item, i) => {


                                                            if(goalDetailState.calculationMethod.id == '2' ){//* 1: Total, 2: Increment

                                                                return (
            
                                                                    <List style={{width: '760%'}} key={i}>
                                                                        <ListItem >
                                                                            <ListItemText 
                                                                                primary = {
                                                                                    <Typography 
                                                                                        variant='subtitle1' 
                                                                                        className={classes.title}
                                                                                        style={{color: '#55dc87'}}
                                                                                    >
                                                                                        <b>+ {numeral(item.result_value).format('0,0')}</b>
                                                                                    </Typography>
                                                                                } 
            
                                                                                secondary = {       
    
                                                                                    <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                                                                        {item.description !== null ? item.description : '-'} 
                                                                                        <br />
                                                                                        <i style={{fontSize: 12}}>({moment(item.result_date).format('DD MMMM YYYY')})</i>
                                                                                    </Typography>
                                                                                }
                                                                            />
                                                                        </ListItem>
                                                                        <Divider />
            
                                                                        <ListItemSecondaryAction>
                                                            
                                                                            <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                                                                    
                                                                                {moment(item.updated_at).format('DD MMMM YYYY h:mm:ss a')}
            
                                                                                <IconButton
                                                                                    onClick={(e) => handleDropdownOpenGoalResult(e, item)}
                                                                                    // style={{marginTop: '-64px', marginRight: '-16px'}}>
                                                                            
                                                                                >
                                                                                    <i className='material-icons'>
                                                                                        more_vert
                                                                                    </i>
                                                                                </IconButton>
                                                                            </Typography>
            
                                                                                <StyledMenu
                                                                                    id="customized-menu-goal-result"
                                                                                    anchorEl={anchorElListGoalResult}
                                                                                    keepMounted
                                                                                    open={Boolean(anchorElListGoalResult)}
                                                                                    onClose={handleDropdownCloseGoalResult}
                                                                                >
                                                                                    <StyledMenuItem
                                                                                        onClick ={ (e) => handleDialogEdit(e, item)}
                                                                                    
                                                                                    >                         
                                                                                        <ListItemText 
                                                                                            
                                                                                            primary = {
            
                                                                                                <Typography variant='subtitle2' className={classes.title}>
                                                                                                    <b>Edit</b>
                                                                                                </Typography>
                                                                                            }  
                                                                                        />
            
                                                                                    </StyledMenuItem>
                                                                                    <StyledMenuItem
                                                                                        // onClick={() => setModalDelete(true)}
                                                                                        onClick ={ (e) => handleDialogDelete(e, item)}
    
                                                                                    >                         
                                                                                        <ListItemText 
                                                                                            primary = {
            
                                                                                                <Typography variant='subtitle2' className={classes.title}>
                                                                                                    <b>Delete</b>
                                                                                                </Typography>
                                                                                            }  
                                                                                        />
            
                                                                                    </StyledMenuItem>
                                                                                </StyledMenu>
                                                                        
                                                                        </ListItemSecondaryAction>
                                                                    </List>
                                                                )
                                                            };


                                                            if(goalDetailState.calculationMethod.id == '1' ){ //*Total

                                                                return (
            
                                                                    <List style={{width: '760%'}} key={i}>
                                                                        <ListItem >
                                                                            <ListItemText 
                                                                                primary = {
                                                                                    <Typography 
                                                                                        variant='subtitle1' 
                                                                                        className={classes.title}
                                                                                        style={{color: '#55dc87'}}
                                                                                    >
                                                                                        You updated goal result to <b> {numeral(item.result_value).format('0,0')}</b>
                                                                                    </Typography>
                                                                                } 
            
                                                                                secondary = {       
    
                                                                                    <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                                                                        { item.description !== null ? item.description : '-' } 
                                                                                        <br />
                                                                                        <i style={{fontSize: 12}}>({moment(item.result_date).format('DD MMMM YYYY')})</i>
                                                                                    </Typography>
                                                                                }
                                                                            />
                                                                        </ListItem>
                                                                        <Divider />
            
                                                                        <ListItemSecondaryAction>
                                                            
                                                                            <Typography variant='subtitle2' className={classes.title} style={{color: 'grey'}}>
                                                                                    
                                                                                {moment(item.updated_at).format('DD MMMM YYYY h:mm:ss a')}
                                                                                {/* {moment(item.result_date).format('DD MMMM YYYY')} */}
            
                                                                                <IconButton
                                                                                    onClick={(e) => handleDropdownOpenGoalResult(e, item)}                                                                            
                                                                                >
                                                                                    <i className='material-icons'>
                                                                                        more_vert
                                                                                    </i>
                                                                                </IconButton>
                                                                            </Typography>
            
                                                                                <StyledMenu
                                                                                    id="customized-menu-goal-result"
                                                                                    anchorEl={anchorElListGoalResult}
                                                                                    keepMounted
                                                                                    open={Boolean(anchorElListGoalResult)}
                                                                                    onClose={handleDropdownCloseGoalResult}
                                                                                >
                                                                                    <StyledMenuItem
                                                                                        onClick ={ (e) => handleDialogEdit(e, item)}

                                                                                    >                         
                                                                                        <ListItemText 
                                                                                            
                                                                                            primary = {
            
                                                                                                <Typography variant='subtitle2' className={classes.title}>
                                                                                                    <b>Edit</b>
                                                                                                </Typography>
                                                                                            }  
                                                                                        />
            
                                                                                    </StyledMenuItem>
                                                                                    <StyledMenuItem
                                                                                        // onClick={() => setModalDelete(true)}
                                                                                        onClick ={ (e) => handleDialogDelete(e, item)}
    
                                                                                    >                         
                                                                                        <ListItemText 
                                                                                            primary = {
            
                                                                                                <Typography variant='subtitle2' className={classes.title}>
                                                                                                    <b>Delete</b>
                                                                                                </Typography>
                                                                                            }  
                                                                                        />
            
                                                                                    </StyledMenuItem>
                                                                                </StyledMenu>
                                                                        
                                                                        </ListItemSecondaryAction>
                                                                    </List>
                                                                )
                                                            };


                                                        }) : null
                                                    }
                                                
                                            </Collapse>
                                        </List>
                                    </Grid>
                                </Grid>
                            </Paper>
                        )
                    }
                    <br />
                    <br />
                    <br />

                </Grid>
            </Grid>

            <DialogCreate 
                classes = { classes }
                isModalCreate = { isModalCreate }
                setModalCreate = {setModalCreate }
                collectionGoalResultList = { collectionGoalResultList }
                setCollectionGoalResultList = { setCollectionGoalResultList }
                goalDetailState = {goalDetailState}
                setFireGoalDetail = { setFireGoalDetail }
            />

            <DialogEdit 
                classes = { classes }
                isModalEdit = { isModalEdit }
                setModalEdit = {setModalEdit }
                collectionGoalResultList = { collectionGoalResultList }
                setCollectionGoalResultList = { setCollectionGoalResultList }
                goalDetailState = {goalDetailState}
                setFireGoalDetail = { setFireGoalDetail }
                goalResultDataDetail = { goalResultDataDetail }
            />

            <DialogDelete 
                classes = { classes }
                isModalDelete = { isModalDelete }
                setModalDelete = { setModalDelete }
                collectionGoalResultList = { collectionGoalResultList }
                setCollectionGoalResultList = { setCollectionGoalResultList }
                goalResultId = { goalResultId }
                goalDetailState = {goalDetailState}
                goalResultNameDescription = { goalResultNameDescription }
            />

            <DialogDeleteGoalMaster
                classes = { classes }
                isModalDeleteGoalMaster = { isModalDeleteGoalMaster }
                setModalDeleteGoalMaster = { setModalDeleteGoalMaster }
                goalDetailState = { goalDetailState }
            />

            <DialogEditGoalMaster
                classes = { classes }
                setModalEditGoalMaster = { setModalEditGoalMaster }
                isModalEditGoalMaster = { isModalEditGoalMaster }
                goalDetailState = { goalDetailState }
            />
        </MuiThemeProvider>
    )
};


export default withStyles(styles)(ViewGoalMoreDetail);