import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Chip, Hidden
    
} from '@material-ui/core';

import moment from 'moment';
import * as d3 from 'd3';
import clsx from 'clsx';
import { CircularProgressbarWithChildren, CircularProgressbar, buildStyles } from 'react-circular-progressbar';

// import ReactD3, {PieChart } from 'react-d3-components'

import DoneIcon from '@material-ui/icons/Done';
import PieHooks from '../../../../components/PieHooks';
import PieHooksAnimated from '../../../../components/PieHooksAnimated';
import Circle from '../../../../components/Circle';


import DialogCreate from '../../Goal/Components/DialogCreate';
// import DialogCreate from '../../../Goal/Components/DialogCreate';
import LightTooltip from '../../../../components/LightTooltip';
import TruncateText from '../../../../utilities/TruncateText';
import TruncateTextShort from '../../../../utilities/TruncateTextShort';
import TruncateTextInDashboard from '../../../../utilities/TruncateTextInDashboard';
import { URL_API } from '../../../../constants/config-api';
import AvatarDummy from '../../../../assets/images/Avatar_dummy.png';
import OrganigramGoal from '../../../../assets/images/Group_2491.png';
import PictResultValue from '../../../../assets/images/Group_2620.png';
import { PieChart } from "recharts";

const numeral = require('numeral');

const YouActiveInHome = props => {

    const { classes, isModalCreate, setModalCreate, fotoQuery, userLoginName, memberPositionList, collectionGoalList, setCollectionGoalList } = props;

 
    const [dataDummy, setDataDummy] = useState([]);

    useEffect(() => {

        let listData = [];

        if(collectionGoalList.length > 0){

            collectionGoalList.map((item, i) => {

                let list = {

                    date: i,
                    value: item.calculatedValue.result_value_percent
    
                };

                listData.push(list);
                
            });

            setDataDummy(listData);
        
        };
        
    },[collectionGoalList])

    return(

        <Fragment>

            <Grid container style={{backgroundColor: '#f4f7fc'}}>
            
                <Grid item sm={12} md={12} lg={12} xl={12}>

                    {
                        collectionGoalList.length > 0 ? collectionGoalList.map((item,i) => {

                            return(

                                <Paper 
                                    key={i} 
                                    elevation={1} 
                                    style={{marginBottom: 16}}
                                    className={
                                        
                                        item.calculatedValue.gap_value_percent > item.calculatedValue.result_value_percent ||
                                        item.calculatedValue.expected_value_percent > item.calculatedValue.result_value_percent 
                                        ? 

                                            classes.paperListKebakaran : classes.paperList
                                    
                                    } 
                                >

                                    {/* 
                                        ``````````````````````````````````
                                        HIDE COMPONENT WHEN screen is 'xs'

                                        ``````````````````````````````````
                                    */}

                                    <Hidden only='xs'>
                                        <Grid 
                                            container 
                                            alignItems="flex-end" 
                                        >

                                                <div className={classes.circularProgressBar}>

                                                    <Circle 
                                                        classes = { classes }
                                                        item = { item }
                                                        index = {i}
                                                        
                                                    />

                                                </div>
                                            <List 
                                                className={classes.layoutMarginLeftList} 
                                                style={{width: 200}}
                                            >
                                                <ListItem style={{paddingLeft: 0, marginRight: 16}}>
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                <b>{item.name }</b>
                                                            </Typography>
                                                        } 
                                                        secondary={
                                                            <Typography variant='subtitle2' className={classes.title}>

                                                                {// *PERIOD
                                                                    item.period !== null && (
                                                                        item.period.name
                                                                    )
                                                                }

                                                                {// *DATE RANGE
                                                                    item.start_date !== null && item.end_date !== null && item.period == null && (
                                                                        <LightTooltip title={moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY')} placement="right-start">
                                                                            <span>
                                                                                {TruncateTextShort(moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY'))}
                                                                            </span>
                                                                        </LightTooltip>
                                                                    )
                                                                }

                                                                {// *DUE DATE

                                                                    item.start_date == null && item.end_date !== null && item.period == null && (
                                                                    
                                                                        moment(item.end_date).format('DD MMMM YYYY')
                                                                        
                                                                    )

                                                                }
                                                            </Typography>
                                                        } 
                                                    />
                                                
                                                </ListItem>
                                            </List>
                                            
                                            <List style={{width: 120}}>

                                                {/* 
                                                    ````````````
                                                    TARGET VALUE

                                                    ````````````                                            
                                                */}

                                                <ListItem
                                                    style={{
                                                        marginRight: 24,  
                                                        paddingLeft: 0,
                                                    }}
                                                >
                                                    <ListItemIcon 
                                                        style={{
                                                            marginLeft: 8
                                                            
                                                        }}
                                                    >
                                                        <IconButton
                                                            style={{backgroundColor: '#aed9ff', padding: '6px'}}
                                                        >
                                                            <i className="material-icons" style={{color: '#4aa9fb'}}>
                                                                radio_button_checked
                                                            </i>
                                                        </IconButton>
                                                    </ListItemIcon>
                                                                    
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                                                            >
                                                                <b>Target </b>
                                                            </Typography>

                                                        } //* Target value

                                                        secondary = {        
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                {numeral(item.calculatedValue.target_value).format('0,0')}
                                                            </Typography>
                                                        }
                                                    
                                                    />
                                                </ListItem>
                                            </List>

                                            {/* 
                                                ```````````
                                                GOAL RESULT

                                                ```````````
                                            */}
                                            <List>
                                                <ListItem>
                                                    <ListItemIcon 
                                                        style={{ marginLeft: 8 }}
                                                    >
                                                        <img src={PictResultValue} style={{width: 40, height: 40 }} />
                                                    </ListItemIcon>
                                                                
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                                                            >
                                                                <b>Goal Result</b>                                                              
                                                            </Typography>
                                                        } 

                                                        secondary = {        

                                                            <LightTooltip title={numeral(item.calculatedValue.result_value).format('0,0')} placement="right-start">
                                                                <Typography variant='subtitle2' className={classes.title}>

                                                                    {/* {TruncateTextInDashboard('1.000.000.000.0')} */}
                                                                    { TruncateTextInDashboard(numeral(item.calculatedValue.result_value).format('0,0'))}
                                                                
                                                                </Typography>
                                                            </LightTooltip>
                                                        }
                                                    />

                                                </ListItem>
                                            </List>
                                            
                                            {/* 
                                                ```````````
                                                POSISI AMAN

                                                ```````````
                                            */}
                                            <List>
                                                <ListItem 
                                                    style={{
                                                        marginRight: 8, 
                                                        paddingLeft: 0,
                                                    }}
                                                >
                
                                                    <ListItemIcon style={{marginLeft: 8}}>
                                                        <IconButton style={{backgroundColor: '#bdffde', padding: '8px'}}>
                                                            <DoneIcon style={{color: '#36d686', fontSize: 21}} />
                                                        </IconButton>
                                                    </ListItemIcon>
                                                    
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                
                                                            >
                                                                <b>Posisi aman</b>
                                                            </Typography>
                                                        } 
                
                                                        secondary = {

                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                {numeral(item.calculatedValue.expected_value_percent).format('0,0')}%
                                                            </Typography>
                                                        } 
                                                    
                                                    />
                                                </ListItem>
                                            </List>
                
                                            <List style={{width: 130}}>
                                                <ListItem 
                                                    style={{
                                                        paddingLeft: 0,
                                                    }}
                                                >
                                                    <ListItemIcon style={{marginLeft: 8}}>
                                                        <IconButton style={{backgroundColor: 'rgb(255, 222, 172)', padding: '11px'}}>
                                                            <i className="material-icons" style={{color: 'orange', fontSize: 14}}>
                                                                calendar_today
                                                            </i>
                                                        </IconButton>
                                                    </ListItemIcon>
                                                    
                                                    <ListItemText 
                                                        className={classes.listItemtext}
                                                        primary = {
                                                            <Typography 
                                                                variant='subtitle2' 
                                                                className={classes.title}
                                                            >
                                                                <b>Tersisa</b>
                                                            </Typography>
                                                        } 
                
                                                        secondary={
                                                            <Typography variant='subtitle2' className={classes.title}>
                                                                {item.time.remaining_days} hari
                                                            </Typography>
                                                        } 
                                                    />
                                                
                                                </ListItem>
                                            </List>
                                        </Grid>
                                    </Hidden>

                                    {/* 
                                        ``````````````````````````````````````````````````````````````
                                        SHOW this component in only size 'xs'.

                                        *smUp = true. If true, screens this size and up will be hidden.

                                        ``````````````````````````````````````````````````````````````
                                    */}

                                    <Hidden smUp={true}>

                                        <br />
                                        <br />
                                        <Grid container alignItems='center' justify='center'>  
                                            <div className={classes.circularProgressBarSmallSize}>
                                                <Circle 
                                                    classes = { classes }
                                                    item = { item }
                                                    index = {i}                                                        
                                                />
                                            </div>
                                        </Grid>

                                        <Grid container>
                                            <Grid item xs={12} style={{textAlign: 'center'}}>
                                                <List 
                                                    className={classes.layoutMarginLeftList} 
                                                    style={{width: 200}}
                                                >
                                                    <ListItem style={{paddingLeft: 0, marginRight: 16}}>
                                                        <ListItemText 
                                                            className={classes.listItemtext}
                                                            primary = {
                                                                <Typography variant='subtitle2' className={classes.title}>
                                                                    <b>{item.name }</b>
                                                                </Typography>
                                                            } 
                                                            secondary={
                                                                <Typography variant='subtitle2' className={classes.title}>

                                                                    {// *PERIOD
                                                                        item.period !== null && (
                                                                            item.period.name
                                                                        )
                                                                    }

                                                                    {// *DATE RANGE
                                                                        item.start_date !== null && item.end_date !== null && item.period == null && (
                                                                            <LightTooltip title={moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY')} placement="right-start">
                                                                                <span>
                                                                                    {TruncateTextShort(moment(item.start_date).format('DD MMMM YYYY') + " - " + moment(item.end_date).format('DD MMMM YYYY'))}
                                                                                </span>
                                                                            </LightTooltip>
                                                                        )
                                                                    }

                                                                    {// *DUE DATE

                                                                        item.start_date == null && item.end_date !== null && item.period == null && (
                                                                        
                                                                            moment(item.end_date).format('DD MMMM YYYY')
                                                                            
                                                                        )

                                                                    }
                                                                </Typography>
                                                            } 
                                                        />
                                                    
                                                    </ListItem>
                                                </List>

                                            </Grid>
                                         </Grid>

                                        <Grid container>
                                            <Grid item xs={12} style={{textAlign: 'center'}} >
                                                <List 
                                                    style={{width: 120}}
                                                >

                                                    {/* 
                                                        ````````````
                                                        TARGET VALUE

                                                        ````````````                                            
                                                    */}

                                                    <ListItem
                                                        style={{
                                                            marginRight: 24,  
                                                            // paddingLeft: 0,
                                                        }}
                                                    >
                                                        <ListItemIcon 
                                                            style={{
                                                                marginLeft: 8
                                                                
                                                            }}
                                                        >
                                                            <IconButton
                                                                style={{backgroundColor: '#aed9ff', padding: '6px'}}
                                                            >
                                                                <i className="material-icons" style={{color: '#4aa9fb'}}>
                                                                    radio_button_checked
                                                                </i>
                                                            </IconButton>
                                                        </ListItemIcon>
                                                                        
                                                        <ListItemText 
                                                            className={classes.listItemtext}
                                                            primary = {
                                                                <Typography 
                                                                    variant='subtitle2' 
                                                                    className={classes.title}
                                                                >
                                                                    <b>Target </b>
                                                                </Typography>

                                                            } //* Target value

                                                            secondary = {        
                                                                <Typography variant='subtitle2' className={classes.title}>
                                                                    {numeral(item.calculatedValue.target_value).format('0,0')}
                                                                </Typography>
                                                            }
                                                        
                                                        />
                                                    </ListItem>
                                                </List>
                                            
                                                 {/* 
                                                    ```````````
                                                    GOAL RESULT

                                                    ```````````
                                                */}
                                                <List>
                                                    <ListItem>
                                                        <ListItemIcon 
                                                            style={{ marginLeft: 8 }}
                                                        >
                                                            <img src={PictResultValue} style={{width: 40, height: 40 }} />
                                                        </ListItemIcon>
                                                                    
                                                        <ListItemText 
                                                            className={classes.listItemtext}
                                                            primary = {
                                                                <Typography 
                                                                    variant='subtitle2' 
                                                                    className={classes.title}
                                                                >
                                                                    <b>Goal Result</b>                                                              
                                                                </Typography>
                                                            } 

                                                            secondary = {        

                                                                <LightTooltip title={numeral(item.calculatedValue.result_value).format('0,0')} placement="right-start">
                                                                    <Typography variant='subtitle2' className={classes.title}>

                                                                        {/* {TruncateTextInDashboard('1.000.000.000.0')} */}
                                                                        { TruncateTextInDashboard(numeral(item.calculatedValue.result_value).format('0,0'))}
                                                                    
                                                                    </Typography>
                                                                </LightTooltip>
                                                            }
                                                        />

                                                    </ListItem>
                                                </List>

                                                {/* 
                                                    ```````````
                                                    POSISI AMAN

                                                    ```````````
                                                */}
                                                <List>
                                                    <ListItem 
                                                        style={{
                                                            marginRight: 8, 
                                                            // paddingLeft: 0,
                                                        }}
                                                    >
                    
                                                        <ListItemIcon style={{marginLeft: 8}}>
                                                            <IconButton style={{backgroundColor: '#bdffde', padding: '8px'}}>
                                                                <DoneIcon style={{color: '#36d686', fontSize: 21}} />
                                                            </IconButton>
                                                        </ListItemIcon>
                                                        
                                                        <ListItemText 
                                                            className={classes.listItemtext}
                                                            primary = {
                                                                <Typography 
                                                                    variant='subtitle2' 
                                                                    className={classes.title}
                    
                                                                >
                                                                    <b>Posisi aman</b>
                                                                </Typography>
                                                            } 
                    
                                                            secondary = {

                                                                <Typography variant='subtitle2' className={classes.title}>
                                                                    {numeral(item.calculatedValue.expected_value_percent).format('0,0')}%
                                                                </Typography>
                                                            } 
                                                        
                                                        />
                                                    </ListItem>
                                                </List>
                    
                                                <List style={{width: 130}}>
                                                    <ListItem 
                                                        style={{
                                                            // paddingLeft: 0,
                                                        }}
                                                    >
                                                        <ListItemIcon style={{marginLeft: 8}}>
                                                            <IconButton style={{backgroundColor: 'rgb(255, 222, 172)', padding: '11px'}}>
                                                                <i className="material-icons" style={{color: 'orange', fontSize: 14}}>
                                                                    calendar_today
                                                                </i>
                                                            </IconButton>
                                                        </ListItemIcon>
                                                        
                                                        <ListItemText 
                                                            className={classes.listItemtext}
                                                            primary = {
                                                                <Typography 
                                                                    variant='subtitle2' 
                                                                    className={classes.title}
                                                                >
                                                                    <b>Tersisa</b>
                                                                </Typography>
                                                            } 
                    
                                                            secondary={
                                                                <Typography variant='subtitle2' className={classes.title}>
                                                                    {item.time.remaining_days} hari
                                                                </Typography>
                                                            } 
                                                        />
                                                    
                                                    </ListItem>
                                                </List>
                                            </Grid>
                                        </Grid>


                                    </Hidden>

                                </Paper>
                            )

                        }) : (

                                <Grid  
                                    container
                                    spacing={0}
                                    direction="row"
                                    justify="center"
                                    alignItems="center"
                                >
                                    <CircularProgress size={32} style={{marginTop: 24, color: 'red'}} />

                                </Grid>
                            )
                    }

                </Grid>

                <Grid container alignContent='center' justify='center'>
                    
                    {/* //*Ukuran yang OKE */}
                    {/* <PieHooksAnimated
                        data={dataDummy}
                        width={120}
                        height={120}
                        innerRadius={30}
                        outerRadius={40}    
                    />  */}

                    {/* 
                        <PieHooks
                            // data={dataDummy}
                            width={170}
                            height={170}
                            innerRadius={25}
                            outerRadius={35}

                        /> 
                    */}

                </Grid>
            </Grid>
            
            <DialogCreate 
                classes = { classes }
                isModalCreate = { isModalCreate }
                setModalCreate = { setModalCreate }
                fotoQuery = { fotoQuery }
                userLoginName = { userLoginName }
                memberPositionList = { memberPositionList }
                setCollectionGoalList = { setCollectionGoalList}
                collectionGoalList = {collectionGoalList }
            />

        </Fragment>
    )

};

export default YouActiveInHome;