import React, { useCallback, useEffect, useState, useContext, Fragment} from "react";
import { makeStyles, createMuiTheme, withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import { 

    Container, Paper, Typography, Grid, TextField, Checkbox, CircularProgress,
    FormControlLabel, Box, Button, MenuItem, Dialog, DialogTitle, DialogContent, 
    DialogContentText, DialogActions, Breadcrumbs, Link, List, ListItem, ListItemText, 
    ListItemAvatar, Avatar, IconButton, InputBase, Divider, Tooltip, Table, Icon,
    TableBody, TableRow, TableCell, TablePagination, TableSortLabel, TableHead, Menu,
    ListItemIcon, Chip, Hidden
    
} from '@material-ui/core';

import axios from 'axios';
import clsx from 'clsx';
import DoneIcon from '@material-ui/icons/Done'; 

import { URL_API } from '../../../constants/config-api';
import YouActiveInHome from './You/YouActiveInHome';

import PictDefaultDashboard from '../../../assets/images/Mask_Group_2.png';
import PictPlusUploadDocument from '../../../assets/images/Group_2268.png';
import PictDoneGreen from '../../../assets/images/Group_2239.png';
import PictPIALA from '../../../assets/images/Group_2667.png';
import PictREFRESH from '../../../assets/images/Group_2725.png';
import PictBackgroundImage from '../../../assets/images/Group_2784.jpg'

const theme = createMuiTheme({
  
    palette: {

        primary: {
            
            main: '#cc0707', //#cc0707, #c62828
            light: '#ff5f52',
            dark: '#8e0000',
            contrastText: '#ffffff'
        }
    }
});

const styles = theme => ({

    root: {
      
        width: '100%',
  
    },
    layoutMarginLeftAndTop: {

        marginLeft: 72, 
        marginTop: 56
    },
    layoutMarginTop: {

        // marginLeft: 72, 
        marginTop: 56
    },
    layoutMarginTopNew: {
        marginTop: theme.spacing(10)

    },
    layoutMarginTopPict: {

        marginTop: theme.spacing(11)
    },
    layoutMarginLeftList: {

        marginLeft: theme.spacing(4), 
        marginTop: theme.spacing(1)
      
    },

    button: {
        // width: '503px',
        // height: '42px',
        borderRadius: 5,
        background: 'linear-gradient(1deg, #c1272d, #c1272d 30%, #d1354a 67%, #f28181)',
        border:0,
        fontFamily:'Nunito',
        marginLeft:theme.spacing(1),
        textTransform: 'capitalize',
        color: 'white'

    },
    buttonDisabled: {
        fontFamily:'Nunito',
        textTransform: 'capitalize',
        color: 'grey',
        fontWeight: 'bold'
    },
    title: {

        fontFamily: 'Nunito'
    },
    
    pictUpload: {

        // marginRight: theme.spacing(37),
        cursor: 'pointer',
        height: 48,
        width: 48   
    },

    timeFrameIcon: {
        fontFamily: 'Nunito',
        color: '#4aa9fb',
        textTransform: 'capitalize',
        // marginRight: theme.spacing(4),
        // marginTop: theme.spacing(9),
        // backgroundColor: '#cfe8fd'
    },
    timeFrameIcontDateRangeText: {
        fontFamily: 'Nunito',
        color: '#4aa9fb',
        textTransform: 'capitalize',
        backgroundColor: '#edf6ff'
    },

    timeFrameIconInModal: {

        fontFamily: 'Nunito',
        color: 'grey',
        textTransform: 'capitalize',
        '&:hover': {
            color: '#edcfd8',//#edcfd8, #cc0707
        },
        borderWidth: 0
    },

    /*
        ````````````````````````````
        PAPER, CIRCULAR PROGRESS BAR

        ````````````````````````````        
    */

    // backgroundPaper : {
        
    //     background: 'url("../../../assets/images/Group_2784.png") no-repeat'
    // },

    paperList: {
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        padding: theme.spacing(0.2),
        
    },
    paperListKebakaran: {
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        padding: theme.spacing(0.2),
        background: '#edcfd8',
        borderBottom: '0.5em solid red'
        // borderLeft: '0.1em solid #c3c0c0de',


    },
    circularProgressBar : {
        
        width: 60, 
        height: 60,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(0.3),
        marginBottom: theme.spacing(2)
    },
    circularProgressBarSmallSize: {
        
        width: 60, 
        height: 60,
    },
    
    /*
        ````````````
        MODAL CREATE 

        ````````````
    */
    textField: {

        minWidth: 425,
        [theme.breakpoints.only('xs')]: {
                   
            minWidth: 200,
        } 
    },

    /*
        `````````````````````````````
        FOTO REAL USER NAME, USERNAME

        `````````````````````````````
    */
    userRealFoto: {
        // margin: 10,
        width: 48,
        height: 48,
        borderRadius: 24
    },
    imageAvatar: {

        width: 50,
        height: 40
    },

    /*
        ``````
        Hai...

        ``````
    */
    textAlignHai : {

        textAlign: 'left',
        [theme.breakpoints.only('xs')]: {
    
            textAlign: 'center'
        }
    },
    
    /*
        ````````````````````````
        GRID GOAL BARU- BARU INI

        ````````````````````````
    */

   gridGoalBaruBaruIni: {//*Not Work!

        [theme.breakpoints.only('sm')]: {
                   
            alignItems: "flex-end", 

        },
        [theme.breakpoints.only('md')]: {
                   
            alignItems: "flex-end", 

        },
        [theme.breakpoints.only('xs')]: {
                   
               justify: 'center',
               alignItems : 'center'
        }

   }


})

const ViewHome = props => {

    const { classes } = props;

    /*
        ```````````````````
        COMPONENT DID MOUNT

        ```````````````````
    */
    const userToken = localStorage.getItem('userToken');
    const statusUserLogin = localStorage.getItem('status_user_login');
    const statusUserLoginAfterParse = JSON.parse(statusUserLogin);

    const [ userTokenState, setUserTokenState ] = useState('');
    const [ userLoginName, setUserLoginName ] = useState('');
    const [ fotoQuery, setFotoQuery ] = useState('');
    const [ memberPositionList, setMemberPositionList ] = useState([]);
    
    const [ collectionGoalList, setCollectionGoalList ] = useState([]);

    const [ loader, setLoader ] = useState(false);

    useEffect(() => {

        setLoader(true);
        if(statusUserLoginAfterParse !== null){

            setUserLoginName(statusUserLoginAfterParse.member_nickname);
            setFotoQuery(statusUserLoginAfterParse.member_photo_url);
            setMemberPositionList(statusUserLoginAfterParse.member_position);
    
            setUserTokenState(userToken)
    
    
            if(userToken !== undefined ){
            
                const header =  {   
    
                    'Accept': "application/json",
                    'Content-Type' : "application/json",
                    'Authorization' : "bearer " + userToken,
        
                };
    
                axios.defaults.headers.common = header;    
    
                //* GOL SELF - ONLY OWNER GOAL USER LOGIN
                axios
                    .get(URL_API + `/swd/goal?options[filter][scope]=self`)
                    .then(function(response){
    
                        setLoader(false);
                        console.log("Response Original : ", response);
                        
                        if(response.status == 200){
                            if(response.data.data !== null){
                                
                                setCollectionGoalList(response.data.data);
                            };
                        };
                    })
                    .catch(function(error){
                        
                        setLoader(false);
                        console.log("Error : ", error.response);
    
                    })
    
            } else { console.log("No Access Token available!")};
        }

    },[])

    /*
        ```````````````````
        HANDLE DIALOG MODAL CREATE 
        
        ```````````````````      
    */
    
    const [ isGoalFire, setGoalFire ] = useState(false);

    const [ isModalCreate, setModalCreate ] = useState(false);

    const handleDialogCreate = () => {

        setGoalFire(true)
        setModalCreate(true);

    };



    return (

        <MuiThemeProvider theme={theme} >


            {
                loader == true && (

                    <Grid  
                        container
                        spacing={40}
                        direction="row"
                        justify="center"
                        alignItems="center"
                    >
                    
                        <Grid item sm={12} style={{textAlign: 'center'}}> 
                            <CircularProgress size={32} style={{marginTop: 80, color: 'red'}} />
                        </Grid>
                    
                    </Grid>
                )
            }

            {
                collectionGoalList.length == 0 && loader == false && (

                    <Grid container >
                        <Grid item sm={12} style={{textAlign: 'center'}} >

                            <img src={PictDefaultDashboard} className={classes.layoutMarginTopPict} style={{height: 200}} alt='Default Dashboard Pict' />
                            <Typography variant='subtitle1' className={classes.title} style={{marginTop: 24, color: 'grey'}} >
                                <b>Hai <i>{userLoginName}</i> :)</b>
                            </Typography>

                            <Typography variant='h5' className={classes.title} style={{marginTop: 8}}>
                                <b>Selamat Datang di AbangBisa</b>
                            </Typography>

                            <Typography variant='subtitle1' className={classes.title} style={{marginTop: 8, color: 'grey'}} >
                                Kami siap mengawal eksekusi Anda !
                            </Typography>

                            <br />
                            <IconButton
                                onClick={() => handleDialogCreate()}
                            >        
                                <img src={PictPlusUploadDocument} />
                            </IconButton>

                            <Typography 
                                variant='subtitle2' 
                                className={classes.title} 
                                style={{color: 'grey'}} 
                            >
                                Buat Goal sekarang !
                            </Typography>
                                
                        </Grid>

                        {
                            isGoalFire == true && isModalCreate !== false && (

                                <YouActiveInHome 
                                    classes = { classes }
                                    isModalCreate = { isModalCreate }
                                    setModalCreate = { setModalCreate }
                                    fotoQuery = { fotoQuery}
                                    userLoginName = { userLoginName }
                                    memberPositionList = { memberPositionList }
                                    collectionGoalList = { collectionGoalList }
                                    setCollectionGoalList = { setCollectionGoalList }
                                />
                            )
                        }

                    </Grid>
                )
            }

            {

                collectionGoalList.length > 0 && (

                    <Fragment>
                        <Grid container style={{backgroundColor: '#f4f7fc'}}>
                            
                            <Grid item xs={1}></Grid>
                            <Grid item xs={9} className={classes.layoutMarginLeftAndTop}  style={{textAlign: 'left'}}>

                                <br />
                                <br />
                                <br />

                                <Paper 
                                    // className={classes.backgroundPaper} 
                                    className={classes.paperList}
                                    elevation={1}  
                                    style={{
                                        // marginLeft:'16px', 
                                        // marginRight: '16px',
                                        backgroundImage: 'url(' + PictBackgroundImage + ')', 
                                        backgroundSize: 'cover', 
                                        backgroundPosition: 'center center',
                                        backgroundRepeat: 'no-repeat',
                                    }}
                                >
                                    <Grid container>
                                        <Grid item sm={4}></Grid>
                                        <Grid 
                                            item sm={8} 
                                            // style={{textAlign: 'left'}}
                                            className={classes.textAlignHai}
                                        >
                                            <br />
                                            <br />
                                            <Typography 
                                                variant="h6" 
                                                className={classes.title}
                                                style={{color: 'white'}}
                                            >
                                                Hai, {userLoginName}
                                            </Typography>
                                            <Typography 
                                                variant="h5" 
                                                className={classes.title}
                                                style={{color: 'white'}}
                                            >
                                                Selamat datang kembali :)
                                            </Typography>
                                            <Typography 
                                                variant="subtitle2" 
                                                className={classes.title}
                                                style={{color: 'white'}}
                                            >
                                                Di halaman <i>home</i> ini, Anda dapat melihat tugas- tugas terbaru, dan mengakses Goal, dan hal-hal lain-nya dengan lebih cepat
                                            </Typography>
                                            <br />
                                            <br />
                                        </Grid>
                                    </Grid>
                                </Paper>
                                
                                <br />

                                <Hidden only='xs'>          

                                    <Paper 
                                        elevation={1} 
                                        className={classes.paperList}
                                    >
                                        <List className={classes.layoutMarginLeftList}>
                                            <ListItem  style={{paddingLeft: 0}}>
                                                <ListItemIcon>
                                                    <img src={PictDoneGreen} style={{marginLeft: 8}} />
                                                </ListItemIcon>

                                                <ListItemText 
                                                    className={classes.listItemtext}
                                                    primary = {

                                                        <Typography variant='subtitle2' className={classes.title} style={{marginLeft: 16}}>
                                                            <b>Create your first goal</b>
                                                        </Typography>
                                                    } 
                                                    
                                                />
                                            </ListItem>
                                            <Divider variant="inset" component="li" />
                                            
                                            <ListItem>
                                                <ListItemIcon >
                                                    <IconButton style={{backgroundColor: '#eaeaea', padding: '7px'}}>
                                                        <DoneIcon style={{color: 'grey', fontSize: 17}} />
                                                    </IconButton>
                                                </ListItemIcon>

                                                <ListItemText 
                                                    className={classes.listItemtext}
                                                    primary = {

                                                        <Typography variant='subtitle2' className={classes.title}>
                                                            Invite Your teammates and start collaborating
                                                        </Typography>
                                                    } 
                                                    
                                                />
                                            </ListItem>
                                            <Divider variant="inset" component="li" />

                                            <ListItem>
                                                <ListItemIcon >
                                                    <IconButton style={{backgroundColor: '#eaeaea', padding: '7px'}}>
                                                        <DoneIcon style={{color: 'grey', fontSize: 17}} />
                                                    </IconButton>
                                                </ListItemIcon>

                                                <ListItemText 
                                                    className={classes.listItemtext}
                                                    primary = {

                                                        <Typography variant='subtitle2' className={classes.title}>
                                                            Create Period
                                                        </Typography>
                                                    } 
                                                    
                                                />
                                            </ListItem>
                                            <Divider variant="inset" component="li" />

                                            <ListItem >
                                                <ListItemIcon >
                                                    <IconButton style={{backgroundColor: '#eaeaea', padding: '7px'}}>
                                                        <DoneIcon style={{color: 'grey', fontSize: 17}} />
                                                    </IconButton>
                                                </ListItemIcon>

                                                <ListItemText 
                                                    className={classes.listItemtext}
                                                    primary = {

                                                        <Typography variant='subtitle2' className={classes.title}>
                                                            Complete your profile
                                                        </Typography>
                                                    } 
                                                    
                                                />
                                            </ListItem>
                                            <Divider variant="inset" component="li" />
                                        </List>
                                    </Paper>
                                </Hidden>
                                <br />
                                
                                <Typography variant='subtitle2' className={classes.title} style={{marginLeft: 24, marginBottom: 8}}>
                                    <b>Goal baru- baru ini</b>
                                </Typography>

                                <YouActiveInHome 
                                    classes = { classes }
                                    isModalCreate = { isModalCreate }
                                    setModalCreate = { setModalCreate }
                                    fotoQuery = { fotoQuery}
                                    userLoginName = { userLoginName }
                                    memberPositionList = { memberPositionList }
                                    collectionGoalList = { collectionGoalList }
                                    setCollectionGoalList = { setCollectionGoalList }

                                />

                                <br />
                                <IconButton 
                                    onClick={() => setModalCreate(true)}
                                    style={{marginLeft: 16}}    
                                >
                                    <img src={PictPlusUploadDocument} style={{width: 40}} />
                                </IconButton>
                                <span style={{fontFamily: 'Nunito'}}>Buat Goal baru</span>
                                <br />
                                <br />
                                <br />

                            </Grid>

                            <Grid item xs={2}></Grid>

                        </Grid>

                        
                       

                        
                    </Fragment>
                )
            }
        </MuiThemeProvider>
    )
};


export default withStyles(styles)(ViewHome);
