import React, { Component } from 'react'

class CommentForm extends Component {
  state = {
    comment: '',
    author: ''
  }
  handleOnChange = ({ target: { name, value } }) =>
    this.setState(_prevState => ({
      [name]: value
    }))
  hasInvalidFields = () => {
    const { comment, author } = this.state
    if (comment.trim() !== '' && author.trim() !== '') {
      return false
    }
    return true
  }
  render () {
    cd 
    const { comment, author } = this.state
    const isDisabled = this.hasInvalidFields() ? true : null
    return (
      <form style={styles.form}>
        <div>
          <textarea
            style={styles.commentBox}
            onChange={this.handleOnChange}
            placeholder="Write something..."
            name="comment"
            value={comment}
          />
        </div>
        <div>
          <label htmlFor="author" aria-labelledby="author">
            Your Name
          </label>
          <input
            style={styles.inputField}
            onChange={this.handleOnChange}
            id="author"
            type="text"
            name="author"
            value={author}
          />
        </div>
        <button style={styles.button} disabled={isDisabled}>
          Add Comment
        </button>
      </form>
    )
  }
}
const styles = {
  form: {
    margin: 'auto',
    padding: '0px',
    width: '500px'
  },
  commentBox: {
    width: '494px',
    height: '80px',
    marginBottom: '12px'
  },
  inputField: {
    width: '360px',
    float: 'right',
  },
  button: {
    marginTop: '12px',
    width: '500px',
    color: '#ffffff',
    backgroundColor: '#767676',
    padding: '6px',
    borderRadius: '8px'
  }
}


/*

    NEXT, MESTI REFACTOR TO 'REACT HOOK' !!!!


*/

// import React,{useState, useCallback} from 'react';

// const CommentForm = props => {

    // const handleChange = (e) => console.log(e.target.value);

    // const [comment ] = useState('');
    // const [author] = useState('');

    const [form, setValues] = useState({
        comment: '',
        author: '',
    });
    // const handleChange = ({ target: { name, value } }) => {

    //     setState(prevState => ({
    //         [name]: value
    //     }))
    // }
    // const handleChange = useCallback(({ target: { name, id } }) => {
      
        // console.log("NAME FROM 'useCheckboxHandler()' : ",name);
        // console.log("ID/KEY FROM 'useCheckboxHandler()' : ", id)
     
        // setCheckbox(prevState =>{
            // setValues({...form, [target.name]: target.value})

        // }
        //   prevState.map(item =>
        
        //     item.name === name ? { ...item, checked: !item.checked } : { ...item }
            
        //   )
        // );
  
        // setIdTipeJangkauan(id)
  
    //   }, []);
    
    // const handleChangeComment = (e) => {

    //     setComment(e.target.value);

    // };

    // const handleChangeAuthor = (e) => {

    //     setAuthor(e.target.value);

    // };
//     const hasEmptyContentField = () => {
       

//         if (comment.trim() !== '' && author.trim() !== '') {
//             return false
//         };

//         return true
//     };

//     const [isDisabled, setIsDisabled ] = useState(hasEmptyContentField);

//     return (

//         <div>
//             <input 
//                 placeholder="Write something..."
//                 type='text' 
//                 value={comment}  
//                 onChange={handleChange}
//                 name="comment"
//             />

//             <label htmlFor='author'>Your Name</label>
//             <input 
//                 // placeholder="Write something..."
//                 onChange={handleChange}
//                 type='text' 
//                 id='author'
//                 value={author}  
//                 name="author"
//             />
//              <button type='submit' disabled={isDisabled}>Add Comment</button>

//          </div>
//     )
// };


export default CommentForm;

/*
    `````````````````
    Solution *v1


    https://github.com/testing-library/dom-testing-library/issues/170

    ```````````````````````````````



    const LoginForm = () => {
    const [form, setValues] = useState({
        username: '',
        password: '',
    });

    const updateField = (e: React.FormEvent<HTMLInputElement>): void => {
        setValues({
            ...form,
            [e.currentTarget.name]: e.currentTarget.value,
        });
    };

    return (
        <>
            <form onSubmit={performLogin}>
                <label htmlFor="username">
                    Username
                </label>
                <input
                    type="text"
                    value={form.username}
                    onChange={updateField}
                />

                <label htmlFor="password">
                    Password
                </label>
                <input
                    type="password"
                    value={form.password}
                    onChange={updateField}
                />
                <button type="submit">Login</button>
            </form>
        </>
    );
}

*/

