import React from 'react'
import { render, cleanup} from '@testing-library/react';
import CommentCard from '../CommentCard';

afterEach(cleanup);

describe('Comment Card Detail', () => {

    //Tulis Spek dulu
    test('it renders the comment and the author', () => {
        // Arrange 
        const props = {
            comment: 'React Testing Library is great',
            author: 'Luke Ghenco'
        };

        // Act 
        const { getByText, debug } = render(<CommentCard {...props} /> );

        // Assert 
        const commment = getByText(props.comment);
        const author = getByText(`${props.author}`)

        expect(commment).toBeDefined()
        expect(author).toBeDefined()
    })
})

/* 

    ```````````````````````````````````````````````````````````
    https://testing-library.com/docs/react-testing-library/api

    ```````````````````````````````````````````````````````````

*/