import { createStore, applyMiddleware } from 'redux'
// import createSagaMiddleware from 'redux-saga'
import rootReducer from '../reducers'
import { rootSaga } from '../middleware-saga';
// import postRegisterWithValidateMiddleware from '../middleware/Signup/post_register_signup_with_validate'; 
// import getDataInPageMiddleware from '../middleware/Signup/get_data_in_page_register';
// import postRegisterMiddleware from '../middleware/Signup/post_register_signup';
// import postVerifyOTPMiddleware from '../middleware/OTP/post_verify_otp';
// import postSigninMiddleware from '../middleware/Signin/post_signin';

/* Define Structure */
// import postQuickFillUnitStructureMiddleware from '../middleware/DefineStructure/post_quick_fill_unit_structure';
// import getUnitStructureUmumMiddleware from '../middleware/DefineStructure/get_unit_structure_umum';
// import getLabelUnitMiddleware from '../middleware/DefineStructure/get_label_unit';

/* COMPLETION */
// import getTokenFromEmailMiddleware from '../middleware/Completion/get_token_from_email';
// import getLabelCompletionProfileMiddleware from '../middleware/Completion/get_label_completion_profile';

// const sagaMiddleware = createSagaMiddleware();

const configureStore = preloadedState => {

  const store = createStore(rootReducer, applyMiddleware(
                                          // sagaMiddleware,

                                          /* SIGN-UP */
                                          // getDataInPageMiddleware,
                                          // postRegisterMiddleware,
                                          // postRegisterWithValidateMiddleware,

                                          /* OTP */
                                          // postVerifyOTPMiddleware,
                                          // postSigninMiddleware,

                                          /* Define Structure */
                                          // postQuickFillUnitStructureMiddleware,
                                          // getUnitStructureUmumMiddleware,
                                          // getLabelUnitMiddleware,
                                          
                                          /*COMPLETION*/
                                          // getTokenFromEmailMiddleware,
                                          // getLabelCompletionProfileMiddleware                                        
                                          
                                        ));
                                      
  // sagaMiddleware.run(rootSaga);

  if (module.hot) {
    
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      store.replaceReducer(rootReducer)
    })
  }

  return store;
}

export default configureStore;
