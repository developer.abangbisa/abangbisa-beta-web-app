import React from 'react'
import { Provider } from 'react-redux';
import { Route, BrowserRouter, Link, Switch } from 'react-router-dom';
import CommentCard from '../screen/Explore/ComponentComments/CommentCard';
import Chips from '../screen/Explore/Hooks/Chips';

/*
    `````````````
    HEADER LAYOUT
  
    `````````````
*/

import Header from '../layout/Header';
import HeaderMembershipOptions from '../layout/HeaderMembershipOptions';
import HeadStructureOrganization from '../layout/HeadStructureOrganization';
import HeaderCompanyProfile from '../layout/HeaderCompanyProfile';
import HeaderMembershipManagement from '../layout/HeaderMembershipManagement';

import HeadNewGeneral from '../layout/HeadNewGeneral';
import HeaderGOAL from '../layout/HeaderGOAL';



import HeaderHREmployee from '../layout/HeaderHREmployee';
import HeaderHREmployeeKeluarga from '../layout/HeaderHREmployeeKeluarga';
import HeaderHREmployeeKepegawaian from '../layout/HeaderHREmployeeKepegawaian';
import HeaderHREmployeeKeahlian from '../layout/HeaderHREmployeeKeahlian';
import HeaderHREmployeeRiwayat from '../layout/HeaderHREmployeeRiwayat';

import HeaderHREmployeeUrgentContact from '../layout/HeaderHREmployeeUrgentContact';
import HeaderProfileDetail from '../layout/HeaderProfileDetail';

import HeaderSO from '../layout/HeaderSO';

/*
    ````````````````````````
    CONTEXT API GLOBAL
  
    ````````````````````````
*/
import ContextGlobalDefineStrucutureDefault from '../screen/AM/QuickSetup/Context/ContextGlobalDefineStructureDefault';
import ContextGlobalFormUnit from '../screen/AM/QuickSetup/Context/ContextGlobalFormUnit';

/*
    ````````````````````````
    ACCOUNT MANAGEMENT/ AM
  
    ````````````````````````
*/

import ViewSignin from '../screen/AM/Signin/ViewSignin';
import ViewRegister from '../screen/AM/Signup/ViewRegister';
import ViewOTP from '../screen/AM/Signup/ViewOTP';
import ViewMembershipStatus from '../screen/AM/MembershipManagement/ViewMembershipStatus';
import ViewMembershipStatus_scenarioIncreaseQuota from '../screen/AM/MembershipManagement/ViewMembershipStatus_scenarioIncreaseQuota';
import ViewMembershipStatus_scenarioPending from '../screen/AM/MembershipManagement/ViewMembershipStatus_scenarioPending';

import ViewMembershipStatusKelola from '../screen/AM/MembershipManagement/ViewMembershipStatusKelola';
import ViewMembershipStatusKelolaPending from '../screen/AM/MembershipManagement/ViewMembershipStatusKelolaPending';
import ViewMembershipStatusDeactivate from '../screen/AM/MembershipManagement/ViewMembershipStatusDeactivate';
import ViewCompletionMembershipOptions from '../screen/AM/MembershipManagement/ViewCompletionMembershipOptions';
import ViewPaymentAfterCountDown from '../screen/AM/MembershipManagement/ViewPaymentAfterCountDown';

import ViewCompletionProfileGroup from '../screen/AM/ProfileCompletion/ViewCompletionProfileGroup';
import ViewCompletionCompanyStructure from '../screen/AM/QuickSetup/ViewCompletionCompanyStructure';
import ViewCompletionCompanyStructureQuestionFirst from '../screen/AM/QuickSetup/ViewCompletionCompanyStructureQuestionFirst';
import ViewCompletionCompanyStructureFormulirKosong from '../screen/AM/QuickSetup/ViewCompletionCompanyStructureFormulirKosong';
import ViewCompletionCompanyStructureQuestionThird from '../screen/AM/QuickSetup/ViewCompletionCompanyStructureQuestionThird';

import ViewUmumData from '../screen/AM/QuickSetup/ViewUmumData';
import ViewUnitStructureFORM from '../screen/AM/QuickSetup/ViewUnitStructureFORM';

// import '../layout/backgroundPaper.css';

import ViewDashboard from '../screen/AM/Dashboard/ViewDashboard';
import ViewMaintenance from '../screen/AM/Dashboard/ViewMaintenance';

import ViewUser from '../screen/AM/UserManagement/ViewUser';
import ViewUserAturKataSandi from '../screen/AM/UserManagement/ViewUserAturKataSandi';

import ViewRole from '../screen/AM/RoleManagement/ViewRole';
import ViewRoleDetail from '../screen/AM/RoleManagement/ViewRoleDetail';

import ViewChangePassword from '../screen/AM/Signin/ViewChangePassword';
import ViewForgetPassword from '../screen/AM/Signin/ViewForgetPassword';

import ViewCompanyProfile from '../screen/AM/CompanyProfile/Profile/ViewCompanyProfile';
import ViewCompanyProfileSetting from '../screen/AM/CompanyProfile/ProfileSetting/ViewCompanyProfileSetting';


/*
    ```````````````````
    HUMAN RESOURCE / HR
  
    ```````````````````
*/

import ViewEmpty from '../screen/HR/Empty/ViewEmpty';
import ViewOrganigram from '../screen/HR/Organigram/ViewOrganigram';
import ViewIdentitas from '../screen/HR/Identitas/ViewIdentitas';
import ViewIdentitasAlamat from '../screen/HR/Identitas/ViewIdentitasAlamat';
import ViewIdentitasInfoTambahan from '../screen/HR/Identitas/ViewIdentitasInfoTambahan';

import ViewKeluarga from '../screen/HR/Keluarga/ViewKeluarga';
import ViewInfoPegawai from '../screen/HR/Kepegawaian/ViewInfoPegawai';
import ViewInfoPosisi from '../screen/HR/Kepegawaian/ViewInfoPosisi';
import ViewKeahlian from '../screen/HR/Keahlian/ViewKeahlian';
import ViewRiwayat from '../screen/HR/Riwayat/ViewRiwayat';
import ViewRiwayatPendidikanFormal from '../screen/HR/Riwayat/ViewRiwayatPendidikanFormal';
import ViewRiwayatPendidikanINFORMAL from '../screen/HR/Riwayat/ViewRiwayatPendidikanINFORMAL';
import ViewRiwayatOrganisasi from '../screen/HR/Riwayat/ViewRiwayatOrganisasi';
import ViewRiwayatPengalamanKerja from '../screen/HR/Riwayat/ViewRiwayatPengalamanKerja';

import ViewUrgentContact from '../screen/HR/KontakDarurat/ViewUrgentContact';

import ViewProfileDetail from '../screen/HR/ProfileEmployee/ViewProfileDetail';
import ViewProfileDetailSeeDetail from '../screen/HR/ProfileEmployeeSeeDetail/ViewProfileDetail';

/*
    ````````````````````````````````````````````````
    HUMAN RESOURCE - STRUCTURE ORGANIZATION / HR-SO
  
    `````````````````````````````````````````````````
*/

import ViewSOTable from '../screen/HR-SO/Table/ViewSOTable';
import ViewMasterSO from '../screen/HR-SO/Master/ViewMasterSO';

/*
    ````````````````````````````````````````````````
    7WD
  
    `````````````````````````````````````````````````
*/
import ViewPeriode from '../screen/7WD/Periode/ViewPeriode';


/*
    ````
    GOAL
  
    ````
*/

import ViewGoal from '../screen/7WD/Goal/ViewGoal';
import ViewGoalMoreDetail from '../screen/7WD/Goal/ViewGoalMoreDetail';
import ViewGoalMoreDetailTabComplete from '../screen/7WD/Goal/ViewGoalMoreDetailTabComplete';

import ViewGoalMoreDetailALL from '../screen/7WD/Goal/ViewGoalMoreDetailALL';

import ViewHome from '../screen/7WD/Home/ViewHome';

/*
  ```````````````````
  CONFIG REDIRECT URL
  
  ```````````````````
*/

import {   

  

          ToLogin, ToDashboard, ToRegister, 
          ToOTP, ToMembershipStatus, ToCompletionMemberhipOptions, 
          ToMembershipStatusDeactivate, 
          
          ToCompletionProfile, 
          


          ToCompletionCompanyStructureQuestionONE,
          ToCompletionCompanyStructureFormulirKosong,
          ToCompletionCompanyStructureQuestionTHIRD,

          ToCompletionCompanyStructure, ToStructureOrganization,
          ToMembershipStatusIncreaseQuotaSEMENTARA, ToMembershipStatusManageSEMENTARA,
          ToMembershipPaymentStatusAfterCountDown, ToRole, 

          ToUserManagement, 
          ToUserManagementManagePassword,
          
          ToCompanyProfile, 
          ToCompanyProfileSetting,
          
          ToMembershipStatusScenarioPENDING_SEMENTARA,
          ToMembershipStatusManagePENDING_SEMENTARA, ToFormUnit, ToRoleDetail,
          ToEmptyStateGeneral, ToOrganigram, ToPreForgetPassword, ToForgotPassword,
          ToHrEmployeeIdentitas, 
          
          ToHrEmployeeIdentitasAlamat, ToHrEmployeeIdentitasInfoTambahan,ToHrEmployeeKeluarga,
          ToHrEmployeeKepegawaianInfoPegawai, ToHrEmployeeKepegawaianInfoPosisi,
          ToHrEmployeeKeahlian, 
          ToHrEmployeeRiwayat, ToHrEmployeeRiwayatPendidikanFormal, ToHrEmployeeRiwayatPendidikanINFORMAL, ToHrEmployeeRiwayatOrganisasi, ToHrEmployeeRiwayatPengalamanKerja,
          
          ToHrEmployeeRiwayatUrgentContact,

          ToHrEmployeeProfileDetail, 
          ToHrEmployeeProfileDetailSeeDetail,
          ToMaintenance,

          /*
              HR-SO
          */
          ToSOTable,
          ToSOMaster,

          /*
              7WD
          */
          To7wdPeriode,

          ToGoal,
          ToGoalDetail,
          ToGoalDetailTabComplete,

          ToGoalDetailALL

        } from '../constants/config-redirect-url';

import '../layout/backgroundSignIn.css';


const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  
  <Route {...rest} render={props => (

    <Layout>
      <Component {...props} />
    </Layout>

  )} />
  
);

/* 

  ````````````````````````````````````````````````````````````````````````````````
    
    #AM

      - Sign in

      - Dashboard,

      - Sign up/ Register

      - OTP,

      - Membership Management ==> Membership Status

      - Membership Management ==> Membership Status - Kelola

      - Membership Management ==> Membership Status - Kelola PENDING

      - Membership Management ==> Membership Status, scenario Increased Quota 

      - Membership Management ==> ViewMembershipStatus_scenarioPending

      - Membership Management ==> Completion Membership Options

      - Membership Management ==> ViewMembershipStatusDeactivate

      - Membership Management ==> ViewPaymentAfterCountDown

      - Completion Profile Group ==>  ViewCompletionProfileGroup

      - Quick Setup ==> ViewCompletionCompanyStructure

      - Quick Setup ==> ViewUmumData

      - Quick Setup ==> FormUnit

      - Role ==> ViewRole
      
      - Role ==> ViewRoleDetail

      - User Management ==>  ViewUser

      - Dashboard ==> ViewCompanyProfile
      

      
  ````````````````````````````````````````````````````````````````````````````````

*/

const SignInLayout = (props) => {

  return (

      <div className="background">
        <Header />
        {props.children}  
      </div>

  );
};

const ForgetLayout = (props) => {

  return (

      <div>
        {props.children}  
      </div>

  );
};

const ViewChangePasswordLayout = (props) => {

  return (

      <div>
        {props.children}  
      </div>

  );
};


const RegisterLayout = (props) => {

  return (

      <div className="background">
        <Header />
        {props.children}  
      </div>

  );
};

const OTPLayout = (props) => {

  return (

      <div className="background">
        <Header />
        {props.children}  
      </div>
  );
};

const DashboardLayout = (props) => {

  return (
      <div>
        <HeaderGOAL />
        {props.children}  
      </div>
  );
};

const MaintenanceLayout = (props) => {

  return (
      <div>
        <HeadNewGeneral />
        {props.children}  
      </div>
  );
};

const MembershipStatusLayout = (props) => {

  return (

      <div>
        <HeaderMembershipManagement />
        {props.children}  
      </div>
  );
};

const MembershipStatusPaymentAfterCountDownLayout = (props) => {

  return (

      <div>
        <HeaderMembershipManagement />
        {props.children}  
      </div>
  );
};

const CompletionMembershipOptionsLayout = (props) => {

  return (

      <div>
        <HeaderMembershipOptions />
        {props.children}  
      </div>
  );
};

const CompletionProfileGroupLayout = (props) => {

  return (

      <div>
        <HeaderMembershipOptions />
        {props.children}  
      </div>
  )
};

const CompletionCompanyStructureQuestionFirstLayout = (props) => {

  return (
      <div>
        <HeaderMembershipOptions />
        {props.children}  
      </div>
  )
};

const CompletionCompanyStructureLayout = (props) => {

  return (
      <div>
        <HeaderMembershipOptions />
        {props.children}  
      </div>
  )
};

const CompletionCompanyStructureQuestionThirdLayout = (props) => {

  return (
      <div>
        <HeaderMembershipOptions />
        {props.children}  
      </div>
  )
};

const CompletionCompanyStructureFormulirKosongLayout = (props) => {

  return (
      <div>
        <HeaderMembershipOptions />
        {props.children}  
      </div>
  )
};

/*
  ````````````
  Quick Setup

  ````````````
*/

const UmumDataStructureOrganizationLayout = (props) => {

  return(
      <div>
          <HeadStructureOrganization />
          <ContextGlobalDefineStrucutureDefault>
            {props.children}  
          </ContextGlobalDefineStrucutureDefault>
      </div>
  )
};

const FormUnitLayout = (props) => {

  return(
      <div>
          <HeadStructureOrganization />
          <ContextGlobalFormUnit>
            {props.children}  
          </ContextGlobalFormUnit>
      </div>
  )
};

const MembershipStatusScenarioIncreasedQuotaLayout = (props) => {

  return (

      <div>
        <HeaderMembershipManagement />
        {props.children}  
      </div>
  );
};

const MembershipStatusScenarioStatusPendingLayout = (props) => {

  return (

      <div>
        <HeaderMembershipManagement />
        {props.children}  
      </div>
  );
};

const MembershipStatusKelolaLayout = (props) => {

  return (

      <div>
        <HeaderMembershipManagement />
        {props.children}  
      </div>
  );
};

const MembershipStatusKelolaPENDINGLayout =  (props) => {

  return (

      <div>
        <HeaderMembershipManagement />
        {props.children}  
      </div>
  );
};



const MembershipStatusDeactivateLayout = (props) => {

  return (

      <div>
        <HeaderMembershipManagement />
        {props.children}  
      </div>
  );
};

const RoleLayout = (props) => { 

  return (
      <div>
        <HeaderMembershipManagement />
        {/* <HeadNewGeneral /> */}
          {props.children}  
      </div>
  );
};

const RoleDetailLayout = (props) => { 

  return (
      <div>
        {/* <HeaderMembershipManagement /> */}
        <HeadNewGeneral />
          {props.children}  
      </div>
  );
};

const UserManagementLayout = (props) => { 

  return (

      <div>
        <HeaderMembershipManagement />
        {/* <HeadNewGeneral /> */}
        {props.children}  
      </div>
  );
};


const UserManagementManagePasswordLayout = (props) => { 

  return (

      <div>
        <HeaderMembershipManagement />
        {props.children}  
      </div>
  );
};

const CompanyProfileLayout = (props) => { 

  return (

      <div>
        <HeaderCompanyProfile />
        {props.children}  
      </div>
  );
};

const CompanyProfileSettingLayout = (props) => { 

  return (
      <div>
        <HeaderMembershipManagement />
          {props.children}  
      </div>
  );
};

const ExploreLayout = (props) => {

  return (

      <div>  
        
        {props.children}  
      </div>
  );
};


/* 

  `````````````````````````````````````````````````````````````````
    
    #HR

      - EMPTY STATE ==> ViewEmpty

      - ORGANIGRAM ==> ViewOrganigram

      - EMPLOYEE ==> ViewEmployee

      - IDENTITAS ==> ViewIdentitas

      - IDENTITAS ALAMAT ==> ViewIdentitasAlamat

      - IDENTITAS INFO TAMBAHAN ==> ViewIdentitasInfoTambahan

      - KELUARGA ==> ViewKeluarga

      - KEPEGAWAIAN ==> ViewInfoPegawai

      - KEPEGAWAIAN ==> ViewInfoPosisi

      - KEAHLIAN ==> ViewKeahlian

      - RIWAYAT KESEHATAN ==> ViewRiwayat

      - RIWAYAT PENDIDIKAN FORMAL ==> ViewRiwayatPendidikanFormal

      - RIWAYAT PENDIDIKAN INFORMAL ==> ViewRiwayatPendidikanINFORMAL

      - RIWAYAT ORGANISASI ==> ViewRiwayatOrganisasi

      - RIWAYAT ORGANISASI ==> ViewPengalamanKerja

      - RIWAYAT KONTAK DARURAT ==> ViewUrgentContact


  ````````````````````````````````````````````````````````````````

*/

const EmptyHrLayout = (props) => {  

  return (

      <div>  
        {/* <HeaderMembershipManagement /> */}
        <HeadNewGeneral />  
        {props.children}  
      </div>
  );
};

const OrganigramLayout = (props) => {

  return (

      <div>  
        {/* <HeaderMembershipManagement /> */}
        <HeadNewGeneral />
        {props.children}  
      </div>
  );
};

const HrEmployeeIdentitasLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployee />
        {props.children}  
      </div>
  );
};

const HrEmployeeIdentitasAlamatLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployee />
        {props.children}  
      </div>
  );
};

const HrEmployeeIdentitasInfoTambahanLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployee />
        {props.children}  
      </div>
  );
};

const HrEmployeeKeluargaLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeKeluarga />
        {props.children}  
      </div>
  );
};

const HrEmployeeKepegawaianInfoPegawaiLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeKepegawaian />
        {props.children}  
      </div>
  );
};

const HrEmployeeKepegawaianInfoPositionLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeKepegawaian />
        {props.children}  
      </div>
  );
};

const HrEmployeeKeahlianLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeKeahlian />
        {props.children}  
      </div>
  );
};

const HrEmployeeRiwayatLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeRiwayat />
        {props.children}  
      </div>
  );
};

const HrEmployeeRiwayatPendidikanFormalLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeRiwayat />
        {props.children}  
      </div>
  );
};

const HrEmployeeRiwayatPendidikanINFORMALLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeRiwayat />
        {props.children}  
      </div>
  );
};

const HrEmployeeRiwayatOrganisasiLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeRiwayat />
        {props.children}  
      </div>
  );
};

const HrEmployeeRiwayatPengalamanKerjaLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeRiwayat />
        {props.children}  
      </div>
  );
};

const HrEmployeeUrgentContactLayout = (props) => {

  return (

      <div>  
        <HeaderHREmployeeUrgentContact />
        {props.children}  
      </div>
  );
};

const HrEmployeeProfileDetailLayout = (props) => {

  return (

      <div>  
        <HeaderProfileDetail />
        {props.children}  
      </div>
  );
};


const HrEmployeeProfileSeeDetailLayout = (props) => {

  return (

      <div>  
        <HeaderProfileDetail />
        {props.children}  
      </div>
  );
};

/* 

  `````````````````````````````````````````````````````````````````
    
    #HR-S0

      - TABLE ==> ViewSOTable
      
      - MASTER ==> ViewMasterSO

  ````````````````````````````````````````````````````````````````

*/

const HrSOTableLayout = (props) => {

  return (

      <div>  
        <HeadNewGeneral />      
        {/* <HeaderMembershipManagement /> */}
        {props.children}  
      </div>
  );
};

const HrMasterSOLayout = (props) => {

  return (

      <div>  
        <HeaderSO />      
        {props.children}  
      </div>
  );

};

/* 
  `````````````````````````````````
    #7WD

      - 7WD ==> ViewPeriode

      - Goal ==> ViewGoal

      - Goal ==> ViewGoalMoreDetail

  `````````````````````````````````
*/

const SevenWDPeriodLayout = (props) => {

  return (
      <div>  
        <HeaderGOAL />              
        {props.children}  
      </div>
  );
};

const SevenWDGoalLayout = (props) => {

  return (

      <div>  
        <HeaderGOAL />
        {props.children}  
      </div>
  );
};

const SevenWDGoalMoreDetailLayout = (props) => {

  return (

      <div>  
        <HeaderGOAL />
        {props.children}  
      </div>
  );
};

const SevenWDGoalMoreDetailTabCompleteLayout = (props) => {

  return (

      <div>  
        <HeaderGOAL />
        {props.children}  
      </div>
  );
};

const SevenWDGoalMoreDetailALLLayout = (props) => {

  return (

      <div>  
        <HeaderGOAL />
        {props.children}  
      </div>
  );
};

const Root = ({ store }) => (
 
      <Provider store={store}>
        <div>
          <Switch>
            <AppRoute exact path="/" layout={SignInLayout} component={ViewSignin} />
            <AppRoute exact path={ToLogin} layout={SignInLayout} component={ViewSignin} />
            <AppRoute exact path={ToPreForgetPassword} layout={ForgetLayout} component={ViewForgetPassword} />
            <AppRoute exact path={ToForgotPassword} layout={ViewChangePasswordLayout} component={ViewChangePassword} />
            
            <AppRoute exact path={ToRegister} layout={RegisterLayout} component={ViewRegister} />
            <AppRoute exact path={ToOTP} layout={OTPLayout} component={ViewOTP} />
            <AppRoute exact path={ToDashboard} layout={DashboardLayout} component={ViewHome} />
            <AppRoute exact path={ToMaintenance} layout={MaintenanceLayout} component={ViewMaintenance} />
          
            <AppRoute exact path={ToMembershipStatus} layout={MembershipStatusLayout} component={ViewMembershipStatus} />
            <AppRoute exact path={ToMembershipStatusDeactivate} layout={MembershipStatusDeactivateLayout} component={ViewMembershipStatusDeactivate} />
            <AppRoute exact path={ToMembershipPaymentStatusAfterCountDown} layout={MembershipStatusPaymentAfterCountDownLayout} component={ViewPaymentAfterCountDown} />
            
            <AppRoute exact path={ToMembershipStatusIncreaseQuotaSEMENTARA} layout={MembershipStatusScenarioIncreasedQuotaLayout} component={ViewMembershipStatus_scenarioIncreaseQuota} />
            <AppRoute exact path={ToMembershipStatusScenarioPENDING_SEMENTARA} layout={MembershipStatusScenarioStatusPendingLayout} component={ViewMembershipStatus_scenarioPending} />
            
            <AppRoute exact path={ToMembershipStatusManageSEMENTARA} layout={MembershipStatusKelolaLayout} component={ViewMembershipStatusKelola} />
            <AppRoute exact path={ToMembershipStatusManagePENDING_SEMENTARA} layout={MembershipStatusKelolaPENDINGLayout} component={ViewMembershipStatusKelolaPending} />

            <AppRoute exact path={ToCompletionMemberhipOptions} layout={CompletionMembershipOptionsLayout} component={ViewCompletionMembershipOptions} />        
            <AppRoute exact path={ToCompletionProfile} layout={CompletionProfileGroupLayout} component={ViewCompletionProfileGroup} />  

            <AppRoute exact path={ToCompletionCompanyStructureQuestionONE} layout={CompletionCompanyStructureQuestionFirstLayout} component={ViewCompletionCompanyStructureQuestionFirst} />                  
            <AppRoute exact path={ToCompletionCompanyStructure} layout={CompletionCompanyStructureLayout} component={ViewCompletionCompanyStructure} />        
            
            <AppRoute exact path={ToCompletionCompanyStructureFormulirKosong} layout={CompletionCompanyStructureFormulirKosongLayout} component={ViewCompletionCompanyStructureFormulirKosong} />                  

            <AppRoute exact path={ToCompletionCompanyStructureQuestionTHIRD} layout={CompletionCompanyStructureQuestionThirdLayout} component={ViewCompletionCompanyStructureQuestionThird} />        
           
            <AppRoute exact path={ToStructureOrganization} layout={UmumDataStructureOrganizationLayout} component={ViewUmumData} />        
            <AppRoute exact path={ToFormUnit} layout={FormUnitLayout} component={ViewUnitStructureFORM} />        

            <AppRoute exact path={ToRole} layout={RoleLayout} component={ViewRole} />
            <AppRoute exact path={ToRoleDetail} layout={RoleDetailLayout} component={ViewRoleDetail} />

            <AppRoute exact path={ToUserManagement} layout={UserManagementLayout} component={ViewUser} />
            <AppRoute exact path={ToUserManagementManagePassword} layout={UserManagementManagePasswordLayout} component={ViewUserAturKataSandi} />

            

            <AppRoute exact path={ToCompanyProfile} layout={CompanyProfileLayout} component={ViewCompanyProfile} />
            <AppRoute exact path={ToCompanyProfileSetting} layout={CompanyProfileSettingLayout} component={ViewCompanyProfileSetting} />

            


            <AppRoute exact path={ToEmptyStateGeneral} layout={EmptyHrLayout} component={ViewEmpty} />
            <AppRoute exact path={ToOrganigram} layout={OrganigramLayout} component={ViewOrganigram} />
            <AppRoute exact path={ToHrEmployeeIdentitas} layout={HrEmployeeIdentitasLayout} component={ViewIdentitas} />
            <AppRoute exact path={ToHrEmployeeIdentitasAlamat} layout={HrEmployeeIdentitasAlamatLayout} component={ViewIdentitasAlamat} />
            <AppRoute exact path={ToHrEmployeeIdentitasInfoTambahan} layout={HrEmployeeIdentitasInfoTambahanLayout} component={ViewIdentitasInfoTambahan} />

            <AppRoute exact path={ToHrEmployeeKeluarga} layout={HrEmployeeKeluargaLayout} component={ViewKeluarga} />
            <AppRoute exact path={ToHrEmployeeKepegawaianInfoPegawai} layout={HrEmployeeKepegawaianInfoPegawaiLayout} component={ViewInfoPegawai} />
            <AppRoute exact path={ToHrEmployeeKepegawaianInfoPosisi} layout={HrEmployeeKepegawaianInfoPositionLayout} component={ViewInfoPosisi} />
            <AppRoute exact path={ToHrEmployeeKeahlian} layout={HrEmployeeKeahlianLayout} component={ViewKeahlian} />
            <AppRoute exact path={ToHrEmployeeRiwayat} layout={HrEmployeeRiwayatLayout} component={ViewRiwayat} />
            <AppRoute exact path={ToHrEmployeeRiwayatPendidikanFormal} layout={HrEmployeeRiwayatPendidikanFormalLayout} component={ViewRiwayatPendidikanFormal} />
            <AppRoute exact path={ToHrEmployeeRiwayatPendidikanINFORMAL} layout={HrEmployeeRiwayatPendidikanINFORMALLayout} component={ViewRiwayatPendidikanINFORMAL} />
            <AppRoute exact path={ToHrEmployeeRiwayatOrganisasi} layout={HrEmployeeRiwayatOrganisasiLayout} component={ViewRiwayatOrganisasi} />
            <AppRoute exact path={ToHrEmployeeRiwayatPengalamanKerja} layout={HrEmployeeRiwayatPengalamanKerjaLayout} component={ViewRiwayatPengalamanKerja} />

            <AppRoute exact path={ToHrEmployeeRiwayatUrgentContact} layout={HrEmployeeUrgentContactLayout} component={ViewUrgentContact} />

            <AppRoute exact path={ToHrEmployeeProfileDetail} layout={HrEmployeeProfileDetailLayout} component={ViewProfileDetail} />
            <AppRoute exact path={ToHrEmployeeProfileDetailSeeDetail} layout={HrEmployeeProfileSeeDetailLayout} component={ViewProfileDetailSeeDetail} />


            <AppRoute exact path={ToSOTable} layout={HrSOTableLayout} component={ViewSOTable} />
            <AppRoute exact path={ToSOMaster} layout={HrMasterSOLayout} component={ViewMasterSO} />

            <AppRoute exact path={To7wdPeriode} layout={SevenWDPeriodLayout} component={ViewPeriode} />

            <AppRoute exact path={ ToGoal } layout={SevenWDGoalLayout} component={ ViewGoal } />
            <AppRoute exact path={ ToGoalDetail } layout={SevenWDGoalMoreDetailLayout} component={ ViewGoalMoreDetail } />
            <AppRoute exact path={ ToGoalDetailTabComplete } layout={SevenWDGoalMoreDetailTabCompleteLayout} component={ ViewGoalMoreDetailTabComplete } />
            <AppRoute exact path={ ToGoalDetailALL } layout={SevenWDGoalMoreDetailALLLayout} component={ ViewGoalMoreDetailALL } />
            <AppRoute exact path="/explore" layout={ExploreLayout} component={Chips} />
          </Switch>
        </div>
      </Provider>
      
);

export default Root;
