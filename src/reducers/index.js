import { combineReducers } from 'redux'

import getDataInPageRegisterReducer from './Signup/getDataInPageRegister';
import postRegisterSignupReducer from './Signup/postRegisterSignup';
import postRegisterSignupWithValidateReducer from './Signup/postRegisterSignupWithValidate';

import postVerifyOtpReducer from './OTP/postVerifyOtp'; 
import postSigninReducer from './Signin/postSignin';

import postQuickFillUnitStructureReducer from './DefineStructure/postQuickFillUnitStructure';


/* DEFINE STRUCTURE */
import getLabelUnitReducer from './DefineStructure/getLabelUnit';
import getUnitStructureUmum from './DefineStructure/getUnitStructureUmum';

/* COMPLETION */
import getTokenFromEmailReducer from './Completion/getTokenFromEmail'
import getLabelCompletionProfileReducer from './Completion/getLabelCompletionProfile';

export default combineReducers({

	/* */
	signup: getDataInPageRegisterReducer,
	register: postRegisterSignupReducer,
	registervalidate: postRegisterSignupWithValidateReducer,

	/* OTP */
	otp: postVerifyOtpReducer,
	signin: postSigninReducer,

	/* DEFINE STRUCTURE */
	unitstructurequickfill: postQuickFillUnitStructureReducer,
	labelUnit: getLabelUnitReducer,

	structureUmum: getUnitStructureUmum,

	token: getTokenFromEmailReducer,
	labelCompletionProfile: getLabelCompletionProfileReducer
	
});