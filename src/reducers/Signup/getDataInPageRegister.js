// import { GET_DATA_IN_PAGE_REGISTER } from '../constants/action-types'
import { GET_DATA_IN_PAGE_REGISTER } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const getDataInPageRegisterReducer = (state = initialState, action) => {

    switch (action.type) {

        case GET_DATA_IN_PAGE_REGISTER.PENDING:
            return{
                ...state,
                loading: true
            }

        case GET_DATA_IN_PAGE_REGISTER.SUCCESS:
            return{
                ...state,
                loading: false,
                data: action.payload
            }

        case GET_DATA_IN_PAGE_REGISTER.ERROR:
            return {
                ...state,
                loading: false
            }
        
        default:
            break;
    }

    return state;
}

export default getDataInPageRegisterReducer;
