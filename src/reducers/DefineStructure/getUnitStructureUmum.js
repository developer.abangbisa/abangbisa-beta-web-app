
import { GET_UNIT_STRUCTURE_UMUM } from '../../constants/action-types';

const initialState = {
    loading : false,
    list : [],
    data: {}
};

const getUnitStructureUmumReducer = (state = initialState, action) => {

    switch (action.type) {

        case GET_UNIT_STRUCTURE_UMUM.PENDING:
            return{
                ...state,
                loading: true
            }

        case GET_UNIT_STRUCTURE_UMUM.SUCCESS:
            return{
                ...state,
                loading: false,
                data: action.payload
            }

        case GET_UNIT_STRUCTURE_UMUM.ERROR:
            return {
                ...state,
                loading: false
            }
        
        default:
            break;
    }

    return state;
}

export default getUnitStructureUmumReducer;
