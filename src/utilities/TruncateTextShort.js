function TruncateTextShort(input) {

    if (input.length > 24)
       return input.substring(0,24) + '...';
    else
       return input;
 };

 export default TruncateTextShort;